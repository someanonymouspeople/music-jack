﻿Shader "Custom/UnlitGreyScale" {
Properties 
{
    _MainTex ("Texture", 2D) = "white" { }
    _EffectAmount ("Amount", Range(0.0,1.0)) = 0.0
    _isActive("Active", Range(0.0,1.0)) = 1.0
    //_Alpha("Opacity",Range(0,1.0)) = 1.0
}
SubShader 
{
	 Tags {"Queue"="Overlay"}
	 Blend SrcAlpha OneMinusSrcAlpha 
    Pass 
    {
 		
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		 
		#include "UnityCG.cginc"
		 
		sampler2D _MainTex;
		 
		struct v2f 
		{
		    float4  pos : SV_POSITION;
		    float2  uv : TEXCOORD0;
		};
		 
		float4 _MainTex_ST;
		uniform float _EffectAmount;
		uniform float _isActive;
		//uniform float _Alpha;
		
		v2f vert (appdata_base v)
		{
		    v2f o;
		    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
		    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
		    return o;
		}
		 
		half4 frag (v2f i) : COLOR
		{
		    half4 texcol = tex2D (_MainTex, i.uv);
	  		if(_isActive == 1.0)
			{
				half3 texcol3 = texcol.rgb;
				if(_EffectAmount > 1)
				{
				_EffectAmount = 1;
				}
				if(_EffectAmount <0)
				{
				_EffectAmount = 0;
				}
	   			texcol3 =lerp(texcol3,dot(texcol3, float3(0.3, 0.59, 0.11)),_EffectAmount);
	   			texcol.rgb = texcol3;
	   			
	   		}
	   		//texcol.a = _Alpha;
		    return texcol;
		}
		ENDCG
 
    }

}
Fallback "Unlit/Transparent"
} 