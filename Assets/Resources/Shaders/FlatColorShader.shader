﻿Shader "Custom/FlatColorShader" 
{
	Properties 
	{
		//_MainTex ("Base (RGB)", 2D) = "white" {}
		_ColorTint ("Color", Color) = (1.0,1.0,1.0,1.0)
		
		
	}
	
	SubShader 
	{
	
		Pass
		{
		
			CGPROGRAM
			
			//Pragmas
			#pragma vertex vertexFunc
			#pragma fragment pixelFunc
			
			//User defined variables
			uniform half4 _ColorTint;
			
			//uniform keyword is a generic keyword expected by shaders in other platforms, not by unity... unity ignores it...
			
			//Base input structs
			struct vertexInput
			{
				//here vertex is a semantic... kinda like an add on variable type.... 
				//basically in the next line im declaring that i need a vertex
				//object that will house 4 halfs and i will name it position
				float4 vertex : POSITION;
			};
			
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
			};
			
			//Vertex funcs
			vertexOutput vertexFunc(vertexInput input)
			{
				vertexOutput outputToReturn;
				// mulitply transforms so that the position can be injected into the engine
				outputToReturn.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return outputToReturn;
			}
			
			//PixelFuncs
			half4 pixelFunc(vertexOutput output) : COLOR
			{
				return _ColorTint;
			}
		
			ENDCG	
		}
		
	} 
	FallBack "Unlit/Transparent"
}
