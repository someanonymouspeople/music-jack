﻿using UnityEngine;
using System.Collections;

public class BeatDisplayScript : MonoBehaviour {

	bool m_isDisabled;
	// Use this for initialization
	void Start () 
	{
		m_isDisabled = false;
		Vector3 posToPlaceBeatDisp  = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height));
		posToPlaceBeatDisp.x += this.gameObject.renderer.bounds.size.x;
		posToPlaceBeatDisp.y -= this.gameObject.renderer.bounds.size.y;
		posToPlaceBeatDisp.z = 0;
		this.gameObject.transform.position = posToPlaceBeatDisp;
	}
	
	void CheckAndEnable()
	{
		if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
		{
			this.GetComponent<TextMesh>().renderer.enabled = false;
			m_isDisabled = true;
			return;
		}
		this.GetComponent<TextMesh>().renderer.enabled = true;

	}
	void Update () 
	{
		CheckAndEnable();
		int beats = (int)PlayerStatManager.GetInstance().GetBeats();
		if(beats >= PlayerStatManager.GetInstance ().m_maxBeats)
		{
			this.gameObject.GetComponent<TextMesh>().text = "Beats : "+PlayerStatManager.GetInstance().GetBeats().ToString();
			return;
		}
		else
		{
		this.gameObject.GetComponent<TextMesh>().text = "Beats : "+PlayerStatManager.GetInstance().GetBeats().ToString() + "\nTimer : "+ PlayerStatManager.GetInstance().GetIntervalLeftBeforeIncrement().ToString();
		}
	}
}
