﻿using UnityEngine;
using System.Collections;

public class CameMove : MonoBehaviour {

	public float speed;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 posToPlaceCam = Camera.main.transform.position;
		posToPlaceCam.x = posToPlaceCam.x + speed * Time.deltaTime;
		Camera.main.transform.position  = posToPlaceCam;
	
	}
}
