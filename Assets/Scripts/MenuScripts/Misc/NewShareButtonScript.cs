using UnityEngine;
using System.Collections;
using GooglePlayGames;

public class NewShareButtonScript : ClickableButtonScript {

	bool m_feedSuccessful = false;
	bool m_attemptingFeed = false;
	IEnumerator CouldNotLogInInvoke()
	{
		Debug.Log ("LeaderBoard could not login invoked");
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Unable to Login \nFacebook servers seem down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		//re enable
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		Debug.Log ("LeaderBoard DB reinited");
	}
	
	IEnumerator AsYourselfInvoke()
	{
		Debug.Log ("LeaderBoard AsYourselfInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Logging in\n to Facebook",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		Debug.Log ("LeaderBoard is clicked");
		
		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
		
		if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
		{
			Debug.Log ("LeaderBoard is connected to internet");
			
			//GPG
			//Find and set the playing platform
			PlayGamesPlatform.Activate();
			
			//enable debug log
			PlayGamesPlatform.DebugLogEnabled = true;
			
			//Login
			Social.localUser.Authenticate((bool success) => {
				if(success)
				{
					Debug.Log("Logged in to google play "+Social.localUser.userName);
				}
				else
				{
					Debug.Log("Coudnt log in to google play");
				}
				
			});
			
			
			
			result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
			
			if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
			{
				Debug.Log ("LeaderBoard FB active");
				//Destroy(m_dialogeBox);
				StartCoroutine("FBActiveInvoke");
				
			}
			else
			{
				StartCoroutine("CouldNotLogInInvoke");
			}
		}
		else
		{
			//Destroy(m_dialogeBox);
			StartCoroutine("NetInactiveInvoke");
		}
		return true;
	}

	bool LogInCallBack(MusicJunkieDialogBoxScript script)
	{
		Destroy(script.gameObject);
		StartCoroutine("AsYourselfInvoke");
		GameConfigurationHolderScript.GetInstance().m_guestMode = false;
		return true;
	}
	
	IEnumerator CancelledInvoke()
	{
		
		Debug.Log ("LeaderBoard CancelledInvoked");
		//Reinit
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		yield break;
	}
	
	
	bool CancelCallBack(MusicJunkieDialogBoxScript script)
	{
		Destroy(script.gameObject);
		
		StartCoroutine("CancelledInvoke");
		return true;
	}

	void GuestModeInvoke()
	{
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		Debug.Log ("LeaderBoard NetInactiveInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		MJUIButtonFiller logInFiller = new MJUIButtonFiller("",LogInCallBack);
		logInFiller.m_customButtonSprite = Resources.Load<Sprite>("UI/CustomUIButtons/FbLoginSmall");
		MJUIButtonFiller cancelFiller = new MJUIButtonFiller("No",CancelCallBack);
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Logged in as guest\n would you like to log in to FB?",2,logInFiller,cancelFiller);
		//Reinit
		
	}




	IEnumerator FBActiveInvoke()
	{
		Debug.Log ("New Share button FBActive invoked");
		m_feedSuccessful= false;
		m_attemptingFeed = true;

		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		if(FB.IsLoggedIn)
		{
			this.enabled = true;
			FB.Feed(link:"https://pixelreflex.weebly.com",linkName:"TEST",linkCaption:"TEST CAPTION",linkDescription:"TEST DESC",callback:FeedSuccessCallBack);
		
		}
		else
		{ 
			FB.Login("public_profile,user_friends,publish_actions",CustomShareButtonCallBack);
		}

		while(m_attemptingFeed)
		{
			yield return null;
		}
		if(!m_feedSuccessful)
		{
			StartCoroutine("FBInactiveInvoke");
		}
		else
		{
			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
			
			Debug.Log ("New Share button enabled");
		}
	}

	IEnumerator NetInactiveInvoke()
	{
		Debug.Log ("New Share button NetInactiveInvoked");
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		
		
		Debug.Log ("New Share button disabled");
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;

		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Internet down\n please connect and retry",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		this.enabled = true;
		Debug.Log ("New Share button enabled");
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
	}

	IEnumerator FBInactiveInvoke()
	{
		Debug.Log ("New Share button NetInactiveInvoked");
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		
		
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("FB servers seems down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		this.enabled = true;
		Debug.Log ("New Share button enabled");
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
	}

	void CustomShareButtonCallBack(FBResult result)
	{
		Debug.Log ("New Share button CustomShareButtonCallBack invoked");
		if(FB.IsLoggedIn)
		{

			FB.Feed(link:"https://pixelreflex.weebly.com",linkName:"TEST",linkCaption:"TEST CAPTION",linkDescription:"TEST DESC",callback:FeedSuccessCallBack);
		}
	
		m_attemptingFeed = false;


	}

	void FeedSuccessCallBack(FBResult result)
	{
		Debug.Log ("New Share button FeedSuccess callback invoked");
		if(m_attemptingFeed)
		{
			m_feedSuccessful = true;
		}
		else
		{
			m_feedSuccessful = false;
		}
		
	}

	void Update()
	{
		if(IsClicked())
		{
			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
			this.enabled = false;

			if(!GameConfigurationHolderScript.GetInstance().m_guestMode)
			{
				Debug.Log ("New share button is clicked");
				
				NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
				result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
				
				if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
				{
					Debug.Log ("New share button is connected to internet");
					
					result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
					
					if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
					{
						Debug.Log ("New share button FB active");
						StartCoroutine("FBActiveInvoke");
						
					}
					else
					{
						StartCoroutine("FBInactiveInvoke");
					}
				}
				else
				{
					StartCoroutine("NetInactiveInvoke");
				}
			}
			else
			{

				GuestModeInvoke();
			}
			
		}
	}


}
