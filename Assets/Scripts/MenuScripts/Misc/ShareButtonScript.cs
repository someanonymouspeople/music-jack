﻿//using UnityEngine;
//using System.Collections;
//
//public class ShareButtonScript : ClickableButtonScript {
//	
//	public bool m_checked = false;
//	bool m_windowOpen = false;
//	bool m_retryingConnection = false;
//	float m_releaseTimer = 0;
//	float m_releaseTime = 1;
//	float m_retryTime = 3;
//	float m_retryTimer = 0;
//
//	MusicJunkieDialogBoxScript m_activeScript = null;
//
//	string linkToCheck;
//	void Start () 
//	{
//		linkToCheck = "https://google.com";
//		NetworkConnectionDelegate netChecker = NetworkConnectionDelegate.GetInstance();
//		NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
//		if((result & NETWORK_CONNECTION_RESULT.NET_ACTIVE) == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
//		{
//			m_checked = true;
//		}
//	}
//	
//	
//	void Update () 
//	{
//
//		if(m_retryingConnection)
//		{
//			RetryConnection();
//			return;
//		}
//
//		if(IsClicked() && !m_windowOpen)
//		{
//
//			Debug.Log("Share button clicked");
//			if(m_checked)
//			{
//				Debug.Log("Share Button Prev net active");
//				if(FB.IsLoggedIn)
//				{	
//					Debug.Log("Share button FB logged in");
//
//					NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsLinkActive("http://facebook.com");
//					if((result & NETWORK_CONNECTION_RESULT.LINK_ACTIVE) == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
//					{
//						Debug.Log("Share button net active and facebook active, feeding");
//						Debug.Log("Shared");
//						FB.Feed(link:linkToCheck,linkName:"TEST",linkCaption:"TEST CAPTION",linkDescription:"TEST DESC",callback:FeedSuccessCallBack);
//						GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
//						this.enabled = false;
//						m_windowOpen = true;
//					}
//					else
//					{
//						Debug.Log("Share Button Net Connection result "+ result);
//						Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
//						GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
//						GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
//						if((result & (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_ACTIVE)) == (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_ACTIVE))
//						{
//							Debug.Log(""+ result);
//							dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("FB servers down\n Please try later",1,new MJUIButtonFiller("Ok",this.CancelCallBack));
//						}
//						else if((result & (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_INACTIVE)) == (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_INACTIVE))
//						{
//							Debug.Log(""+ result);
//							dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Not connected to Internet\n Press retry to try again\n or cancel to go back",2,new MJUIButtonFiller("Retry",this.RetryCallBack),new MJUIButtonFiller("Cancel",this.CancelCallBack));
//						}
//
//						m_windowOpen = true;
//						//this.enabled = false;
//					}
//				}
//				else
//				{
//
//					Debug.Log("Share Button logging in");
//					FB.Login("basic_info,publish_actions",CustomShareButtonCallBack);
//					this.enabled = false;
//				}
//			}
//			else
//			{
//				m_isClicked = false;
//				Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
//				GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
//				GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
//				dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Not connected to Internet\n Press retry to try again\n or cancel to go back",2,new MJUIButtonFiller("Retry",this.RetryCallBack),new MJUIButtonFiller("Cancel",this.CancelCallBack));
//				m_windowOpen = true;
//				this.enabled = false;
//				StartCoroutine("ReleaseControl");
//				
//			}
//		}
//	}
//
//	void FeedSuccessCallBack(FBResult result)
//	{
//		this.enabled = true;
//		m_isClicked = false;
//		Debug.Log("FBFeed result : "+result.Text);
//		m_windowOpen = false;
//		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
//	}
//	
//	IEnumerator ReleaseControl()
//	{
//		this.enabled = true;
//		m_isClicked = false;
//		yield return new WaitForSeconds(m_releaseTime);
//		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
//		m_windowOpen=  false;
//	}
//
//
//	
//	void RetryConnection()
//	{
//		if(m_checked && m_retryTimer<m_retryTime)
//		{
//			m_retryTimer += Time.deltaTime;
//			if(FB.IsLoggedIn)
//			{
//				m_retryTimer = m_retryTime;
//			}
//		}
//		else
//		{
//			m_retryTimer = 0;
//			if(FB.IsLoggedIn && NetworkConnectionDelegate.GetInstance().IsConnectedToInternet() == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
//			{
//
//				m_activeScript.transform.FindChild("TextMesh").GetComponent<TextMesh>().text = "ConnectionSuccesful";
//
//				if(m_releaseTimer<m_releaseTime)
//				{
//					m_releaseTimer+= Time.deltaTime;
//				}
//				else
//				{
//					m_releaseTimer = 0;
//					Destroy(m_activeScript.transform.gameObject);
//					Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
//					m_retryingConnection = false;
//					StartCoroutine("ReleaseControl");
//				}
//			}
//			else
//			{
//				m_activeScript.transform.FindChild("TextMesh").GetComponent<TextMesh>().text = "Connection Unsuccesful";
//
//				if(m_releaseTimer<m_releaseTime)
//				{
//					m_releaseTimer+= Time.deltaTime;
//				}
//				else
//				{
//					m_releaseTimer = 0;
//					Destroy(m_activeScript.transform.gameObject);
//					Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
//					m_retryingConnection = false;
//					StartCoroutine("ReleaseControl");
//				}
//			}
//		}
//	}
//	
//	//FB delegates
//	public void CustomShareButtonCallBack(FBResult result)
//	{
//		Debug.Log(result.Text);
//		this.enabled = true;
//	}
//	static void CustomInitComplete()
//	{
//		Debug.Log("FB Initialized");
//	}
//	static void CustomHideUnity(bool checkFlag)
//	{
//		Debug.Log ("Unity hiding");
//		if(!checkFlag)
//		{
//			Time.timeScale = 0;
//		}
//		else
//		{
//			Time.timeScale = 1;
//		}
//	}
//	
//	//Button delegates
//	public bool CancelCallBack(MusicJunkieDialogBoxScript script)
//	{
//		Destroy(script.gameObject);
//		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
//		StartCoroutine("ReleaseControl");
//		return true;
//	}
//	public bool RetryCallBack(MusicJunkieDialogBoxScript script)
//	{
//		m_retryingConnection =true;
//		Destroy(script.gameObject);
//		
//		GameObject newDialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
//		m_activeScript = newDialogueBox.GetComponent<MusicJunkieDialogBoxScript>();
//		m_activeScript.InitializeDialogBox("Retrying....\n Please wait",0);
//		
//		NetworkConnectionDelegate netChecker = NetworkConnectionDelegate.GetInstance();
//		
//		NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
//		if((result & NETWORK_CONNECTION_RESULT.NET_ACTIVE) == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
//		{
//			m_checked = true;
//		}
//		if(m_checked)
//		{
//			FB.Init(CustomInitComplete);
//		}
//		
//		result = NetworkConnectionDelegate.GetInstance().IsLinkActive("http:/www.facebook.com");
//		if((result & NETWORK_CONNECTION_RESULT.LINK_ACTIVE) == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
//		{
//			FB.Login("basic_info,publish_actions",CustomShareButtonCallBack);
//		}
//
//		//StartCoroutine("RetryConnection");
//
//		return true;
//	}
//}
//
//
//
////using UnityEngine;
////using System.Collections;
////
////public class ShareButtonScript : ClickableButtonScript {
////	
////	public bool m_checked = false;
////	bool m_windowOpen = false;
////	float m_releaseTimer = 0;
////	float m_releaseTime = 1;
////
////	string linkToCheck;
////	void Start () 
////	{
////		linkToCheck = "https://www.facebook.com/praveen.benjamin.90";
////		NetworkConnectionDelegate netChecker = NetworkConnectionDelegate.GetInstance();
////		NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
////		if((result & NETWORK_CONNECTION_RESULT.NET_ACTIVE) == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
////		{
////			m_checked = true;
////		}
////	}
////
////
////	void Update () 
////	{
////		if(IsClicked() && !m_windowOpen)
////		{
////			if(m_checked)
////			{
////				if(FB.IsLoggedIn)
////				{	
////					NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsLinkActive(linkToCheck);
////					if((result & NETWORK_CONNECTION_RESULT.LINK_ACTIVE) == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
////					{
////						Debug.Log("Shared");
////						FB.Feed(link:linkToCheck,linkName:"TEST",linkCaption:"TEST CAPTION",linkDescription:"TEST DESC");
////					}
////					else
////					{
////						Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
////						GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
////						GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
////						if((result & (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_ACTIVE)) == (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_ACTIVE))
////						{
////							Debug.Log(""+ result);
////							dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("FB servers down\n Please try later",1,new MJUIButtonFiller("Ok",this.CancelCallBack));
////						}
////						else if((result & (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_INACTIVE)) == (NETWORK_CONNECTION_RESULT.LINK_INACTIVE|NETWORK_CONNECTION_RESULT.NET_INACTIVE))
////						{
////							Debug.Log(""+ result);
////							dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Not connected to Internet\n Press retry to try again\n or cancel to go back",2,new MJUIButtonFiller("Retry",this.RetryCallBack),new MJUIButtonFiller("Cancel",this.CancelCallBack));
////						}
////						m_windowOpen = true;
////					}
////				}
////				else
////				{
////					FB.Login("basic_info,publish_actions",CustomShareButtonCallBack);
////				}
////			}
////			else
////			{
////				Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
////				GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
////				GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
////				dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Not connected to Internet\n Press retry to try again\n or cancel to go back",2,new MJUIButtonFiller("Retry",this.RetryCallBack),new MJUIButtonFiller("Cancel",this.CancelCallBack));
////				m_windowOpen = true;
////
////			}
////		}
////	}
////
////	IEnumerator ReleaseControl()
////	{
////		yield return new WaitForSeconds(m_releaseTime);
////		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
////		m_windowOpen=  false;
////	}
////	//FB delegates
////	public void CustomShareButtonCallBack(FBResult result)
////	{
////		
////	}
////	static void CustomInitComplete()
////	{
////		Debug.Log("FB Initialized");
////	}
////	static void CustomHideUnity(bool checkFlag)
////	{
////		Debug.Log ("Unity hiding");
////		if(!checkFlag)
////		{
////			Time.timeScale = 0;
////		}
////		else
////		{
////			Time.timeScale = 1;
////		}
////	}
////
////	//Button delegates
////	public bool CancelCallBack(MusicJunkieDialogBoxScript script)
////	{
////		Destroy(script.gameObject);
////		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
////		StartCoroutine("ReleaseControl");
////		return true;
////	}
////	public bool RetryCallBack(MusicJunkieDialogBoxScript script)
////	{
////		NetworkConnectionDelegate netChecker = NetworkConnectionDelegate.GetInstance();
////
////		NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
////		if((result & NETWORK_CONNECTION_RESULT.NET_ACTIVE) == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
////		{
////			m_checked = true;
////		}
////		if(m_checked)
////		{
////			FB.Init(CustomInitComplete,CustomHideUnity);
////		}
////
////		result = NetworkConnectionDelegate.GetInstance().IsLinkActive(linkToCheck);
////		if((result & NETWORK_CONNECTION_RESULT.LINK_ACTIVE) == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
////		{
////			FB.Login("basic_info,publish_actions",CustomShareButtonCallBack);
////		}
////
////		Destroy(script.gameObject);
////		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
////		StartCoroutine("ReleaseControl");
////		return true;
////	}
////}
