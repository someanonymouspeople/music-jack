﻿using UnityEngine;
using System.Collections;

public class MenuButtonScript : MonoBehaviour {

	public GAME_SCENES m_sceneToLoad;
	void Start () 
	{
	
	}

	void Update () 
	{

		if(Input.GetMouseButton(0))
		{
			if(this.gameObject.renderer.bounds.Contains(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y,this.gameObject.transform.position.z)))
			{
				AppManager.GetInstance().ChangeScene(m_sceneToLoad);
			}
		}

	
	}
}
