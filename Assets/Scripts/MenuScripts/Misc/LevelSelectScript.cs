﻿using UnityEngine;
using System.Collections;

public enum LEVEL_DIFFICULTY
{
	EASY,
	NORMAL,
	HARD,
	IMPOSSIBLE
}
public class LevelSelectScript : ClickableButtonScript {

	public LEVEL_DIFFICULTY m_difficulty;
	// Update is called once per frame
	void Update () 
	{
		if(IsClicked())
		{
			switch(m_difficulty)
			{
				case LEVEL_DIFFICULTY.EASY:
				{
				AppManager.GetInstance().ChangeBackGround(BACKGROUND_TYPE.BACKGROUND_PINK);
				}
					break;
				case LEVEL_DIFFICULTY.NORMAL:
				{
				AppManager.GetInstance().ChangeBackGround(BACKGROUND_TYPE.BACKGROUND_BLUE);
				}
					break;
				case LEVEL_DIFFICULTY.HARD:
				{
				AppManager.GetInstance().ChangeBackGround(BACKGROUND_TYPE.BACKGROUND_YELLOW);
				}
					break;
				case LEVEL_DIFFICULTY.IMPOSSIBLE:
				{
				AppManager.GetInstance().ChangeBackGround(BACKGROUND_TYPE.BACKGROUND_GREEN);
				}
					break;
			}
		}
	}
}
