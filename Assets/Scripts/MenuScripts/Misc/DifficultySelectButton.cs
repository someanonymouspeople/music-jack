﻿using UnityEngine;
using System.Collections;


public class DifficultySelectButton : ClickableButtonScript {
	
	public GAME_DIFFICULTY m_difficulty;

	//for lack of a system to control each menu independantly, this inefficient way will have to be used
	void Update () 
	{
		if(IsClicked())
		{
			AppManager.GetInstance().SetGameDifficulty(m_difficulty);
		}
		if(AppManager.GetInstance().GetGameDifficulty() == m_difficulty)
		{
			this.gameObject.transform.localScale = new Vector2(1.25f,1.25f);
			this.gameObject.renderer.material.color = Color.grey;
			this.GetComponentInChildren<TextMesh>().renderer.material.color = Color.white;
		}
		else
		{
			this.gameObject.transform.localScale = Vector2.one;
			this.gameObject.renderer.material.color = Color.black;
			this.GetComponentInChildren<TextMesh>().renderer.material.color = Color.white;
		}
	}
}
