﻿using UnityEngine;
using System.Collections;

public class LinearBGChangeButton : ClickableButtonScript {

	void Awake()
	{
		this.gameObject.renderer.material.color = Color.black;
		this.GetComponentInChildren<TextMesh>().renderer.material.color = Color.white;
	}

	IEnumerator IsSelectedCoroutine()
	{
		this.gameObject.renderer.material.color = Color.grey;
		this.GetComponentInChildren<TextMesh>().renderer.material.color = Color.white;
		yield return new WaitForSeconds(0.2f);
		this.gameObject.renderer.material.color = Color.black;
		this.GetComponentInChildren<TextMesh>().renderer.material.color = Color.white;
	}
	void Update () 
	{
		if(IsClicked())
		{
			StartCoroutine("IsSelectedCoroutine");
			BACKGROUND_TYPE activeBG = ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().m_type;
			if((int)activeBG == (int)BACKGROUND_TYPE.BACKGROUND_YELLOW)
			{
				activeBG = BACKGROUND_TYPE.BACKGROUND_PINK;
			}
			else
			{
				int activeBGIndex = (int)activeBG;
				activeBGIndex++;
				activeBG = (BACKGROUND_TYPE)activeBGIndex;
			}
			AppManager.GetInstance().ChangeBackGround(activeBG);
			//StartCoroutine(ParallaxingBackGroundManager.GetInstance().FadeInOutBackGround(activeBG,1));
		}

	}
}
