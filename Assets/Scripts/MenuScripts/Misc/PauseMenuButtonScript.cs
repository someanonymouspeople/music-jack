﻿using UnityEngine;
using System.Collections;

public enum PAUSE_MENU_OPTIONS
{
	RESTART,
	MENU,
	RESUME
};

public class PauseMenuButtonScript : ClickableButtonScript {
	
	public PAUSE_MENU_OPTIONS m_action;
	
	void Update () 
	{
		if(IsClicked())
		{
			switch(m_action)
			{
				case PAUSE_MENU_OPTIONS.RESTART:
				{
					AppManager.GetInstance().Restart();
					AppManager.GetInstance().Resume();
					AppManager.GetInstance().m_hudScript.DestroyElements();
				}
					break;
				case PAUSE_MENU_OPTIONS.RESUME:
				{
					AppManager.GetInstance().Resume();
				}
					break;
				case PAUSE_MENU_OPTIONS.MENU:
				{
					AppManager.GetInstance().MainMenu();
					AppManager.GetInstance().Resume();
					AppManager.GetInstance().m_hudScript.DestroyElements();
					
				}
					break;
			}

			Destroy(this.transform.parent.gameObject);
		}
		
		
	}
}
