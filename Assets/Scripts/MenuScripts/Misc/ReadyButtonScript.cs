﻿using UnityEngine;
using System.Collections;

public class ReadyButtonScript : ClickableButtonScript {

	bool m_colorHasntBeenSet = true;
	// Update is called once per frame
	void Start()
	{
//		if(PlayerStatManager.GetInstance().GetBeats() < 1 )
//		{
//			renderer.material.color = new Color(70.0f/255,70.0f/255,70.0f/255);
//			GetComponentInChildren<TextMesh>().text = "Not enough\nBeats!";
//			GetComponentInChildren<TextMesh>().characterSize =0.045f;
//			GetComponentInChildren<TextMesh>().alignment = TextAlignment.Center;
//			
//			m_colorHasntBeenSet = true;
//		}
	}
	void Update () 
	{

		if(PlayerStatManager.GetInstance().GetBeats() > 0 && m_colorHasntBeenSet)
		{
			if(AppManager.GetInstance().m_selectedSongIndex != 999)
			{
				GetComponentInChildren<TextMesh>().text = "Ready!";
				GetComponentInChildren<TextMesh>().characterSize =0.075f;
				GetComponentInChildren<TextMesh>().alignment = TextAlignment.Center;
				renderer.material.color = Color.black;
				m_colorHasntBeenSet = false;
			}
			else
			{
				GetComponentInChildren<TextMesh>().text = "  Select a\nsong";
				GetComponentInChildren<TextMesh>().characterSize =0.045f;
				GetComponentInChildren<TextMesh>().alignment = TextAlignment.Center;
				renderer.material.color = Color.grey;
				return;

			}
		}


		if(IsClicked())
		{
			//if(PlayerStatManager.GetInstance().DecrementBeats(1))
			//{
				AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_LOADING);
			//}

		}
	}
}
