﻿using UnityEngine;
using System.Collections;

public class PostGameScoreDispScript : MonoBehaviour {

	float m_highScoreToDisp;
	float m_displayingScore;
	float m_dispTime = 3.0f;
	float m_dispLerpTimer = 0;
	void Start () 
	{
		m_highScoreToDisp = PlayerStatManager.GetInstance().GetHighScore();
	}

	void Update()
	{
		float scoreToDisp = m_displayingScore;
		m_displayingScore = Mathf.Lerp(0,m_highScoreToDisp,m_dispLerpTimer/m_dispTime);
		if(m_dispLerpTimer < m_dispTime)
		{
			m_dispLerpTimer += Time.deltaTime;
		}
		else
		{
			m_dispLerpTimer = m_dispTime;
		}
		this.GetComponentInChildren<TextMesh>().text = ""+(int)m_displayingScore;

	}

}
