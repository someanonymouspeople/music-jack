﻿using UnityEngine;
using System.Collections;

public class InstructionsToolHandlerScript : MonoBehaviour {


	void Awake () 
	{
		GameObject go = GameObject.Instantiate(Resources.Load("UI/GamePlayInstructionsTool")) as GameObject;
		go.transform.position = new Vector3(0,0,-2);
		go.GetComponent<GamplayInstructionsScript>().DisableButtons();
	}
}
