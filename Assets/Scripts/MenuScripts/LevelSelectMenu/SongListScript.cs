﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
public class SongListScript : MonoBehaviour {

	public Vector2 m_commonScale;
	public float m_elementsPerPage;
	public List<GameObject> m_songListElementList;
	List<GameObject> m_pageList;
	GameObject m_library;
	public int m_distanceBetweenPages;
	int m_activePage;
	void Start () 
	{
		AudioManager.GetInstance().LoadAllAudioFileData(GameConfigurationHolderScript.GetInstance().GetSongDataPath());
		AppManager.GetInstance().SetSelectedSong(999);
		m_activePage = 0;
		m_songListElementList = new List<GameObject>();
		m_pageList = new List<GameObject>();
		GameObject sampleElement = Instantiate(Resources.Load("GameObjects/Utilities/SongListElement")) as GameObject;
		float scaleFactor = sampleElement.transform.localScale.x/m_elementsPerPage;

		m_commonScale.x = sampleElement.transform.localScale.x * scaleFactor;
		m_commonScale.y = sampleElement.transform.localScale.y * scaleFactor;
		PopulateSongList();
		Destroy(sampleElement);
	}

	private void PopulateSongList()
	{
		GameObject sampleElement = Instantiate(Resources.Load("GameObjects/Utilities/SongListElement")) as GameObject;
		m_library = new GameObject();
		m_library.name = "Library";
		List<GameObject> songsPerPage = new List<GameObject>();
		int arrayIndex = 0;
		List<FileInfo> songList = AudioManager.GetInstance().GetSoundFileData();
		if(songList.Count <1)
		{
			Debug.Log("No songs data, unable to populate list");
			Destroy(sampleElement);
			return;
		}
		float posToPlacePageY = (sampleElement.transform.localScale.y * m_commonScale.y * m_elementsPerPage/2) - this.transform.GetChild(0).renderer.bounds.extents.y * m_commonScale.y;
		Vector3 posToPlaceElement = new Vector3(this.gameObject.transform.position.x,posToPlacePageY,this.gameObject.transform.position.z);

		foreach(FileInfo song in songList)
		{
			GameObject listElement = Instantiate(Resources.Load("GameObjects/Utilities/SongListElement")) as GameObject;
			listElement.GetComponent<SongListButton>().SetRespectiveSongIndex(songList.IndexOf(song)); 
			listElement.transform.localScale = m_commonScale;
			listElement.gameObject.tag = "Song";
			listElement.GetComponent<MeshRenderer>().enabled = false;
			if(song.Name.Length>25)
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = song.Name.Remove(25);
				listElement.transform.GetComponentInChildren<TextMesh>().text += "...";
			}
			else
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = song.Name;
			}
			m_songListElementList.Add(listElement);

			//if elements per page count not met
			if(arrayIndex<m_elementsPerPage)
			{

				songsPerPage.Add(listElement);
				arrayIndex ++;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
			}
			else
			{
				GameObject currentPage = new GameObject();
				currentPage.tag= "Page";
				currentPage.name = "Page " + m_pageList.Count;
				foreach(GameObject element in songsPerPage)
				{
					element.transform.parent = currentPage.transform;
				}

				m_pageList.Add(currentPage);
				songsPerPage.Clear();
				arrayIndex = 0;
				songsPerPage.Add(listElement);
				posToPlaceElement.y = posToPlacePageY;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
				arrayIndex ++;
			}


		}

		//if leftover elements add them to a new page as well
		if(songsPerPage.Count >0)
		{
			GameObject currentPage = new GameObject();
			currentPage.gameObject.tag = "Page";
			currentPage.name = "Page "+ m_pageList.Count;
			foreach(GameObject element in songsPerPage)
			{
				element.transform.parent = currentPage.transform;
			}
			m_pageList.Add(currentPage);
		}

		//for sanity's sake and for ease of repositioning assign the pages to a library 
		foreach(GameObject page in m_pageList)
		{
			page.transform.parent = m_library.transform;
		}

		//destroy sample taken
		Destroy(sampleElement);

		//reposition the list appropriately
		RePosLibrary();
		m_library.transform.parent = this.gameObject.transform;

	}

	public void SetNextPage()
	{
		if((m_activePage+1) < m_pageList.Count)
		{
			Vector2 posToPlaceLibrary = m_library.transform.position;
			posToPlaceLibrary.x -= m_distanceBetweenPages;
			m_library.gameObject.transform.position = posToPlaceLibrary;
			m_activePage ++;
		}

	}

	public void SetPrevPage()
	{
		if((m_activePage-1) >=0)
		{
			Vector2 posToPlaceLibrary = m_library.transform.position;
			posToPlaceLibrary.x+= m_distanceBetweenPages;
			m_library.gameObject.transform.position = posToPlaceLibrary;
			m_activePage--;
		}
	}
	private void RePosLibrary()
	{
		Vector3 posToPlacePage = new Vector3(this.gameObject.transform.position.x,this.gameObject.transform.position.y,0);
		for(int i = 0; i<m_library.transform.childCount;i++)
		{
			GameObject pageToConsider = m_library.transform.GetChild(i).gameObject;
			if(pageToConsider.gameObject.tag == "Page")
			{
				pageToConsider.transform.position = posToPlacePage;
				posToPlacePage.x += m_distanceBetweenPages;
			}
		}
	}


}
