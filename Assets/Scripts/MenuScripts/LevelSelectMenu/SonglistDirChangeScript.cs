﻿using UnityEngine;
using System.Collections;

public class SonglistDirChangeScript : ClickableButtonScript {

	bool m_directorySet = false;

	bool CustomDirectorySetterManagerCallBack(string path)
	{
		if(path != "NotSet")
		{
			GameConfigurationHolderScript.GetInstance().SetSongDataPath(path);
			m_directorySet = true;
			ResetScene();
		}
		else
		{
			m_directorySet = false;
		}
		return m_directorySet;
	}

	void ChangeFolderInvoke()
	{
		GameObject directorySetter = GameObject.Instantiate(Resources.Load("UI/DirectorySetterPrefab")) as GameObject;
		directorySetter.GetComponent<DirectorySetterManagerScript>().SetCallBack(CustomDirectorySetterManagerCallBack);
	}

	void ResetScene()
	{
		AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_SETTINGS);
	}

	void Update () 
	{
		if(IsClicked())
		{
			Destroy(GameObject.Find("MenuEngine").gameObject);
			//this.enabled = false;
			ChangeFolderInvoke();
			GameConfigurationHolderScript.GetInstance().m_songDataPath = "NotSet";

		}
	}
}
