﻿using UnityEngine;
using System.Collections;

public class LoadingScreenScript : MonoBehaviour {

	public TextMesh m_linkedTextMesh;
	public float m_waitTime;
	public float m_waitTimer;
	public bool m_hasStartedLoading;
	public float m_prevCompletionPercentage;
	// Use this for initialization
	void Start () 
	{
		m_prevCompletionPercentage = 0;
		GameObject.Instantiate(Resources.Load("UI/LoadingScreenPictureViewer"));
		//init the other elements
		Vector3 posToSetCam = new Vector3(0,0,Camera.main.transform.position.z);
		Camera.main.transform.position = posToSetCam;
		ParallaxingBackGroundManager.GetInstance().m_backGroundBeingManaged.transform.position = Camera.main.transform.position;
		m_linkedTextMesh.text = "Loading ...";
		if( m_waitTime == 0)
		{
			m_waitTime = 1;
		}
		m_waitTimer = 0;
		m_hasStartedLoading = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!m_hasStartedLoading)
		{
			if(m_waitTimer < m_waitTime)
			{
				m_waitTimer+= Time.deltaTime;
				return;
			}
			else
			{
				m_waitTimer = 0;
				m_hasStartedLoading = true;
				MJAssetLoader.GetInstance().StartTasks();
			}

		}
		else
		{
			float completionPercentage = 0;
			if(MJAssetLoader.GetInstance().CheckIfFinished(out completionPercentage))
			{
				
				if(m_waitTimer < m_waitTime)
				{
					m_waitTimer += Time.deltaTime;
					return;
				}
				else
				{
					if(!LoadingScreenPicViewer.GetInstance().m_isStable)
					{
						return;
					}
					MJAssetLoader.GetInstance().ClearTaskList();
					AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_GAME);
					return;
				}

			}
			else
			{
				Debug.Log("Loading"+ completionPercentage);
			}
		}
	
	}
}
