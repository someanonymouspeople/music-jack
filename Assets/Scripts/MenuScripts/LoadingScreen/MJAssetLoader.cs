﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate IEnumerator LoadTask();
public delegate void UnloadTask();


public enum TASK_TYPE
{
	TYPE_LOAD_AUDIO,
	TYPE_MUSIC_PROCESSING,
	TYPE_MUSIC_PROCESSING_TIMED,
	TYPE_COROUTINE
}
public class TaskElement
{
	public TASK_TYPE m_taskType;
	public LoadTask m_loadingTask;
	public UnloadTask m_unloadTask;
	public Task m_taskObject;//To be assigned by the internal code. Please dont fill in
	public object m_extraInfo;
	public object m_extraInfo2;

}


public class MJAssetLoader : MonoBehaviour {

	public static MJAssetLoader m_instance;
	public List<Task> m_runningTasksHandleList;
	List<TaskElement> m_taskList;
	protected int m_completionPercentage;
	protected float m_completionPercentIncrement;
	bool m_isFinished;
	public Task m_activeTask;

	public void Awake()
	{
		if(MJAssetLoader.GetInstance()!= null)
		{
			Destroy(this);
			return;
		}
		m_instance = this;
		Init();
	}

	public static MJAssetLoader GetInstance()
	{
		return m_instance;
	}

	public void Refresh()
	{

	}

	public void Init()
	{
		m_isFinished = false;
		m_completionPercentage = 0;
		m_runningTasksHandleList = new List<Task>();
		m_taskList = new List<TaskElement>();
	}

	public void AddTask(TaskElement element)
	{
		if(!element.Equals(null))
		{
			m_taskList.Add(element);
		}
	}

	public bool RemoveTask(int index)
	{
		if(m_taskList != null)
		{
			if(m_taskList.Count > 0)
			{
				index = Mathf.Clamp(index,0,m_taskList.Count -1);
				m_taskList[index].m_unloadTask();
				m_taskList[index].m_taskObject.Stop();
				m_taskList.RemoveAt(index);
				return true;
			}
			else
			{
				Debug.Log("Task list empty");
				return false;
			}
		}
		else
		{
			Debug.Log("MJAsset manager not initialized");
			return false;
		}

	
	}


	
	public bool RemoveTask(TaskElement element)
	{
		if(m_taskList != null)
		{
			if(m_taskList.Count > 0)
			{
				TaskElement temp = m_taskList.Find(x=> x.m_loadingTask ==element.m_loadingTask && x.m_unloadTask == element.m_unloadTask);
				if(!temp.Equals(null))
				{
					element.m_taskObject.Stop();
					element.m_unloadTask();
					m_taskList.Remove(element);
					return true;
				}
			}
			else
			{
				Debug.Log("Task list empty");
				return false;
			}
		}
		else
		{
			Debug.Log("MJAsset manager not initialized");

		}
		return false;
	}

	public void ClearTaskList()
	{
		if(m_taskList != null)
		{
			foreach(TaskElement element in m_taskList)
			{
				if(element.m_unloadTask != null)
				{
					element.m_unloadTask();
				}
			}
			m_taskList.Clear();
		}
		else
		{
			Debug.Log("MJAsset manager not initialized");
			return;
		}
	}

	public void StartTasks()
	{
		m_completionPercentIncrement = 100.0f/m_taskList.Count;
		for(int i = 0; i<m_taskList.Count ;i++)
		{
			switch (m_taskList[i].m_taskType)
			{
				case TASK_TYPE.TYPE_LOAD_AUDIO:
				{

					Task tempTask = new Task(AudioManager.GetInstance().LoadAudioFileInThread(AudioManager.GetInstance().GetAudioFilePath((int)m_taskList[i].m_extraInfo)),true);
					m_taskList[i].m_taskObject = tempTask;
					m_runningTasksHandleList.Add(tempTask);
					
				}
					break;
				case TASK_TYPE.TYPE_MUSIC_PROCESSING:
				{
					
					Task tempTask = new Task(MusicAnalyzerV2.GetInstance().CalculateBeatTimeLine(AudioManager.GetInstance().GetAudioFile((int)m_taskList[i].m_extraInfo),BEAT_DETECTION_MODES.TONE_BASED,true));
					m_taskList[i].m_taskObject = tempTask;
					m_runningTasksHandleList.Add(tempTask);
					
				}
				break;
				case TASK_TYPE.TYPE_MUSIC_PROCESSING_TIMED:
				{
					Task tempTask = new Task(MusicAnalyzerV2.GetInstance().CalculateBeatTimeLine(AudioManager.GetInstance().GetAudioFile((int)m_taskList[i].m_extraInfo),BEAT_DETECTION_MODES.TONE_BASED,true,false,0,((int)m_taskList[i].m_extraInfo2)));
					m_taskList[i].m_taskObject = tempTask;
					m_runningTasksHandleList.Add(tempTask);
				}
				break;
				case TASK_TYPE.TYPE_COROUTINE:
				{
					Task tempTask = new Task(m_taskList[i].m_loadingTask(),true);
					m_taskList[i].m_taskObject = tempTask;
					m_runningTasksHandleList.Add(tempTask);
				}
					break;
				default:
				{
					Debug.Log("Invalid task type");
				}
					break;
			}



		}
		if(m_runningTasksHandleList.Count>0)
		{
		m_activeTask = m_runningTasksHandleList[0];
		}
	}

	public void CheckAndRelease()
	{
		foreach(Task t in m_runningTasksHandleList)
		{
			if(!t.Running)
			{
				m_completionPercentage += (int)m_completionPercentIncrement;
				StartCoroutine(LoadingScreenPicViewer.GetInstance().SwitchScreen(Random.Range(10,((LoadingScreenPicViewer.GetInstance().m_screenOptions.Length-1)*10)+9)));
			}
		}
		m_runningTasksHandleList.RemoveAll(t => t.Running == false);
		if(m_runningTasksHandleList.Count>0)
		{
			m_activeTask = m_runningTasksHandleList[0];
		}
	}
	
	public bool CheckIfFinished(out float completionPercentage)
	{
		completionPercentage= 0;
		completionPercentage = m_completionPercentage;
		CheckAndRelease();
		if(m_runningTasksHandleList.Count ==0)
		{
			//m_isFinished = true;
			return true;
		}
		return false;
	}
	
}
