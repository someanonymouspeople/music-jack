﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ACHIEVEMENTS
{
	ACHIEVEMENT_FIRST_TIMER = 1,
	ACHIEVEMENT_ONE_SONG = 2 ,
	ACHIEVEMENT_FIVE_SONGS = 4,
	ACHIEVEMENT_TEN_SONGS = 8,
	ACHIEVEMENT_FIVE_GATES = 16,
	ACHIEVEMENT_TEN_GATES = 32,
	ACHIEVEMENT_FIFTY_GATES = 64,
	ACHIEVEMENT_HUNDRED_GATES = 128,
	ACHIEVEMENT_CUSTOMIZED_CHARACTER = 256,
	ACHIEVEMENT_PURCHASED_MULTIPLIER = 512
}

public class AchievementListScript : MonoBehaviour 
{

	public List<ListElementScript> m_elementList;
	public List<ListElementScript[]> m_pages;

	public float m_xDistanceBetweenPages;
	public int m_elementsPerPage;
	public ACHIEVEMENTS m_achievementsUnlocked;


	public int m_currentPage;
	public int m_nextPage;
	public float m_distBetweenElements;
	public string m_xmlToReadFrom;
	public AchievementList m_achievementList;

	public void SetNextPage()
	{
		m_nextPage = m_currentPage+1;
		if(m_nextPage > m_pages.Count-1)
		{
			m_nextPage = m_pages.Count-1;
		}
	}
	public void SetPrevPage()
	{
		m_nextPage = m_currentPage-1;
		if(m_nextPage <0)
		{
			m_nextPage = 0;
		}
	}

	void Start () 
	{
		m_achievementList = new AchievementList();
		m_pages = new List<ListElementScript[]>();
		m_elementList = new List<ListElementScript>();
		m_achievementsUnlocked = AppManager.GetInstance().GetAchievements();
		m_xmlToReadFrom = "AchievementXml.xml";
		m_achievementList = XmlSupport.DeserializeXml<AchievementList>(m_xmlToReadFrom);

		PopulateScreenAndElementList();
		CheckAndSetIsActivated(m_elementList);

	}

	public void CheckAndSetIsActivated(List<ListElementScript> elementListToCheck)
	{
		for(int i = 0;i<elementListToCheck.Count;i++)
		{
			if((m_achievementsUnlocked & elementListToCheck[i].m_achievementValue) == elementListToCheck[i].m_achievementValue)
			{
				m_elementList[i].SetActivated(true);
			}
		}
	}

	public void PopulateScreenAndElementList()
	{
		GameObject emptyGo = new GameObject();
		emptyGo.name = "ListElements";
		int numberOfAchievements = System.Enum.GetNames(typeof(ACHIEVEMENTS)).Length;
		float drawPosX = 0;
		float drawPosY = 0;

		int achievementIndex = 0;

		for(int i = 0;i <(numberOfAchievements/m_elementsPerPage)+1; i++)
		{
			int listAlloc = numberOfAchievements -achievementIndex;
			if(listAlloc < m_elementsPerPage)
			{
				listAlloc = numberOfAchievements -achievementIndex;
			}
			else
			{
				listAlloc = m_elementsPerPage;
			}

			ListElementScript[] tempArray = new ListElementScript[listAlloc];
			for(int j =0 ; j< m_elementsPerPage;j++)
			{
				if(achievementIndex<numberOfAchievements)
				{
					GameObject go = Instantiate(Resources.Load("GameObjects/Utilities/ListElement")) as GameObject ;
					go.transform.parent = transform;
					go.AddComponent<ListElementScript>();
					ListElementScript tempScript = go.GetComponent<ListElementScript>();

					tempScript.SetName(m_achievementList.m_achievementElementList[achievementIndex].m_achievementName);
					tempScript.SetDesc(m_achievementList.m_achievementElementList[achievementIndex].m_achievementDesc);

					tempScript.SetPos(new Vector3 (drawPosX,drawPosY,0));
					tempArray[j] = go.GetComponent<ListElementScript>();
					drawPosY -= m_distBetweenElements;
					tempScript.gameObject.transform.parent = emptyGo.transform;
					tempScript.SetValue((ACHIEVEMENTS)System.Enum.GetValues(typeof(ACHIEVEMENTS)).GetValue(achievementIndex));
					m_elementList.Add(tempScript);
				}
				achievementIndex ++;
			}
			drawPosY += m_distBetweenElements * m_elementsPerPage;
			drawPosX += m_xDistanceBetweenPages;
			m_pages.Add(tempArray);

		}
		float emptyGoPosX = this.gameObject.transform.position.x - this.gameObject.renderer.bounds.extents.x + this.gameObject.renderer.bounds.extents.x/2;
		float emptyGoPosY = this.gameObject.transform.position.y + this.gameObject.renderer.bounds.extents.y - this.gameObject.renderer.bounds.extents.x/4;
		emptyGo.transform.position = new Vector3(emptyGoPosX,emptyGoPosY,0);
		emptyGo.transform.parent = this.transform.parent;

	}

	public void CheckAndSwitchPages()
	{
		if(m_currentPage!=m_nextPage)
		{
			Vector3 tempPos;
			if(m_nextPage > m_currentPage)
			{
				tempPos = GameObject.Find("ListElements").transform.position;
				tempPos.x -= m_xDistanceBetweenPages;
			}
			else
			{
				tempPos = GameObject.Find("ListElements").transform.position;
				tempPos.x += m_xDistanceBetweenPages;
			}
			GameObject.Find("ListElements").transform.position = tempPos;
			m_currentPage = m_nextPage;
		}
	}
	

	void Update () 
	{
		CheckAndSwitchPages();
	}
}
