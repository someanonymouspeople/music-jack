﻿using UnityEngine;
using System.Collections;

public class FBButtonScript : ClickableButtonScript {


	public void Start()
	{
		FB.Init(onCustomInitComplete,OnCustomHideUnity,null);
	}
	static void onCustomInitComplete()
	{
		Debug.Log("FB Initialized");
	}
	static void OnCustomHideUnity(bool checkFlag)
	{
		Debug.Log ("Unity hiding");
		if(!checkFlag)
		{
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}
	}
	static void FBLoginCallBack(FBResult result)
	{
		Debug.Log("Logged in :/");
	}
	void Update () 
	{
		if(IsClicked())
		{
			if(!FB.IsLoggedIn)
			{
				FB.Login("basic_info,publish_actions",FBLoginCallBack);
			}
			else
			{
				FB.Feed(link:"https://developers.facebook.com/apps/260234987495712",linkName:"TEST",linkCaption:"TEST CAPTION",linkDescription:"TEST DESC");
			}
		}
	}

}
