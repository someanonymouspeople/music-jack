﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
public class LeaderBoardButtonScript : ClickableButtonScript {

	bool m_attemptingLogin = true;
	IEnumerator CouldNotLogInInvoke()
	{
		Debug.Log ("LeaderBoard could not login invoked");
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Unable to Login \nFacebook servers seem down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		//re enable
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		Debug.Log ("LeaderBoard DB reinited");
	}
	IEnumerator FBActiveInvoke()
	{
		Debug.Log ("Leaderboard FBActive invoked");
		
		m_attemptingLogin = true;
		FB.Login("public_profile,user_friends,publish_actions",CustomConnectedButtonCallBack);
		
		while(m_attemptingLogin)
		{
			yield return null;
		}
		
		if(FB.IsLoggedIn)
		{
			ConnectedModeInvoke();
			//((PlayGamesPlatform) Social.Active).ShowLeaderboardUI("CgkI-8Lb87ISEAIQAg");
			//this.enabled = true;
			//GameConfigurationHolderScript.GetInstance().m_guestMode = false;
			//GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
			//Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		}
		else
		{
			StartCoroutine("CouldNotLogInInvoke");
		}
		
	}
	
	void CustomConnectedButtonCallBack(FBResult result)
	{
		Debug.Log ("LeaderBoard CustomShareButtonCallBack invoked");
		
		m_attemptingLogin = false;
		
		
	}
	IEnumerator NetInactiveInvoke()
	{
		Debug.Log ("LeaderBoard NetInactiveInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Internet down\n please connect and retry",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		//Reinit
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		
	}
	
	IEnumerator AsYourselfInvoke()
	{
		Debug.Log ("LeaderBoard AsYourselfInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Logging in\n to Facebook",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		Debug.Log ("LeaderBoard is clicked");
		
		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
		
		if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
		{
			Debug.Log ("LeaderBoard is connected to internet");
			
			//GPG
			//Find and set the playing platform
			PlayGamesPlatform.Activate();
			
			//enable debug log
			PlayGamesPlatform.DebugLogEnabled = true;
			
			//Login
			Social.localUser.Authenticate((bool success) => {
				if(success)
				{
					Debug.Log("Logged in to google play "+Social.localUser.userName);
				}
				else
				{
					Debug.Log("Coudnt log in to google play");
				}
				
			});
			
			
			
			result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
			
			if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
			{
				Debug.Log ("LeaderBoard FB active");
				//Destroy(m_dialogeBox);
				StartCoroutine("FBActiveInvoke");
				
			}
			else
			{

				StartCoroutine("CouldNotLogInInvoke");
			}
		}
		else
		{
			//Destroy(m_dialogeBox);
			StartCoroutine("NetInactiveInvoke");
		}
		return true;
	}
	


	bool LogInCallBack(MusicJunkieDialogBoxScript script)
	{
		Destroy(script.gameObject);
		StartCoroutine("AsYourselfInvoke");
		//GameConfigurationHolderScript.GetInstance().m_guestMode = false;
		return true;
	}

	IEnumerator CancelledInvoke()
	{
		Debug.Log ("LeaderBoard CancelledInvoked");
		//Reinit
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		yield break;
	}


	bool CancelCallBack(MusicJunkieDialogBoxScript script)
	{
		Destroy(script.gameObject);

		StartCoroutine("CancelledInvoke");
		return true;
	}

	void GuestModeInvoke()
	{

		Debug.Log ("LeaderBoard NetInactiveInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		MJUIButtonFiller logInFiller = new MJUIButtonFiller("",LogInCallBack);
		logInFiller.m_customButtonSprite = Resources.Load<Sprite>("UI/CustomUIButtons/FbLoginSmall");
		MJUIButtonFiller cancelFiller = new MJUIButtonFiller("Back",CancelCallBack);
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Accessing Leaderboards requires\n you to login via Facebook",2,logInFiller,cancelFiller);


	}

	bool EasyCallBack(MusicJunkieDialogBoxScript script)
	{
		//Destroy(script.gameObject);
		((PlayGamesPlatform) Social.Active).ShowLeaderboardUI("CgkI-8Lb87ISEAIQAg");
		this.enabled = true;
		//GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		//Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		return true;
	}

	bool MediumCallBack(MusicJunkieDialogBoxScript script)
	{
		//Destroy(script.gameObject);
		((PlayGamesPlatform) Social.Active).ShowLeaderboardUI("CgkI-8Lb87ISEAIQAw");
		this.enabled = true;
		//GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		//Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		return true;
	}

	bool HardCallBack(MusicJunkieDialogBoxScript script)
	{
		//Destroy(script.gameObject);
		((PlayGamesPlatform) Social.Active).ShowLeaderboardUI("CgkI-8Lb87ISEAIQBA");
		this.enabled = true;
		//GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		//Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		return true;
	}

	void ConnectedModeInvoke()
	{
		Debug.Log ("LeaderBoard ConnectedModeInvoked");
		
//		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
//		MJUIButtonFiller easyFiller = new MJUIButtonFiller("Easy",EasyCallBack);
//		MJUIButtonFiller mediumFiller = new MJUIButtonFiller("Medium",MediumCallBack);
//		MJUIButtonFiller hardFiller = new MJUIButtonFiller("Hard",HardCallBack);
//		MJUIButtonFiller cancelFiller = new MJUIButtonFiller("Cancel",CancelCallBack);
//
//		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Select Leaderboard",4,easyFiller,mediumFiller,hardFiller,cancelFiller);
	
		GameObject.Instantiate(Resources.Load("UI/LeaderBoardDispPrefab"));

	}
	void Update () 
	{
		if(IsClicked())
		{
			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
			this.enabled = false;

			if(!GameConfigurationHolderScript.GetInstance().m_guestMode)
			{
				ConnectedModeInvoke();
			}
			else
			{

				GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
				this.enabled = false;
				GuestModeInvoke();
			}
		}

	}
}
