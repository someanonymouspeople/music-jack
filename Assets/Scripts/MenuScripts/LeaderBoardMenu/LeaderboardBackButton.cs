﻿using UnityEngine;
using System.Collections;

public class LeaderboardBackButton : ClickableButtonScript {

	void Update()
	{
		if(IsClicked())
		{
			Debug.Log ("LeaderBoard CancelledInvoked");
			//Reinit
			this.enabled = true;
			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
			Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
			Destroy(this.transform.parent.gameObject);
		}
	}
}
