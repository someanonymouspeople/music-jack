﻿using UnityEngine;
using System.Collections;

public class PauseButtonScript : ClickableButtonScript 
{
	public bool m_isHidden;
	
	public void SetHidden(bool hidden)
	{
		if(hidden)
		{

			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
			this.gameObject.renderer.material.color = new Color(1,1,1,0);
			m_isHidden = true;
		}
		else
		{
			Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
			this.gameObject.renderer.material.color = new Color(1,1,1,1);
			m_isHidden = false;
		}
	}

	public bool UpdatePauseButton()
	{
		if(!m_isHidden)
		{
			if(IsClicked())
			{
				return true;
			}
		}
		return false;
	}
}
