﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpriteParticleSystem : MonoBehaviour 
{
	public int m_systemLifetime;
	public int m_particlesPerSecond;
	public SpriteParticleScript m_particleToSpawn;

	public int m_sizeOverLifetime;
	public Color m_colorOverLifetime;
	public Vector2 m_velocityOverLifetime;
	public Quaternion m_rotationOverLifetime;

	public List <SpriteParticleScript> m_particleList;

	public float m_spawnTimer = 0;
	public float m_systemTimer = 0;
	public void UpdateSystem() 
	{

		if(m_systemTimer < m_systemLifetime || m_systemLifetime == 0)
		{
			//update && remove
			for(int i =0; i<m_particleList.Count;i++)
			{
				//update
				m_particleList[i].UpdateParticle();

				//remove
				if(!m_particleList[i].IsAlive())
				{
					Destroy (m_particleList[i].gameObject);
					m_particleList.RemoveAt(i);
				}
			}

			//spawn
			if(m_spawnTimer > (float)(1.0f/(float)m_particlesPerSecond))
			{
				m_spawnTimer = 0;
				GameObject tempParticle = Instantiate(m_particleToSpawn.gameObject,this.gameObject.transform.position,new Quaternion(0,0,0,0)) as GameObject;
				SpriteParticleScript tempScript = m_particleToSpawn.GetComponent<SpriteParticleScript>();
				tempParticle.AddComponent("SpriteParticleScript");
				tempParticle.GetComponent<SpriteParticleScript>().CopyAttributes(tempScript);
				tempParticle.GetComponent<SpriteParticleScript>().SetFinalAttributes(m_sizeOverLifetime,m_colorOverLifetime,m_velocityOverLifetime,m_rotationOverLifetime);
				m_particleList.Add(tempParticle.GetComponent<SpriteParticleScript>());
			}
			else
			{
				m_spawnTimer += Time.deltaTime;
			}


			m_systemTimer+= Time.deltaTime;
		}
		else
		{
			m_systemTimer += Time.deltaTime;
		}

	}
}
