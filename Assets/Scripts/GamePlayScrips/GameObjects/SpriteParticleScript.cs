﻿using UnityEngine;
using System.Collections;

public class SpriteParticleScript : MonoBehaviour
{
	
	public float m_particleLifetime;
	public float m_timeRemaining;
	public float m_startSize;
	public Color m_startColor;
	public Vector2 m_startVelocity;
	public Quaternion m_startRotation;
	
	public float m_finalSize;
	public float m_sizeDiff;
	public Color m_finalColor;
	public Color m_colorDiff;
	public Vector2 m_finalVelocity;
	public Vector2 m_velDiff;
	public Quaternion m_finalRotation;
	public Quaternion m_rotDiff;
	
	public bool m_isAlive;
	public Vector2 m_currentVelocity;
	void Start()
	{
		m_isAlive = true;
	}
	public bool IsAlive()
	{
		return m_isAlive;
	}
	public virtual void SetFinalAttributes(int finalSize,Color finalColor,Vector2 finalVelocity,Quaternion finalRotation)
	{
		m_finalSize = finalSize;
		m_sizeDiff = m_finalSize - m_startSize;
		m_finalColor = finalColor;
		m_finalVelocity = finalVelocity;
		m_velDiff = m_finalVelocity - m_startVelocity;
		m_finalRotation = finalRotation;
		m_rotDiff = m_finalRotation = m_startRotation;
		m_timeRemaining = m_particleLifetime;
		m_currentVelocity = m_startVelocity;
	}

	public virtual void CopyAttributes(SpriteParticleScript tempScript)
	{
		m_particleLifetime =tempScript.m_particleLifetime;
		m_startSize = tempScript.m_startSize;
		m_startColor = tempScript.m_startColor;
		m_startVelocity = tempScript.m_startVelocity;
		m_startRotation = tempScript.m_startRotation;
	}
	
	public virtual void UpdateParticle()
	{
		Vector2 localScale = this.gameObject.transform.localScale;
		Quaternion rotation = this.gameObject.transform.rotation;
		Color color = this.gameObject.renderer.material.color;
		
		//check and set size
		if(localScale.x != m_finalSize)
		{
			if(m_finalSize > localScale.x)
			{
				localScale.x += m_sizeDiff * Time.deltaTime/m_particleLifetime;
				localScale.y += m_sizeDiff * Time.deltaTime/m_particleLifetime;
			}
			else
			{
				localScale.x -= m_sizeDiff * Time.deltaTime/m_particleLifetime;
				localScale.y -= m_sizeDiff * Time.deltaTime/m_particleLifetime;
			}
			this.gameObject.transform.localScale = localScale;
		}
		//check and set color
		
		if(color != m_finalColor)
		{
			float lerpPos =  1- ((m_timeRemaining/m_particleLifetime));
			color = Color.Lerp(m_startColor,m_finalColor,lerpPos);
			
			this.gameObject.renderer.material.color = color;
			
		}
		
		//check and set Velocity
		if(m_currentVelocity != m_finalVelocity)
		{
			if(m_currentVelocity.x < m_finalVelocity.x)
			{
				m_currentVelocity.x += m_velDiff.x * (Time.deltaTime/m_particleLifetime);
			}
			else
			{
				m_currentVelocity.x -= m_velDiff.x * (Time.deltaTime/m_particleLifetime);
			}
			if(m_currentVelocity.y < m_finalVelocity.y)
			{
				m_currentVelocity.y += m_velDiff.y * (Time.deltaTime/m_particleLifetime);
			}
			else
			{
				m_currentVelocity.y -= m_velDiff.y * (Time.deltaTime/m_particleLifetime);
			}
			
			this.gameObject.transform.position += (Vector3)m_currentVelocity;
		}
		//check and set rotation
		
		if(rotation != m_finalRotation)
		{
			if(rotation.x < m_finalRotation.x)
			{
				rotation.x += m_rotDiff.x * (Time.deltaTime/m_particleLifetime);
			}
			else
			{
				rotation.x -= m_rotDiff.x * (Time.deltaTime/m_particleLifetime);
			}
			if(rotation.y < m_finalRotation.y)
			{
				rotation.y += m_rotDiff.y * (Time.deltaTime/m_particleLifetime);
			}
			else
			{
				rotation.y -= m_rotDiff.y * (Time.deltaTime/m_particleLifetime);
			}
			if(rotation.z < m_finalRotation.z)
			{
				rotation.z += m_rotDiff.z * (Time.deltaTime/m_particleLifetime);
			}
			else
			{
				rotation.z -= m_rotDiff.z * (Time.deltaTime/m_particleLifetime);
			}
			this.gameObject.transform.rotation = rotation;
		}

		m_timeRemaining -= Time.deltaTime;
		if(m_timeRemaining < 0)
		{
			m_isAlive = false;
		}
		
	}
	
}