﻿using UnityEngine;
using System.Collections;

public class FollowTransformScript : MonoBehaviour 

{

	public Transform m_transformToFollow;
	
	public Vector3 m_distanceBetween;
	public Vector3 m_startDistBetween;
	public bool m_freezeX;
	public bool m_freezeY;
	public bool m_freezeZ;

	private Vector3 m_posRelativeToTransform;
	private Vector3 m_startPos;

	public void SetFreezeX(bool isFrozen)
	{
		m_freezeX = isFrozen;
	}

	public void SetFreezeY(bool isFrozen)
	{
		m_freezeY = isFrozen;
	}

	public void SetFreezeZ(bool isFrozen)
	{
		m_freezeZ = isFrozen;
	}

	public void SetTransformTarget(Transform transform,bool calcAndMaintainDist = true)
	{
		m_transformToFollow = transform;
		if(calcAndMaintainDist)
		{
			m_distanceBetween = transform.position - this.gameObject.transform.position;
			m_startDistBetween = m_distanceBetween;
		}
	}

	void Start () 
	{
		m_distanceBetween = new Vector3();
		m_distanceBetween = m_transformToFollow.transform.position - this.transform.position;
		m_startPos = this.transform.position;
	}

	public void SetDistanceBetween(Vector3 distance)
	{
		m_distanceBetween = distance;
	}

	public Vector3 GetStartDistanceBetween()
	{
		return m_startDistBetween;
	}
	
	void Update () 
	{
		m_posRelativeToTransform = m_transformToFollow.transform.position - m_distanceBetween;
		Vector3 tempPos = m_posRelativeToTransform;

		if(m_freezeX)
		{
			tempPos.x = m_startPos.x;
		}
		if(m_freezeY)
		{
			tempPos.y = m_startPos.y;
		}
		if(m_freezeZ)
		{
			tempPos.z =m_startPos.z;
		}

		this.transform.position = new Vector3(tempPos.x,tempPos.y,tempPos.z);
	}
}
