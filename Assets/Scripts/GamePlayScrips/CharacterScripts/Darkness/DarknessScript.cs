﻿using UnityEngine;
using System.Collections;

public class DarknessScript : DarknessStateMachine {


	public float m_introTime;
	public float m_extroTime;
	public float m_gameLostMotionTime;
	public float m_closingInTime;
	float m_governingValue;
	public float m_chaseMaxSpeedPercent;
	float m_darknessCommonTimer;

	public int m_minScreenPercentage;
	public int m_maxScreenPercentage;
	float m_maxLocalXTravel;
	float m_minLocalXTravel;
	float m_creepSpeed;

	float m_screenTravelInUnits;
	Vector2 m_screenSizeInUnits;

	//helper variables
	Vector3 m_animSpecificStartPos;
	float m_prevRelativeSpeed;
	public bool m_varyDistanceIn_Game;
	public bool m_isActive;

	public float m_easyCreepSpeedDivisor;
	public float m_mediumCreepSpeedDivisor;
	public float m_hardCreepSpeedDivisor;

	bool m_flatPlaning;
	
	GameplayScript.DarknessCallBack m_ifJunkieLostCallBack;

	public float GetMinLocalXTravel()
	{
		return m_minLocalXTravel;
	}

	public float GetMaxLocalXTravel()
	{
		return m_maxLocalXTravel;
	}

	public void Awake()
	{

		m_darknessCommonTimer = 0;

		if(m_introTime == 0)
		{
			m_introTime = 1;
		}

		if(m_extroTime == 0)
		{
			m_extroTime = 2;
		}

		if(m_gameLostMotionTime == 0)
		{
			m_gameLostMotionTime = 4;
		}

		if(m_minScreenPercentage == 0)
		{
			m_minScreenPercentage = 10;
		}
		if(m_maxScreenPercentage == 0)
		{
			m_maxScreenPercentage = 25;
		}


		m_screenSizeInUnits = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height)) - Camera.main.ScreenToWorldPoint(Vector2.zero);
		m_minLocalXTravel = m_screenSizeInUnits.x * ((float)m_minScreenPercentage/100);
		m_maxLocalXTravel = m_screenSizeInUnits.x * ((float)m_maxScreenPercentage/100);
		if(m_creepSpeed == 0)
		{
			switch(AppManager.GetInstance().GetGameDifficulty())
			{
				case GAME_DIFFICULTY.DIFFICULTY_EASY:
				{
					m_creepSpeed = (m_maxLocalXTravel - m_minLocalXTravel)/m_easyCreepSpeedDivisor;
					m_chaseMaxSpeedPercent = 50;
				}	
				break;
				case GAME_DIFFICULTY.DIFFICULTY_MEDIUM:
				{
					m_creepSpeed = (m_maxLocalXTravel - m_minLocalXTravel)/m_mediumCreepSpeedDivisor;
					
				m_chaseMaxSpeedPercent = 65;
				}
				break;
				case GAME_DIFFICULTY.DIFFICULTY_HARD:
				{
					m_creepSpeed = (m_maxLocalXTravel - m_minLocalXTravel)/m_hardCreepSpeedDivisor;
					m_chaseMaxSpeedPercent = 80;
				}
				break;
			}
		}
		Vector3 localSpritePos = this.transform.GetChild(0).transform.localPosition;
		localSpritePos.x = - (m_screenSizeInUnits.x/2) - this.transform.GetChild(0).renderer.bounds.size.x*1f;
		this.transform.GetChild(0).transform.localPosition = localSpritePos;
		m_isActive = false;
		m_animSpecificStartPos = this.transform.position;
		this.gameObject.transform.parent = Camera.main.transform;

	}

	public void InitializeDarkness(GameplayScript.DarknessCallBack ifJunkieLostCallBack)
	{
		m_ifJunkieLostCallBack = ifJunkieLostCallBack;
	}

	protected override void CheckAndInitStateChanges ()
	{
		if(m_nextState != m_currentState)
		{
			switch(m_nextState)
			{
			case DARKNESS_STATES.STATE_CREATED:
			{
				m_darknessCommonTimer = 0;
				m_prevState = m_currentState;
				m_currentState = m_nextState;

			}
				break;
				
			case DARKNESS_STATES.STATE_INTROING:
			{
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case DARKNESS_STATES.STATE_INGAME:
			{
				m_darknessCommonTimer = 0;
				m_isActive = true;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case DARKNESS_STATES.STATE_CLOSING_IN:
			{
				m_darknessCommonTimer = 0;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case DARKNESS_STATES.STATE_GAME_LOST:
			{
				m_darknessCommonTimer = 0;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case DARKNESS_STATES.STATE_LEAVING:
			{
				m_darknessCommonTimer = 0;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case DARKNESS_STATES.STATE_DEAD:
			{
				m_prevState = m_currentState;
				m_currentState = m_nextState;
				m_nextState = DARKNESS_STATES.STATE_DESTROYED;
			}
				break;
			case DARKNESS_STATES.STATE_DESTROYED:
			{

				Destroy(this.gameObject);
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;			
			}
		}
	}

	protected override void CreatedUpdate ()
	{

	}

	protected override void IntroingUpdate ()
	{
		if(m_ifJunkieLostCallBack == null)
		{
			Debug.Log("Darkness not inited, the script will be disabled. Please init darkness");
			this.enabled = false;
			return;
		}


//		float xPosToReach = m_maxLocalXTravel * 0.5f;// - (m_maxLocalXTravel/3);
//		Vector3 pos = this.transform.position;
//		if(m_darknessCommonTimer < m_introTime)
//		{
//			m_darknessCommonTimer += Time.deltaTime;
//
//			if(pos.x < xPosToReach)
//			{
//				pos.x = Mathf.Lerp(m_animSpecificStartPos.x,xPosToReach,m_darknessCommonTimer/m_introTime);
//			}
//
//		}
//		else
//		{
//
//			pos.x = xPosToReach;
	
			//this.SetState(DARKNESS_STATES.STATE_INGAME);
		//}

		//this.gameObject.transform.position = pos;
	}

	protected override void InGameUpdate ()
	{
		if(m_isActive)
		{
			//get relation between min and max speed
			float relativeHeroSpeed = GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed();

			//adjust ingame position if can varyDistanceIn_Game
			if(m_varyDistanceIn_Game)
			{
				Vector3 localPos = this.gameObject.transform.localPosition;
				m_governingValue = (1-((GameplayScript.GetInstance().GetHeroScript().m_speed - GameplayScript.GetInstance().GetHeroScript().m_minSpeed)/ ((GameplayScript.GetInstance().GetHeroScript().m_maxSpeed - GameplayScript.GetInstance().GetHeroScript().m_minSpeed)* (m_chaseMaxSpeedPercent/100))));

				localPos.x += m_creepSpeed * Time.deltaTime * m_governingValue;

				if(localPos.x < m_minLocalXTravel)
				{
					localPos.x = m_minLocalXTravel;
				}
				this.gameObject.transform.localPosition = localPos;

			}

		}
	}

	protected override void ClosingInUpdate ()
	{

	}

	protected override void GameLostUpdate ()
	{
		Vector3 localPos = this.gameObject.transform.localPosition;
		if(m_darknessCommonTimer < m_gameLostMotionTime)
		{
			m_darknessCommonTimer += Time.deltaTime;
			localPos.x = Mathf.Lerp(m_animSpecificStartPos.x,m_screenSizeInUnits.x, m_darknessCommonTimer/m_gameLostMotionTime);
		}
		else
		{
			this.SetState(DARKNESS_STATES.STATE_DEAD);
		}
		this.gameObject.transform.localPosition = localPos;
	}



	protected override void LeavingUpdate ()
	{
		if(m_darknessCommonTimer < m_extroTime)
		{
			Vector3 localPos = this.gameObject.transform.localPosition;
			localPos.x -= m_creepSpeed*3*Time.deltaTime;
			this.gameObject.transform.localPosition = localPos;
			//add anim code or extro code here
		}
		else
		{
			this.SetState(DARKNESS_STATES.STATE_DESTROYED);
			UpdateStateMachine();
		}
	}

	protected override void FixedMachineUpdate ()
	{
		Vector3 localPos = this.gameObject.transform.localPosition;
		if(localPos.x > m_maxLocalXTravel && GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed() < 0.8f)
		{
			if(m_darknessCommonTimer < m_closingInTime)
			{
				m_darknessCommonTimer += Time.deltaTime;
			}
			else
			{
				m_enableFixedMachineUpdate = false;
				m_ifJunkieLostCallBack.Invoke();
				m_animSpecificStartPos = localPos;
			}
		}
	}

	protected override void UpdateDeath ()
	{

	}

	protected override void UpdatePostDeath ()
	{

	}


	public void UpdateDarkness()
	{
		UpdateStateMachine();
	}

}
