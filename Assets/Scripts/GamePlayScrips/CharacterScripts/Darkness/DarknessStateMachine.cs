﻿using UnityEngine;
using System.Collections;


public enum DARKNESS_STATES
{
	STATE_UN_INITED,
	STATE_CREATED,
	STATE_INTROING,
	STATE_INGAME,
	STATE_CLOSING_IN,
	STATE_GAME_LOST,
	STATE_LEAVING,
	STATE_DEAD,
	STATE_DESTROYED
};


public abstract class DarknessStateMachine: MonoBehaviour 
{
	protected bool m_enableFixedMachineUpdate;
	protected DARKNESS_STATES m_prevState;
	public DARKNESS_STATES m_currentState;
	protected DARKNESS_STATES m_nextState;
	//public Animator m_darknessAnimator;

	protected void Start () 
	{
		m_enableFixedMachineUpdate= true;
		//m_darknessAnimator = this.gameObject.transform.GetComponentInChildren<Animator>();
		m_currentState = DARKNESS_STATES.STATE_UN_INITED;
		m_prevState = DARKNESS_STATES.STATE_UN_INITED;
		m_nextState = DARKNESS_STATES.STATE_CREATED;
		//SetAnim(JUNKIE_ANIMATION.ANIM_INTRO_TWO);
		
	}
	void Awake()
	{

	}
	
	public void SetState(DARKNESS_STATES state)
	{
		m_nextState = state;
	}
	
	public DARKNESS_STATES GetState()
	{
		return m_currentState;
	}

//	public void SetAnim(JUNKIE_ANIMATION anim)
//	{
//		if(anim != m_currentAnimation)
//		{
//			switch(anim)
//			{
//			case JUNKIE_ANIMATION.ANIM_PRE_INTRO:
//			{
//				
//				//m_darknessAnimator.SetInteger("AnimParam",0);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_PRE_INTRO;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_INTRO_ONE:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",1);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_INTRO_ONE;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_INTRO_TWO:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",2);
//				//m_darknessAnimator.speed = (1f);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_INTRO_TWO;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_IDEAL_SPEED:
//			{
//				//m_darknessAnimator.speed = (1f);
//				//m_darknessAnimator.SetInteger("AnimParam",3);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_IDEAL_SPEED;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_MEDIUM_SPEED:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",4);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_MEDIUM_SPEED;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_HIGH_SPEED:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",5);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_HIGH_SPEED;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_STUMBLE:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",6);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_STUMBLE;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_HIT:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",7);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_HIT;
//			}
//				break;
//			case JUNKIE_ANIMATION.ANIM_LEAVING:
//			{
//				//m_darknessAnimator.SetInteger("AnimParam",3);
//				m_currentAnimation = JUNKIE_ANIMATION.ANIM_LEAVING;
//			}
//				break;
//				
//			}
//		}
//	}
	
	
	protected abstract void CheckAndInitStateChanges();
	protected abstract void FixedMachineUpdate();
	protected abstract void CreatedUpdate();
	protected abstract void InGameUpdate();
	protected abstract void ClosingInUpdate();
	protected abstract void GameLostUpdate();
	protected abstract void LeavingUpdate();
	protected abstract void UpdateDeath();
	protected abstract void UpdatePostDeath(); 
	protected abstract void IntroingUpdate();
	
	protected void UpdateStateMachine()
	{

		CheckAndInitStateChanges();

		switch(m_currentState)
		{
		case DARKNESS_STATES.STATE_CREATED:
		{
			CreatedUpdate();
		}
			break;
		
		case DARKNESS_STATES.STATE_INTROING:
		{
			IntroingUpdate();
		}
			break;
		case DARKNESS_STATES.STATE_INGAME:
		{
			InGameUpdate();
		}
			break;
		case DARKNESS_STATES.STATE_CLOSING_IN:
		{
			ClosingInUpdate();
		}
			break;

		case DARKNESS_STATES.STATE_GAME_LOST:
		{
			GameLostUpdate();
		}
			break;
		case DARKNESS_STATES.STATE_LEAVING:
		{
			LeavingUpdate();
		}
			break;

		case DARKNESS_STATES.STATE_DEAD:
		{
			UpdateDeath();
		}
			break;		
		case DARKNESS_STATES.STATE_DESTROYED:
		{
			UpdatePostDeath();
		}
			break;
		}
		if(m_enableFixedMachineUpdate)
		{
			FixedMachineUpdate();
		}

		
	}
	
}
