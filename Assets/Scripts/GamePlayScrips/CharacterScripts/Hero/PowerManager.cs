﻿using UnityEngine;
using System.Collections;

public class PowerManager : MonoBehaviour 
{
	public delegate bool AllPowerEnableCriteriaChecker();
	public Transform m_transformToInstillPowerTo;
	public Power[] m_powerSlots;
	AllPowerEnableCriteriaChecker m_criteriaChecker;
	public Power m_activePower = null;
	void Start()
	{
		m_criteriaChecker = null;
		for(int i = 0; i<m_powerSlots.Length; i++)
		{
			m_powerSlots[i].SetState(POWER_STATES.IS_DISABLED);
			//m_powerSlots[i].InitPower(m_transformToInstillPowerTo);
		}
	}

	private void DisableRest(Power except)
	{
		for(int i =0;i<m_powerSlots.Length; i++)
		{
			if(m_powerSlots[i] != except)
			{
				m_powerSlots[i].SetState(POWER_STATES.IS_DISABLED);
			}
		}
	}


	public void InitPowerManager(AllPowerEnableCriteriaChecker criteriaChecker)
	{
		m_criteriaChecker = criteriaChecker;
		for(int i = 0; i<m_powerSlots.Length; i++)
		{
			m_powerSlots[i].SetState(POWER_STATES.IS_DISABLED);
			m_powerSlots[i].InitPower(m_transformToInstillPowerTo);
		}
	}
	public Power GetActivePower()
	{
		if(m_activePower.m_isEngaged)
		{
			return m_activePower;
		}
		else
		{
			return null;
		}

	}
	public void UpdatePowerManager()
	{
		if(m_criteriaChecker != null)
		{
			if(m_activePower == null)
			{
				if(m_criteriaChecker())
				{
					for(int i =0;i<m_powerSlots.Length; i++)
					{

						m_powerSlots[i].SetState(POWER_STATES.IS_AVAILABLE);
					}
				}
				for(int i =0;i<m_powerSlots.Length; i++)
				{	
					if(m_powerSlots[i].m_powerState == POWER_STATES.IS_AVAILABLE && m_powerSlots[i].IsClicked())
					{
						m_activePower = m_powerSlots[i];
						m_powerSlots[i].SetState(POWER_STATES.IS_ENGAGED);
						DisableRest(m_powerSlots[i]);
					}
					m_powerSlots[i].UpdatePower();
				}
			}
			else
			{
				if(m_activePower.m_powerState == POWER_STATES.IS_DISABLED)
				{
					m_activePower = null;
				}
				else
				{
					m_activePower.UpdatePower();
				}
			}
			
		}
	}


}
