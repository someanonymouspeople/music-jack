﻿using UnityEngine;
using System.Collections;


public enum JUNKIE_STATES
{
	STATE_CREATED,
	STATE_ENTERING,
	STATE_INTROING,
	STATE_INGAME,
	STATE_HYPED,
	STATE_PHASED,
	STATE_HIT,
	STATE_STUMBLED,
	STATE_LEAVING,
	STATE_DYING,
	STATE_DEAD,
	STATE_DESTROYED
};

public enum JUNKIE_ANIMATION
{
	ANIM_PRE_INTRO,
	ANIM_INTRO_ONE,
	ANIM_INTRO_TWO,
	ANIM_IDEAL_SPEED,
	ANIM_MEDIUM_SPEED,
	ANIM_HIGH_SPEED,
	ANIM_STUMBLE,

	ANIM_HIT,
	ANIM_LEAVING
};



public abstract class JunkieStateMachine : MonoBehaviour 
{
	protected bool m_enableFixedMachineUpdate;
	protected JUNKIE_STATES m_prevState;
	public JUNKIE_STATES m_currentState;
	protected JUNKIE_STATES m_nextState;
	public JUNKIE_ANIMATION m_currentAnimation;
	public bool m_collisionEnabled;
	public Animator m_heroAnimator;

	protected void Start () 
	{
		m_enableFixedMachineUpdate= true;
		m_heroAnimator = this.gameObject.transform.GetComponentInChildren<Animator>();
		m_prevState = JUNKIE_STATES.STATE_CREATED;
		SetAnim(JUNKIE_ANIMATION.ANIM_INTRO_TWO);


	}
	void Awake()
	{
		m_collisionEnabled = false;
	}

	public void SetState(JUNKIE_STATES state)
	{
		m_nextState = state;
	}

	public JUNKIE_STATES GetState()
	{
		return m_currentState;
	}

	public void EnableCollision()
	{
		m_collisionEnabled = true;
	}
	public void DisableCollision()
	{
		m_collisionEnabled = false;
	}
	public void SetAnim(JUNKIE_ANIMATION anim)
	{
		if(anim != m_currentAnimation)
		{
			switch(anim)
			{
			case JUNKIE_ANIMATION.ANIM_PRE_INTRO:
			{

				m_heroAnimator.SetInteger("AnimParam",0);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_PRE_INTRO;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_INTRO_ONE:
			{
				m_heroAnimator.SetInteger("AnimParam",1);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_INTRO_ONE;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_INTRO_TWO:
			{
				m_heroAnimator.SetInteger("AnimParam",2);
				m_heroAnimator.speed = (1f);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_INTRO_TWO;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_IDEAL_SPEED:
			{
				m_heroAnimator.speed = (1f);
				m_heroAnimator.SetInteger("AnimParam",3);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_IDEAL_SPEED;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_MEDIUM_SPEED:
			{
				m_heroAnimator.SetInteger("AnimParam",4);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_MEDIUM_SPEED;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_HIGH_SPEED:
			{
				m_heroAnimator.SetInteger("AnimParam",5);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_HIGH_SPEED;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_STUMBLE:
			{
				m_heroAnimator.SetInteger("AnimParam",6);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_STUMBLE;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_HIT:
			{
				m_heroAnimator.SetInteger("AnimParam",7);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_HIT;
			}
				break;
			case JUNKIE_ANIMATION.ANIM_LEAVING:
			{
				m_heroAnimator.SetInteger("AnimParam",3);
				m_currentAnimation = JUNKIE_ANIMATION.ANIM_LEAVING;
			}
				break;
				
			}
		}
	}


	protected abstract void CheckAndInitStateChanges();
	protected abstract void FixedMachineUpdate();
	protected abstract void CreatedUpdate();
	protected abstract void EnteringUpdate();
	protected abstract void InGameUpdate();
	protected abstract void HitUpdate();
	protected abstract void StumbledUpdate();
	protected abstract void HypedUpdate();
	protected abstract void LeavingUpdate();
	protected abstract void UpdateDeath();
	protected abstract void UpdatePostDeath(); 
	protected abstract void IntroingUpdate();
	protected abstract void PhasedUpdate();
	protected abstract void DyingUpdate();

	protected void UpdateStateMachine()
	{
		if(m_enableFixedMachineUpdate)
		{
		FixedMachineUpdate();
		}
		CheckAndInitStateChanges();
		switch(m_currentState)
		{
		case JUNKIE_STATES.STATE_CREATED:
		{
			CreatedUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_ENTERING:
		{
			EnteringUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_INTROING:
		{
			IntroingUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_INGAME:
		{
			InGameUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_PHASED:
		{
			PhasedUpdate();
		}
			break;
			
		case JUNKIE_STATES.STATE_HYPED:
		{
			HypedUpdate();
		}
			break;
			
		case JUNKIE_STATES.STATE_HIT:
		{
			HitUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_STUMBLED:
		{
			StumbledUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_LEAVING:
		{
			LeavingUpdate();
		}
			break;

		case JUNKIE_STATES.STATE_DYING:
		{
			DyingUpdate();
		}
			break;
		case JUNKIE_STATES.STATE_DEAD:
		{
			UpdateDeath();
		}
			break;		
		case JUNKIE_STATES.STATE_DESTROYED:
		{
			UpdatePostDeath();
		}
			break;
		}

	}

}
