using UnityEngine;
using System.Collections;


public enum POWER_STATES
{
	IS_AVAILABLE,
	IS_DISABLED,
	IS_ENGAGED
}
public abstract class Power : ClickableButtonScript
{
	public POWER_STATES m_powerState;
	public float m_flashTimer;
	public float m_powerTime;
	protected float m_powerTimer;
	public int m_powerTimeIncrement;
	public int m_powerTimeCap;
	protected Transform m_transformToInstillPowerTo;
	protected abstract void PowerEngagedUpdate();
	protected abstract void PowerEngagedFunc();
	protected abstract void PowerDisEngagedFunc();
	protected abstract void PowerMadeAvailableFunc();

	public Color baseColor;

	protected IEnumerator Flash(float time)
	{
		while(m_flashTimer < time)
		{
			m_flashTimer += Time.deltaTime;
			if(m_flashTimer > time/2)
			{
				this.renderer.material.color = Color.Lerp(this.baseColor,Color.white,1-((m_flashTimer - (time/2))/(time/2)));
			}
			else
			{
				this.renderer.material.color = Color.Lerp(this.baseColor,Color.white,m_flashTimer/(time/2));
			}
			yield return null;
		}
		this.renderer.material.color = baseColor;
		yield break;
	}

	void Awake()
	{
		if(m_powerTimeCap == 0)
		{
			m_powerTimeCap = 15;
		}
	}
	public void InitPower(Transform transformToInstillPowerTo)
	{
		m_transformToInstillPowerTo = transformToInstillPowerTo;
		m_powerTimer = 0;
		if(m_powerTime == 0)
		{
			m_powerTime = 5;
		}
		SetState(POWER_STATES.IS_DISABLED);
	}
	public void EnablePower()
	{
		m_powerState = POWER_STATES.IS_AVAILABLE;
	}

	public void SetState(POWER_STATES state)
	{
		m_powerState = state;
		switch(state)
		{
			case POWER_STATES.IS_DISABLED:
			{
				PowerDisEngagedFunc();	
				
			}
			break;
			case POWER_STATES.IS_AVAILABLE:
			{
				PowerMadeAvailableFunc();	
				
			}
				break;
			case POWER_STATES.IS_ENGAGED:
			{
				PowerEngagedFunc();
				
			}
				break;
		}
	}
	
	public virtual void UpdatePower()
	{

		if(m_powerState == POWER_STATES.IS_ENGAGED)
		{
			if(m_powerTimer<m_powerTime)
			{
				m_powerTimer+= Time.deltaTime;
				PowerEngagedUpdate();
			}
			else
			{
				m_powerTimer = 0;
				SetState(POWER_STATES.IS_DISABLED);
				Debug.Log(""+this.gameObject.name+ " is disabled");
			}
		}

	}
}