using UnityEngine;
using System.Collections;




public class JunkieScript : JunkieStateMachine 
{
	
	public static bool AreApproximateBy(float value1,float value2 ,float epsilon)
	{
		float max = Mathf.Max(value1,value2);
		float min = Mathf.Min(value1,value2);
		if(Mathf.Abs(max - min) <= epsilon )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public float topValue;
	
	//Motion specific
	//speed increment and decrement vaiables
	public float m_speedIncrementPerHype = 0;
	public float m_speedStages;
	private float m_speedIncrement;
	public float m_speedDamping;
	//acceleration and deceleration specific
	float m_speedAfterAcceleration;
	float m_speedDiff;
	float m_speedUpTime;
	public bool m_isAccelerating = false;
	float m_speedAfterDecel;
	float m_decelSpeedDiff;
	bool m_isDecelerating = false;
	float m_decelTime;
	
	//speed and its respective clamp variables
	public float m_speed;
	public float m_startSpeed;
	public float m_minSpeed;
	public float m_maxSpeed;
	
	//Junkie motion effect managing variables
	//booleans marking motion states
	public bool m_gravity;
	public bool m_hyped;
	public bool m_floating;
	public bool m_flatPlaning;

	//variables governing diferent motion states
	//gravity state
	public float m_accelDueToGravity;
	//junkie moving state
	public Vector2 m_velocity;
	public Vector2 m_dirVec;
	public float m_jumpTriggerHeight;
	float m_minTrackedHeight;
	//common variables
	public float m_angle;
	public float m_lerpVal;
	public float m_slopeHeight;
	public float m_minJumpHeight;
	public float m_minDownJumpAngle;
	public float m_minJumpAngle;
	//Junkie sprite specific
	public float m_maxXTravel;
	float m_xToSendSpriteTo;
	
	//Junkie animation variables
	float m_introTimer =0;
	public float m_introTime =1.5f;
	float m_extroTimer = 0;	
	public float m_extroTime;
	Vector2 m_extroPoint;
	float m_phaseTimer =0;
	public float m_phaseTime;
	public float m_fadeFactor;
	int m_opacitySwitch = -1;
	
	//variables required to determine the motion of the junkie
	public Vector3 m_targetPos;
	public int m_maxScreenPercent;
	public int m_minScreenPercent;
	public bool m_forward;
	public float m_drag;


	void Awake()
	{

		if(m_maxScreenPercent == 0)
		{
			m_maxScreenPercent = 80;
		}
		if(m_minScreenPercent == 0)
		{
			m_minScreenPercent = 40;
		}

		//m_startSpeed = 30;
		//m_minSpeed = 30;
		//m_speed = m_startSpeed;


		m_targetPos = new Vector3(this.gameObject.transform.position.x + 1,-15f,this.gameObject.transform.position.z);
		this.gameObject.transform.GetChild(0).GetComponent<SpriteParticleSystem>().enabled = false;
	}
	
	void Start()
	{

		m_minTrackedHeight = -Camera.main.orthographicSize+GetSprite().renderer.bounds.size.y * 2;
		m_floating = true;
		m_flatPlaning = true;
		m_velocity = new Vector2(1,0);
		base.Start ();
		m_dirVec = new Vector3(1,0,0);
		//m_maxXTravel =  Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * (m_maxScreenPercent/100),0,0)).x - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width*(m_minScreenPercent/100),0,0)).x;
		float distBetweenInWorld = 0;
		float maxScreenInPixels = Screen.width*((float)m_maxScreenPercent/100.0f);
		float minScreenInPixels = Screen.width*((float)m_minScreenPercent/100.0f);
		Vector2 maxScreenPoint = Camera.main.ScreenToWorldPoint(new Vector2(maxScreenInPixels,0));
		Vector2 minScreenPoint = Camera.main.ScreenToWorldPoint(new Vector2(minScreenInPixels,0));
		if(!m_forward)
		{
			distBetweenInWorld = maxScreenPoint.x- this.gameObject.transform.position.x;
		}
		else
		{
			distBetweenInWorld = minScreenPoint.x- this.gameObject.transform.position.x;
		}
		m_speed = distBetweenInWorld/m_introTime;
		m_maxXTravel = Vector2.Distance(maxScreenPoint,minScreenPoint);
		if(m_drag == 0)
		{
			m_drag = m_maxSpeed/100;
		}
		//m_speedIncrementPerHype = (m_maxSpeed-m_minSpeed)/ m_speedStages;
		
	}

	public float GetMinSpeed()
	{
		return m_minSpeed;
	}

	public float GetMaxSpeed()
	{
		return m_maxSpeed;
	}
	
	public Vector3 GetSpritePos()
	{
		return transform.GetChild(0).transform.position;
	}
	
	public GameObject GetSprite()
	{
		return transform.GetChild(0).gameObject;
	}
	

	protected override void CheckAndInitStateChanges()
	{
		if(m_nextState != m_currentState)
		{
			switch(m_nextState)
			{
			case JUNKIE_STATES.STATE_CREATED:
			{
				
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_ENTERING:
			{
				
				//SetAnim(JUNKIE_ANIMATION.ANIM_INTRO_ONE);
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
				
			case JUNKIE_STATES.STATE_INTROING:
			{
				SetAnim(JUNKIE_ANIMATION.ANIM_INTRO_ONE);
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_INGAME:
			{
				this.GetSprite().collider2D.enabled = true;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
				
			case JUNKIE_STATES.STATE_HYPED:
			{
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_PHASED:
			{
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
				
			case JUNKIE_STATES.STATE_HIT:
			{
				this.GetSprite().collider2D.enabled = false;
				SetAnim(JUNKIE_ANIMATION.ANIM_HIT);
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_STUMBLED:
			{
				this.GetSprite().collider2D.enabled = false;
				SetAnim(JUNKIE_ANIMATION.ANIM_STUMBLE);
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_LEAVING:
			{
				SetAnim(JUNKIE_ANIMATION.ANIM_IDEAL_SPEED);
				Vector2 extroPoint = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height/2));
				extroPoint.x += 1.0f;
				m_extroPoint = extroPoint;
				float distanceToExtro = extroPoint.x - gameObject.transform.position.x;
				m_speed = distanceToExtro/m_extroTime;
				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_DEAD:
			{
				m_prevState = m_currentState;
				m_currentState = m_nextState;
				m_nextState = JUNKIE_STATES.STATE_DESTROYED;
			}
				break;
			case JUNKIE_STATES.STATE_DYING:
			{
				m_prevState =m_currentState;
				m_currentState = m_nextState;
			}
				break;
			case JUNKIE_STATES.STATE_DESTROYED:
			{
				Destroy(this.gameObject);

				m_prevState = m_currentState;
				m_currentState = m_nextState;
			}
				break;			
			}
		}
	}
	
	protected override void CreatedUpdate ()
	{
		SetState(JUNKIE_STATES.STATE_ENTERING);
		
	}
	protected override void EnteringUpdate ()
	{
		Vector3 currentPos = this.gameObject.transform.position;

		if(m_forward)
		{
			if(this.gameObject.transform.position.x < Camera.main.ScreenToWorldPoint(new Vector3(Screen.width*((float)m_minScreenPercent/100),0,0)).x)
			{
				currentPos.x += m_speed * Time.deltaTime;
				gameObject.transform.position = currentPos;
			}
			else
			{
				SetState(JUNKIE_STATES.STATE_INGAME);

				AccelerateJunkieTo(m_startSpeed,1.0f);
				//m_xToSendSpriteTo = this.gameObject.transform.GetChild(0).transform.localPosition.x;
				
			}
		}
		else
		{
			if(this.gameObject.transform.position.x < Camera.main.ScreenToWorldPoint(new Vector3(Screen.width*((float)m_maxScreenPercent/100),0,0)).x)
			{
				currentPos.x += m_speed * Time.deltaTime;
				gameObject.transform.position = currentPos;
			}
			else
			{
				SetState(JUNKIE_STATES.STATE_INGAME);
				
				AccelerateJunkieTo(m_startSpeed,1.0f);
				//m_xToSendSpriteTo = this.gameObject.transform.GetChild(0).transform.localPosition.x;
				
			}
		}

		DarknessScript dark = GameplayScript.GetInstance().GetDarknessScript();

		if(dark.GetState() == DARKNESS_STATES.STATE_CREATED)
		{
			m_introTimer+= Time.deltaTime;
			if(m_introTimer > m_introTime - dark.m_introTime)
			{
				dark.SetState(DARKNESS_STATES.STATE_INTROING);
			}
		}

	}
	protected override void IntroingUpdate ()
	{
		if(m_introTimer < m_introTime)
		{
			m_introTimer += Time.deltaTime;
		}
		else
		{
			m_speed = m_startSpeed;
			SetState(JUNKIE_STATES.STATE_INGAME);
		}
	}

	public void AccelerateJunkieBy(float speed,float time)
	{
		m_speedUpTime = time;
		if(!m_isAccelerating)
		{
			if(m_isDecelerating)
			{
				m_speedAfterDecel += speed;
				return;
				
			}
			m_isAccelerating = true;
			
			if(m_speed + speed < m_maxSpeed)
			{
				m_speedAfterAcceleration = m_speed+speed;
				m_speedIncrement = m_speed -m_startSpeed;
				m_speedDiff = m_speedAfterAcceleration - m_speed;
			}
			else
			{
				m_speedAfterAcceleration = m_maxSpeed;
				m_speedDiff = m_maxSpeed - m_speed;
			}
			m_isAccelerating = true;
			
		}
		else
		{
			if(m_speedAfterAcceleration + speed < m_maxSpeed)
			{
				m_speedAfterAcceleration = m_speedAfterAcceleration+speed;
			}
			else
			{
				m_speedAfterAcceleration = m_maxSpeed;
			}
		}
		
	}
	
	
	public void AccelerateJunkieTo(float speed,float time)
	{
		if(m_speedAfterAcceleration == speed)
		{
			return;
		}
		m_speedUpTime = time;
		
		m_isAccelerating = true;
		
		if(speed < m_maxSpeed)
		{
			m_speedAfterAcceleration = speed;
			m_speedDiff = speed - m_speed;
		}
		else
		{
			m_speedAfterAcceleration = m_maxSpeed;
			m_speedDiff = m_maxSpeed - m_speed;
		}
		m_isAccelerating = true;
		
	}
	

	
	public void DecelJunkieBy(float speed,float time)
	{
		m_isAccelerating = false;
		
		m_speedUpTime = 0;
		m_decelTime = time;
		if(!m_isDecelerating)
		{
			m_isDecelerating = true;
			m_speedAfterDecel = m_speed - speed;
			if(m_speedAfterDecel < m_minSpeed)
			{
				m_speedAfterDecel = m_minSpeed;
				
			}
			m_decelSpeedDiff =  m_speed - m_speedAfterDecel;
		}
		else
		{
			m_speedAfterDecel = m_speedAfterDecel - speed;
			if(m_speedAfterDecel < m_minSpeed)
			{
				m_speedAfterDecel = m_minSpeed;
			}
			m_decelSpeedDiff =  m_speed - m_speedAfterDecel;
		}
	}
	
	public void DecelJunkieTo(float speed,float time)
	{
		m_isAccelerating = false;
		
		m_speedUpTime = 0;
		if(m_speed == m_speedAfterDecel)
		{
			return;
		}
		
		m_decelTime = time;
		
		m_isDecelerating = true;
		m_speedAfterDecel = speed;
		if(m_speedAfterDecel <m_minSpeed)
		{
			m_speedAfterDecel = m_minSpeed;
			
		}
		m_decelSpeedDiff =  m_speed - m_speedAfterDecel;
		
	}
	
	public float GetRelativeHypeLevel(int scoreMultiplier)
	{
		float relativeMultiplier  = ((float)scoreMultiplier/32);
		return relativeMultiplier;
	}
	
	public void UnHype()
	{
		DecelJunkieBy((m_speed - m_minSpeed)/4,0.5f);
//		m_speedIncrement -= m_speedIncrement/2;
//		if(m_speedIncrement<0)
//		{
//			m_speedIncrement = 0;
//		}
//		m_xToSendSpriteTo -= m_maxXTravel/2;
//		if(m_xToSendSpriteTo<0.1f)
//		{
//			m_hyped = false;
//			m_xToSendSpriteTo = 0;
//			//GameplayScript.GetInstance().SetMultiplier(1);
//
//			DecelJunkieTo(1.0f,0.5f);
//		}
//		else if(m_xToSendSpriteTo >m_maxXTravel)
//		{
//			m_xToSendSpriteTo = m_maxXTravel;
//		}
		
		
	}
	
	public void SetSpeed(float speed)
	{
		if(speed < m_maxSpeed)
		{
			m_speed = speed;
			return;
		}
		m_speed = m_maxSpeed;
	}
	
	public void SetSpeedRelativeTo(float relativeValue, float maxValue = 60)
	{
		m_speed = m_minSpeed + relativeValue * maxValue;
		if(m_speed > m_maxSpeed)
		{
			m_speed = m_maxSpeed;
		}
		else if (m_speed < m_minSpeed)
		{
			m_speed = m_minSpeed;
		}
	}
	
	public void IncrementSpeed(float increment)
	{
		if(m_speed < m_maxSpeed)
		{
			m_speed += increment;
		}
	}
	
	public void DecrementSpeed(float increment)
	{
		if(m_speed < m_maxSpeed)
		{
			m_speed -= increment;
		}
	}
	
	public void DecrementSpeedByFactor(float factor)
	{
		m_speed -= m_speed* factor;
		if(m_speed < m_startSpeed)
		{
			m_speed = m_minSpeed;
		}
	}
	
	public void IncrementSpeedByFactor(float factor)
	{
		m_speed += m_speed* factor;
		if(m_speed > m_maxSpeed)
		{
			m_speed = m_maxSpeed;
		}
	}
	
	
	public float GetSpeed()
	{
		return m_speed;
	}
	
	public float GetRelativeSpeed()
	{
		float valueToReturn = (m_speed - m_minSpeed)/(m_maxSpeed - m_minSpeed);
		valueToReturn = Mathf.Clamp(valueToReturn,0,1);
		return valueToReturn;
	}
	
	
	
	public void onTriggerEnter2D(Collider2D collider)
	{
		if(m_currentState == JUNKIE_STATES.STATE_INGAME )
		{
			if(collider.tag == "Collectables")
			{
				JunkieCollisionManager.GetInstance().AddCollisionToCheck(collider,1);
			}
			else if(collider.tag == "Obstacles" && m_collisionEnabled )
			{
				JunkieCollisionManager.GetInstance().AddCollisionToCheck(collider,1);
			}
		}
	}


	private void ReposJunkieSprite()
	{
		FollowTransformScript tempCamFollowScript  = Camera.main.gameObject.GetComponent<FollowTransformScript>();
		FollowTransformScript tempBGFollowScript  = GameObject.Find("ParallaxBackGroundManager").transform.GetChild(0).gameObject.GetComponent<FollowTransformScript>();

		float distanceBetweenToSet = GetRelativeSpeed()* m_maxXTravel;
		if(distanceBetweenToSet <0.1f)
		{
			distanceBetweenToSet = 0;
		}
		if(!m_forward)
		{
			distanceBetweenToSet *= -1;
		}
		Vector3 camZzz = tempCamFollowScript.GetStartDistanceBetween() + new Vector3(distanceBetweenToSet,0,0);
		Vector3 BGZzz = tempBGFollowScript.GetStartDistanceBetween() + new Vector3(distanceBetweenToSet,0,0);
	
		tempCamFollowScript.SetDistanceBetween(camZzz);
		tempBGFollowScript.SetDistanceBetween(BGZzz);


	}

	protected override void InGameUpdate ()
	{
		if(GameplayScript.GetInstance().m_coreGameplayElementsInitialized)
		{
			GovernSpeedAnim();
			ReposJunkieSprite();
		}
	}
	protected override void HitUpdate ()
	{
		if(m_phaseTimer < m_phaseTime)
		{
			this.GetComponentInChildren<Collider2D>().isTrigger = false;
			m_phaseTimer += Time.deltaTime;
			Color tempColor = this.GetComponentInChildren<SpriteRenderer>().color;
			if(tempColor.a <=0 || tempColor.a >= 1)
			{
				m_opacitySwitch *= -1;
			}
			tempColor.a += (Time.deltaTime*m_fadeFactor) *m_opacitySwitch;
			this.GetComponentInChildren<SpriteRenderer>().color = tempColor;
		}
		else
		{
			m_phaseTimer = 0;
			this.GetComponentInChildren<Collider2D>().isTrigger = true;
			Color tempFinalColor = this.GetComponentInChildren<SpriteRenderer>().color; 
			tempFinalColor.a = 1;
			this.GetComponentInChildren<SpriteRenderer>().color = tempFinalColor; 
			SetState(JUNKIE_STATES.STATE_INGAME);
		}
		
	}
	
	protected override void StumbledUpdate()
	{
		if(m_phaseTimer < m_phaseTime)
		{
			this.GetComponentInChildren<Collider2D>().isTrigger = false;
			m_phaseTimer += Time.deltaTime;
			Color tempColor = this.GetComponentInChildren<SpriteRenderer>().color;
			if(tempColor.a <=0 || tempColor.a >= 1)
			{
				m_opacitySwitch *= -1;
			}
			tempColor.a += (Time.deltaTime*m_fadeFactor) *m_opacitySwitch;
			this.GetComponentInChildren<SpriteRenderer>().color = tempColor;
		}
		else
		{
			m_phaseTimer = 0;
			this.GetComponentInChildren<Collider2D>().isTrigger = true;
			Color tempFinalColor = this.GetComponentInChildren<SpriteRenderer>().color; 
			tempFinalColor.a = 1;
			this.GetComponentInChildren<SpriteRenderer>().color = tempFinalColor;
			
			SetState(JUNKIE_STATES.STATE_INGAME);
		}
	}
	protected override void HypedUpdate ()
	{
		
	}
	protected override void LeavingUpdate ()
	{
		if(m_extroTimer < m_extroTime)
		{
			m_extroTimer += Time.deltaTime;
			Vector2 posToSet = gameObject.transform.position;
			posToSet.y = Mathf.Lerp(posToSet.y,0,0.04f);
			posToSet.x += m_speed * Time.deltaTime;
			this.gameObject.transform.position = posToSet;
		}
		else
		{
			this.SetState(JUNKIE_STATES.STATE_DEAD);
		}
	}

	protected override void DyingUpdate ()
	{
		if(m_extroTimer < m_extroTime)
		{
			m_extroTime += Time.deltaTime;
		}
		else
		{
			this.SetState(JUNKIE_STATES.STATE_DEAD);
		}
	}
	
	protected override void UpdateDeath ()
	{
		
	}
	
	protected override void UpdatePostDeath ()
	{
		
	}

	protected override void PhasedUpdate ()
	{
		if(m_phaseTimer < m_phaseTime)
		{
			this.GetComponentInChildren<Collider2D>().enabled = false;
			this.GetComponentInChildren<Collider2D>().isTrigger = false;
			m_phaseTimer += Time.deltaTime;
			Color tempColor = this.GetComponentInChildren<SpriteRenderer>().color;
			if(tempColor.a <=0 || tempColor.a >= 1)
			{
				m_opacitySwitch *= -1;
			}
			tempColor.a += (Time.deltaTime*m_fadeFactor) *m_opacitySwitch;
			this.GetComponentInChildren<SpriteRenderer>().color = tempColor;
		}
		else
		{
			m_phaseTimer = 0;
			this.GetComponentInChildren<Collider2D>().enabled = true;
			this.GetComponentInChildren<Collider2D>().isTrigger = true;
			Color tempFinalColor = this.GetComponentInChildren<SpriteRenderer>().color; 
			tempFinalColor.a = 1;
			this.GetComponentInChildren<SpriteRenderer>().color = tempFinalColor;
			
			SetState(JUNKIE_STATES.STATE_INGAME);
		}
	}
	
	private void UpdateSpeedVariable()
	{
		if(!m_floating)
		{
			if(m_dirVec.y >0)
			{
				if(m_speed > m_minSpeed)
				{
					m_speed -= m_dirVec.y*m_speedDamping*Time.deltaTime; 
				}
				else
				{
					m_speed = m_minSpeed;
				}
			}
			
			else if(m_dirVec.y <0)
			{
				if(m_speed < m_maxSpeed)
				{
					m_speed -= m_dirVec.y*(m_speedDamping *2)*Time.deltaTime; 
				}
				else
				{
					m_speed = m_maxSpeed;
				}
				
			}
			return;
		}

		
	}
	
	private void ReOrientJunkie()
	{
		if(m_floating)
		{
		float angle = Mathf.Atan2(m_velocity.y,m_velocity.x)* Mathf.Rad2Deg;
		GetSprite().transform.rotation = Quaternion.AngleAxis(angle,GetSprite().transform.forward);
		}
		else
		{
			float angle = Mathf.Atan2(m_dirVec.y,m_dirVec.x)* Mathf.Rad2Deg;
			GetSprite().transform.rotation = Quaternion.AngleAxis(angle,GetSprite().transform.forward);
		}
	}
	
	public void ShouldFloatCheck()
	{
		if(m_dirVec.x == 1 && m_velocity.y == 0)
		{
			m_floating = false;
		}
		else
		{
			m_floating = true;
		}
	}
	
	public void RockBottomSequence()
	{
		Vector3 posToSet = new Vector3(this.transform.position.x,m_minTrackedHeight,this.transform.position.z);
		this.transform.position = posToSet;
		m_velocity = new Vector2(1,0);
		m_dirVec= new Vector2(1,0);
		m_floating = true;
		DecelJunkieTo(m_minSpeed,0.5f);
		
	}

	private void GovernSpeedAnim()
	{
		if((m_speed- m_minSpeed) > (m_maxSpeed - m_minSpeed) * 0.7f)
		{
			SetAnim(JUNKIE_ANIMATION.ANIM_HIGH_SPEED);
		}
		else if((m_speed - m_minSpeed) > (m_maxSpeed - m_minSpeed) *0.4f)
		{
			SetAnim(JUNKIE_ANIMATION.ANIM_MEDIUM_SPEED);
		}
		else
		{
			SetAnim(JUNKIE_ANIMATION.ANIM_IDEAL_SPEED);
		}
	}

	protected override void FixedMachineUpdate ()
	{
		if(m_currentState == JUNKIE_STATES.STATE_INGAME || m_currentState == JUNKIE_STATES.STATE_HYPED || m_currentState == JUNKIE_STATES.STATE_HIT || m_currentState == JUNKIE_STATES.STATE_STUMBLED)
		{
			//assign the target as the lowest point on screen to make sure he falls if there is no target
			if(m_targetPos == Vector3.zero)
			{
				m_targetPos.y = -10;
				m_targetPos.x = this.gameObject.transform.position.x +1;
			}

			m_speed -= m_drag*Time.deltaTime * GetRelativeSpeed();
			//Acceleration logic
			if(m_isAccelerating)
			{
				if(m_speed < m_maxSpeed && m_speed<m_speedAfterAcceleration)
				{
					m_speed += (m_speedDiff/m_speedUpTime)* Time.deltaTime;
				}
				else
				{
					m_isAccelerating = false;
				}
			}
			else if(m_isDecelerating)
			{
				if(m_speed >m_minSpeed && m_speed> m_speedAfterDecel)
				{
					m_speed -= (m_decelSpeedDiff/m_decelTime)*Time.deltaTime;
				}
				else
				{
					m_isDecelerating = false;
				}
			}

			//Junkie path folow logic
			Vector2 junkiePos = this.gameObject.transform.position;
			Vector2 posToMoveJunkie = junkiePos;


			//if flatplaning check when to stop
			if(m_flatPlaning)
			{
				if(AreApproximateBy(posToMoveJunkie.y,m_targetPos.y,GetSprite().renderer.bounds.extents.y))
				{
					m_flatPlaning = false;
				}
				else
				{
					posToMoveJunkie += new Vector2(1,0) * m_speed * Time.deltaTime;
				}
			}
			else

			{
			//if floating check when to stop
			if(m_floating)
			{
				//return to avoid calculation anomalies
				if(m_targetPos.y > posToMoveJunkie.y - GetSprite().renderer.bounds.extents.y && m_velocity.y <=0)
				{
					m_slopeHeight = 0;
					m_velocity.y = 0;
					m_floating = false;
					posToMoveJunkie.x += m_dirVec.x* m_speed*Time.deltaTime;
					posToMoveJunkie.y = m_targetPos.y +GetSprite().renderer.bounds.extents.y;
					
					this.gameObject.transform.position = new Vector3(posToMoveJunkie.x,posToMoveJunkie.y,this.gameObject.transform.position.z) ;
					return;
				}
			}
			
			if(m_floating)
			{
				//if floating apply gravity to velocity
				if(m_gravity)
				{
					m_velocity.y -= m_accelDueToGravity * Time.deltaTime;
					posToMoveJunkie.y += m_speed * m_velocity.y * Time.deltaTime;
				}
				posToMoveJunkie.x += m_dirVec.x* m_speed*Time.deltaTime;
				
			}
			else
			{
				//if not floating, follow path
				posToMoveJunkie += m_dirVec * m_speed*Time.deltaTime;
				if(m_dirVec.y == 0)
				{
					
					//reduce velocity slowly to avoid jerky reorientation
					if(!AreApproximateBy(m_velocity.y,0,0.01f))
					{
						m_velocity.y += ((m_velocity.y)*-1) * (m_speedDamping*2)* Time.deltaTime;
					}
					else
					{
						m_velocity.y = 0;
					}

				}
				//align to path as accurately as possible
				if(posToMoveJunkie.y  != m_targetPos.y +GetSprite().renderer.bounds.extents.y)
				{
					topValue = m_targetPos.y +GetSprite().renderer.bounds.extents.y;
					posToMoveJunkie.y = topValue;	
				}
			}
			}
			//Pre speed and velocity calc function calls
			//Speed and velocity calc code block
			UpdateSpeedVariable();

			//post speed and velocity calc fuction calls
			ReOrientJunkie();

			this.gameObject.transform.position = new Vector3(posToMoveJunkie.x,posToMoveJunkie.y,this.gameObject.transform.position.z);
		}
	}

	public void NoFurtherTargetSequence()
	{
		if(m_dirVec.y == 0)
		{
			m_flatPlaning = true;
		}
		else
		{
			m_flatPlaning = false;
			m_floating = true;
		}

		m_targetPos = Vector2.zero;
	}

	//Function that is used to tell the junkie where to head to next based on his current movement state
	public void DirectJunkie(Vector3 pos,Vector3 dirVec)
	{
		if(dirVec.x ==0)
		{
			Debug.Log("apparently junkie is not supposed to move :/");
		}
		//ignore higher targets if floating
		if(m_floating)
		{
			if(pos.y >this.gameObject.transform.position.y|| pos.x - m_speed * Time.deltaTime > this.gameObject.transform.position.x)
			{
				return;
			}

		}
		//if valid target :-

		// if the character is following a drawn path
		if(!m_floating && !m_flatPlaning)
		{
			//Set direction
			m_dirVec = dirVec;

			//calculate currentSlopeHeight
			if(m_dirVec.y > m_velocity.y)
			{

				m_slopeHeight += pos.y-m_targetPos.y;
			}
			else
			{

				m_slopeHeight = 0;
			}

			//Make character float if necessary
			if(m_dirVec.y < m_minDownJumpAngle)
			{
				if(m_speed < m_startSpeed)
				{
					m_speed = m_startSpeed;
				}
				m_floating = true;
				
				m_dirVec.x = 1;
			}
			else if(m_slopeHeight > m_minJumpHeight  && m_dirVec.y > m_minJumpAngle)						
			{
				m_floating = true;
					
			}

			// build velocity according to slope
			if(m_dirVec.y >0)
			{
				m_velocity.y = m_dirVec.y;
			}

			//hard set velocity to move forward
			m_velocity.x = 1;
		}
		else if(m_flatPlaning)
		{
			if(pos.y < transform.position.y)
			{
				m_flatPlaning = false;
				m_floating = true;
			}
		}

		//set target position
		m_targetPos = pos;
	}
	
	public Vector2 GetTargetPos()
	{
		return m_targetPos;
	}
	
	public void UpdateJunkie () 
	{
		UpdateStateMachine();
	}

	public void DestroyJunkie()
	{
		this.m_enableFixedMachineUpdate = false;
		Destroy(this.gameObject);
	}
}