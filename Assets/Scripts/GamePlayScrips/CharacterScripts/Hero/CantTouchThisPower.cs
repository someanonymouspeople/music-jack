﻿using UnityEngine;
using System.Collections;

public class CantTouchThisPower : Power {

	protected override void PowerDisEngagedFunc ()
	{

		GameplayScript.GetInstance().ResetPowerUpCountHolder();
		if(GameplayScript.GetInstance() != null)
		{
			GameplayScript.GetInstance().GetHeroScript().m_collisionEnabled = true;
		}
		this.gameObject.renderer.material.color = Color.grey;
		JunkieCollisionManager.GetInstance().m_linkedParticleSystem.enableEmission = true;
		GameplayScript.GetInstance().GetHero().transform.FindChild("CantTouchThisParticles").GetComponent<ParticleSystem>().enableEmission = false;
	}
	
	protected override void PowerEngagedFunc ()
	{
		StartCoroutine(Flash(0.5f));
		if(GameplayScript.GetInstance() != null)
		{
			GameplayScript.GetInstance().IncrementPowerTwoUsed(1);
			GameplayScript.GetInstance().GetHeroScript().m_collisionEnabled = false;
		}
		JunkieCollisionManager.GetInstance().m_linkedParticleSystem.enableEmission = false;
		GameplayScript.GetInstance().GetHero().transform.FindChild("CantTouchThisParticles").GetComponent<ParticleSystem>().enableEmission = true;
		this.gameObject.renderer.material.color = baseColor;
	}
	
	protected override void PowerMadeAvailableFunc ()
	{
		this.gameObject.renderer.material.color = baseColor;
	}

	protected override void PowerEngagedUpdate ()
	{
		//Debug.Log("CantTouchThis enabled");
	}

	public override void UpdatePower()
	{
		Vector2 localScale = this.gameObject.transform.localScale;
		if(m_powerState == POWER_STATES.IS_ENGAGED)
		{
			localScale = Vector2.Lerp(localScale,Vector2.one*1.5f,0.08f);
			if(m_powerTimer<m_powerTime)
			{
				m_powerTimer+= Time.deltaTime;
				PowerEngagedUpdate();
			}
			else
			{
				m_powerTimer = 0;
				m_powerTime += m_powerTimeIncrement;
				m_powerTime = Mathf.Clamp(m_powerTime,0,m_powerTimeCap);
				SetState(POWER_STATES.IS_DISABLED);
				Debug.Log(""+this.gameObject.name+ " is disabled");
			}
		}
		else
		{
			localScale = Vector2.Lerp(localScale,Vector2.one,0.08f);
		}
		this.gameObject.transform.localScale = localScale;
	}
}
