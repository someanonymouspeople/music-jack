﻿using UnityEngine;
using System.Collections;

public class ItsAllMinePower : Power {


	public float m_dragDist;
	protected override void PowerDisEngagedFunc ()
	{

		GameplayScript.GetInstance().ResetPowerUpCountHolder();
		this.gameObject.renderer.material.color = Color.grey;
		JunkieCollisionManager.GetInstance().m_linkedParticleSystem.enableEmission = true;
		GameplayScript.GetInstance().GetHero().transform.FindChild("ItsAllMineParticles").GetComponent<ParticleSystem>().enableEmission = false;
	}

	protected override void PowerEngagedFunc ()
	{
		StartCoroutine(Flash(0.5f));
		GameplayScript.GetInstance().IncrementPowerOneUsed(1);
		this.gameObject.renderer.material.color = baseColor;
		JunkieCollisionManager.GetInstance().m_linkedParticleSystem.enableEmission = false;
		GameplayScript.GetInstance().GetHero().transform.FindChild("ItsAllMineParticles").GetComponent<ParticleSystem>().enableEmission = true;
	}

	protected override void PowerMadeAvailableFunc ()
	{
		this.gameObject.renderer.material.color = baseColor;
	}


	protected override void PowerEngagedUpdate ()
	{
		CollectableManager manager = GameplayScript.GetInstance().GetCollectableManager();
		foreach(Collectable co in manager.m_spawnedObjectList)
		{
			if(co.gameObject != null)
			{
				if(co.gameObject.transform.position.x <m_transformToInstillPowerTo.gameObject.transform.position.x + m_dragDist && co.gameObject.transform.position.x > m_transformToInstillPowerTo.gameObject.transform.position.x)
				{
					Vector2 heroX = m_transformToInstillPowerTo.transform.position;
					heroX.y = 0;
					Vector2 coX = co.transform.position;
					coX.y = 0;

					float distBetween = Vector2.Distance(heroX,coX);
					distBetween = distBetween/m_dragDist;
					Vector2 pos = co.gameObject.transform.position;

					pos.y = Mathf.Lerp(co.m_spawnY,m_transformToInstillPowerTo.position.y,1-distBetween);
					co.gameObject.transform.position = pos;
				}
			}
		}

	}

	public override void UpdatePower()
	{
		Vector2 localScale = this.gameObject.transform.localScale;
		if(m_powerState == POWER_STATES.IS_ENGAGED)
		{
			localScale = Vector2.Lerp(localScale,Vector2.one*1.5f,0.08f);
			if(m_powerTimer<m_powerTime)
			{
				m_powerTimer+= Time.deltaTime;
				PowerEngagedUpdate();
			}
			else
			{
				m_powerTimer = 0;
				m_powerTime += m_powerTimeIncrement;
				m_powerTime = Mathf.Clamp(m_powerTime,0,m_powerTimeCap);
				SetState(POWER_STATES.IS_DISABLED);
				Debug.Log(""+this.gameObject.name+ " is disabled");
			}
		}
		else
		{
			localScale = Vector2.Lerp(localScale,Vector2.one,0.08f);
		}
		this.gameObject.transform.localScale = localScale;
	}



}
