﻿using UnityEngine;
using System.Collections;

public class HeroSpriteScript : MonoBehaviour {

	JunkieScript m_parentScript;
	void Start () 
	{
		m_parentScript = this.gameObject.transform.parent.GetComponent<JunkieScript>();
	}

	public void OnTriggerEnter2D(Collider2D collider)
	{
		m_parentScript.onTriggerEnter2D(collider);
	}
	
}
