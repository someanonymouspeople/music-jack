using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum COLLISION_TYPES
{
	JUNKIE_WITH_OBSTACLE,
	JUNKIE_WITH_COLLECTABLE,
};

public class CollisionListElement
{
	public CollisionListElement(Collider2D collidee,int referenceId)
	{
		m_referenceId = referenceId;
		m_collidee = collidee.gameObject.GetComponent<Collider2D>();
	}

	public Collider2D m_collidee;
	public int m_referenceId;
	public COLLISION_TYPES m_collisionType;
	
};


public class JunkieCollisionManager : MonoBehaviour 
{
	public static JunkieCollisionManager m_instance;
	List<CollisionListElement> m_collisionList;
	public GameObject m_heroJunkieSprite;
	public JunkieScript m_heroJunkieScript;
	public Collider2D m_heroCollider;
	public ParticleSystem m_linkedParticleSystem;
	float m_particleTimer;
	float m_particleTime;
	
	void Awake()
	{
		m_particleTime = 0.5f;
		//m_particleTimer = 0;
		m_linkedParticleSystem=  this.gameObject.GetComponentInChildren<ParticleSystem>();
		m_heroJunkieSprite = this.gameObject.transform.FindChild("HeroSprite").gameObject;
		m_heroJunkieScript = m_heroJunkieSprite.gameObject.transform.parent.GetComponent<JunkieScript>();
		m_heroCollider = m_heroJunkieSprite.gameObject.GetComponent<Collider2D>();
		m_collisionList = new List<CollisionListElement>();
		m_instance = this;
	}
	
	public static JunkieCollisionManager GetInstance()
	{
		return m_instance;
	}
	
	public void AddCollisionToCheck(Collider2D collidee,int collisionId)
	{
		CollisionListElement tempElement = new CollisionListElement(collidee,collisionId);
		if(collidee.gameObject.tag == "Collectables")
		{
			tempElement.m_collisionType = COLLISION_TYPES.JUNKIE_WITH_COLLECTABLE;
		}
		else if(collidee.gameObject.tag == "Obstacles")
		{
			tempElement.m_collisionType = COLLISION_TYPES.JUNKIE_WITH_OBSTACLE;
		}
		else 
		{
			return;
		}
		if(m_collisionList.Count >0)
		{
		for(int i =0; i<m_collisionList.Count;i++)
		{
			if(m_collisionList[i].m_collidee != tempElement.m_collidee)
			{
				m_collisionList.Add(tempElement);
			}
		}
		}
		else
		{
			m_collisionList.Add(tempElement);
		}

	}

//		public void AddCollisionToCheck(Collision collidee,int collisionId)
//		{
//			CollisionListElement tempElement = new CollisionListElement(collidee.gameObject.collider2D,collisionId);
//			if(collidee.gameObject.tag == "Collectables")
//			{
//				tempElement.m_collisionType = COLLISION_TYPES.JUNKIE_WITH_COLLECTABLE;
//			}
//			else if(collidee.gameObject.tag == "Obstacles")
//			{
//				tempElement.m_collisionType = COLLISION_TYPES.JUNKIE_WITH_OBSTACLE;
//			}
//			m_collisionList.Add(tempElement);
//		}

	
	public void RemoveElementFromList(CollisionListElement elementToRemove)
	{
		for(int it =0; it<m_collisionList.Count;it++)
		{
			if(m_collisionList[it] == elementToRemove)
			{
				m_collisionList.RemoveAt(it);
				return;
			}
		}
	}
	
	private void CollisionParticleLogic()
	{
		if(m_particleTimer< m_particleTime)
		{
			m_particleTimer+= Time.deltaTime;
			float emitedParticles = m_linkedParticleSystem.emissionRate;
			emitedParticles-= 1;
			if(emitedParticles>0)
			{
				m_linkedParticleSystem.emissionRate= emitedParticles;
			}
			else
			{
				m_linkedParticleSystem.emissionRate= 0;
			}
		}
		else
		{
			m_particleTimer= 0;



		}	
	}
	public void OnCollisionWithCollectable(CollisionListElement element)
	{
		if(element.m_collidee != null)
		{
			element.m_collidee.GetComponent<BaseObject>().SetState(BASE_OBJECT_STATES.STATE_TRIGGERED);
			m_linkedParticleSystem.emissionRate = 50;
			m_particleTimer = 0;
			m_heroJunkieScript.IncrementSpeed(m_heroJunkieScript.GetMaxSpeed()*0.01f);
		
		}
	}

	public void OnCollisionWithObstacle(CollisionListElement element)
	{

			Vector2 distVector = new Vector3(0,m_heroCollider.transform.position.y - element.m_collidee.transform.position.y,0);
			float length = distVector.magnitude;

			if(length > m_heroCollider.renderer.bounds.size.y)
			{

				GameplayScript.GetInstance().DecrementMultiplierByFactor(4);
				//GameplayScript.GetInstance().ReduceScore(50);
				m_heroJunkieScript.UnHype();
				m_heroJunkieScript.SetState(JUNKIE_STATES.STATE_STUMBLED);

			}
			else
			{

				GameplayScript.GetInstance().DecrementMultiplierByFactor(4);
				//GameplayScript.GetInstance().ReduceScore(100);
				m_heroJunkieScript.UnHype();
				m_heroJunkieScript.SetState(JUNKIE_STATES.STATE_HIT);
			}
		MJCamScript.GetInstance().StartShake(2,0.25f);

	}



	public void UpdateCollisionManager()
	{
		CollisionParticleLogic();
		for(int it = 0; it<m_collisionList.Count;it++)
		{
			switch(m_collisionList[it].m_collisionType)
			{

				case COLLISION_TYPES.JUNKIE_WITH_COLLECTABLE:
				{
				OnCollisionWithCollectable(m_collisionList[it]);
				}
				break;
				case COLLISION_TYPES.JUNKIE_WITH_OBSTACLE:
				{
				OnCollisionWithObstacle(m_collisionList[it]);
				}
				break;
			}

			m_collisionList.RemoveAt(it);
			m_collisionList.RemoveAll(x=> x.m_collidee ==null);
		}

	}
	
	
}
