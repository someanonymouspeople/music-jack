using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SPAWN_TAG
{
	TAG_OBSTACLE,
	TAG_COLLECTABLE,
	TAG_TUNNEL
}

public enum SPAWN_LANE
{
	LANE_ONE = 1,
	LANE_TWO,
	LANE_THREE,
	LANE_FOUR,
	LANE_FIVE,
	LANE_SIX
}

public class YPositionedBeatTimeLineEvent : BeatTimeLineEvent
{
	public YPositionedBeatTimeLineEvent() : base()
	{
		m_beatYPos = 0;
	}
	public YPositionedBeatTimeLineEvent(BeatTimeLineEvent ev) 
	{
		m_beatEvent = ev.m_beatEvent;
		m_timeStamp = ev.m_timeStamp;
		m_averageEnergyOfSong = ev.m_averageEnergyOfSong;
		m_instantEnergyOfSong = ev.m_instantEnergyOfSong;
		m_spawnedIndicator = ev.m_spawnedIndicator;
		m_beatYPos = 0;
	}
	public float m_beatYPos;

}

public class MinMax
{
	public MinMax()
	{
		m_min = 0;
		m_max = 0;
	}
	public MinMax(float min,float max)
	{
		m_min = min;
		m_max = max;
	}
	public float m_min;
	public float m_max;
}


public class GameplayScript : MonoBehaviour 
{

	float m_startBGMusicVolume;
	//singleton instance
	static GameplayScript m_gamePlayScript;
	
	Vector2 m_screenSizeInUnits;
	
	//SPAWN TEST
	float m_nextSpawnY;
	float[] m_spawnTimer;
	int m_numberOfLanes;
	float m_spawnTime;
	float m_baseSpawnTime ;
	float m_maxBaseSpawnTime;
	SPAWN_LANE m_prevLane;

	// COLLECTABLE VARIANCE
	public bool m_randomCollectables;
	DETECTION_TONE m_nextDetectionToneToSpawnFor;
	float m_timeToEnd;

	bool m_gameOver;
	Vector3 m_currJunkieTarget;
	Vector3 prevElementInTargetList;

	//gamePlayScript specific vars
	public bool m_coreGameplayElementsInitialized = false;
	public int m_baseScoreValue ;
	int m_score =0;
	float m_multiplier = 1;
	float m_botClamp;
	public float m_beatMaxSpawnTime;
	public float m_beatMinSpawnTime;
	public float m_indicatorMinSpawnTime;
	public float m_indicatorMaxSpawnTime;
	public int m_maxMultiplier;
	float m_maxMultThisRound;

	//reference vars
	public BaseManager[] m_managers;
	protected CollectableManager m_collectableManager;
	protected AssetIndicatorManager m_indicatorManager;

	//gameplay script cache
	private int m_maxBeatsInSong;
	private int m_beatsCollected;
	protected GameObject m_heroJunkie;
	protected JunkieScript m_heroScript;
	protected List<YPositionedBeatTimeLineEvent> m_yPositionedBeatTimeLine;
	int m_currentBeatIndex;
	int m_currentIndicatorIndex = 0;
	bool m_noMoreBeats;
	public Color m_primaryBackGroundColor;
	DarknessScript m_darknessScript;
	bool m_darknessWon;
	public int m_powerEnableCountHolder;
	public int m_powerEnableCount;
	public int m_powerEnableCountIncrement;
	public int m_powerEnableCountCap;
	public int m_powersEnabled;
	public int m_powerOneUsedThisRound;
	public int m_powerTwoUsedThisRound;

	double[] preAllocatedDoubleArray;
	public delegate void BeatCallBack(DETECTION_TONE tone);
	public delegate void DarknessCallBack();
	public delegate void InstructionsSettingCallBack(bool shouldPlayInstructions,GameObject caller);

	public void IncrementBeatsCollected( int increment)
	{
		m_beatsCollected += increment;
	}
	public int GetBeatsCollected()
	{
		return m_beatsCollected;
	}

	public bool LocateDarknessAndSetDarknessVariable()
	{
		GameObject go = GameObject.Find("Darkness");
		if(go)
		{
			m_darknessScript  = go.GetComponent<DarknessScript>();
			return true;
		}
		Debug.Log("DarknessNotFound");
		return false;
	}

	public bool IsGameOver()
	{
		return m_gameOver;
	}
	public AssetIndicatorManager GetIndicatorManager()
	{
		if(m_indicatorManager != null)
		{
		return m_indicatorManager;
		}
		else
		{
			AssetIndicatorManager im = this.gameObject.GetComponentInChildren<AssetIndicatorManager>();
			if(im != null)
			{
				return im;
			}
		}
		return null;
	}

	public CollectableManager GetCollectableManager()
	{
		if(m_collectableManager != null)
		{
			return m_collectableManager;
		}
		return null;
	}

	public static GameplayScript GetInstance()
	{
		return m_gamePlayScript;
	}


	public GameObject GetHero()
	{
		return m_heroJunkie;
	}

	public void IncrementSpeed(float increment)
	{
		m_heroScript.IncrementSpeed(increment);
	}
	
	public void DecrementSpeed(float decrement)
	{
		m_heroScript.DecrementSpeed(decrement);
	}
	
	public void DecrementSpeedByFactor(float factor)
	{
		m_heroScript.DecrementSpeedByFactor(factor);
	}
	
	public void IncrementSpeedByFactor(float factor)
	{
		m_heroScript.IncrementSpeedByFactor(factor);
	}

	public void SetHeroSpeed(float speed)
	{
		m_heroScript.SetSpeed(speed);
	}

	public float GetHeroSpeed()
	{
		return m_heroScript.GetSpeed();
	}

	public Vector3 GetHeroPos()
	{
		return m_heroJunkie.gameObject.transform.position;
	}



	public void ReduceScore(int value)
	{
		m_score -= value;
	}
	public void IncrementScore(int value)
	{
		m_score += value;
	}

	public void IncrementPowerOneUsed(int value)
	{
		m_powerOneUsedThisRound += value;
	}

	public void IncrementPowerTwoUsed(int value)
	{
		m_powerTwoUsedThisRound += value;
	}
	void Awake()
	{
		if(m_powerEnableCountCap == 0)
		{
			m_powerEnableCountCap = 25;
		}
		m_powerOneUsedThisRound = 0;
		m_powerTwoUsedThisRound = 0;
		if(m_powerEnableCountIncrement == 0)
		{
			m_powerEnableCountIncrement = 5;
		}
		m_powerEnableCount = m_powerEnableCountIncrement;
		m_powerEnableCountHolder = 0;
		m_powersEnabled = 1;
		preAllocatedDoubleArray= new double[1024];
		m_startBGMusicVolume = AppManager.GetInstance().GetBackGroundMusicVolume();
		switch(AppManager.GetInstance().GetGameDifficulty())
		{
		case GAME_DIFFICULTY.DIFFICULTY_EASY:
		{
			m_baseScoreValue = 25;
		}
			break;
		case GAME_DIFFICULTY.DIFFICULTY_MEDIUM:
		{
			m_baseScoreValue =50;
		}
			break;
		case GAME_DIFFICULTY.DIFFICULTY_HARD:
		{
			m_baseScoreValue =75;
		}
			break;
		}

		m_primaryBackGroundColor = Color.white;
		
	
		switch(ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().m_type)
		{
			case BACKGROUND_TYPE.BACKGROUND_BLUE:
			{
			m_primaryBackGroundColor = new Color(0,46.0f/255.0f,142.0f/255.0f);
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_GREEN:
			{
			m_primaryBackGroundColor = new Color(0,141.0f/255.0f,51.0f/255.0f);
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_PINK:
			{
			m_primaryBackGroundColor = new Color(255.0f/255.0f,56.0f/255.0f,110.0f/255.0f);
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_YELLOW:
			{
			m_primaryBackGroundColor = new Color(228.0f/255.0f,76.0f/255.0f,1.0f/255.0f);
			}
				break;
			default:
			{
			m_primaryBackGroundColor = Color.white;
			}
			break;
		}


		if(m_beatMaxSpawnTime == 0)
		{
			m_beatMaxSpawnTime = 4;
		}
		if(m_beatMinSpawnTime == 0)
		{
			m_beatMinSpawnTime = 1;
		}

		m_noMoreBeats= false;
		m_currentBeatIndex = 0;


		m_yPositionedBeatTimeLine = MusicAnalyzerV2.GetInstance().GetPostProcessedBeatTimeLine();

		m_maxBeatsInSong = m_yPositionedBeatTimeLine.Count;
		m_beatsCollected = 0;

		m_numberOfLanes = System.Enum.GetNames(typeof(SPAWN_LANE)).Length;
		if(GameplayScript.GetInstance())
		{
			Destroy(GameplayScript.GetInstance().gameObject);
			m_gamePlayScript = this;
		}
		else
		{
			m_gamePlayScript = this;
		}
		m_heroJunkie = GameObject.Find("HeroDude");
		m_heroScript = GameObject.Find("HeroDude").GetComponent<JunkieScript>();

		ParallaxingBackGroundManager.GetInstance().SetSaturationThreshold(1);
		ParallaxingBackGroundManager.GetInstance().SetDesaturationThreshold(0);
		ParallaxingBackGroundManager.GetInstance().SetSaturationValue(0);
		
		m_timeToEnd = 0;
		ParallaxingBackGroundManager.GetInstance().RefreshManager();
		m_gameOver = false;
		m_screenSizeInUnits.y =  2 * Camera.main.orthographicSize;
		m_screenSizeInUnits.x = m_screenSizeInUnits.y * Camera.main.aspect;
		m_botClamp = (-m_screenSizeInUnits.y/2) + 1.3f;

		
		//calc first spawn location
		Vector2 camSize = m_screenSizeInUnits;
		Vector2 camPos = Camera.main.transform.position;
		Vector2 camScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2());

		Vector2 posToPlaceNextBeat;
		m_prevLane = GetRandomSpawnLane(m_prevLane,true);
		posToPlaceNextBeat.y = camScreenOffset.y + 0.5f +(((camSize.y -1)/m_numberOfLanes) *(int)m_prevLane);
		m_nextSpawnY = posToPlaceNextBeat.y;
			

		m_spawnTimer = new float[5];
		for(int i = 0; i<m_spawnTimer.Length; i++)
		{
			m_spawnTimer[i] = 0;
		}

		//set beat mask times
		m_spawnTime = 0.2f;
		m_baseSpawnTime = 0.2f;
		m_maxBaseSpawnTime = 0.4f;

		//init HUD or refresh HUD
		AudioManager.GetInstance().StopAllAudio();

		AppManager.GetInstance().gameObject.AddComponent<HUDScript>().InitializeHUDElements();	
		AppManager.GetInstance().gameObject.AddComponent<HUDScript>().RefreshHUD();	

		//Initiate Darkness
		if(LocateDarknessAndSetDarknessVariable())
		{

			m_darknessScript.InitializeDarkness(DarknessWonCallBack);
		}
		m_darknessWon= false;

	}

	void Start()
	{

	}
	
	
	public JunkieScript GetHeroScript()
	{
		return m_heroScript;
	}

	public DarknessScript GetDarknessScript()
	{
		return m_darknessScript;
	}
	public int GetScore()
	{
		return m_score;
	}

	public float GetMultiplier()
	{
		return m_multiplier;
	}

	public void SetMultiplier(int multiplier)
	{
		m_multiplier = multiplier;
	}
	
	public void IncrementMultiplierByFactor(int factor)
	{
		m_multiplier *= factor;
		if(m_multiplier>m_maxMultiplier)
		{
			m_multiplier = m_maxMultiplier;
		}
		if(m_maxMultThisRound < m_multiplier)
		{
			m_maxMultThisRound = m_multiplier;
		}
	}

	public void IncrementMultiplierBasedOnDifficulty()
	{
		switch(AppManager.GetInstance().GetGameDifficulty())
		{
		case GAME_DIFFICULTY.DIFFICULTY_EASY:
		{
			m_multiplier += 0.25f;
		}
			break;
		case GAME_DIFFICULTY.DIFFICULTY_MEDIUM:
		{
			m_multiplier += 0.5f;
		}
			break;
		case GAME_DIFFICULTY.DIFFICULTY_HARD:
		{
			m_multiplier +=1;
		}
			break;
		}
		m_multiplier = Mathf.Clamp(m_multiplier,1,m_maxMultiplier);
		if(m_multiplier > m_maxMultThisRound)
		{
			m_maxMultThisRound = m_multiplier;
		}
	}

	public void IncrementMultiplier(int count)
	{
		m_multiplier += count;
		if(m_multiplier>m_maxMultiplier)
		{
			m_multiplier = m_maxMultiplier;
		}
		if(m_maxMultThisRound < m_multiplier)
		{
			m_maxMultThisRound = m_multiplier;
		}
	}
	
	public void DecrementMultiplierByFactor(int factor)
	{
		float valueToDecrementBy = m_multiplier/factor;
		if(valueToDecrementBy < 1)
		{
			valueToDecrementBy = 1;
		}
		m_multiplier -= (int)valueToDecrementBy;
		if(m_multiplier < 1)
		{
			m_multiplier = 1;
		}
	}

	public void DecrementMultiplier(int count)
	{
		m_multiplier -= count;
		if(m_multiplier < 1)
		{
			m_multiplier = 1;
		}
	}

	public void ResetMultiplier()
	{
		m_multiplier = 1;
	}

	public void IncrementPowerUpCountHolder(int count)
	{
		m_powerEnableCountHolder+= count;
	}
	public void ResetPowerUpCountHolder()
	{
		m_powerEnableCountHolder = 0;
	}

	bool PowerEnableCriteriaChecker()
	{
		if(m_powerEnableCount <= m_powerEnableCountHolder)
		{

			m_powerEnableCountHolder = 0;
			m_powerEnableCount += m_powerEnableCountIncrement*m_powersEnabled;
			m_powerEnableCount = Mathf.Clamp(m_powerEnableCount,0,m_powerEnableCountCap);
			m_powersEnabled++;
			return true;
		}
		return false;
	}

	public void LoadCoreGameplayElements()
	{
		m_maxMultThisRound = 1;
		Camera.main.gameObject.AddComponent<FollowTransformScript>();
		Camera.main.gameObject.GetComponent<FollowTransformScript>().SetTransformTarget(m_heroJunkie.transform);
		Camera.main.gameObject.GetComponent<FollowTransformScript>().SetFreezeZ(true);
		Camera.main.gameObject.GetComponent<FollowTransformScript>().SetFreezeY(true);


		FollowTransformScript tempScript = GameObject.Find("ParallaxBackGroundManager").transform.GetChild(0).gameObject.AddComponent<FollowTransformScript>();
		tempScript.SetTransformTarget(m_heroJunkie.gameObject.transform);
		tempScript.SetFreezeY(true);
		tempScript.SetFreezeZ(true);

		m_coreGameplayElementsInitialized = true;
		foreach(BaseManager bm in m_managers)
		{
			if(bm is CollectableManager)
			{
				m_collectableManager = (CollectableManager)bm;
			}
			else if(bm is AssetIndicatorManager)
			{
				m_indicatorManager = (AssetIndicatorManager)bm;
			}
		}
		AppManager.GetInstance().m_hudScript.GetPowerManager().InitPowerManager(PowerEnableCriteriaChecker);

		BaseObject bo = m_indicatorManager.SpawnHeroIndicator(99);
		bo.transform.parent = Camera.main.transform;
		bo.transform.position = new Vector2();
	}

	public float GetTimeRemaining()
	{
		if(m_coreGameplayElementsInitialized)
		{
		return m_timeToEnd;
		}
		return 999;
	}

	public void UnloadCoreGameplayElements()
	{

		Destroy(ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetComponent<FollowTransformScript>());
		Destroy (Camera.main.GetComponent<FollowTransformScript>());
	}

	//Clear resources
	public void GameEndSequence()
	{
		// reset all elements in the beat time line so they be go through spawn logic again
		for(int i = 0;i<m_yPositionedBeatTimeLine.Count; i++)
		{
			m_yPositionedBeatTimeLine[i].m_spawnedIndicator = false;

		}
		//Clear all objects and handles in memory
		if(m_coreGameplayElementsInitialized)
		{
		m_collectableManager.ClearCollectables();
		}

		//destroy the touchPos eq so it may be recreated if required
		if(Camera.main.transform.FindChild("TouchPosEq") != null)
		{
		Destroy(Camera.main.transform.FindChild("TouchPosEq").gameObject);
		}
		//Clear any and all indicators on the screen
		GetIndicatorManager().ClearIndicatorList();

		//unload core gameplay elements
		UnloadCoreGameplayElements();
		AudioManager.GetInstance().SetBackGroundVolume(m_startBGMusicVolume);
	}

	//to move to gameManager
	public void EndGameSequence()
	{
		Debug.Log("MJ EndGameSequence Entering");
		GameEndSequence();
		Debug.Log("MJ EndGameSequence cleared resources");
		//m_indicatorManager.ClearIndicatorList();
		AppManager.GetInstance().SetPowerOneUsed(m_powerOneUsedThisRound);
		AppManager.GetInstance().SetPowerTwoUsed(m_powerTwoUsedThisRound);
		AppManager.GetInstance().SetFinalScore(m_score);
		AppManager.GetInstance().SetMaxMultThisRound(m_maxMultThisRound);
		float relativeBeats = (float)m_beatsCollected/(float)m_maxBeatsInSong;
		AppManager.GetInstance().SetLevelCompletionPercent((int)(relativeBeats*100));
		Debug.Log("MJ EndGameSequence set post game variables");
		m_heroScript.SetState(JUNKIE_STATES.STATE_LEAVING);
		Debug.Log("MJ EndGameSequence Exiting");
	}

	public void InstructionSettingCallBack(bool shouldPlayInstructions,GameObject caller)
	{
		Destroy (caller);
		AppManager.GetInstance().Resume();
		GameConfigurationHolderScript.GetInstance().SetShouldPlayInstructionsBool(shouldPlayInstructions);
		m_darknessScript.SetState(DARKNESS_STATES.STATE_INGAME);

		AudioClip audioToPlay = AudioManager.GetInstance().GetAudioFile(AppManager.GetInstance().GetSelectedSongReference());
		AudioManager.GetInstance().PlayAtSource(Camera.main.audio,audioToPlay,AppManager.GetInstance().m_backGroundMusicVolume);
	}
	private void InitializeCoreGameplayElements()
	{
		if(!m_coreGameplayElementsInitialized)
		{
			LoadCoreGameplayElements();
			m_darknessScript.SetState(DARKNESS_STATES.STATE_INGAME);

			AudioClip audioToPlay = AudioManager.GetInstance().GetAudioFile(AppManager.GetInstance().GetSelectedSongReference());

			
			m_timeToEnd = Mathf.Abs(audioToPlay.length);
			ParallaxingBackGroundManager.GetInstance().SetGameTime(m_timeToEnd);


			if(GameConfigurationHolderScript.GetInstance().GetShouldPlayInstructions())
			{
				AppManager.GetInstance().Pause();
				GameObject instructionsTool = GameObject.Instantiate(Resources.Load("UI/GamePlayInstructionsTool")) as GameObject;
				instructionsTool.GetComponent<GamplayInstructionsScript>().InitializeInstructionTool(InstructionSettingCallBack);
			}
			else
			{

				AudioManager.GetInstance().PlayAtSource(Camera.main.audio,audioToPlay,AppManager.GetInstance().m_backGroundMusicVolume);
			}
			//init musicJunkie's cape
//			MusicJunkieVisualizerCape.GetInstance().Init();
//			GameObject tempViz = MusicJunkieVisualizerCape.GetInstance().GetVisualizer();
//			tempViz.transform.position = new Vector2();
//			tempViz.transform.localScale = new Vector2(0.25f,0.25f);
//			tempViz.transform.rotation = Quaternion.AngleAxis(90,this.transform.forward);
//			tempViz.transform.parent = GetHeroScript().transform;
//			tempViz.transform.localPosition = new Vector2();
		}
	}

	private Vector3 ReturnPrevTarget()
	{
		//iterate to find prev target
		List<Vector3> smoothedPositionList = SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList();
		List<Vector3> touchPositionsBehindJunkie = smoothedPositionList.FindAll((x) => x.x < m_heroJunkie.transform.position.x);
		if(touchPositionsBehindJunkie.Count>0)
		{
			prevElementInTargetList = touchPositionsBehindJunkie[touchPositionsBehindJunkie.Count-1];
		}
		else
		{
			prevElementInTargetList = m_heroJunkie.transform.position;
		}
		return prevElementInTargetList;
	}

	private void DirectJunkiesMovement()
	{
		Vector3 prevTarget = ReturnPrevTarget();

		List<Vector3> smoothedPositionList = SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList();
		int positionsLesserThanHeroX = smoothedPositionList.FindAll((x)=> x.x<m_heroJunkie.transform.position.x).Count;


		if(smoothedPositionList.Count >2 && positionsLesserThanHeroX<smoothedPositionList.Count)
		{
			if(m_heroJunkie.transform.position.x >= m_heroScript.GetTargetPos().x)
			{
				//get first element that is in front of junkie
				Vector3 potentialTarget =smoothedPositionList[positionsLesserThanHeroX];
				m_currJunkieTarget = potentialTarget;
				//direct his movement
				m_heroScript.DirectJunkie(potentialTarget, (potentialTarget - prevTarget).normalized);

			}
		}
		else
		{
			//if there is no further target
			m_heroScript.NoFurtherTargetSequence();
		}
	}
	
	public SPAWN_LANE GetRandomSpawnLane(SPAWN_LANE lastSpawn, bool isLockedToNearest,int laneCount = 1)
	{
		if(isLockedToNearest)
		{
			int laneBehindPrev = (int)lastSpawn - laneCount;
			if(laneBehindPrev < 1)
			{
				laneBehindPrev = 1;
			}
			int laneAheadOfPrev = (int)lastSpawn + laneCount;
			if(laneAheadOfPrev > m_numberOfLanes)
			{
				laneAheadOfPrev = m_numberOfLanes;
			}
			int laneChecker = Random.Range(laneBehindPrev * 10, (laneAheadOfPrev * 10 + 9))/10;

			return (SPAWN_LANE)laneChecker;


		}
		else
		{
			int laneChecker = Random.Range(0,m_numberOfLanes*10)/10;

			return (SPAWN_LANE)laneChecker;

		}
	}
	

	public Collectable CustomBeatCallBack(DETECTION_TONE tone,Vector2 posToPlaceCollectable)
	{

		Collectable toReturn = null;

		if(!m_randomCollectables)
		{
			toReturn = (Collectable)m_collectableManager.SpawnObject((int)tone,posToPlaceCollectable);
		}
		else
		{
			toReturn = (Collectable)m_collectableManager.SpawnRandomObject(posToPlaceCollectable);
		}


		return toReturn;
	}



	private void ProcessBeatTimeLine(List<YPositionedBeatTimeLineEvent> yPosedBeatTimeLine)
	{
		float timeToReach = m_beatMinSpawnTime + ((1-m_heroScript.GetRelativeSpeed()) * (m_beatMaxSpawnTime-m_beatMinSpawnTime)); 
		Vector2 camSize = m_screenSizeInUnits;
		Vector2 camPos = Camera.main.transform.position;
		Vector2 camScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2());
		float currentTime = Camera.main.audio.time;

		//spawn indicators
		if(!m_noMoreBeats&& yPosedBeatTimeLine.Count >1)
		{
			float indicatorTimeToCheck = yPosedBeatTimeLine[m_currentIndicatorIndex].m_timeStamp - timeToReach;
			float variableBeatSpawnTime = m_indicatorMinSpawnTime + ( (1- GetHeroScript().GetRelativeSpeed()) * (m_indicatorMaxSpawnTime - m_indicatorMinSpawnTime));

			if(!yPosedBeatTimeLine[m_currentIndicatorIndex].m_spawnedIndicator && Camera.main.audio.time > indicatorTimeToCheck - variableBeatSpawnTime)
			{
				//if(m_spawnTimer[(int)yPosedBeatTimeLine[m_currentIndicatorIndex].m_beatEvent.m_beatTone] > m_spawnTime)
				//{
					m_spawnTimer[(int)yPosedBeatTimeLine[m_currentIndicatorIndex].m_beatEvent.m_beatTone] =0;
					yPosedBeatTimeLine[m_currentIndicatorIndex].m_spawnedIndicator = true;
					m_indicatorManager.SpawnCollectableIndicator((int)yPosedBeatTimeLine[m_currentIndicatorIndex].m_beatEvent.m_beatTone,new Vector2(camPos.x,yPosedBeatTimeLine[m_currentIndicatorIndex].m_beatYPos),m_indicatorManager.m_objectReferences[m_indicatorManager.m_objectReferences.Length-1],variableBeatSpawnTime).transform.parent = Camera.main.transform;

				//}
				if(m_currentIndicatorIndex +1 < yPosedBeatTimeLine.Count-1)
				{
					m_currentIndicatorIndex++;
				}
			}
		

			//spawn beats
			yPosedBeatTimeLine.Sort((x,y) => -1* x.m_averageEnergyOfSong.CompareTo(y.m_averageEnergyOfSong));
			float maxVolume = yPosedBeatTimeLine[0].m_averageEnergyOfSong;
			maxVolume -= maxVolume*0.25f;
			float minVolume = maxVolume *0.25f;
			yPosedBeatTimeLine.Sort((x,y) => x.m_timeStamp.CompareTo(y.m_timeStamp));
		

			YPositionedBeatTimeLineEvent currentBeat = yPosedBeatTimeLine[m_currentBeatIndex];
			YPositionedBeatTimeLineEvent nextBeat = yPosedBeatTimeLine[m_currentBeatIndex +1];
			
			float timeToCheck = (currentBeat.m_timeStamp- timeToReach);
			if(Camera.main.audio.time > timeToCheck )
			{
				if(currentBeat.m_spawnedIndicator)
				{
					m_spawnTimer[(int)currentBeat.m_beatEvent.m_beatTone] = 0;
					
					// beat and indicator spawn code
					Vector2 posToPlaceCollectable;
					
					//spawn beat in prevCalced pos
					posToPlaceCollectable.y = currentBeat.m_beatYPos;
					posToPlaceCollectable.x = camPos.x + camSize.x/2 + 0.5f;
					
					Collectable collectable = CustomBeatCallBack(currentBeat.m_beatEvent.m_beatTone,posToPlaceCollectable);
					if(collectable!= null)
					{
						collectable.InitBeatBasedCollectable(m_heroScript,timeToReach,m_primaryBackGroundColor);
					}
					

					//Calc pos to place next beat
					Vector2 posToPlaceNextBeat;
					posToPlaceNextBeat = new Vector2(posToPlaceCollectable.x,nextBeat.m_beatYPos);
					m_nextSpawnY = posToPlaceNextBeat.y;

					
				}
				
				m_currentBeatIndex += 1;
				if(m_currentBeatIndex >= yPosedBeatTimeLine.Count-1)
				{
					m_noMoreBeats = true;
					
				}
			}
		}
	}

	public void DarknessWonCallBack()
	{
		m_gameOver = true;
		m_darknessWon = true;
		m_darknessScript.SetState(DARKNESS_STATES.STATE_GAME_LOST);
		m_heroScript.SetState(JUNKIE_STATES.STATE_DYING);
	}

	public void DarknessWonUpdate()
	{
		if(m_heroScript != null)
		{
			m_heroScript.UpdateJunkie();
		}
		if(m_darknessScript.GetState() == DARKNESS_STATES.STATE_DEAD)
		{
			AppManager.GetInstance().SetFinalScore(m_score);
			AppManager.GetInstance().SetMaxMultThisRound(m_maxMultThisRound);
			AppManager.GetInstance().SetLevelCompletionPercent(100);
			EndGameSequence();
			if(m_heroScript != null)
			{
				Destroy(m_heroJunkie);
			}
			m_darknessScript.UpdateDarkness();
			AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_POST_GAME);
		}
		else
		{
			float volumeToSet =AudioManager.GetInstance().GetBackGroundVolume();
			volumeToSet -= (m_startBGMusicVolume/m_darknessScript.m_extroTime) *Time.deltaTime;
			AudioManager.GetInstance().SetBackGroundVolume(volumeToSet);
			m_darknessScript.UpdateDarkness();
		}
	}

	public void JunkieWonUpdate()
	{
		if(m_darknessScript != null)
		{
			m_darknessScript.UpdateDarkness();
		}
		m_heroScript.UpdateJunkie();
		if(m_heroScript.GetState() == JUNKIE_STATES.STATE_DEAD)
		{
			m_heroScript.UpdateJunkie();
			AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_POST_GAME);
		}
	}


	public void UpdateGamePlay () 
	{
		if(!m_gameOver)
		{
			//CORE INGAME LOGIC
			//if junkie is in the game
			if(m_heroScript.GetState() == JUNKIE_STATES.STATE_INGAME || m_heroScript.GetState() == JUNKIE_STATES.STATE_HIT || m_heroScript.GetState() == JUNKIE_STATES.STATE_STUMBLED || m_heroScript.GetState() == JUNKIE_STATES.STATE_PHASED )
			{
				//Initialization
				if(!m_coreGameplayElementsInitialized)
				{
					InitializeCoreGameplayElements();
					return;
				}
				//Time updation
				m_timeToEnd -= Time.deltaTime;
				if(m_timeToEnd <= 0)
				{
					Debug.Log("Game Over ... initiating end game sequence");
					m_darknessScript.SetState(DARKNESS_STATES.STATE_LEAVING);
					m_gameOver= true;
					EndGameSequence();
					return;
				}



				float instantEnergy = 0;
				float averageEnergy = 0;
				DETECTION_TONE toneDetectedIn = DETECTION_TONE.NONE;
				if(m_timeToEnd > 5.0f)
				{

					ProcessBeatTimeLine(m_yPositionedBeatTimeLine);
				}

				MusicAnalyzerV2.GetInstance().ProcessSong(out instantEnergy, out averageEnergy,ref preAllocatedDoubleArray);
				TouchPosEqualizerScript.GetInstance().UpdateTouchPosEqualizer();
				instantEnergy = 20 * Mathf.Log10(instantEnergy) + 203;
				averageEnergy = 20 * Mathf.Log10(averageEnergy) + 203;
				float relativeSpeed = averageEnergy/80;

				// beat spawnTimer
				m_spawnTime = m_baseSpawnTime + ((m_maxBaseSpawnTime - m_baseSpawnTime) * GetHeroScript().GetRelativeSpeed());

				for(int i =0 ; i<m_spawnTimer.Length; i++)
				{
					if(m_spawnTimer[i] < m_maxBaseSpawnTime)
					{
						m_spawnTimer[i] += Time.deltaTime;
					}
				}

				//update managers associated with gameplay
				for(int i =0;i <m_managers.Length;i++)
				{
					m_managers[i].UpdateManager();
				}

				//Touch position follow logic
				//optimize target instances
				DirectJunkiesMovement();
	
				//bottom clamp
				if(m_heroJunkie.transform.position.y -m_heroScript.GetSprite().renderer.bounds.extents.y < m_botClamp)
				{
					m_heroScript.RockBottomSequence();
				}
					
				//increment score based on multiplier
				m_score += (int)(((float)m_baseScoreValue * (float)m_multiplier) * Time.deltaTime);

				//update junkies collision manager
				JunkieCollisionManager.GetInstance().UpdateCollisionManager();
			}

			float divisor = m_darknessScript.GetMaxLocalXTravel() * 0.9f - m_darknessScript.GetMinLocalXTravel();
			float dividend = m_darknessScript.transform.localPosition.x - m_darknessScript.GetMinLocalXTravel();
			ParallaxingBackGroundManager.GetInstance().SetSaturationValue(1-(dividend/divisor));

			m_heroScript.UpdateJunkie();
			m_darknessScript.UpdateDarkness();

		}
		else
		{

			if(m_darknessWon)
			{
				DarknessWonUpdate();
			}
			else
			{
				JunkieWonUpdate();
			}
			if(m_heroScript.GetState()== JUNKIE_STATES.STATE_DESTROYED)
			{
				Destroy(GameplayScript.GetInstance().GetDarknessScript().gameObject);
				//UnloadCoreGameplayElements();
			}
		}
	}

}
