﻿using UnityEngine;
using System.Collections;

public class HUDScript : MonoBehaviour {
	
	GameObject m_HUD;
	GameObject m_timeLine;
	public Vector2 m_HUDPos;
	GameObject m_pauseMenu;
	TextMesh m_speedMesh;
	TextMesh m_scoreMesh;
	TextMesh m_multiplierMesh;
	TextMesh m_multiplierSubMesh;
	GameObject m_pauseButton;
	Vector2 m_screenOffset;
	Vector2 m_screenSize;
	PowerManager m_powerManagerScript;
	void Awake()
	{
		if(m_HUDPos.Equals(Vector2.zero))
		{
			GameObject tempHUD = (GameObject)Instantiate(Resources.Load("UI/HUDPrefab2"));
			m_screenOffset = Camera.main.ScreenToWorldPoint(Vector2.zero);
			m_screenSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
			m_HUDPos = new Vector2();
			m_HUDPos.x = Camera.main.transform.position.x + m_screenSize.x *0.175f;

			Destroy(tempHUD);
		}
	}
	public PowerManager GetPowerManager()
	{
		if(m_powerManagerScript !=null)
		{
			return m_powerManagerScript;
		}
		return null;
	}
	public void InitializeHUDElements()
	{
		//init main layout object
		if(m_HUD ==null)
		{
			//search for it
			m_HUD = GameObject.Find("HUD");
			if(m_HUD== null)
			{
				//if not instantiate it
				m_HUD = (GameObject)Instantiate(Resources.Load("UI/HUDPrefab2"));
				m_HUD.name = "HUD";
			}
		}
		m_powerManagerScript = m_HUD.transform.FindChild("PowerHolder").GetComponent<PowerManager>();
		m_powerManagerScript.m_transformToInstillPowerTo = GameplayScript.GetInstance().GetHeroScript().gameObject.transform;
		m_HUD.transform.localPosition = m_HUDPos;
		m_scoreMesh = m_HUD.transform.FindChild("ScoreMesh").GetComponent<TextMesh>();
		m_multiplierMesh = m_HUD.transform.FindChild("MultiplierMesh").GetComponent<TextMesh>();
		m_multiplierSubMesh = m_HUD.transform.FindChild("MultiplierSubMesh").GetComponent<TextMesh>();
		m_speedMesh = m_HUD.transform.FindChild("SpeedMesh").GetComponent<TextMesh>();
		m_pauseButton = m_HUD.transform.FindChild("PowerHolder/PauseButton").gameObject;
		m_HUD.transform.parent = Camera.main.transform;
		m_timeLine = m_HUD.transform.FindChild("Console").transform.FindChild("SongTimeLine").gameObject;
		m_timeLine.transform.localScale = new Vector2(0,1);

	}
	public void RefreshHUD()
	{
		if(m_pauseMenu)
		{
			m_pauseMenu.GetComponent<MenuSuspender>().DestroyMenu();
			Destroy(m_pauseMenu);
		}
		if(!m_HUD)
		{
			InitializeHUDElements();
		}
		else
		{
			m_timeLine.transform.localScale = new Vector2(1,1);
		}
		ResetElements();
	}
	
	public void DestroyElements()
	{
		Destroy(m_HUD);
		//m_HUD = null;
		m_timeLine = null;

	}

	public void UnHidePause()
	{
		m_HUD.transform.FindChild("PowerHolder/PauseButton").GetComponent<PauseButtonScript>().SetHidden(false);
	}
	public void HidePause()
	{
		m_HUD.transform.FindChild("PowerHolder/PauseButton").GetComponent<PauseButtonScript>().SetHidden(true);
	}

	public void ResetElements()
	{

		m_scoreMesh.text = "" + 0;
		m_multiplierMesh.text = ""+1;
		m_multiplierSubMesh.text = "";
		
		float minSpeed = GameplayScript.GetInstance().GetHeroScript().GetMinSpeed();
		float speedToDisplay =  (GameplayScript.GetInstance().GetHeroScript().GetSpeed());
		float speedHigherThanMin = speedToDisplay - GameplayScript.GetInstance().GetHeroScript().GetMinSpeed();
		speedToDisplay = minSpeed + speedHigherThanMin*4.34f;
		if(speedToDisplay < minSpeed)
		{
			speedToDisplay = minSpeed;
		}
		m_speedMesh.text = ""+(int)speedToDisplay;

		Vector2 scaleToSet = new Vector2(0,1);
		m_timeLine.transform.localScale = scaleToSet;
	}
	public void UpdateElements(float songTime,float currentTime)
	{
		if(m_scoreMesh == null)
		{
			RefreshHUD();
		}
		m_powerManagerScript.UpdatePowerManager();
		m_scoreMesh.text = "" + ((GameplayScript.GetInstance().GetScore()/10)*10);
		float multiplier = GameplayScript.GetInstance().GetMultiplier();
//		float excessMult = multiplier - (int)multiplier;
//
//		if(excessMult == 0)
//		{
//			m_multiplierSubMesh.text = "";
//		}
//		else if(excessMult <= 0.25f)
//		{
//			m_multiplierSubMesh.text = "1/\n4";
//		}
//		else if(excessMult <= 0.5f)
//		{
//			m_multiplierSubMesh.text = "1/\n2";
//		}
//		else if(excessMult <= 0.75f)
//		{
//			m_multiplierSubMesh.text = "3/\n4";
//		}

		m_multiplierMesh.text = ""+(int)multiplier;
		float minSpeed = GameplayScript.GetInstance().GetHeroScript().GetMinSpeed();
		float speedToDisplay =  (GameplayScript.GetInstance().GetHeroScript().GetSpeed());
		float speedHigherThanMin = speedToDisplay - GameplayScript.GetInstance().GetHeroScript().GetMinSpeed();

		speedToDisplay = minSpeed + speedHigherThanMin*4.34f;
		if(speedToDisplay < minSpeed)
		{
			speedToDisplay = minSpeed;
		}
		m_speedMesh.text = ""+(int)speedToDisplay;
		if(m_pauseButton.GetComponent<PauseButtonScript>().UpdatePauseButton())
		{
			PauseSequence();
			return;
		}

		Vector2 scaleToSet = new Vector2(0,1);
		scaleToSet.x = currentTime/songTime;
		if(m_timeLine != null)
		{
			m_timeLine.transform.localScale = scaleToSet;
		}
	}

	public void PauseSequence()
	{
		AppManager.GetInstance().Pause();
		GameObject ps = (GameObject)Instantiate(Resources.Load("GameObjects/Utilities/PauseScreen"));

		ps.transform.parent = Camera.main.transform;
		Vector3 posToSet = Vector3.zero;
		ps.transform.localPosition = posToSet;
		m_pauseMenu = ps;
	}	
}
