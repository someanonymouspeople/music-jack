using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
public enum GAME_SCENES
{
	SCENE_MAIN_MENU,
	SCENE_CREDITS,
	SCENE_OPTIONS,
	SCENE_SHOP,
	SCENE_LEADERBOARDS,
	SCENE_ACHIEVEMENTS,
	SCENE_HOW_TO_PLAY,
	SCENE_SETTINGS,
	SCENE_LEVEL_SELECT,
	SCENE_GAME,
	SCENE_CHALLENGE,
	SCENE_LOADING,

	SCENE_POST_GAME
}

public enum GAME_DIFFICULTY
{
	DIFFICULTY_EASY,
	DIFFICULTY_MEDIUM,
	DIFFICULTY_HARD
}

public class AppManager : MonoBehaviour 
{

	//HUD(To move to GameManager)
	public HUDScript m_hudScript;

	//global variables
	public static int MJ_UNIT_METRE = 30;

	public static AppManager m_instance;
	public GAME_SCENES m_prevScene;
	public GAME_SCENES m_currentScene;
	public ACHIEVEMENTS m_acheivementsUnlocked = 0;
	public bool m_isPaused = false;
	public float m_backGroundMusicVolume;
	public float m_audioEffectsVolume;
	public int m_selectedSongIndex;
	public int m_prevSelectedSongIndex;
	public BACKGROUND_TYPE m_currentBGType;
	int m_finalScore;
	float m_maxMultThisRound;
	int m_levelCompletionPercent;
	public bool m_runTimeLoadMode;
	public int m_calculationWindow;
	public AudioClip m_menuMusic;
	public GAME_DIFFICULTY m_gameDifficulty;
	public int m_powerOneUsed;
	public int m_powerTwoUsed;

	public void Pause()
	{
		SmoothenedTouchPositionStorer.GetInstance().enabled = false;
		AudioManager.GetInstance().PauseAllAudio();
		m_hudScript.HidePause();
		m_isPaused = true;
	}

	public void Resume()
	{
		SmoothenedTouchPositionStorer.GetInstance().enabled = true;
		AudioManager.GetInstance().ResumeAllAudio();
		m_hudScript.UnHidePause();
		m_hudScript.RefreshHUD();
		m_isPaused = false;
	}
	public void SetPowerOneUsed(int value)
	{
		m_powerOneUsed = value;
	}
	public void SetPowerTwoUsed(int value)
	{
		m_powerTwoUsed = value;
	}
	public void SetGameDifficulty(GAME_DIFFICULTY difficulty)
	{
		m_gameDifficulty = difficulty;
	}

	public GAME_DIFFICULTY GetGameDifficulty()
	{
		return m_gameDifficulty;
	}

	//To move to gameManager
	public void Restart()
	{
		Destroy(GameplayScript.GetInstance().GetDarknessScript().gameObject);
		GameplayScript.GetInstance().EndGameSequence();
		AudioManager.GetInstance().StopAllAudio();
		GameplayScript.GetInstance().UnloadCoreGameplayElements();
		ParallaxingBackGroundManager.GetInstance().SetSaturationValue(0);
		AppManager.GetInstance().m_prevSelectedSongIndex = AppManager.GetInstance().GetSelectedSongReference();
		AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_LOADING);

	}

	//To move to gameManager
	public void MainMenu()
	{
		Destroy(GameplayScript.GetInstance().GetDarknessScript().gameObject);
		AudioManager.GetInstance().StopAllAudio();
		GameplayScript.GetInstance().EndGameSequence();
		GameplayScript.GetInstance().UnloadCoreGameplayElements();

		Camera.main.transform.position = new Vector3(0,0,Camera.main.transform.position.z);
		AppManager.GetInstance().ChangeScene(GAME_SCENES.SCENE_MAIN_MENU);
		AppManager.GetInstance().m_prevSelectedSongIndex = AppManager.GetInstance().GetSelectedSongReference();
	}

	public ACHIEVEMENTS GetAchievements()
	{
		//CheckUnlocks();
		return m_acheivementsUnlocked;
	}

	public void AddUnlockedAchievement(ACHIEVEMENTS achievementToUnlock)
	{
		m_acheivementsUnlocked |= achievementToUnlock;
	}

	void Awake()
	{

		if(AppManager.GetInstance() != null)
		{
			Destroy(this.gameObject);
			return;
		}
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		m_prevSelectedSongIndex = int.MaxValue;
		m_instance = this;
		DontDestroyOnLoad(this.gameObject);
	}
	public static AppManager GetInstance()
	{
		return m_instance;
	}

	//To move to GameLevelManager


	public void ChangeBackGround(BACKGROUND_TYPE type)
	{
		m_currentBGType = type;
		ParallaxingBackGroundManager.GetInstance().LoadNewBackGround(m_currentBGType);
	}




	void CustomInitComplete(FBResult result)
	{
		Debug.Log("FB init complete");
	}
	static void CustomHideUnity(bool checkFlag)
	{
		Debug.Log ("Unity hiding");
		if(!checkFlag)
		{
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}
	}
	//initialize the application
	void Start()
	{
		if(!Camera.main.audio.isPlaying)
		{
			//play menu music
			Camera.main.audio.clip = m_menuMusic;
			Camera.main.audio.Play();
		}

		
		//initialize the fb SDK
//		NETWORK_CONNECTION_RESULT result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
//		if((result & NETWORK_CONNECTION_RESULT.NET_ACTIVE) == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
//		{
//			//FB.Init(CustomInitComplete,CustomHideUnity);
//		}
		//SaveFileCreatorScript.GetInstance().Initialize();
		if(SaveFileCreatorScript.GetInstance().FileExists("MJAppData"))
		{
			m_backGroundMusicVolume = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJAppData","BackGroundMusicVolume");
			Camera.main.audio.volume = m_backGroundMusicVolume/100;
			m_audioEffectsVolume = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJAppData","AudioEffectsVolume");
			Camera.main.transform.FindChild("SoundEffectPlayer").audio.volume = m_audioEffectsVolume/100;
			m_currentBGType = (BACKGROUND_TYPE)SaveFileCreatorScript.GetInstance().GetStoredFloat("MJAppData","BackGroundType");
		}
		else
		{
			SaveFileCreatorScript.GetInstance().CreateFile("MJAppData");
			m_backGroundMusicVolume = 100;
			Camera.main.audio.volume = m_backGroundMusicVolume = 1;
			m_audioEffectsVolume = 100;
			Camera.main.transform.FindChild("SoundEffectPlayer").audio.volume = 1;
			m_currentBGType = BACKGROUND_TYPE.BACKGROUND_PINK;
		}
		PlayerStatManager.GetInstance();


		ParallaxingBackGroundManager.GetInstance().LoadNewBackGround(m_currentBGType);
	}

	void Save()
	{
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJAppData",m_backGroundMusicVolume,"BackGroundMusicVolume");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJAppData",m_audioEffectsVolume,"AudioEffectsVolume");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJAppData",(int)m_currentBGType,"BackGroundType");
		SaveFileCreatorScript.GetInstance().CloseFileAndSerializeValues("MJAppData");
	}

	public void SetFinalScore(int score)
	{
		m_finalScore = score;
		PlayerStatManager.GetInstance().CheckAndSetHighScore(score);
	}
	public void SetLevelCompletionPercent(int percent)
	{
		m_levelCompletionPercent = percent;

	}
	public void SetMaxMultThisRound(float mult)
	{
		m_maxMultThisRound = mult;

	}

	public void SetSelectedSong(int index)
	{
		m_selectedSongIndex = index;
	}
	public int GetFinalScore()
	{
		return m_finalScore;
	}
	public float GetMaxMult()
	{
		return m_maxMultThisRound;
	}
	public int GetLevelCompletionPercent()
	{
		return m_levelCompletionPercent;
	}

	//to move to gameManager
	public int GetSelectedSongReference()
	{
		return m_selectedSongIndex;
	}


	public float GetBackGroundMusicVolume()
	{
		return m_backGroundMusicVolume;
	}

	public float GetAudioEffectVolume()
	{
		return m_audioEffectsVolume;
	}

	public void SetBackGroundVolume(float volume)
	{
		m_backGroundMusicVolume = volume;
	}

	public void SetAudioEffectVolume(float volume)
	{
		m_audioEffectsVolume = volume;
	}

	public GAME_SCENES GetCurrentScene()
	{
		return m_currentScene;
	}

	public void ChangeScene(GAME_SCENES scene)
	{

		switch(scene)
		{
		case GAME_SCENES.SCENE_MAIN_MENU:
		{
			if(!Camera.main.audio.isPlaying)
			{
				//play menu music
				Camera.main.audio.clip = m_menuMusic;
				Camera.main.audio.Play();
			}
//			if(m_hudScript)
//			{
//				m_hudScript.DestroyElements();
//				m_hudScript = null;
//			}
			Application.LoadLevel("MainMenuScene");
		}
			break;
		case GAME_SCENES.SCENE_CREDITS:
		{
			Application.LoadLevel("CreditsScene");
		}
			break;
		case GAME_SCENES.SCENE_SHOP:
		{

		}
			break;
		case GAME_SCENES.SCENE_OPTIONS:
		{
			Application.LoadLevel("OptionsScene");
		}
			break;
		case GAME_SCENES.SCENE_LEADERBOARDS:
		{
			Application.LoadLevel("LeaderBoardsScene");
		}
			break;
		case GAME_SCENES.SCENE_ACHIEVEMENTS:
		{
			Application.LoadLevel("AchievementsScene");
		}
			break;
		case GAME_SCENES.SCENE_HOW_TO_PLAY:
		{
			Application.LoadLevel("HowToPlayScene");
		}
			break;
		case GAME_SCENES.SCENE_SETTINGS:
		{
			Application.LoadLevel("SettingsScene");
		}
			break;
		case GAME_SCENES.SCENE_LEVEL_SELECT:
		{
			Application.LoadLevel("LevelSelectScene");
		}
			break;
		case GAME_SCENES.SCENE_LOADING:
		{
			if(AudioManager.GetInstance().ContainsData())
			{
				if(GetSelectedSongReference() == 999)
				{
					
					Debug.Log("Loading random ref number");
					m_selectedSongIndex = Random.Range(0,AudioManager.GetInstance().GetSoundFileData().Count-1);

					if(AudioManager.GetInstance().CheckAudioFile(m_selectedSongIndex) == null)
					{
						TaskElement musicLoadingTask = new TaskElement();
						musicLoadingTask.m_taskType = TASK_TYPE.TYPE_LOAD_AUDIO;
						musicLoadingTask.m_extraInfo = m_selectedSongIndex;
						MJAssetLoader.GetInstance().AddTask(musicLoadingTask);

						TaskElement musicProcessingTask = new TaskElement();
						musicProcessingTask.m_taskType = TASK_TYPE.TYPE_MUSIC_PROCESSING;
						musicProcessingTask.m_extraInfo = m_selectedSongIndex;
						MJAssetLoader.GetInstance().AddTask(musicProcessingTask);

					}
					
				}
				else if(GetSelectedSongReference() < AudioManager.GetInstance().m_soundFiles.Count)
				{
					Debug.Log("Loading Ref Number :"+ AppManager.GetInstance().GetSelectedSongReference());

					//init the music analyzer
					MusicAnalyzerV2.GetInstance().SetDetectionMode(BEAT_DETECTION_MODES.TONE_BASED);
					MusicAnalyzerV2.GetInstance().SetAudioSource(Camera.main.audio);

					if(AudioManager.GetInstance().CheckAudioFile(m_selectedSongIndex) == null)
					{
						TaskElement musicLoadingTask = new TaskElement();
						musicLoadingTask.m_taskType = TASK_TYPE.TYPE_LOAD_AUDIO;
						musicLoadingTask.m_extraInfo = m_selectedSongIndex;
						MJAssetLoader.GetInstance().AddTask(musicLoadingTask);
					}
					if(AppManager.GetInstance().m_prevSelectedSongIndex != AppManager.GetInstance().GetSelectedSongReference())
					{
						if(!m_runTimeLoadMode)
						{
							Debug.Log("PostProcessing... PrevIndex : "+ AppManager.GetInstance().m_prevSelectedSongIndex + " CurrentIndex : "+ AppManager.GetInstance().GetSelectedSongReference());
							TaskElement musicProcessingTask = new TaskElement();
							musicProcessingTask.m_taskType = TASK_TYPE.TYPE_MUSIC_PROCESSING;
							musicProcessingTask.m_extraInfo = m_selectedSongIndex;
							MJAssetLoader.GetInstance().AddTask(musicProcessingTask);
						}
						else
						{
							MusicAnalyzerV2.GetInstance().m_willBeUsedDuringRunTime = true;
							Debug.Log("PostProcessing... PrevIndex : "+ AppManager.GetInstance().m_prevSelectedSongIndex + " CurrentIndex : "+ AppManager.GetInstance().GetSelectedSongReference());
							TaskElement musicProcessingTask = new TaskElement();
							musicProcessingTask.m_taskType = TASK_TYPE.TYPE_MUSIC_PROCESSING_TIMED;
							musicProcessingTask.m_extraInfo = m_selectedSongIndex;
							musicProcessingTask.m_extraInfo2 = m_calculationWindow;
							MJAssetLoader.GetInstance().AddTask(musicProcessingTask);
						}
					}

					//AudioManager.GetInstance().GetAudioFile(AppManager.GetInstance().GetSelectedSongReference());
				}
			}
			Application.LoadLevel("LoadingScene");
		}
			break;
		case GAME_SCENES.SCENE_GAME:
		{
			m_finalScore = 0;
			Camera.main.audio.clip = null;
			AudioManager.GetInstance().StopAllAudio();
			if(AppManager.GetInstance().gameObject.GetComponent<HUDScript>() == null)
			{
				m_hudScript = AppManager.GetInstance().gameObject.AddComponent<HUDScript>();
			}

			Application.LoadLevel("Alpha");

		}
			break;
		case GAME_SCENES.SCENE_CHALLENGE:
		{
			Application.LoadLevel("ChallengeScene");
		}
			break;
		case GAME_SCENES.SCENE_POST_GAME:
		{
			Application.LoadLevel("PostGameScene");
			m_hudScript.DestroyElements();
			AudioManager.GetInstance().StopAllAudio();
			Camera.main.transform.position = new Vector3(0,0,-5);

			ParallaxingBackGroundManager.GetInstance().ResetSaturation();

		}
			break;

		}


		m_prevScene = m_currentScene;
		m_currentScene = scene;

		if(scene == GAME_SCENES.SCENE_POST_GAME)
		{
			ParallaxingBackGroundManager.GetInstance().RefreshCurrentBackGround();
		}



	}

	void Update()
	{
		while(Application.isLoadingLevel)
		{
			return;
		}


		//PlayerStatManager.GetInstance().UpdatePlayerManager();
		if(!m_isPaused)
		{
			//AudioManager.GetInstance().UpdateAudioManager();
			ParallaxingBackGroundManager.GetInstance().UpdateBackGround();

			switch (m_currentScene)
			{
				case GAME_SCENES.SCENE_GAME:
				{
					GameplayScript.GetInstance().UpdateGamePlay();
					if(Camera.main.audio.clip != null)
					{
						if(GameplayScript.GetInstance().IsGameOver())
						{
							m_hudScript.UpdateElements(Camera.main.audio.clip.length,Camera.main.audio.clip.length);
						}
						else
						{
							m_hudScript.UpdateElements(Camera.main.audio.clip.length,Camera.main.audio.time);
						}
					}
					else
					{
						m_hudScript.UpdateElements(999,0);
					}
				}

					break;
				default:
				{

				}
					break;
			}


		}

	}

	void OnApplicationPause()
	{
#if UNITY_ANDROID 
		Debug.Log("User pressed escape");
		PlayerStatManager.GetInstance().EndGameSave();
#endif
	}
	void OnApplicationQuit()
	{
		Save();
		PlayerStatManager.GetInstance().EndGameSave();
	}

}
