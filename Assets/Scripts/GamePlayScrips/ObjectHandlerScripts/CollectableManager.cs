﻿using UnityEngine;
using System.Collections;

public class CollectableManager : BaseManager 
{



	public void ClearCollectables()
	{
		if(m_spawnedObjectList.Count > 0)
		{
			for(int i =0 ;i <m_spawnedObjectList.Count; i++)
			{
				if(((BaseObject)m_spawnedObjectList[i]).gameObject != null)
				{
					Destroy(((BaseObject)m_spawnedObjectList[i]).gameObject);
				}
			}
		}
		m_spawnedObjectList.Clear();
	}
	public void ClearCollectables(int referenceNumber)
	{
		
		for(int i =0 ;i <m_spawnedObjectList.Count; i++)
		{
			if(m_spawnedObjectList[i] != null)
			{
				if(((BaseObject)m_spawnedObjectList[i]).m_referenceNumber == referenceNumber)
				{
					Destroy(((BaseObject)m_spawnedObjectList[i]).gameObject);
					m_spawnedObjectList[i] = null;
				}
			}
		}
		
	}
}
