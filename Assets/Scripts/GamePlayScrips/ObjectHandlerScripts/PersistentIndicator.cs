﻿using UnityEngine;
using System.Collections;

public class PersistentIndicator : BaseObject 
{
	
	public GameObject m_linkedObject;
	public bool m_obstacleMode;
	//public bool m_isAlive;
	public int m_opacitySwitch;
	
	void Start () 
	{
		m_opacitySwitch = 1;
	}
	
	
	public void InitIndicator(ClusterObject baseObject,bool obstacleMode)
	{
		m_linkedObject = baseObject.gameObject;
		m_obstacleMode = obstacleMode;
		if(m_obstacleMode)
		{
			this.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("GameObjects/Utilities/ObstacleIndicator") ;
		}
		Color color = this.gameObject.renderer.material.color;
		color.a = 0;
		this.gameObject.renderer.material.color = color;
		
	}
	public void InitIndicator(BaseObject baseObject)
	{
		m_linkedObject = baseObject.gameObject;

		
	}
	
	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				m_isPassified = true;
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				//m_isAlive = false;
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}
	
	protected override void UpdateCreated ()
	{
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	
	protected override void UpdateInGame ()
	{
		float minX = Camera.main.transform.position.x - this.gameObject.renderer.bounds.extents.x;
		float maxX = Camera.main.transform.position.x + (Camera.main.orthographicSize * Camera.main.aspect) - this.gameObject.renderer.bounds.extents.x;
		float minXToEndOfScreen = maxX- minX;
		float curDistBetweenObAndEndOfScreen = m_linkedObject.transform.position.x - m_linkedObject.renderer.bounds.extents.x - maxX;
		if(curDistBetweenObAndEndOfScreen > 0)
		{

			if(!m_obstacleMode)
			{
				this.GetComponent<SpriteRenderer>().sprite = m_linkedObject.GetComponent<SpriteRenderer>().sprite;
				gameObject.transform.rotation = m_linkedObject.transform.rotation;
				this.gameObject.transform.localScale = new Vector2(0.5f * m_linkedObject.transform.localScale.x,0.5f*m_linkedObject.transform.localScale.y);
			}



			if(curDistBetweenObAndEndOfScreen < 15)
			{
				Color color = this.gameObject.renderer.material.color;
				color.a = 1- (curDistBetweenObAndEndOfScreen/ 15);
				this.gameObject.renderer.material.color = color;

				Vector3 pos = this.gameObject.transform.position;



				pos.x = minX + (1-(curDistBetweenObAndEndOfScreen/15)) * minXToEndOfScreen;
				pos.y = m_linkedObject.transform.position.y;
				this.gameObject.transform.position = pos;
				this.gameObject.transform.parent = Camera.main.transform;
			}
		}
		else
		{
			Color color = this.gameObject.renderer.material.color;
			color.a = 0;
			this.gameObject.renderer.material.color = color;
		}
		
	}
	
	
	protected override void UpdateDestroyed ()
	{
		Destroy(this.gameObject);
	}
	
	
}
