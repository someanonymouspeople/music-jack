﻿using UnityEngine;
using System.Collections;

public class CollectableIndicator : BaseObject 
{

	public BaseObject m_linkedObject;

	public bool m_isAlive;
	public int m_opacitySwitch;
	public bool m_timeBased;
	public float m_timeRemaining;
	float m_startTime;
	GameObject m_textMesh;

	void Start () 
	{
		m_opacitySwitch = 1;
	}


	public void InitIndicator(BaseObject baseObject,float timeRemaining = 0)
	{

		m_timeBased = true;
		m_startTime = timeRemaining;
		m_timeRemaining = timeRemaining;

		m_linkedObject = baseObject;
		this.gameObject.layer = LayerMask.NameToLayer("HideWhenPaused");
		this.GetComponent<SpriteRenderer>().sprite = baseObject.GetComponent<SpriteRenderer>().sprite;
		this.gameObject.transform.localScale = new Vector2(1.0f,1.0f);
		this.gameObject.AddComponent<BoxCollider2D>();
		Color color = this.gameObject.renderer.material.color;
		
		color.a =1;
		this.gameObject.renderer.material.color = color;

		this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
		GameObject textMesh = Instantiate(Resources.Load("GameObjects/Utilities/TextMesh")) as GameObject;
		textMesh.transform.parent = gameObject.transform;
		textMesh.transform.position =new Vector2();
		textMesh.transform.localPosition = new Vector2(-0.5f,0);
		m_textMesh = textMesh;

		Color textColor = m_textMesh.renderer.material.color;
		textColor.a = 0;
		m_textMesh.renderer.material.color = textColor;
		m_textMesh.GetComponentInChildren<TextMesh>().transform.localScale = new Vector2(1,1);
		//this.gameObject.transform.parent = Camera.main.transform;
	}

	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				m_isPassified = true;
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				m_isAlive = false;
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}
	
	protected override void UpdateCreated ()
	{
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	
	protected override void UpdateInGame ()
	{


		m_textMesh.GetComponentInChildren<TextMesh>().text = ""+(int)(m_timeRemaining * 60);
		Color color = this.gameObject.renderer.material.color;
		float percentTimeRemaining = (m_timeRemaining/m_startTime) * 100;
		if(percentTimeRemaining < 50)
		{
			color = Color.Lerp(GameplayScript.GetInstance().m_primaryBackGroundColor,Color.white,(percentTimeRemaining/50));
		}
		else
		{
			color.a = 1-((percentTimeRemaining - 50)/50);
			Color textColor = m_textMesh.renderer.material.color;
			textColor.a = color.a;
			m_textMesh.renderer.material.color = textColor;
		}
		//color.a =(1-m_timeRemaining/m_startTime);
		this.gameObject.renderer.material.color = color;
		m_timeRemaining -= Time.deltaTime;
		Vector3 pos = this.gameObject.transform.position;
		pos.x = Camera.main.transform.position.x + ((1-(m_timeRemaining/m_startTime))*(Camera.main.orthographicSize * Camera.main.aspect));
		if(!float.IsInfinity(pos.x))
		{
		this.gameObject.transform.position = pos;
		}
		if(m_timeRemaining<0)
		{
			this.SetState(BASE_OBJECT_STATES.STATE_DESTROYED);
		}
		//this.gameObject.transform.parent = Camera.main.transform;

	}
	
	
	protected override void UpdateDestroyed ()
	{
		Destroy(this.gameObject);
	}
	

}
