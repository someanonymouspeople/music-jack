﻿using UnityEngine;
using System.Collections;


public class ObstacleIndicator : BaseObject 
{
	
	public Obstacle m_linkedObject;
	float m_timeLeftToSpawn;
	float m_startTimeLeft;
	public bool m_isActive;
	public bool m_isAlive;
	public int m_opacitySwitch;
	
	void Start () 
	{
		m_opacitySwitch = 1;
	}
	

	public void InitIndicator(Obstacle baseObject,float timeLeftToSpawnObstacle)
	{
		m_linkedObject = baseObject;
		m_timeLeftToSpawn = timeLeftToSpawnObstacle;
		m_startTimeLeft = timeLeftToSpawnObstacle;

		this.GetComponent<SpriteRenderer>().sprite = baseObject.GetComponent<SpriteRenderer>().sprite;
		this.gameObject.transform.localScale = new Vector2(0.3f,0.3f);
		this.gameObject.AddComponent<BoxCollider2D>();
		this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
		this.transform.parent = Camera.main.transform;
		//this.gameObject.transform.parent = Camera.main.transform;
	}
	
	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				m_isPassified = true;
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				m_isAlive = false;
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}
	
	protected override void UpdateCreated ()
	{
		Color color = this.gameObject.renderer.material.color;

		color.a =0;
		this.gameObject.renderer.material.color = color;
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}

	protected override void UpdateTriggered ()
	{
		//flicker code
		Color color = this.gameObject.renderer.material.color;
		if(color.a > 0.8f )
		{
			m_opacitySwitch *= -1;
			color.a = 0.8f;
		}
		else if(color.a < 0.2f)
		{
			m_opacitySwitch *= -1;
			color.a = 0.2f;
		}
		color.a += Time.deltaTime* ((1-(m_timeLeftToSpawn/m_startTimeLeft)) *2) * m_opacitySwitch;
		this.gameObject.renderer.material.color = color;
	}
	
	protected override void UpdateInGame ()
	{
		m_timeLeftToSpawn -= Time.deltaTime;

		if(m_timeLeftToSpawn < m_startTimeLeft/3)
		{
			Color color = this.gameObject.renderer.material.color;
			
			color.a =1;
			this.gameObject.renderer.material.color = color;
			this.SetState(BASE_OBJECT_STATES.STATE_TRIGGERED);
		}
		

	}
	
	
	protected override void UpdateDestroyed ()
	{
		Destroy(this.gameObject);
	}
	
	
}

