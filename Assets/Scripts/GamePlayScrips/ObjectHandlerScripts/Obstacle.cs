﻿using UnityEngine;
using System.Collections;


public class Obstacle : BaseObject 
{

	public float m_crossingSpeed;
	public float m_speed;
	public void SetCrossingSpeed(float secs)
	{
		m_crossingSpeed = secs;
		m_speed = (Camera.main.orthographicSize*2 * Camera.main.aspect)/ m_crossingSpeed;
	}

	void Start()
	{

	}

	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{

			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{

			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				m_isPassified = true;
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				SetState(BASE_OBJECT_STATES.STATE_DESTROYED);
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}
	
	protected override void UpdateCreated ()
	{
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	
	protected override void UpdateInGame ()
	{
		if(m_isPassified)
		{
			SetState(BASE_OBJECT_STATES.STATE_PASSIFIED);
		}
		if(m_triggered)
		{
			SetState(BASE_OBJECT_STATES.STATE_TRIGGERED);
		}
	}
	
	protected override void UpdateTriggered ()
	{
		if(!m_triggered)
		{
			SetState(BASE_OBJECT_STATES.STATE_INGAME);
		}
	}
	protected override void UpdatePassified ()
	{
		if(!m_isPassified)
		{
			SetState(BASE_OBJECT_STATES.STATE_INGAME);
		}
	}
	
	protected override void UpdateDestroyed ()
	{
		Destroy(this.gameObject);
	}
	
	protected override void FixedMachineUpdate ()
	{
		Vector2 pos = this.gameObject.transform.position;
		pos.x -= m_speed * Time.deltaTime;
		this.gameObject.transform.position = pos;
		
	}
	
}
