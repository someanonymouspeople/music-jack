﻿using UnityEngine;
using System.Collections;

public class AssetIndicatorManager : BaseManager {
	
	float m_indicatorSpawnScreenX;
	
	public BaseObject SpawnPersistentIndicator(int referenceNumber,Vector2 pos, BaseObject asset)
	{
		Vector3 posToPlace = new Vector3();
		posToPlace.x = pos.x - asset.renderer.bounds.extents.x;
		posToPlace.y = pos.y;
		posToPlace.z = 0;
		BaseObject go = Instantiate(m_objectReferences[3],posToPlace,new Quaternion()) as BaseObject;
		go.m_referenceNumber = referenceNumber;
		m_spawnedObjectList.Add(go);
		go.GetComponent<PersistentIndicator>().InitIndicator(asset);
		return go;
	}
	public BaseObject SpawnPersistentIndicator(int referenceNumber,Vector2 pos, ClusterObject asset,bool isObstacle)
	{
		Vector3 posToPlace = new Vector3();
		posToPlace.x = pos.x - asset.renderer.bounds.extents.x;
		posToPlace.y = pos.y;
		posToPlace.z = 0;
		BaseObject go = Instantiate(m_objectReferences[3],posToPlace,new Quaternion()) as BaseObject;
		go.m_referenceNumber = referenceNumber;
		m_spawnedObjectList.Add(go);
		go.GetComponent<PersistentIndicator>().InitIndicator(asset,isObstacle);
		return go;
	}

	public BaseObject SpawnCollectableIndicator(int referenceNumber,Vector2 pos,BaseObject collectable,float timeRemaining = 0)
	{
		Vector3 posToPlace = new Vector3();
		posToPlace.x = 0;// pos.x - collectable.renderer.bounds.extents.x;
		posToPlace.y = pos.y;
		posToPlace.z = 0;
		BaseObject go = Instantiate(m_objectReferences[0],posToPlace,new Quaternion()) as BaseObject;
		go.m_referenceNumber = referenceNumber;
		m_spawnedObjectList.Add(go);
		go.GetComponent<CollectableIndicator>().InitIndicator(collectable,timeRemaining);
		return go;
	}
	
	public BaseObject SpawnObstacleIndicator(int referenceNumber, Vector2 pos,Obstacle Obstacle,float timeToSpawnObstacle)
	{
		Vector3 posToPlace = new Vector3();
		posToPlace.x = pos.x - Obstacle.renderer.bounds.extents.x * 0.5f;
		posToPlace.y = pos.y;
		posToPlace.z = 0;
		BaseObject go = Instantiate(m_objectReferences[1],posToPlace,new Quaternion()) as BaseObject;
		go.m_referenceNumber = referenceNumber;
		m_spawnedObjectList.Add(go);
		go.GetComponent<ObstacleIndicator>().InitIndicator(Obstacle,timeToSpawnObstacle);
		return go;
	}
	
	public BaseObject SpawnHeroIndicator(int referenceNumber)
	{
		BaseObject go = Instantiate(m_objectReferences[2],new Vector3(),new Quaternion()) as BaseObject;
		go.m_referenceNumber = referenceNumber;
		m_spawnedObjectList.Add(go);
		go.GetComponent<HeroIndicator>().InitIndicator(GameplayScript.GetInstance().GetHero());
		return go;
	}
	
	public override void UpdateManager ()
	{
		m_objectCount = m_spawnedObjectList.Count;
		foreach(BaseObject bo in m_spawnedObjectList)
		{
			if(bo)
			{
				bo.UpdateStateMachine();
			}
			else
			{
				m_toRemoveList.Add(bo);
			}
			
		}
		
		foreach(BaseObject bo in m_toRemoveList)
		{
			
			m_spawnedObjectList.Remove(bo);
		}
		
	}
	
	public void ClearIndicatorList()
	{
		if(m_spawnedObjectList.Count>0)
		{
			for(int i =0 ;i <m_spawnedObjectList.Count; i++)
			{
				if(m_spawnedObjectList[i] != null && ((BaseObject)m_spawnedObjectList[i]).gameObject !=null)
				{
					Destroy(((BaseObject)m_spawnedObjectList[i]).gameObject);
				}
			}
		}
		m_spawnedObjectList.Clear();
	}
	public void ClearIndicatorList(int referenceNumber)
	{

		for(int i =0 ;i <m_spawnedObjectList.Count; i++)
		{
			if(m_spawnedObjectList[i] != null)
			{
			if(((BaseObject)m_spawnedObjectList[i]).m_referenceNumber == referenceNumber)
			{
				Destroy(((BaseObject)m_spawnedObjectList[i]).gameObject);
				m_spawnedObjectList[i] = null;
			}
			}
		}
	
	}
}
