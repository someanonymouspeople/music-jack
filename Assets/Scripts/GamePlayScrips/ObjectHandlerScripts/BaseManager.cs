﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseManager : MonoBehaviour 
{

	//variables
	public BaseObject[] m_objectReferences;
	public ArrayList m_spawnedObjectList;
	protected ArrayList m_toRemoveList;
	public float m_objectCount;
	public GameObject m_playerReference;
	public float m_spawnDistanceFromPlayer;
	public float m_destroyThreshold;
	public float m_constantCoOrd;
	public Vector3 m_prevSpawnPos;

	//virtual methods
	virtual public BaseObject SpawnRandomObject(Vector3 pos,bool randomRotate = false, bool randomSize = false, float maxScale = 1,float minScale = 1)
	{
		int objectToSpawnDecider = Random.Range(0,m_objectReferences.Length*10)/10;
		Quaternion rotation = new Quaternion();
		Vector2 scale = new Vector2(1,1);
		pos.z = -1;
		if(randomRotate)
		{

			rotation = Quaternion.AngleAxis(Random.Range(0,360),this.transform.forward);
		}

		if(randomSize)
		{
			float scaleValue = Random.Range(minScale,maxScale);
			scale = new Vector2(scaleValue,scaleValue);
		}
		BaseObject go = Instantiate(m_objectReferences[objectToSpawnDecider],pos,rotation) as BaseObject;
		go.transform.localScale = scale;
		m_spawnedObjectList.Add(go);
		return go;
	}

	virtual public BaseObject SpawnObject(int referenceNumber,Vector3 pos,bool randomRotate = false, bool randomSize = false, float maxScale = 1,float minScale = 1)
	{
		Quaternion rotation = new Quaternion();
		Vector2 scale = new Vector2(1,1);
		pos.z = -1;
		if(randomRotate)
		{
			
			rotation = Quaternion.AngleAxis(Random.Range(0,360),this.transform.forward);
		}
		
		if(randomSize)
		{
			float scaleValue = Random.Range(minScale,maxScale);
			scale = new Vector2(scaleValue,scaleValue);
		}
		BaseObject go = Instantiate(m_objectReferences[referenceNumber],pos,rotation) as BaseObject;
		go.transform.localScale = scale;
		m_spawnedObjectList.Add(go);
		return go;
	}

	virtual public BaseObject SpawnObject(int baseReferenceNumber,int objectReferenceNumber,Vector3 pos,bool randomRotate = false, bool randomSize = false, float maxScale = 1,float minScale = 1)
	{
		Quaternion rotation = new Quaternion();
		Vector2 scale = new Vector2(1,1);
		pos.z = -1;
		if(randomRotate)
		{
			
			rotation = Quaternion.AngleAxis(Random.Range(0,360),this.transform.forward);
		}
		
		if(randomSize)
		{
			float scaleValue = Random.Range(minScale,maxScale);
			scale = new Vector2(scaleValue,scaleValue);
		}
		BaseObject go = Instantiate(m_objectReferences[objectReferenceNumber],pos,rotation) as BaseObject;
		go.m_referenceNumber = baseReferenceNumber;
		go.transform.localScale = scale;
		m_spawnedObjectList.Add(go);
		return go;
	}


	public virtual void UpdateManager () 
	{
		m_objectCount = m_spawnedObjectList.Count;
		foreach(BaseObject bo in m_spawnedObjectList)
		{
			if(bo)
			{
			bo.UpdateStateMachine();
			if(bo.transform.position.x -m_destroyThreshold <m_playerReference.transform.position.x)
			{
				bo.SetState(BASE_OBJECT_STATES.STATE_LEFT);
			}
			}
			else
			{
				m_toRemoveList.Add(bo);
			}


		}

		foreach(BaseObject bo in m_toRemoveList)
		{
			m_spawnedObjectList.Remove(bo);
		}

	}
	
	//regular methods
	void Awake()
	{
		m_spawnedObjectList = new ArrayList();
		m_toRemoveList = new ArrayList();
	}
}
