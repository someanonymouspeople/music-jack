﻿using UnityEngine;
using System.Collections;


public class HeroIndicator : BaseObject 
{
	
	public GameObject m_hero;
	bool m_isHidden;
	public bool m_isAlive;
	public int m_opacitySwitch;
	
	void Start () 
	{
		m_isHidden = true;
		m_opacitySwitch = 1;
	}
	
	
	public void InitIndicator(GameObject hero)
	{
		m_hero = hero;

		this.GetComponent<SpriteRenderer>().sprite = hero.GetComponentInChildren<SpriteRenderer>().sprite;
		this.gameObject.transform.localScale = new Vector2(0.8f,0.8f);
		this.gameObject.AddComponent<BoxCollider2D>();
		this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
		//this.gameObject.transform.parent = Camera.main.transform;
	}
	
	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				
			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				m_isPassified = true;
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				m_isAlive = false;
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}
	
	protected override void UpdateCreated ()
	{
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	
	protected override void UpdateInGame ()
	{
		//positioning code
		float camTopY = Camera.main.transform.position.y + Camera.main.orthographicSize;
		Vector3 heroPos = m_hero.transform.position;
		if(!(heroPos.y > camTopY))
		{
			m_isHidden = true;
			this.gameObject.renderer.enabled = false;
			//this.gameObject.renderer.material.color = new Color(1,1,1,0);
			return;
		}
		else 
		{
			if(m_isHidden)
			{
				this.gameObject.renderer.enabled = true;
				m_isHidden = false;
				Vector2 posToPlace = new Vector2(m_hero.transform.GetChild(0).transform.position.x, camTopY -1.66f );
				this.gameObject.transform.position = posToPlace;
			}
			else
			{
				//flicker code
				Color color = this.gameObject.renderer.material.color;
				if(color.a > 0.8f )
				{
					m_opacitySwitch *= -1;
					color.a = 0.8f;
				}
				else if(color.a < 0.2f)
				{
					m_opacitySwitch *= -1;
					color.a = 0.2f;
				}
				color.a += Time.deltaTime*  ((m_hero.transform.transform.position.y-this.gameObject.transform.position.y)/5) * 5 * m_opacitySwitch;
				this.gameObject.renderer.material.color = color;

				//reposition code
				Vector2 posToPlace = new Vector2(m_hero.transform.GetChild(0).transform.position.x, camTopY -1.66f );
				this.gameObject.transform.position = posToPlace;
				this.gameObject.transform.rotation = m_hero.transform.GetChild(0).transform.rotation;
			}
		}

	}
	
	
	protected override void UpdateDestroyed ()
	{
		Destroy(this.gameObject);
	}
	
	
}

