﻿using UnityEngine;
using System.Collections;

public class Collectable : BaseObject {
	
	public int m_score;
	public float m_speed;
	public JunkieScript m_scriptToTrack;
	float m_timeToReach;
	float m_timeRemaining;
	public float m_spawnY;
	//public bool m_isBeatBased;

	public static float Lerp(float a, float b, float t)
	{
		return a+((b-a)*t);
	}

	public void InitBeatBasedCollectable(JunkieScript scriptToTrack,float travelTime,Color colorToSet)
	{
		m_spawnY = this.gameObject.transform.position.y;
		m_timeRemaining = travelTime;
		m_timeToReach=travelTime;
		this.gameObject.transform.parent = Camera.main.transform;
		//m_isBeatBased =true;

		m_scriptToTrack = scriptToTrack;

		ParticleSystem sys = this.GetComponentInChildren<ParticleSystem>();
		if(sys != null)
		{
			sys.startColor = colorToSet;
		}


	}
	protected override void InitStateChanges ()
	{
		if(m_currentState != m_nextState)
		{
			switch (m_nextState)
			{
			case BASE_OBJECT_STATES.STATE_CREATED:
			{
				//this.SetState(BASE_OBJECT_STATES.STATE_INGAME);
			}
				break;
			case BASE_OBJECT_STATES.STATE_INGAME:
			{

			}
				break;
			case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				GameplayScript.GetInstance().IncrementBeatsCollected(1);
				GameplayScript.GetInstance().IncrementMultiplierBasedOnDifficulty();
				GameplayScript.GetInstance().IncrementScore(m_score);
				GameplayScript.GetInstance().IncrementPowerUpCountHolder(1);
				this.SetState(BASE_OBJECT_STATES.STATE_DESTROYED);

			}
				break;
			case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				this.SetState(BASE_OBJECT_STATES.STATE_DESTROYED);
			}
				break;
				
			case BASE_OBJECT_STATES.STATE_DESTROYED:
			{

				Destroy(this.gameObject);
			}
				break;
			case BASE_OBJECT_STATES.STATE_LEFT:
			{
				//GameplayScript.GetInstance().GetHeroScript().UnHype();
				//GameplayScript.GetInstance().DecrementMultiplierByFactor(4);
				GameplayScript.GetInstance().ResetPowerUpCountHolder();
				SetState(BASE_OBJECT_STATES.STATE_DESTROYED);
			}
				break;
				
			}
			m_prevState = m_currentState;
			m_currentState = m_nextState;
		}
	}

	protected override void UpdateCreated ()
	{

		this.SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	
	protected override void UpdatePassified ()
	{

	}

	protected override void UpdateTriggered ()
	{

	}

	protected override void UpdateInGame ()
	{

	}

	protected override void FixedMachineUpdate ()
	{

		m_timeRemaining -= Time.deltaTime;
		Vector3 camPos = Camera.main.transform.position;

		float distanceToCover = (camPos.x + Camera.main.orthographicSize * Camera.main.aspect) - (m_scriptToTrack.transform.position.x + m_scriptToTrack.GetSprite().renderer.bounds.extents.x);                    
		float timeToCoverIn = (1-m_scriptToTrack.GetRelativeSpeed()) * GameplayScript.GetInstance().m_beatMaxSpawnTime;

		float startX = camPos.x + Camera.main.orthographicSize * Camera.main.aspect;
		float endX = startX - distanceToCover;

		float lerpValue = 1-(m_timeRemaining/m_timeToReach);

		Vector2 pos = this.transform.position;
		pos.x = Lerp(startX,endX,lerpValue);
		this.gameObject.transform.position = pos;
	
		if(this.transform.position.x < camPos.x -10 )
		{
			this.SetState(BASE_OBJECT_STATES.STATE_LEFT);
		}
	}


}
