﻿using UnityEngine;
using System.Collections;

public enum BASE_OBJECT_STATES
{
	STATE_CREATED,
	STATE_INGAME,
	STATE_TRIGGERED,
	STATE_PASSIFIED,
	STATE_DESTROYED,

	STATE_LEFT
};
public abstract class BaseObjectStateMachine : MonoBehaviour 
{

	public BASE_OBJECT_STATES m_currentState;
	public BASE_OBJECT_STATES m_prevState;
	public BASE_OBJECT_STATES m_nextState;

	public BASE_OBJECT_STATES GetState()
	{
		return m_currentState;
	}
	protected virtual void UpdateCreated()
	{
		SetState(BASE_OBJECT_STATES.STATE_INGAME);
	}
	protected virtual void UpdateInGame()
	{
		SetState(BASE_OBJECT_STATES.STATE_TRIGGERED);
	}
	protected virtual void UpdateTriggered()
	{
		SetState(BASE_OBJECT_STATES.STATE_PASSIFIED);
	}
	protected virtual void UpdatePassified()
	{
		SetState(BASE_OBJECT_STATES.STATE_DESTROYED);
	}
	protected virtual void UpdateDestroyed()
	{
		Destroy(this.gameObject);
	}

	protected virtual void FixedMachineUpdate()
	{

	}
	public void SetState(BASE_OBJECT_STATES state)
	{
		m_nextState = state;
	}

	abstract protected void InitStateChanges();

	public void UpdateStateMachine()
	{
		FixedMachineUpdate();
		InitStateChanges();
		switch(m_currentState)
		{
		case BASE_OBJECT_STATES.STATE_CREATED:
			{
				UpdateCreated();
			}
			break;
		case BASE_OBJECT_STATES.STATE_INGAME:
			{
				UpdateInGame();
			}
			break;
		case BASE_OBJECT_STATES.STATE_TRIGGERED:
			{
				UpdateTriggered();
			}
			break;
		case BASE_OBJECT_STATES.STATE_PASSIFIED:
			{
				UpdatePassified();
			}
			break;
		case BASE_OBJECT_STATES.STATE_DESTROYED:
			{
				UpdateDestroyed();
			}
			break;
		case BASE_OBJECT_STATES.STATE_LEFT:
			{
				UpdateDestroyed();
			}
			break;
		}

	}

}
