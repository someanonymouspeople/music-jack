﻿using UnityEngine;
using System.Collections;

public abstract class BaseObject : BaseObjectStateMachine 
{
	protected bool m_triggered;
	protected bool m_isPassified;
	public int m_referenceNumber;
	//public BaseObject m_baseObject;

	public void Awake()
	{
		m_referenceNumber = 0;
	}

	public void SetPassified(bool passified)
	{
		m_isPassified = passified;
	}

	public void SetTriggered(bool triggered)
	{
		m_triggered = triggered;
	}
	void Start () 
	{
		m_isPassified = false;
		m_currentState = BASE_OBJECT_STATES.STATE_CREATED;
		m_nextState = BASE_OBJECT_STATES.STATE_CREATED;
		m_prevState = BASE_OBJECT_STATES.STATE_CREATED;
	}
	
}
