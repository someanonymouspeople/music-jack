﻿using UnityEngine;
using System.Collections;

public class SetSortingLayer : MonoBehaviour 
{
	public string sortingLayerName;       // The name of the sorting layer .
	public int sortingOrder;      //The sorting order

	void Awake ()
	{
		// Set the sorting layer and order.
		renderer.sortingLayerName = sortingLayerName;
		renderer.sortingOrder=sortingOrder;
	
	}

}