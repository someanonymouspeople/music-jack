using UnityEngine;
using System.Collections;

public class OrbitingPosRandomizers : PosRandomizer 
{
	public GameObject m_objectToOrbit;
	public float m_orbitDistVariance;
	float m_speedRelativeToOrbit;
	float m_halfOrbitLength;
	public Vector2 m_orbitDir;
	public Vector2 m_orbitDirRangeX;
	public Vector2 m_orbitDirRangeY;
	float m_orbitDistOffsetFromEnd;
	float m_totalOrbitDist;
	Vector2 m_relativeScale;
	public bool m_elliptical;
	public bool m_fixedOrbit;
	public float m_debugRelativeDistToEndOfOrbit;
	public float m_speedMultiplier;
	public int m_orbitOrder;
	bool m_inFront;
	public bool m_startFromCenter;
	public float m_minSpeedPercentAtEndOfOrbit;

	new public void Awake()
	{
		base.Awake();
		m_orbitDirRangeX.Normalize();
		m_orbitDirRangeY.Normalize();

		GameObject orbiterHolder = new GameObject();
		orbiterHolder.name = this.gameObject.name + " container";
		orbiterHolder.transform.position = new Vector3();

		if(m_objectToOrbit == null)
		{
			orbiterHolder.transform.parent = transform.parent.gameObject.transform;
		}
		else
		{
			orbiterHolder.transform.parent = m_objectToOrbit.transform;
		}

		this.transform.parent = orbiterHolder.transform;
		m_inFront = true;

	}

	private void FillOrbitVars()
	{
		if(!m_fixedOrbit)
		{
			m_orbitDir = new Vector2(Random.Range(m_orbitDirRangeX.x *10,m_orbitDirRangeX.y * 10)/10,Random.Range(m_orbitDirRangeY.x * 10,m_orbitDirRangeY.y*10)/10);
			m_orbitDir.Normalize();
			m_orbitDistOffsetFromEnd = Random.Range(this.renderer.bounds.extents.x*10,(this.renderer.bounds.extents.x + m_orbitDistVariance)*10)/10;
			m_totalOrbitDist = new Vector2(((m_objectToOrbit.renderer.bounds.size.x +m_orbitDistOffsetFromEnd*2)* m_orbitDir.x),((m_objectToOrbit.renderer.bounds.size.y + m_orbitDistOffsetFromEnd*2) * m_orbitDir.y)).magnitude;
		}
		else
		{
			m_orbitDir = new Vector2(Random.Range(m_orbitDirRangeX.x *10,m_orbitDirRangeX.y * 10)/10,Random.Range(m_orbitDirRangeY.x * 10,m_orbitDirRangeY.y*10)/10);
			m_orbitDir.Normalize();
			m_orbitDistOffsetFromEnd = this.renderer.bounds.size.x + m_orbitDistVariance;
			m_totalOrbitDist = new Vector2(((m_objectToOrbit.renderer.bounds.size.x +m_orbitDistOffsetFromEnd*2)* m_orbitDir.x),((m_objectToOrbit.renderer.bounds.size.y + m_orbitDistOffsetFromEnd*2) * m_orbitDir.y)).magnitude;
		}
	}

	private bool CheckIfOrbitEndHit()
	{
		if(Vector2.Distance(this.transform.position,m_objectToOrbit.transform.position) > (m_totalOrbitDist/2 + m_orbitDistOffsetFromEnd))
		{
			return true;
		}
		return false;
	}

	public float GetObjectToOrbitSpeed()
	{
		return ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetLayerSpeed(m_objectToOrbit.renderer.sortingOrder);
	}

	public override void Init (bool offScreen)
	{

		FillOrbitVars();
		m_reSpawning = false;
		if(m_spinWhileMoving)
		{
			if(m_spinDegreeVariance > 360 || m_spinDegreeVariance <-360)
			{
				m_spinDegreeVariance = 360;
			}
		}
		if(!m_objectToOrbit.GetComponent<OrbitingPosRandomizers>())
		{
			m_speed = ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetLayerSpeed(m_objectToOrbit.renderer.sortingOrder);
		}
		else
		{
			m_speed = m_objectToOrbit.GetComponent<OrbitingPosRandomizers>().GetObjectToOrbitSpeed();
		}
		m_speed = Random.Range(Mathf.Clamp(m_speed-m_speedVariance,0,m_speed), m_speed+m_speedVariance);

		m_speed *= m_speedMultiplier;
		this.renderer.sortingOrder = m_objectToOrbit.renderer.sortingOrder +m_orbitOrder;

		float orthographicCamSize = Camera.main.orthographicSize;
		ScreenSizeInUnits.y = orthographicCamSize *2;
		ScreenSizeInUnits.x = ScreenSizeInUnits.y* Camera.main.aspect;

		RandomizeAndAssignAttributes();

	}

	protected void RandomizeAndAssignAttributes ()
	{
		Vector3 quadPos;
		Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2(0,0));
		m_spinDegreesPerSec = Random.Range(-((float)m_spinDegreeVariance)*10,(float)m_spinDegreeVariance*10)/10;
		quadPos = this.gameObject.transform.parent.position;
		

		float randomY=0;
		float randomX=0;

		randomX = m_objectToOrbit.transform.position.x;
		randomY = m_objectToOrbit.transform.position.y;
		if(!m_startFromCenter)
		{
			Vector2 dirVec = m_orbitDir;
			if(dirVec.x < 0)
			{
				dirVec.x *= -1;
			}
			if(dirVec.y < 0)
			{
				dirVec.y *=-1;
			}
			if(dirVec.x ==0 || dirVec.y == 0)
			{
			randomX = Random.Range(m_objectToOrbit.transform.position.x -((m_objectToOrbit.renderer.bounds.extents.x  - m_orbitDistOffsetFromEnd) * dirVec.x ),m_objectToOrbit.transform.position.x +((m_objectToOrbit.renderer.bounds.extents.x +m_orbitDistOffsetFromEnd)* dirVec.x));
			randomY = Random.Range(m_objectToOrbit.transform.position.y -((m_objectToOrbit.renderer.bounds.extents.y-m_orbitDistOffsetFromEnd) * dirVec.y),m_objectToOrbit.transform.position.y +((m_objectToOrbit.renderer.bounds.extents.y +m_orbitDistOffsetFromEnd)* dirVec.y));
			}
			else
			{
				randomX = Random.Range(m_objectToOrbit.transform.position.x -((m_objectToOrbit.renderer.bounds.extents.x  - m_orbitDistOffsetFromEnd) * dirVec.x ),m_objectToOrbit.transform.position.x +((m_objectToOrbit.renderer.bounds.extents.x +m_orbitDistOffsetFromEnd)* dirVec.x));
				float distBetween = randomX - m_objectToOrbit.transform.position.x;
				randomY = (m_objectToOrbit.transform.position.y + ((distBetween) * m_orbitDir.x ));
			}

		}
		//set transform
		quadPos = new Vector3 (randomX,randomY,quadPos.z);
		//find and set scale
		float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
		Vector2 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
		m_currentScale = scale;
		this.gameObject.transform.localScale = scale;
		this.gameObject.transform.parent.position = quadPos;
				
			
	} 



	private void CheckAndSpin()
	{
		if(m_spinWhileMoving)
		{
			Vector3 currentRotation = this.transform.rotation.eulerAngles;
			currentRotation.z+= m_spinDegreeVariance*Time.deltaTime;
			//catch exceptions in rotation and change appropriately
			if(currentRotation.z > 360)
			{
				currentRotation.z = 0;
			}
			else if(currentRotation.z < 0 )
			{
				currentRotation.z = 360;
			}
			
			this.transform.eulerAngles = currentRotation;
		}
	}

	public GameObject GetRootObjectToOrbit()
	{
		if(m_objectToOrbit.GetComponent<OrbitingPosRandomizers>())
		{
			return m_objectToOrbit.GetComponent<OrbitingPosRandomizers>().GetRootObjectToOrbit();
		}
		return m_objectToOrbit;
	}

	private Vector3 GetEllipticalOrbitOffsetVector(Vector2 eyePos,float relativeDistanceToEndOfOrbit,float illusionPercentage)
	{
		//Get screen dimensions to calculate max possible travel
		Vector2 screenDimensions = new Vector2(Camera.main.orthographicSize*2 * Camera.main.aspect,Camera.main.orthographicSize *2);
		Vector2 screenOffset = Camera.main.ScreenToWorldPoint(Vector2.zero);
		Vector2 maxIncrementAlongTheAxes = screenDimensions * illusionPercentage;

		//Get distance between object and eye and find relative distance between ob and end of screen
		float distanceBetweenEyeAndObAlongY = Vector2.Distance(new Vector2(0,eyePos.y),new Vector2(0,this.gameObject.transform.parent.position.y));
		float distanceBetweenEyeAndObAlongX = Vector2.Distance(new Vector2(eyePos.x,0),new Vector2(this.gameObject.transform.parent.position.x,0));
		float relativeDistanceY = distanceBetweenEyeAndObAlongY/Vector2.Distance(new Vector2(0,eyePos.y),new Vector2(0,screenOffset.y + screenDimensions.y));
		float relativeDistanceX = distanceBetweenEyeAndObAlongX/Vector2.Distance(new Vector2(eyePos.x,0),new Vector2(screenOffset.x + screenDimensions.x,0));

		//calculate required increment based on relative distance of object from eye
		Vector3 incrementAlongTheAxes = Vector2.zero;
		incrementAlongTheAxes.x = maxIncrementAlongTheAxes.x*relativeDistanceX* relativeDistanceToEndOfOrbit;
		incrementAlongTheAxes.y = maxIncrementAlongTheAxes.y*relativeDistanceY* relativeDistanceToEndOfOrbit;

		//invert the increment along axes to get accurate results
		incrementAlongTheAxes.x *= m_orbitDir.y;
		incrementAlongTheAxes.y *= m_orbitDir.x;
		incrementAlongTheAxes.z = this.transform.position.z;
		//if the eyeposition is behind object make object move front and vice versa
		if((eyePos.x - this.transform.parent.position.x) > 0)
		{
			incrementAlongTheAxes.x *=-1;
		}
		//return increment
		return incrementAlongTheAxes;
	}

	private void CheckAndResetAlongOrbit()
	{
		//Maintain object within orbit
		Vector2 dirVec = m_orbitDir;
		if(dirVec.x < 0)
		{
			dirVec.x *= -1;
		}
		if(dirVec.y < 0)
		{
			dirVec.y *=-1;
		}
		Vector2 posToSet = this.gameObject.transform.parent.position;
		if(this.gameObject.transform.parent.position.x> m_objectToOrbit.transform.position.x + (m_totalOrbitDist/2 * dirVec.x))
		{
			posToSet.x = m_objectToOrbit.transform.position.x + (m_totalOrbitDist/2 * dirVec.x);
		}
		if(this.gameObject.transform.parent.position.x < m_objectToOrbit.transform.position.x - (m_totalOrbitDist/2 * dirVec.x))
		{
			posToSet.x = m_objectToOrbit.transform.position.x - (m_totalOrbitDist/2 * dirVec.x);
		}
		if(this.gameObject.transform.parent.position.y> m_objectToOrbit.transform.position.y + (m_totalOrbitDist/2 * dirVec.y))
		{
			posToSet.y = m_objectToOrbit.transform.position.y + (m_totalOrbitDist/2 * dirVec.y);
		}
		if(this.gameObject.transform.parent.position.y< m_objectToOrbit.transform.position.y - (m_totalOrbitDist/2 * dirVec.y))
		{
			posToSet.y = m_objectToOrbit.transform.position.y - (m_totalOrbitDist/2 * dirVec.y);
		}
		this.gameObject.transform.parent.position = posToSet;
	}

	public override void UpdateRandomizer (float songLength)
	{
		Vector3 quadPos = this.transform.parent.position;
		//if need to spin, spin
		CheckAndSpin();
			
		//update motion
		float relativeDistanceToEndOfOrbit = 1- (Vector2.Distance(this.gameObject.transform.parent.position,this.m_objectToOrbit.transform.position)/(m_totalOrbitDist/2));
		if(relativeDistanceToEndOfOrbit < 0.001f)
		{
			m_inFront = !m_inFront;
			m_orbitDir *= -1;
			CheckAndResetAlongOrbit();
		}

		if(m_inFront)
		{
			this.renderer.sortingOrder = m_objectToOrbit.renderer.sortingOrder+m_orbitOrder;

		}
		else
		{
			this.renderer.sortingOrder = m_objectToOrbit.renderer.sortingOrder-m_orbitOrder;

		}
		float smoothenedSpeed = 0;

		smoothenedSpeed = Mathf.Lerp(m_speed * m_minSpeedPercentAtEndOfOrbit/100,m_speed,relativeDistanceToEndOfOrbit);

		m_debugRelativeDistToEndOfOrbit = relativeDistanceToEndOfOrbit;


		float ETA = songLength/smoothenedSpeed;
		float unitsPerSecond = m_totalOrbitDist/ETA;
		Vector3 increment = m_orbitDir *unitsPerSecond * Time.deltaTime;
		
		quadPos = this.gameObject.transform.parent.localPosition;
		quadPos +=increment;
		if(m_elliptical)
		{
			this.gameObject.transform.localPosition = GetEllipticalOrbitOffsetVector(Camera.main.transform.position,relativeDistanceToEndOfOrbit,0.125f);
		}
		this.gameObject.transform.parent.localPosition = quadPos;

		if(m_canScaleWithEnergy && Camera.main.audio.clip != null)
		{
			m_toneBandToDeriveEnergy = Mathf.Clamp(m_toneBandToDeriveEnergy,0,System.Enum.GetNames(typeof(DETECTION_TONE)).Length-1);
			float maxVolume = 100;
			float instantVolume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.008f)[m_toneBandToDeriveEnergy];
			float scalePercentToSet = m_minScalePercent + ((instantVolume/maxVolume) * (m_maxScalePercent- m_minScalePercent));
			Vector2 scaleToSet = new Vector2(m_currentScale.x * (scalePercentToSet/100),m_currentScale.y * (scalePercentToSet/100));
			this.transform.localScale = scaleToSet;
		}



	}

	public override void UpdateRandomizer ()
	{
		Vector3 quadPos = this.transform.parent.position;
		//if need to spin, spin
		CheckAndSpin();
		
		//update motion
		float relativeDistanceToEndOfOrbit = 1- (Vector2.Distance(this.gameObject.transform.parent.position,this.m_objectToOrbit.transform.position)/(m_totalOrbitDist/2));
		if(relativeDistanceToEndOfOrbit < 0.001f)
		{
			m_inFront = !m_inFront;
			m_orbitDir *= -1;
			CheckAndResetAlongOrbit();
		}
		
		if(m_inFront)
		{
			this.renderer.sortingOrder = m_objectToOrbit.renderer.sortingOrder+m_orbitOrder;
		}
		else
		{
			this.renderer.sortingOrder = m_objectToOrbit.renderer.sortingOrder-m_orbitOrder;
		}
		m_speedRelativeToOrbit = m_speed*relativeDistanceToEndOfOrbit;
		
		if(m_speedRelativeToOrbit < m_speed * 0.1f)
		{
			m_speedRelativeToOrbit = m_speed * 0.1f;
		}

		Vector3 increment = m_orbitDir *m_speedRelativeToOrbit/100 * Time.deltaTime;
		
		quadPos = this.gameObject.transform.parent.localPosition;
		quadPos +=increment;
		if(m_elliptical)
		{
			this.gameObject.transform.localPosition = GetEllipticalOrbitOffsetVector(Camera.main.transform.position,relativeDistanceToEndOfOrbit,0.125f);
		}
		this.gameObject.transform.parent.localPosition = quadPos;

	}



}
