﻿using UnityEngine;
using System.Collections;


public enum RANDOMIZED_MOVE_DIRECTION
{
	DIRECTION_LEFT,
	DIRECTION_RIGHT,
	DIRECTION_TOP,
	DIRECTION_BOTTOM,
	DIRECTION_DIAGONAL_LR_TOP,
	DIRECTION_DIAGONAL_RL_TOP,
	DIRECTION_DIAGONAL_LR_BOT,
	DIRECTION_DIAGONAL_RL_BOT

};

public enum SPECIALIZED_DIR_ATTS
{
	DIR_ATTS_NONE,
	DIAGONAL_LR_TOP,
	DIAGONAL_RL_TOP,
	DIAGONAL_LR_BOT,
	DIAGONAL_RL_BOT

}
public class ClusterRandomizer : BGElementProperties 
{
	// sprites to set at random
	public Sprite[] m_spriteReferences;
	//current objects list
	GameObject[] m_objectReferences;

	//Design variables
	//general
	public int m_numberOfObjects;
	public int m_sortingLayer;
	public string m_sortingLayerName;

	//spawn related

	//spawn variance
	public bool m_obstacleMode;
	public bool m_firstSpawnInScreen;
	bool m_shouldSpawnInScreen = false;
	public bool m_lockToTopOrBot;
	public Vector2[] m_spawnExtremes;
	public Vector2 m_scaleVarianceAllowance;
	RANDOMIZED_MOVE_DIRECTION m_direction = RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT;
	SPECIALIZED_DIR_ATTS m_dirAtts = SPECIALIZED_DIR_ATTS.DIR_ATTS_NONE;
	float m_minDistanceBetweenSpawn;
	float m_maxDistanceBetweenSpawn;

	//motion variance
	bool m_canMoveDiagonally = true;
	bool m_shouldSpin = false;
	int m_spinChance;
	int m_spinVariance;
	bool m_ofVaryingSpeed =true;
	public float m_speedVariance;
	public float m_directionalVarianceAllowance;
	public bool m_randomizeSpawnSpin;

	//helper variables
	Vector2 m_screenSizeInUnits;
	bool m_playInMenus = true;
	public bool m_selfSpawnIndicators;
	public bool m_spawnWithBorder;
	float m_relativeBorderSize = 0.2f;
	public float m_minObstacleSpawnTime;
	public float m_maxObstacleSpawnTime;

	void Start () 
	{
		if(m_spinVariance > 360 || m_speedVariance < -360)
		{
			m_spinVariance = 360;
		}

	}

	public void Init()
	{

	
		GameplayScript gameplayScriptRef = GameplayScript.GetInstance();
		float orthographicCamSize = Camera.main.orthographicSize;
		m_screenSizeInUnits.y = orthographicCamSize *2;
		m_screenSizeInUnits.x = m_screenSizeInUnits.y* Camera.main.aspect;
		m_objectReferences = new GameObject[m_numberOfObjects];
		for(int i = 0; i<m_objectReferences.Length;i++)
		{
			m_objectReferences[i] = new GameObject();
			m_objectReferences[i].gameObject.transform.parent = transform.parent;
			m_objectReferences[i].AddComponent<SpriteRenderer>();
			m_objectReferences[i].AddComponent<BGElementProperties>();
			m_objectReferences[i].GetComponent<BGElementProperties>().CanBeSaturated(m_canBeSaturated);
			m_objectReferences[i].name = this.name + "ClusterObject "+ i.ToString();
			m_objectReferences[i].AddComponent<ClusterObject>();
			m_objectReferences[i].renderer.sortingLayerName =m_sortingLayerName;
			m_objectReferences[i].renderer.sortingOrder = m_sortingLayer;
			m_objectReferences[i].transform.parent = this.transform;
			AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_firstSpawnInScreen,m_randomizeSpawnSpin);
			m_objectReferences[i].GetComponent<SpriteRenderer>().material.shader = Resources.Load("Shaders/GreyScaleShader") as Shader;

			//additional checks to perform if the app is in the GameScene
			if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
			{
				if(m_selfSpawnIndicators)
				{
					AssetIndicatorManager aim = GameplayScript.GetInstance().GetIndicatorManager();
					if(aim != null)
					{
						aim.SpawnPersistentIndicator(100,new Vector2(),m_objectReferences[i].GetComponent<ClusterObject>(),true);
					}
				}

				if(m_obstacleMode)
				{
					m_objectReferences[i].tag = "Obstacles";
					Rigidbody2D rb = m_objectReferences[i].AddComponent<Rigidbody2D>();
					rb.mass = 0;
					rb.isKinematic = true;
					PolygonCollider2D coll = m_objectReferences[i].AddComponent<PolygonCollider2D>();
				}

			}
			if(m_spawnWithBorder)
			{


				GameObject border = new GameObject();
				border.AddComponent<SpriteRenderer>();
				border.renderer.GetComponent<SpriteRenderer>().sprite = m_objectReferences[i].GetComponent<SpriteRenderer>().sprite;
				border.renderer.material.color = Color.black;
				border.renderer.sortingLayerName =m_sortingLayerName;
				border.renderer.sortingOrder = m_sortingLayer;
				border.name = "Border";
				
				border.transform.parent = m_objectReferences[i].transform;
				border.transform.localPosition = new Vector3(0,0,0f);
				Vector2 localScale = m_objectReferences[i].transform.localScale;
				border.transform.localScale = new Vector3(1.0f + m_relativeBorderSize,1.0f + m_relativeBorderSize,0);
				border.transform.rotation = m_objectReferences[i].transform.rotation;
			}


		}

	}

	public void RefreshRandomizer()
	{
		try{
		for(int i = 0; i<m_objectReferences.Length; i++)
		{
			Destroy(m_objectReferences[i].gameObject);
		}
		}
		catch(System.Exception x)
		{
			if(x is System.NullReferenceException)
			{

				return;
			}
		}

	}

	public void ReInit()
	{
		if(!m_playInMenus)
		{
			if(AppManager.GetInstance().m_currentScene == GAME_SCENES.SCENE_GAME)
			{
				RefreshRandomizer();
				Init();

			}
		}
		else
		{
			RefreshRandomizer();
			Init();
		}
	}
	
	public ClusterObject AssignRandomizedAttributes(ClusterObject go,bool spawnInScreen,bool randomizeSpawnSpin)
	{
		//declare variables to set
		float randomizedAngle;
		Vector2 randomizedPos;
		float randomizedScale;
		float randomizedSpeed;
		Vector2 randomizedDirection;
		Vector2 tempScreenPos = Camera.main.ScreenToWorldPoint(Vector2.zero);
		Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2(0,0));

		float random = Random.Range(0.0f,(m_spawnExtremes.Length*10.0f)-1)/10.0f;
		int spawnExtremeChecker = (int)(random);
		randomizedPos = Vector2.zero;

		//Assign random Sprite reference
		int randomSprite= Random.Range(0,(m_spriteReferences.Length*10)-1)/10;
		go.GetComponent<SpriteRenderer>().sprite = m_spriteReferences[randomSprite];

		//Caculate speed
		if(m_ofVaryingSpeed)
		{
			float baseSpeed = ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetLayerSpeed(m_sortingLayer);
			float speedToSet =  Random.Range(baseSpeed,baseSpeed+ m_speedVariance);
			randomizedSpeed = speedToSet;
		}
		else
		{
			randomizedSpeed = ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetLayerSpeed(m_sortingLayer);
		}

		//Calculate spawnPosition
		if(!spawnInScreen)
		{
			if(!m_obstacleMode)
			{
				if(m_dirAtts == SPECIALIZED_DIR_ATTS.DIR_ATTS_NONE)
				{
					switch(m_direction)
					{

						case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
						{
							randomizedPos.x = tempScreenPos.x - go.renderer.bounds.size.x - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
							randomizedPos.y = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						}
						break;
						case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
						{
							randomizedPos.x = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,0)).x + go.renderer.bounds.size.x + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
							randomizedPos.y = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						}
							break;
						case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
						{
						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
							randomizedPos.x = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						}
							break;
						case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
						{
						randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
							randomizedPos.x = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						}
							break;
					
					}
				}
				else
				{
					switch(m_dirAtts)
					{
						
						case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_BOT:
						{
						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
							break;
					case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_TOP:
						{
							randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
							randomizedPos.x = Random.Range( m_spawnExtremes[spawnExtremeChecker].x*10, m_spawnExtremes[spawnExtremeChecker].y*10)/10;
							randomizedPos.x += tempScreenPos.x ;
						}
							break;
						case SPECIALIZED_DIR_ATTS.DIAGONAL_RL_BOT:
						{
						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
							break;
						case SPECIALIZED_DIR_ATTS.DIAGONAL_RL_TOP:
						{
						randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
							break;
						
					}
				}
			}
			else
			{
				float spawnTime = Random.Range(m_minObstacleSpawnTime,m_maxObstacleSpawnTime);

				if(m_dirAtts == SPECIALIZED_DIR_ATTS.DIR_ATTS_NONE)
				{
					switch(m_direction)
					{
						
					case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
					{
						randomizedPos.x = tempScreenPos.x - go.renderer.bounds.size.x - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.y = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
					}
						break;
					case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
					{
						float timeToTravelScreenLength = 0;
						float unitsPerSecond = 0;
						if(AppManager.GetInstance().m_currentScene == GAME_SCENES.SCENE_GAME)
						{
							timeToTravelScreenLength =AudioManager.GetInstance().GetAudioFile(AppManager.GetInstance().GetSelectedSongReference()).length/randomizedSpeed;
							unitsPerSecond = (Camera.main.orthographicSize*2 * Camera.main.aspect)/timeToTravelScreenLength;
							randomizedPos.x = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,0)).x + go.renderer.bounds.size.x + spawnTime * unitsPerSecond;
						}
						else
						{
							randomizedPos.x = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,0)).x + go.renderer.bounds.size.x + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						}
						randomizedPos.y = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
					}
						break;
					case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
					{

						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
					}
						break;
					case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
					{
						randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
					}
						break;
						
					}
				}
				else
				{
					switch(m_dirAtts)
					{
						
					case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_BOT:
					{
						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
						break;
					case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_TOP:
					{
						randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range( m_spawnExtremes[spawnExtremeChecker].x*10, m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
						break;
					case SPECIALIZED_DIR_ATTS.DIAGONAL_RL_BOT:
					{
						randomizedPos.y = tempScreenPos.y - go.renderer.bounds.size.y - Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
						break;
					case SPECIALIZED_DIR_ATTS.DIAGONAL_RL_TOP:
					{
						randomizedPos.y = Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + go.renderer.bounds.size.y + Random.Range(m_minDistanceBetweenSpawn,m_maxDistanceBetweenSpawn);
						randomizedPos.x = Random.Range(tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].x*10,tempScreenPos.x + m_spawnExtremes[spawnExtremeChecker].y*10)/10;
						randomizedPos.x += tempScreenPos.x ;
					}
						break;
						
					}
				}
			}
		}
		else
		{
			
			randomizedPos.y = Random.Range(m_spawnExtremes[spawnExtremeChecker].x*10,m_spawnExtremes[spawnExtremeChecker].y*10)/10;
			randomizedPos.x = Random.Range(currentScreenOffset.x,currentScreenOffset.x + m_screenSizeInUnits.x);
		}

		if(m_lockToTopOrBot)
		{
			if(randomizedPos.y > currentScreenOffset.y + Camera.main.orthographicSize)
			{
				if(randomizedPos.y < currentScreenOffset.y + Camera.main.orthographicSize*2)
				{
				randomizedPos.y = currentScreenOffset.y + Camera.main.orthographicSize*2;
				}
			}
			else
			{
				if(randomizedPos.y > currentScreenOffset.y)
				{
				randomizedPos.y = currentScreenOffset.y;
				}
			}
		}

		//Calculate scale
		randomizedScale = Random.Range(m_scaleVarianceAllowance.x *10,m_scaleVarianceAllowance.y*10)/10;
		randomizedDirection = Vector2.zero;
		//calculate direction
		switch(m_direction)
		{
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
		{
			randomizedDirection = new Vector2(1,0);
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
		{
			randomizedDirection = new Vector2(-1,0);
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
		{
			randomizedDirection = new Vector2(0,1);
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
		{
			randomizedDirection = new Vector2(0,-1);
		}
			break;
		}
	
		if(m_canMoveDiagonally)
		{
			//if can move diagonally switch direction and calculate new directional Vector according to variance
			switch(m_direction)
			{
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
			{
				Vector2 newDirection = randomizedDirection;
				newDirection.x = 1;
				switch(m_dirAtts)
				{
				case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_TOP:
				{
					newDirection.y = Random.Range(-m_directionalVarianceAllowance,0);
				}
					break;
				case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_BOT:
				{
					newDirection.y = Random.Range(0,m_directionalVarianceAllowance);
				}
					break;
				default:
				{
					newDirection.y = Random.Range(-m_directionalVarianceAllowance,m_directionalVarianceAllowance);
				}
					break;

				}


				randomizedDirection = newDirection;

			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
			{
				Vector2 newDirection = randomizedDirection;
				newDirection.x = -1;
				switch(m_dirAtts)
				{
				case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_TOP:
				{
					newDirection.y = Random.Range(-m_directionalVarianceAllowance,0);
				}
					break;
				case SPECIALIZED_DIR_ATTS.DIAGONAL_LR_BOT:
				{
					newDirection.y = Random.Range(0,m_directionalVarianceAllowance);
				}
					break;
				default:
				{
					newDirection.y = Random.Range(-m_directionalVarianceAllowance,m_directionalVarianceAllowance);
				}
					break;
					
				}

				newDirection.y = Random.Range(-m_directionalVarianceAllowance,m_directionalVarianceAllowance);

				randomizedDirection = newDirection;
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
			{
				Vector2 newDirection = randomizedDirection;
				newDirection.y = 1;
				newDirection.x = Random.Range(-m_directionalVarianceAllowance,m_directionalVarianceAllowance);

				randomizedDirection = newDirection;
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
			{
				Vector2 newDirection = randomizedDirection;
				newDirection.y = -1;
				newDirection.x = Random.Range(-m_directionalVarianceAllowance,m_directionalVarianceAllowance);

				randomizedDirection = newDirection;
			}
				break;
			}
		}

		randomizedDirection = randomizedDirection.normalized;



		//Speed is to be a percentage of heros speed (Calculated within clusterObject itself, because of the hero's varying speed)
		//randomizedSpeed = randomizedSpeed/100;

		//if should spin check and spin
		if(m_shouldSpin)
		{
			if (Random.Range(0,100)< m_spinChance)
			{
				go.SetSpin(Random.Range(-m_spinVariance,m_spinVariance));
			}
			else
			{
				go.SetSpin(0);
			}
		}

		//finally set angle
		if(randomizeSpawnSpin)
		{
		randomizedAngle = Random.Range(0,360);
		go.SetAngle(randomizedAngle,new Vector3(0,0,1));
		}
		else
		{
			go.SetAngle(360 - (Mathf.Atan2(randomizedDirection.y,randomizedDirection.x) * Mathf.Rad2Deg),go.transform.forward);
		}


		if(m_obstacleMode && AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
		{
			Destroy(go.gameObject.GetComponent<PolygonCollider2D>());
			go.gameObject.AddComponent<PolygonCollider2D>();
		}

		if(m_spawnWithBorder)
		{
			if( this.transform.Find(go.name+"/Border") != null)
			{
				GameObject border = this.transform.Find(go.name+"/Border").gameObject;
				border.renderer.GetComponent<SpriteRenderer>().sprite = go.GetComponent<SpriteRenderer>().sprite;
			}
		}


		go.SetSpeed(randomizedSpeed);
		go.SetDirection(randomizedDirection);
		go.SetPosition(randomizedPos);
		go.SetScale(randomizedScale,true);

		return go;
	}


	public void UpdateClusterRandomizer () 
	{
		if(m_objectReferences != null)
		{
		//Check if the object has move out of screen and reposition it randomly
		for(int i =0 ;i<m_objectReferences.Length;i++)
		{
			switch(m_direction)
			{
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
				{
				if(m_objectReferences[i].gameObject.transform.position.x >= Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,0)).x + m_objectReferences[i].renderer.bounds.size.x)
					{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
					}
					else
					{
					m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject();
					}
				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
				{
				if(m_objectReferences[i].gameObject.transform.position.x <= Camera.main.ScreenToWorldPoint(new Vector2(0,0)).x - m_objectReferences[i].renderer.bounds.size.x)
					{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
					}
					else
					{
					m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject();
					}
				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
				{
				if(m_objectReferences[i].gameObject.transform.position.y >= Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + m_objectReferences[i].renderer.bounds.size.x)
					{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
					}
					else
					{
					m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject();
					}
				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
				{
				if(m_objectReferences[i].gameObject.transform.position.x <= Camera.main.ScreenToWorldPoint(new Vector2(0,0)).y - m_objectReferences[i].renderer.bounds.size.x)
					{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
					}
					else
					{
					m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject();
					}
				}
				break;
			}
		}

		}
	}
	

	public void UpdateClusterRandomizer (float songLength,bool isLockedToPlayer) 
	{
		if(m_objectReferences != null)
		{
		//Check if the object has move out of screen and reposition it randomly
		for(int i =0 ;i<m_objectReferences.Length;i++)
		{
			switch(m_direction)
			{
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
			{
				if(m_objectReferences[i].gameObject.transform.position.x >= Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,0)).x + m_objectReferences[i].renderer.bounds.size.x)
				{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
				}
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
			{
				if(m_objectReferences[i].gameObject.transform.position.x <= Camera.main.ScreenToWorldPoint(new Vector2(0,0)).x - m_objectReferences[i].renderer.bounds.size.x)
				{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
				}
			
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
			{
				if(m_objectReferences[i].gameObject.transform.position.y >= Camera.main.ScreenToWorldPoint(new Vector2(0,Screen.height)).y + m_objectReferences[i].renderer.bounds.size.x)
				{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
				}
		
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
			{
				if(m_objectReferences[i].gameObject.transform.position.x <= Camera.main.ScreenToWorldPoint(new Vector2(0,0)).y - m_objectReferences[i].renderer.bounds.size.x)
				{
					AssignRandomizedAttributes(m_objectReferences[i].GetComponent<ClusterObject>(),m_shouldSpawnInScreen,m_randomizeSpawnSpin);
				}
			
			}
				break;
			}

			if(m_canScaleWithEnergy && Camera.main.audio.clip != null)
			{
				m_toneBandToDeriveEnergy = Mathf.Clamp(m_toneBandToDeriveEnergy,0,System.Enum.GetNames(typeof(DETECTION_TONE)).Length-1);
				float maxVolume = 100;
				float instantVolume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.008f)[m_toneBandToDeriveEnergy];
				float scalePercentToSet = m_minScalePercent + ((instantVolume/maxVolume) * (m_maxScalePercent- m_minScalePercent));
				float scaleToSet = m_objectReferences[i].GetComponent<ClusterObject>().m_startScale.x * (scalePercentToSet/100);
				m_objectReferences[i].GetComponent<ClusterObject>().SetScale(scaleToSet);
			}

			if(isLockedToPlayer)
			{
				m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject();
			}
			else
			{
				m_objectReferences[i].GetComponent<ClusterObject>().UpdateClusterObject(songLength);
			}
			
		}
		}
		
	}
}
