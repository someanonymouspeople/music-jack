﻿using UnityEngine;
using System.Collections;


public enum LINKED_OBJECT_ACTIONS
{
	ACTION_MOVE_TOWARDS,
	ACTION_MOVE_AWAY,
	ACTION_NULL
}

public class LinkedObject : MonoBehaviour 
{
	public LINKED_OBJECT_ACTIONS m_actionToPerform;
	public GameObject m_parent;
	public float m_destDistFromCenterOfParent;
	public float m_startDistBetweenObs;
	public Vector2 m_dirToTravel;
	public Vector2 m_linkedObjectDest;
	public Vector2 m_startOffsetFromParent;
	public Vector2 m_startOffsetVariance;
	public bool m_reCalcPos;
	public int m_sortingOrder;


	public void StopMotion()
	{
		m_dirToTravel = Vector3.zero;
	}
	// Use this for initialization
	public void InitLinkedObject (float speedFactor) 
	{
		if(m_parent )
		{
			this.transform.parent = m_parent.transform;
		}
		Vector3 posToPlaceLinkedOb = Vector3.zero;
		if(m_reCalcPos)
		{
			posToPlaceLinkedOb = new Vector3( Random.Range(this.transform.parent.transform.position.x + m_startOffsetFromParent.x - m_startOffsetVariance.x, this.transform.parent.transform.position.x + m_startOffsetFromParent.x + m_startOffsetVariance.x),Random.Range(this.transform.parent.transform.position.y + m_startOffsetFromParent.y - m_startOffsetVariance.y,this.transform.parent.transform.position.y +  m_startOffsetFromParent.y + m_startOffsetVariance.y),this.transform.parent.transform.position.z);
		}
		posToPlaceLinkedOb.z = this.transform.parent.position.z;
		posToPlaceLinkedOb.z -= m_sortingOrder;
		this.transform.position = posToPlaceLinkedOb;
		this.renderer.sortingLayerName = "BackGround";
		this.renderer.sortingOrder = this.transform.parent.renderer.sortingOrder + m_sortingOrder;
		m_dirToTravel = this.transform.parent.transform.position - this.transform.position;
		m_startDistBetweenObs = m_dirToTravel.magnitude;
		m_dirToTravel.Normalize();
		m_linkedObjectDest = (Vector2)this.transform.parent.transform.position + (m_dirToTravel * m_destDistFromCenterOfParent);
	}

	public void UpdateLinkedObject(float speed,bool inSong,float songLength)
	{
		Vector3 posToMoveObject = this.gameObject.transform.position;

		Vector3 increment;

		if(inSong)
		{
			float timeToTravelScreenLength = songLength/speed;
			float unitsPerSecond = m_startDistBetweenObs/timeToTravelScreenLength;
			
			increment = m_dirToTravel *unitsPerSecond * Time.deltaTime;
		}
		else
		{
			increment = m_dirToTravel * speed/100 * Time.deltaTime;
		}

		switch(m_actionToPerform)
		{
		case LINKED_OBJECT_ACTIONS.ACTION_MOVE_AWAY:
		{
			posToMoveObject -= increment;
		}
			break;
		case LINKED_OBJECT_ACTIONS.ACTION_MOVE_TOWARDS:
		{
			posToMoveObject += increment;
		}
			break;
		case LINKED_OBJECT_ACTIONS.ACTION_NULL:
		{
			
		}
			break;
			
		}

		this.gameObject.transform.position = posToMoveObject;
	}

}
