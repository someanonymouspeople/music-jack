﻿using UnityEngine;
using System.Collections;

public class JumpingJackScript : MonoBehaviour {

	//public Color m_startColor;
	public bool m_posMode;
	Vector2 startPos;
	public int m_linkedSubBandIndex;
	public float m_maxJumpInUnits;
	int m_subBandCount;
	public int m_subBandCountToConsider;
	Vector2 m_camSizeInUnits;
	public int m_spikeSensitivity;
	Vector2 m_camOffset;
	JunkieScript m_script;
	bool m_terrainGradientBool;
	float m_refValue;
	bool m_direction;
	bool m_isMirror;
	bool m_stopAtPlayer;

	public void SetStopAtPlayer(bool stopAtPlayer)
	{
		m_stopAtPlayer = stopAtPlayer;
	}
	public void SetIsMirror(bool mirror)
	{
		m_isMirror = mirror;
	}
	public void SetTerrainGradientBool(bool gradientBool)
	{
		m_terrainGradientBool = gradientBool;
	}
	public void SetLinkedSubBandIndex(int index)
	{
		m_linkedSubBandIndex = index;
	}
	public void SetMaxJumpInUnits(float max)
	{
		m_maxJumpInUnits = max;
	}
	public void SetRefValue(float refValue)
	{
		m_refValue = refValue;
	}
	public void SetDirection(bool direction)
	{
		m_direction = direction;
	}
	void Awake()
	{

		m_script = GameplayScript.GetInstance().GetHeroScript();
		m_camSizeInUnits = new Vector2();
		m_camSizeInUnits.y = Camera.main.orthographicSize * 2;
		m_camSizeInUnits.x = m_camSizeInUnits.y * Camera.main.aspect;
		m_subBandCount = MusicAnalyzerV2.GetInstance().m_subBandCount;
		m_subBandCountToConsider = m_subBandCount;

		Vector3 posToSetParticle = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		posToSetParticle.z = 0;
		this.gameObject.transform.position = posToSetParticle;

		startPos = posToSetParticle;
	}

	public void SetSpikeSensitivity(int sensitivity)
	{
		m_spikeSensitivity = sensitivity;
	}
	public void Start()
	{
		m_camOffset = Camera.main.ScreenToWorldPoint(new Vector2());
		float distBetween = 0;
		if(m_stopAtPlayer)
		{
		 	distBetween = (m_camOffset.x + m_camSizeInUnits.x) - m_script.transform.position.x;
		}
		else
		{
			distBetween = m_camSizeInUnits.x;
		}
		distBetween = Mathf.Round(distBetween * 1000)/1000;
		
		SmoothenedTouchPositionStorer.GetInstance().m_distBetweenPlayerAndEndOfScreen = distBetween;
		float scaleXToSet = ( distBetween)/(float)m_subBandCountToConsider;
		SmoothenedTouchPositionStorer.GetInstance().SetMinDistanceBetweenDraw(scaleXToSet);
		this.transform.localScale = new Vector2(scaleXToSet,this.transform.localScale.y);
	}

	private void CheckAndSetLinkedSubBandIndexAndColor()
	{
		float distBetweenJunkieAndScreenEnd = m_camOffset.x + m_camSizeInUnits.x - m_script.gameObject.transform.position.x- m_script.GetSprite().renderer.bounds.size.x;

		float relativePos = 1 - (this.gameObject.transform.position.x - (m_script.gameObject.transform.position.x- m_script.GetSprite().renderer.bounds.extents.x))/distBetweenJunkieAndScreenEnd;

		relativePos = relativePos * m_subBandCount;
		m_linkedSubBandIndex = (int)relativePos;
		if(m_linkedSubBandIndex > MusicAnalyzerV2.GetInstance().m_subBandCount)
		{
			Debug.Log ("zzz");
		}

		Color tempColor = this.renderer.material.color;
		if(!m_terrainGradientBool)
		{
			tempColor = Color.Lerp(Color.black,Color.white,(float)m_linkedSubBandIndex/(float)m_subBandCount);
		}
		else
		{
			tempColor = Color.Lerp(Color.white,Color.black,(float)m_linkedSubBandIndex/(float)m_subBandCount);
		}

			this.renderer.material.color = tempColor;

	}

	public void UpdateInPlace(float instantVolume,float averageVolume)
	{
		m_camOffset = Camera.main.ScreenToWorldPoint(Vector2.zero);
		float distToConsider = 0; 
		Vector3 pos = this.transform.position;
		float relativePos = ((float)m_linkedSubBandIndex/(float)MusicAnalyzerV2.GetInstance().m_subBandCount);
		if(!m_direction)
		{
			relativePos = 1.0f- relativePos;
		}
		if(m_stopAtPlayer)
		{
			distToConsider = m_camOffset.x + m_camSizeInUnits.x - m_script.gameObject.transform.position.x- m_script.GetSprite().renderer.bounds.size.x;
			pos.x = ((relativePos) * distToConsider) + m_script.gameObject.transform.position.x- m_script.GetSprite().renderer.bounds.extents.x;
		}
		else
		{
			distToConsider = m_camSizeInUnits.x * 0.9f;
			pos.x = ((relativePos) * distToConsider) + m_camOffset.x;
		}


		Vector3 posToConsider = SmoothenedTouchPositionStorer.GetInstance().GetPositionClosestToX(pos.x,this.renderer.bounds.size.x);
		if(!posToConsider.Equals(Vector3.zero))
		{
			pos.y = posToConsider.y;
		}
		else
		{
			pos.y = -99;
		}
		this.transform.position = pos;
	
			float scaleXToSet = (distToConsider)/(float)m_subBandCountToConsider;

			Vector2 scale = this.gameObject.transform.localScale;
			scale.x = scaleXToSet;
		if(pos.x > GameplayScript.GetInstance().GetHeroPos().x)
		{
			//float maxVolume = 1000;
			if(m_linkedSubBandIndex < MusicAnalyzerV2.GetInstance().m_subBandCount)
			{
				float volume = instantVolume;
		
				volume += (volume - averageVolume) *((float)m_spikeSensitivity );


				float scaleToSetAlongY = (volume) * 0.01f;

				if(scaleToSetAlongY < 0.05f)
				{
					scaleToSetAlongY = 0.05f;
				}

				if(m_isMirror)
				{
					scaleToSetAlongY = -scaleToSetAlongY;
				}

				scale.y = scaleToSetAlongY;
				this.gameObject.transform.localScale = scale;
			}
		}
		else
		{
			this.gameObject.transform.localScale = new Vector2(scale.x,0.05f);
		}
	}

//	public void UpdateNormal()
//	{
//		m_camOffset = Camera.main.ScreenToWorldPoint(new Vector2());
//		float maxVolume = 100;
//		if(m_stopAtPlayer)
//		{
//			if(this.gameObject.transform.position.x <m_script.gameObject.transform.position.x - m_script.GetSprite().renderer.bounds.extents.x )
//			{
//				Destroy(this.gameObject);
//				return;
//			}
//		}
//		else
//		{
//			if(this.gameObject.transform.position.x <m_camOffset.x )
//			{
//				Destroy(this.gameObject);
//				return;
//			}
//		}
//		CheckAndSetLinkedSubBandIndexAndColor();
//		if(Camera.main.audio.clip != null)
//		{
//			if(!m_posMode)
//			{
//				Vector2 scale = this.gameObject.transform.localScale;
//
//				if(m_linkedSubBandIndex < MusicAnalyzerV2.GetInstance().m_subBandCount)
//				{
//					float volume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfSubBand(m_linkedSubBandIndex,0.01f);
//
//					if(volume > 0)
//					{
//						float scaleToSetAlongY = (volume/maxVolume) * m_maxJumpInUnits;
//						scale.y = scaleToSetAlongY;
//					}
//					else
//					{
//						scale.y = 0.5f;
//					}
//					this.gameObject.transform.localScale = scale;
//				}
//			}
//			else
//			{
//				Vector2 pos = this.gameObject.transform.localPosition;
//				Mathf.Clamp(m_linkedSubBandIndex,0,MusicAnalyzerV2.GetInstance().m_subBandCount-1);
//				float volume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfSubBand(m_linkedSubBandIndex,0.01f);
//				if(volume > 0)
//				{
//					float posToSetAlongY = (volume/maxVolume) * m_maxJumpInUnits;
//					pos.y =startPos.y + posToSetAlongY;
//				}
//				else
//				{
//					pos.y = startPos.y;
//				}
//				this.gameObject.transform.localPosition = pos;
//			}
//		}
//
//	}
	
}
