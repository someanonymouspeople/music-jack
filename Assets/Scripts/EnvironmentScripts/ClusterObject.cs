﻿using UnityEngine;
using System.Collections;

public class ClusterObject : MonoBehaviour 
{
	Vector2 m_direction;
	float m_speed;
	float m_degreesPerSecond;
	public Vector2 m_startScale;

	public void Awake()
	{
		m_startScale = this.transform.localScale;
	}
	public void SetSpin(float degreesPerSecond)
	{
		m_degreesPerSecond = degreesPerSecond;
	}
	public void SetAngle(float angle,Vector3 axis)
	{
		this.gameObject.transform.rotation = Quaternion.AngleAxis(angle,axis);
	}
	public void SetDirection(Vector2 dirVec)
	{
		m_direction = dirVec;
	}
	public void SetPosition(Vector2 pos)
	{
		Vector3 pos3 = new Vector3(pos.x,pos.y,0);
		this.gameObject.transform.position = pos3;
	}
	public void SetSpeed(float speed)
	{
		m_speed = speed;
	}
	public void SetScale(float scale,bool modStartScale = false)
	{
		if(modStartScale)
		{
			m_startScale = new Vector2(scale,scale);
		}
		this.gameObject.transform.localScale = new Vector2(scale,scale);
	}

	public void Spin()
	{
		float outAngle =0;
		Vector3 outAxis = Vector3.forward;
		this.transform.rotation.ToAngleAxis(out outAngle,out outAxis);
		outAngle += m_degreesPerSecond * Time.deltaTime;
		this.transform.rotation = Quaternion.AngleAxis(outAngle,Vector3.forward);
	}

	public void UpdateClusterObject()
	{
		GameObject hero = GameObject.Find("HeroDude");

		//speed is divided by hundred if songlength is not considered because 
		//we want to consider the speed as a percent of the heros speed

		if(hero)
		{
			this.gameObject.transform.position += (Vector3)m_direction * m_speed * Time.deltaTime * GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed();
		}
		else
		{
			this.gameObject.transform.position += ((Vector3)m_direction * m_speed/100 * Time.deltaTime);
		}
//
//		if(hero)
//		{
//			this.gameObject.transform.position += (Vector3)m_direction * m_speed/100 * Time.deltaTime * GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed();
//		}
//		else
//		{
//			this.gameObject.transform.position += ((Vector3)m_direction * m_speed/100 * Time.deltaTime);
//		}
		Spin();
	}
	public void UpdateClusterObject(float songLength)
	{

		float timeToTravelScreenLength = songLength/m_speed;
		float unitsPerSecond = (Camera.main.orthographicSize*2 * Camera.main.aspect)/timeToTravelScreenLength;
		Vector3 posToSet = this.gameObject.transform.position +=(Vector3)m_direction * unitsPerSecond * Time.deltaTime;
		posToSet.z = 0;
		this.gameObject.transform.position =posToSet;
		Spin ();
	}
}
