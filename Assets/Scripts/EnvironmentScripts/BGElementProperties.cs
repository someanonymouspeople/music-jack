﻿using UnityEngine;
using System.Collections;

public class BGElementProperties : MonoBehaviour 
{
	public bool m_canBeSaturated;
	public bool m_canScaleWithEnergy;
	public float m_minScalePercent;
	public float m_maxScalePercent;
	public int m_toneBandToDeriveEnergy;
	public Vector2 m_currentScale;


	public void CanBeSaturated(bool canBeSaturated)
	{
		m_canBeSaturated = canBeSaturated;
	}
	public bool CanBeSaturated()
	{
		return m_canBeSaturated;
	}



}
