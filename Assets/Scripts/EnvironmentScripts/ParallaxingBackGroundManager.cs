using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BACKGROUND_TYPE
{
	BACKGROUND_PINK,
	BACKGROUND_BLUE,
	BACKGROUND_GREEN,
	BACKGROUND_YELLOW
};


public class ParallaxingBackGroundManager : MonoBehaviour
{
	public BACKGROUND_TYPE m_backGroundTypeBeingManaged;
	public ParallaxingBackGround m_backGroundBeingManaged;
	public JunkieScript m_heroScript;
	public static ParallaxingBackGroundManager m_instance;

	public float m_saturationValue;
	public float m_saturationThreshold;
	public float m_desaturationThreshold;

	public bool m_greyEffect;

	float m_gameTime;

	public void Desaturate()
	{
		if(m_backGroundBeingManaged)
		{
			m_backGroundBeingManaged.DesaturateHueRelativeTo(0,1);
		}
	}
	public void ResetSaturation()
	{
		if(m_backGroundBeingManaged)
		{
			m_backGroundBeingManaged.DesaturateHueRelativeTo(1,1);
		}
	}

	public void SetSaturationValue(float value)
	{
		m_saturationValue = value;
	}

	public void SetSaturationThreshold(float value)
	{
		m_saturationThreshold = value;
	}

	public void SetDesaturationThreshold(float value)
	{
		m_desaturationThreshold = value;
	}
	
	public void DesaturationLogic()
	{	
		if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
		{
			if(m_backGroundBeingManaged)
			{
				float curValue = m_saturationValue;
				curValue -= m_desaturationThreshold;

				if(curValue <0)
				{
					curValue = 0;
				}
				float maxValue = m_saturationThreshold;
				maxValue -= m_desaturationThreshold;
				m_backGroundBeingManaged.DesaturateHueRelativeTo(curValue,maxValue);
			}
		}
		return;
	}

	public void SetGameTime(float songLength)
	{
		m_gameTime = songLength;
	}
	void Awake()
	{
		m_saturationThreshold = 0;
		m_saturationValue = 0;
		m_desaturationThreshold =0;
		m_gameTime = 999;
		//Destroy elements containing other instances of this script and maintain the first script spawned
		if(ParallaxingBackGroundManager.GetInstance())
		{
			Destroy(this.gameObject);
		}
		else
		{
			DontDestroyOnLoad(this.gameObject);
			m_instance = this;
		}
	}

	public ParallaxingBackGround GetManagedBackGround()
	{
		return m_backGroundBeingManaged;
	}

	public static ParallaxingBackGroundManager GetInstance()
	{
		return m_instance;
	}

	public void RefreshManager(float songLength)
	{
		//Check and initialize values the manager will need to know
		GAME_SCENES currentScene = AppManager.GetInstance().GetCurrentScene();
		if(currentScene == GAME_SCENES.SCENE_GAME)
		{
			m_heroScript = GameplayScript.GetInstance().GetHeroScript();
		}
		else
		{
			m_heroScript = null;
		}
		RefreshCurrentBackGround();

	}

	public void RefreshManager()
	{
		//Check and initialize values the manager will need to know
		GAME_SCENES currentScene = AppManager.GetInstance().GetCurrentScene();
		if(currentScene == GAME_SCENES.SCENE_GAME)
		{
			m_heroScript = GameplayScript.GetInstance().GetHeroScript();
		}
		else
		{
			m_heroScript = null;
		}
		RefreshCurrentBackGround();
		
	}

	private void InitBackGround()
	{
		if(m_backGroundBeingManaged)
		{
			ResetSaturation();
			GAME_SCENES currentScene = AppManager.GetInstance().GetCurrentScene();
			if(currentScene == GAME_SCENES.SCENE_GAME)
			{
				m_heroScript = GameplayScript.GetInstance().GetHeroScript();
			}
			else
			{
				m_heroScript = null;
			}
			m_backGroundBeingManaged.Initialize(m_heroScript);
			return;
		}
		Debug.Log("No bg loaded yet");
	}

	public IEnumerator FadeInOutBackGround(BACKGROUND_TYPE type, float fadeInTime)
	{
		// if any other backgrounds running destroy
		if(m_backGroundBeingManaged.m_type == type)
		{
			yield break;
		}

		ParallaxingBackGround prevBG = null;
		if(m_backGroundBeingManaged)
		{
			prevBG = m_backGroundBeingManaged;
		}

		//try to load new background
		GameObject bgToLoad = null;
		switch(type)
		{
		case BACKGROUND_TYPE.BACKGROUND_PINK:
		{
			bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_pink_1")) as GameObject;
		}
			break;
		case BACKGROUND_TYPE.BACKGROUND_GREEN:
		{
			bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Green")) as GameObject;
		}
			break;
		case BACKGROUND_TYPE.BACKGROUND_BLUE:
		{
			bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Blue")) as GameObject;
		}
			break;
		case BACKGROUND_TYPE.BACKGROUND_YELLOW:
		{
			bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Yellow")) as GameObject;
		}
			break;
		}
		
		if(bgToLoad)
		{
			//bg loaded
			bgToLoad.transform.parent = ParallaxingBackGroundManager.GetInstance().gameObject.transform;
			m_backGroundTypeBeingManaged = type;
			m_backGroundBeingManaged = bgToLoad.GetComponent<ParallaxingBackGround>();
			InitBackGround();
		}

		// fade in fade out

		for(float alpha = 0; alpha < 1; alpha+= Time.deltaTime/fadeInTime)
		{

			//SetOpacityOfRenderers(m_backGroundBeingManaged.gameObject,alpha);
			if(prevBG)
			{

				//SetOpacityOfRenderers(m_backGroundBeingManaged.gameObject,(1-alpha));
			}
			yield return null;
		}
		if(prevBG)
		{
			Destroy(prevBG.gameObject);
		}
		//bg load failed
		Debug.Log("BackGround load unsuccessful");
		yield break;
	}
	
	public void LoadNewBackGround(BACKGROUND_TYPE type)
	{
		// if any other backgrounds running destroy
		if(m_backGroundBeingManaged)
		{
			if(m_backGroundBeingManaged.GetType() != type)
			{
				Destroy(m_backGroundBeingManaged.gameObject);
			}
			else
			{
				return;
			}
		}
			//try to load new background
			GameObject bgToLoad = null;
			switch(type)
			{
			case BACKGROUND_TYPE.BACKGROUND_PINK:
			{
				bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_pink_1")) as GameObject;
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_GREEN:
			{
				bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Green")) as GameObject;
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_BLUE:
			{
				bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Blue")) as GameObject;
			}
				break;
			case BACKGROUND_TYPE.BACKGROUND_YELLOW:
			{
				bgToLoad = GameObject.Instantiate(Resources.Load("ParallaxBackGrounds/Parallax_Yellow")) as GameObject;
			}
			break;
			}
			
			if(bgToLoad)
			{
				//bg loaded
				bgToLoad.transform.parent = ParallaxingBackGroundManager.GetInstance().gameObject.transform;
				//bgToLoad.transform.parent = Camera.main.transform;
				m_backGroundTypeBeingManaged = type;
				m_backGroundBeingManaged = bgToLoad.GetComponent<ParallaxingBackGround>();
				InitBackGround();
				return;
			}
			
			//bg load failed
			Debug.Log("BackGround load unsuccessful");


	}
	
	public void RefreshCurrentBackGround()
	{
		//reinit background
		SetGameTime(999);
		InitBackGround();
	}

	public void UpdateBackGround ()
	{
		if(m_heroScript)
		{
			if(m_greyEffect)
			{
				DesaturationLogic();
			}
			m_backGroundBeingManaged.UpdateBackGround(m_gameTime);
		}
		else
		{
			m_backGroundBeingManaged.UpdateBackGround();
		}
	}
}
