﻿using UnityEngine;
using System.Collections;

public class StaticBGScript : BGElementProperties {

	public Vector2 m_startScale;
	void Awake () 
	{
		m_startScale = this.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_canScaleWithEnergy && Camera.main.audio.clip != null)
		{
			m_toneBandToDeriveEnergy = Mathf.Clamp(m_toneBandToDeriveEnergy,0,System.Enum.GetNames(typeof(DETECTION_TONE)).Length-1);
			float maxVolume = 120;
			float instantVolume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.008f)[m_toneBandToDeriveEnergy];
			float scalePercentToSet = m_minScalePercent + ((instantVolume/maxVolume) * (m_maxScalePercent- m_minScalePercent));
			Vector2 scaleToSet = new Vector2(m_startScale.x * (scalePercentToSet/100),m_startScale.y * (scalePercentToSet/100));
			this.transform.localScale = scaleToSet;
		}
	
	}
}
