using UnityEngine;
using System.Collections;

public enum BASE_OBJECT_OPTIONS
{
	MOVE_TOWARD,
	MOVE_AWAY,
	NULL
}
public class LinkedPosRandomizer : PosRandomizer {
	
	public LinkedObject[] m_linkedObjects;
	public LinkedObject m_primaryLink;

	//FLASH HACK
	public float m_minDistanceToSpawnLight;
	public bool m_spawnedLight;
	// /FLASH HACK

	public override void Awake ()
	{
		m_spawnedLight = false;
		foreach(LinkedObject lo in m_linkedObjects)
		{
			lo.InitLinkedObject(1);
		}
		base.Awake();
	}

	public override void Init (bool offScreen)
	{
		//FLASH HACK
		GameObject go = this.transform.FindChild("CenterLight").gameObject;

		if(go)
		{
			if(go.GetComponent<ConstantTransformScript>() != null)
			{
				m_spawnedLight = false;
				go.GetComponent<ConstantTransformScript>().m_shouldFade = true;
				go.GetComponent<ConstantTransformScript>().Reset();
				Destroy(go.GetComponent<ConstantTransformScript>());
			go.renderer.enabled = false;
			}
		}
		// /FLASH HACK
		base.Init (offScreen);
	}
	public void UpdateLinkedObjects(float songLength,bool inSong = false)
	{
		foreach(LinkedObject lo in m_linkedObjects)
		{
			lo.UpdateLinkedObject(m_speed,inSong,songLength);
		}
	}

	public override void UpdateRandomizer ()
	{
		//FLASH HACK
		float distBetween = Vector2.Distance(this.transform.position , m_primaryLink.transform.position) ;
		if(distBetween < m_minDistanceToSpawnLight && !m_spawnedLight )
		{
			m_spawnedLight = true;
			Camera.main.GetComponent<FlashScript>().Flash();
			GameObject go = this.transform.FindChild("CenterLight").gameObject;
			go.renderer.enabled = true;
			go.AddComponent<ConstantTransformScript>();
			go.GetComponent<ConstantTransformScript>().m_type = CONST_TRANFORM_TYPE.CONST_TRANSFORM_SCALE;

		}
		// /FLASH HACK

		UpdateLinkedObjects(0);
		base.UpdateRandomizer ();
	}

	public override void UpdateRandomizer (float songLength)
	{
		//FLASH HACK
		float distBetween = Vector2.Distance(this.transform.position , m_primaryLink.transform.position) ;
		if(distBetween < m_minDistanceToSpawnLight && !m_spawnedLight )
		{
			m_spawnedLight = true;
			m_primaryLink.GetComponent<LinkedObject>().StopMotion();
			Camera.main.GetComponent<FlashScript>().Flash();
			GameObject go = this.transform.FindChild("CenterLight").gameObject;
			go.renderer.enabled = true;
			go.AddComponent<ConstantTransformScript>();
			go.GetComponent<ConstantTransformScript>().m_shouldFade = true;
			go.GetComponent<ConstantTransformScript>().SetCyclesToFade(3);
			go.GetComponent<ConstantTransformScript>().m_type = CONST_TRANFORM_TYPE.CONST_TRANSFORM_SCALE;
			go.GetComponent<ConstantTransformScript>().m_axesToIncrement = new Vector3(1,1,0);
		}
		// /FLASH HACK
		UpdateLinkedObjects(songLength,true);
		base.UpdateRandomizer (songLength);
	}

}
