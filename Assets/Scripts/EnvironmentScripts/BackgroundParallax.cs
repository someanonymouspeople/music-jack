﻿using UnityEngine;
using System.Collections;


public class BackgroundParallax : MonoBehaviour
{
	public PosRandomizer[] RandomBGItems;
	public ClusterRandomizer[] m_clusterRandomizers;
	public Transform[] backgrounds;
	public float[] m_layerSpeedsPercentages;
	public JunkieScript m_heroScript;
	protected bool m_menuMode = false;
	public static BackgroundParallax m_instance;
	GAME_SCENES currentScene;


	public static BackgroundParallax GetInstance()
	{
		return m_instance;
	}


	
	public void ReInit(BACKGROUND_TYPE type)
	{
		if(AppManager.GetInstance().GetCurrentScene()==GAME_SCENES.SCENE_GAME)
		{
			m_heroScript = GameObject.Find("HeroDude").GetComponent<JunkieScript>();
		}
		for(int i= 0;i<m_clusterRandomizers.Length ; i++)
		{
			m_clusterRandomizers[i].RefreshRandomizer();
		}
		Start ();
		for(int i = 0; i < backgrounds.Length; i++)
		{
			Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
			currentTexOffset.x = 0;
			backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
		}
	}

	public float GetLayerSpeed(int referenceNumber)
	{
		if(referenceNumber > m_layerSpeedsPercentages.Length-1)
		{
			referenceNumber = m_layerSpeedsPercentages.Length -1;
		}
		else if(referenceNumber < 0)
		{
			referenceNumber = 0;
		}

		float layerSpeed;
		if(!m_menuMode)
		{
		layerSpeed = m_layerSpeedsPercentages[referenceNumber] * m_heroScript.GetSpeed();
		return layerSpeed;
		}
		else
		{
			return m_layerSpeedsPercentages[referenceNumber];
		}
	}

	public float GetDefaultLayerSpeed(int referenceNumber)
	{
		if(referenceNumber > m_layerSpeedsPercentages.Length-1)
		{
			referenceNumber = m_layerSpeedsPercentages.Length -1;
		}
		else if(referenceNumber < 0)
		{
			referenceNumber = 0;
		}
		
		float layerSpeed;
		if(!m_menuMode)
		{
			layerSpeed = m_layerSpeedsPercentages[referenceNumber];
			return layerSpeed;
		}
		else
		{
			return m_layerSpeedsPercentages[referenceNumber];
		}
	}

	void Awake()
	{
		if(BackgroundParallax.GetInstance())
		{
			Destroy(this.gameObject);
		}
		else
		{
			DontDestroyOnLoad(this.gameObject);
			m_instance = this;
		}

	}
	void Start()
	{
		if(AppManager.GetInstance().GetCurrentScene()==GAME_SCENES.SCENE_GAME)
		{
			m_heroScript = GameObject.Find("HeroDude").GetComponent<JunkieScript>();
		}
		currentScene = AppManager.GetInstance().GetCurrentScene();
		if(currentScene != GAME_SCENES.SCENE_GAME)
		{
			m_menuMode = true;
		}
		else
		{
			m_menuMode = false;
		}


		for(int i=0; i<RandomBGItems.Length; i++)
		{
			bool spawnOffScreen = false;
			RandomBGItems[i].Init(false);

		}
	 
		//initialize clusters
		for(int i=0; i<m_clusterRandomizers.Length; i++)
		{
			m_clusterRandomizers[i].Init();
		}
		ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().UpdateBGElementList();

	}
	
	public void UpdateBackGround ()
	{
		if(!m_menuMode)
		{
			for(int i=0; i<RandomBGItems.Length; i++)
			{
				RandomBGItems[i].UpdateRandomizer();
			}

			float highestSpeed = 0;
			for(int i = 0; i < backgrounds.Length; i++)
			{
				float layerSpeed = 0;
				if(i<m_layerSpeedsPercentages.Length)
				{
					layerSpeed = (m_layerSpeedsPercentages[i]*m_heroScript.GetSpeed());
					layerSpeed = layerSpeed/100;
					highestSpeed = layerSpeed;
				}
				else
				{
					layerSpeed = highestSpeed;
				}
				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += layerSpeed*Time.deltaTime;
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}

		}
		else
		{
			for(int i=0; i<RandomBGItems.Length; i++)
			{
				RandomBGItems[i].UpdateRandomizer();
			}
			
			float highestSpeed = 0;
			for(int i = 0; i < backgrounds.Length; i++)
			{
				float layerSpeed = 0;
				if(i<m_layerSpeedsPercentages.Length)
				{
					layerSpeed = (m_layerSpeedsPercentages[i]);
					layerSpeed = layerSpeed/100;
					highestSpeed = layerSpeed;
				}
				else
				{
					layerSpeed = highestSpeed;
				}
				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += layerSpeed*Time.deltaTime;
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}
		}
		for(int i = 0;i<m_clusterRandomizers.Length;i++)
		{
			m_clusterRandomizers[i].UpdateClusterRandomizer();
		}
	}
}
