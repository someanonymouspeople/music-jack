﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallaxingBackGround : MonoBehaviour
{
	private List<BGElementProperties> m_backgroundElementList;
	public PosRandomizer[] RandomBGItems;
	public ClusterRandomizer[] m_clusterRandomizers;
	public Transform[] backgrounds;
	public float[] m_layerSpeedsPercentages;
	public bool[] m_speedLockedToPlayer;

	public int m_offScreenSpawnCount;
	public int m_offScreenSpawnChance;

	public JunkieScript m_heroScript;
	GAME_SCENES currentScene;
	public BACKGROUND_TYPE m_type;
	public int m_numberOfBGElementsToNotDesaturate;
	public int m_chanceToDesaturate;


	private void SaturateRandomElements()
	{
		//reset any changes to the greyscale shader
		foreach(BGElementProperties go in m_backgroundElementList)
		{
			if(go.gameObject.renderer != null )
			{
				go.gameObject.renderer.material.SetFloat("_isActive",1.0f);
				go.gameObject.renderer.material.SetFloat("_EffectAmount",0);
			}
		}
		DesaturateHueRelativeTo(0,1);

		//set saturation to the random number of elements
		int itemsRemaining = m_numberOfBGElementsToNotDesaturate;
		List <BGElementProperties> listToConsider = new List<BGElementProperties>();
		listToConsider = m_backgroundElementList.ConvertAll(delegate(BGElementProperties input) {return input;	});
		listToConsider.RemoveAll(x=> x.gameObject.GetComponent<ClusterObject>());
		if(m_chanceToDesaturate > 0)
		{
			while(itemsRemaining > 0)
			{
				foreach(BGElementProperties bge in listToConsider)
				{
					if(itemsRemaining > 0)
					{
						if(bge.CanBeSaturated())
						{
							if(Random.Range(0,100)< m_chanceToDesaturate)
							{
								if(bge.renderer != null)
								{
									if(bge.GetComponent<LinkedPosRandomizer>())
									{
										for(int i = 0; i<bge.gameObject.transform.childCount+1; i++)
										{
											bge.gameObject.GetComponentsInChildren<Renderer>()[i].material.SetFloat("_isActive",0.0f);
											
										}
									}
									else
									{
										bge.gameObject.renderer.material.SetFloat("_isActive",0.0f);
									}
									itemsRemaining-= 1;
								}
								else
								{
									if(bge.gameObject.GetComponent<ClusterRandomizer>())
									{
										Renderer[] renderers = bge.gameObject.GetComponentsInChildren<Renderer>();
										for(int i = 0; i<renderers.Length; i++)
										{
											renderers[i].material.SetFloat("_isActive",0.0f);
										}
										itemsRemaining --;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	public void UpdateBGElementList()
	{
		//UpdateBGElementList
		m_backgroundElementList.Clear();

		//populateBGElement list 
		for(int i =0; i<transform.GetComponentsInChildren<BGElementProperties>().Length; i++)
		{
			m_backgroundElementList.Add(transform.GetComponentsInChildren<BGElementProperties>()[i]);

		}
		m_backgroundElementList.RemoveAll(x=> x==null);

	}
	void Awake()
	{
		m_backgroundElementList = new List<BGElementProperties>();

		for(int i =0; i<transform.GetComponentsInChildren<BGElementProperties>().Length; i++)
		{
			m_backgroundElementList.Add(transform.GetComponentsInChildren<BGElementProperties>()[i]);
		}

		m_backgroundElementList.RemoveAll(x=> x==null);

		if(RandomBGItems.Length < m_offScreenSpawnCount)
		{
			m_offScreenSpawnCount = RandomBGItems.Length;
		}
	}

	public void DesaturateHueRelativeTo(float curValue,float maxValue)
	{
		//iterateThroughMaterailList

		m_backgroundElementList.RemoveAll(x=> x==null);
		foreach(BGElementProperties go in m_backgroundElementList)
		{
			if(go.gameObject.renderer != null )
			{
				float valueToSet = 1 - (curValue/maxValue);
				go.gameObject.renderer.material.SetFloat("_EffectAmount",valueToSet);
			}
		}
	}

	public BACKGROUND_TYPE GetType()
	{
		return m_type;
	}
	public void Initialize(JunkieScript heroScript)
	{

		if(heroScript)
		{
			m_heroScript = heroScript;

		}
		Vector3 parentPos = this.gameObject.transform.position;
		parentPos.x = 0;
		parentPos.y= 0;
		this.gameObject.transform.position = parentPos;


		//Refresh all cluster randomizers
		for(int i= 0;i<m_clusterRandomizers.Length ; i++)
		{
			m_clusterRandomizers[i].ReInit();
		}
		int randomSpawnChecker = m_offScreenSpawnCount;
		//Refresh all position randomizers
		for(int i =0 ;i<RandomBGItems.Length;i++)
		{
			bool spawnOffScreen = false;

			if(randomSpawnChecker>0)
			{
				if(Random.Range(0,100)< m_offScreenSpawnChance)
				{
					spawnOffScreen = true;
					randomSpawnChecker--;
				}
			}

			RandomBGItems[i].Init(spawnOffScreen);
			//Debug.Log("Name : "+RandomBGItems[i].name+" position : "+RandomBGItems[i].gameObject.transform.position.x);
		}

		//Refresh all Background meshes
		for(int i = 0; i < backgrounds.Length; i++)
		{
			Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
			currentTexOffset.x = 0;
			backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			backgrounds[i].transform.localScale = backgrounds[i].GetComponent<StaticBGScript>().m_startScale;
		}
		this.gameObject.transform.position = Vector3.zero;
		UpdateBGElementList();
		if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
		{
			SaturateRandomElements();
		}

	}
	
	public float GetLayerSpeed(int referenceNumber)
	{
		//Clamp the reference number being passed to avoid range exceptions
		if(referenceNumber > m_layerSpeedsPercentages.Length-1)
		{
			referenceNumber = m_layerSpeedsPercentages.Length -1;
		}
		else if(referenceNumber < 0)
		{
			referenceNumber = 0;
		}

		//Get and return the layer speed percentage
		float layerSpeed;

			layerSpeed=m_layerSpeedsPercentages[referenceNumber];

		return layerSpeed;
	}
	

	
	public void UpdateBackGround ()
	{
		if(m_heroScript)
		{
			for(int i=0; i<RandomBGItems.Length; i++)
			{
				RandomBGItems[i].UpdateRandomizer();
			}
			
			float highestSpeed = 0;
			for(int i = 0; i < backgrounds.Length; i++)
			{
				float layerSpeed = 0;
				if(i<m_layerSpeedsPercentages.Length)
				{
					layerSpeed = (m_layerSpeedsPercentages[i]*m_heroScript.GetSpeed());
					layerSpeed = layerSpeed/100;
					highestSpeed = layerSpeed;
				}
				else
				{
					layerSpeed = highestSpeed;
				}
				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += layerSpeed*Time.deltaTime;
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}
			
		}
		else
		{
			for(int i=0; i<RandomBGItems.Length; i++)
			{
				RandomBGItems[i].UpdateRandomizer();
			}
			
			float highestSpeed = 0;
			for(int i = 0; i < backgrounds.Length; i++)
			{
				float layerSpeed = 0;
				if(i<m_layerSpeedsPercentages.Length)
				{
					layerSpeed = (m_layerSpeedsPercentages[i]);
					layerSpeed = layerSpeed/100;
					highestSpeed = layerSpeed;
				}
				else
				{
					layerSpeed = highestSpeed;
				}
				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += layerSpeed*Time.deltaTime;
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}
		}
		for(int i = 0;i<m_clusterRandomizers.Length;i++)
		{
			m_clusterRandomizers[i].UpdateClusterRandomizer();
		}
	}

	public void UpdateBackGround (float songLength)
	{

		for(int i=0; i<RandomBGItems.Length; i++)
		{
			RandomBGItems[i].UpdateRandomizer(songLength);
		}
		
		float highestSpeed = 0;
		for(int i = 0; i < backgrounds.Length; i++)
		{
			float layerSpeed = 0;
			if(i<m_layerSpeedsPercentages.Length)
			{

				layerSpeed = m_layerSpeedsPercentages[backgrounds[i].gameObject.renderer.sortingOrder];
				highestSpeed = layerSpeed;
			}
			else
			{
				layerSpeed = highestSpeed;
			}
			if(m_speedLockedToPlayer[backgrounds[i].gameObject.renderer.sortingOrder])
			{
				float distanceToTravel = Camera.main.orthographicSize *2 * Camera.main.aspect;
				float timeToTravelDistance = distanceToTravel/layerSpeed;
				float speedToTravelAt = distanceToTravel/timeToTravelDistance;

				float multiplicationFactor = m_heroScript.GetRelativeSpeed();
				multiplicationFactor = Mathf.Clamp(multiplicationFactor,0.02f,1);
				layerSpeed = speedToTravelAt * multiplicationFactor;

				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += layerSpeed/distanceToTravel * Time.deltaTime;
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);


//				layerSpeed = (layerSpeed/(100 * Camera.main.orthographicSize *2 * Camera.main.aspect)) * m_heroScript.GetSpeed();
//				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
//				currentTexOffset.x += layerSpeed*Time.deltaTime;
//				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}
			else
			{
				Vector2 currentTexOffset = backgrounds[i].gameObject.renderer.material.GetTextureOffset("_MainTex");
				currentTexOffset.x += (layerSpeed/songLength) * (Time.deltaTime);
				backgrounds[i].gameObject.renderer.material.SetTextureOffset("_MainTex",currentTexOffset);
			}
		}
			

		for(int i = 0;i<m_clusterRandomizers.Length;i++)
		{
			if(m_speedLockedToPlayer[m_clusterRandomizers[i].m_sortingLayer])
			{
				m_clusterRandomizers[i].UpdateClusterRandomizer(songLength,true);
			}
			else
			{
				m_clusterRandomizers[i].UpdateClusterRandomizer(songLength,false);
			}
		}
	}


}
