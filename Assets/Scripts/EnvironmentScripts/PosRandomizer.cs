﻿using UnityEngine;
using System.Collections;

public class PosRandomizer : BGElementProperties {

	public RANDOMIZED_MOVE_DIRECTION m_direction;
	protected Vector3 m_moveDirection;
	protected Vector2 ScreenSizeInUnits;
	public float m_maxDistOffset;
	public float m_spawnMin;
	public float m_spawnMax;
	public float m_minScaleVariance;
	public float m_maxScaleVariance;
	public float m_speed;
	public float m_speedVariance;
	protected bool m_randomizeAtSpawn;
	public bool m_spinWhileMoving;
	protected float m_spinDegreesPerSec;
	public float m_spinDegreeVariance;
	protected Vector3 m_startScale;

	public float m_baseSpawnRotDegrees;
	public float m_rotDegreesVariance;

	public float m_reSpawnTimer;
	public float m_baseRespawnTime;
	protected float m_reSpawnTime;
	public float m_reSpawnTimeVariance;
	protected bool m_reSpawning;
	public bool m_specialistSpawn;
	public float m_specialistPos;
	public float m_specialistVariance;
	public virtual void Awake()
	{

		m_startScale = this.renderer.transform.localScale;
		m_currentScale = m_startScale;
	}



	public virtual void Init(bool offScreen)
	{
		m_reSpawning = false;
		if(m_spinWhileMoving)
		{
			if(m_spinDegreeVariance > 360 || m_spinDegreeVariance <-360)
			{
				m_spinDegreeVariance = 360;
			}

		}
		m_speed = ParallaxingBackGroundManager.GetInstance().GetManagedBackGround().GetLayerSpeed(this.renderer.sortingOrder);
		m_speed = Random.Range(m_speed, m_speed+m_speedVariance);

		//m_startScale = this.renderer.transform.localScale;
		switch(m_direction)
		{
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
			{
			m_moveDirection = new Vector2(-1,0);
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
			{
			m_moveDirection = new Vector2(1,0);
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
			{
			m_moveDirection = new Vector2(0,1);
			}
				break;
			case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
			{
			m_moveDirection = new Vector2(0,-1);
			}
				break;
		}
		float orthographicCamSize = Camera.main.orthographicSize;
		ScreenSizeInUnits.y = orthographicCamSize *2;
		ScreenSizeInUnits.x = ScreenSizeInUnits.y* Camera.main.aspect;

		//Debug.Log("Name : "+this.gameObject.name+" prevPos : "+this.gameObject.transform.position.x);
		RandomizeAndAssignAttributes(offScreen);
		if(m_specialistSpawn)
		{
			Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float randomSpawnVar = 0;
			switch(m_direction)
			{
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
				{
					randomSpawnVar = Random.Range(currentScreenOffset.y + m_spawnMin,currentScreenOffset.y + m_spawnMax);
				this.gameObject.transform.position = new Vector3(Random.Range(m_specialistPos-m_specialistVariance,m_specialistPos + m_specialistVariance),randomSpawnVar,gameObject.transform.position.z);
				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
				{
					randomSpawnVar = Random.Range(currentScreenOffset.y + m_spawnMin,currentScreenOffset.y + m_spawnMax);
				this.gameObject.transform.position = new Vector3(Random.Range(m_specialistPos-m_specialistVariance,m_specialistPos - m_specialistVariance),randomSpawnVar,gameObject.transform.position.z);
				}
					break;

				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
				{
					randomSpawnVar = Random.Range(currentScreenOffset.x + m_spawnMin,currentScreenOffset.x + m_spawnMax);
				this.gameObject.transform.position = new Vector3(randomSpawnVar,Random.Range(m_specialistPos-m_specialistVariance,m_specialistPos - m_specialistVariance),gameObject.transform.position.z);
				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
				{
					randomSpawnVar = Random.Range(currentScreenOffset.x + m_spawnMin,currentScreenOffset.x + m_spawnMax);
				this.gameObject.transform.position = new Vector3(randomSpawnVar,Random.Range(m_specialistPos-m_specialistVariance,m_specialistPos - m_specialistVariance),gameObject.transform.position.z);
				}
					break;
			}

		}
	}

	protected virtual bool CheckIfOutOfBounds()
	{
		Vector3 quadPos = this.transform.position;
		Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2(0,0));
		switch(m_direction)
		{
			
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
		{
			if(quadPos.x + this.renderer.bounds.size.x < currentScreenOffset.x)
			{
				return true;
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
		{
			if(quadPos.x + this.renderer.bounds.size.x > ScreenSizeInUnits.x +  currentScreenOffset.x )
			{

				return true;
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
		{
			if(quadPos.y + this.renderer.bounds.size.y > currentScreenOffset.y + ScreenSizeInUnits.y)
			{

				return true;
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
		{
			if(quadPos.x + this.renderer.bounds.size.y < currentScreenOffset.y)
			{

				return true;
			}
		}
			break;
		}
		return false;

	}

	protected virtual void RandomizeAndAssignAttributes(bool offScreen)
	{
		Vector3 quadPos;
		Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2(0,0));
		m_spinDegreesPerSec = Random.Range(-((float)m_spinDegreeVariance)*10,(float)m_spinDegreeVariance*10)/10;
		quadPos = this.gameObject.transform.position;
		
		Vector3 currentScale = this.gameObject.transform.localScale;
		float randomY=0;
		float randomX=0;

		if(offScreen)
		{
			switch(m_direction)
			{
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
				{
					randomX = Random.Range(currentScreenOffset.x + ScreenSizeInUnits.x + this.renderer.bounds.extents.x,currentScreenOffset.x + ScreenSizeInUnits.x +this.renderer.bounds.extents.x + m_maxDistOffset);
					randomY = Random.Range(currentScreenOffset.y + m_spawnMin,currentScreenOffset.y + m_spawnMax);
					//set transform
					quadPos = new Vector3 (randomX,randomY,quadPos.z);
					
					//find and set scale
					float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
					Vector3 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
				m_currentScale = scale;
					this.gameObject.transform.localScale = scale;
					this.gameObject.transform.position = quadPos;

				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
				{
					randomX = Random.Range(currentScreenOffset.x - ScreenSizeInUnits.x - this.renderer.bounds.extents.x - m_maxDistOffset,currentScreenOffset.x - this.renderer.bounds.extents.x);
					randomY = Random.Range(currentScreenOffset.y + m_spawnMin,currentScreenOffset.y + m_spawnMax);
					//set transform
					quadPos = new Vector3 (randomX,randomY,quadPos.z);
					
					//find and set scale
					float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
					Vector3 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
					m_currentScale = scale;
					this.gameObject.transform.localScale = scale;
					this.gameObject.transform.position = quadPos;

				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
				{
				randomY = Random.Range(currentScreenOffset.y - ScreenSizeInUnits.y - this.renderer.bounds.extents.y - m_maxDistOffset,currentScreenOffset.y - this.renderer.bounds.extents.y);
				randomX = Random.Range(currentScreenOffset.x + m_spawnMin,currentScreenOffset.x + m_spawnMax);
					//set transform
					quadPos = new Vector3 (randomX,randomY,quadPos.z);
					
					//find and set scale
					float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
					Vector3 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
				m_currentScale = scale;
					this.gameObject.transform.localScale = scale;
					this.gameObject.transform.position = quadPos;	

				}
					break;
				case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
				{
				randomY = Random.Range(currentScreenOffset.y + ScreenSizeInUnits.y + this.renderer.bounds.extents.y ,currentScreenOffset.y +ScreenSizeInUnits.y + this.renderer.bounds.extents.y + m_maxDistOffset);
					randomX = Random.Range(currentScreenOffset.x + m_spawnMin,currentScreenOffset.x + m_spawnMax);
					//set transform
					quadPos = new Vector3 (randomX,randomY,quadPos.z);

					//find and set scale
					float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
					Vector3 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
				m_currentScale = scale;
					this.gameObject.transform.localScale = scale;
					this.gameObject.transform.position = quadPos;	

				}
					break;
			}
		}
		else
		{

			randomX = Random.Range(currentScreenOffset.x , currentScreenOffset.x+ ScreenSizeInUnits.x);
			randomY = Random.Range(currentScreenOffset.y + m_spawnMin, currentScreenOffset.y+ m_spawnMax);
			//set position
			quadPos = new Vector3 (randomX,randomY,quadPos.z);
			//set rotation
			this.transform.rotation = Quaternion.AngleAxis(Random.Range(m_baseSpawnRotDegrees- m_rotDegreesVariance,m_baseSpawnRotDegrees+m_rotDegreesVariance),transform.forward);

			//find and set scale
			float scaleToSet = Random.Range(m_minScaleVariance,m_maxScaleVariance);
			Vector3 scale = new Vector3 (m_startScale.x * scaleToSet,m_startScale.y *scaleToSet,this.gameObject.transform.localScale.z);
			m_currentScale = scale;
			this.gameObject.transform.localScale = scale;
			this.gameObject.transform.position = quadPos;

		}
	}



	public virtual void UpdateRandomizer(float songLength) 
	{
		Vector3 quadPos = this.gameObject.transform.position;
		if(!m_reSpawning)
		{
			//Check if needs to respawn
			if(CheckIfOutOfBounds())
			{
				m_reSpawning = true;
				m_reSpawnTime = Random.Range(m_baseRespawnTime,m_baseRespawnTime+m_reSpawnTimeVariance);
				return;
			}
			//if not respawning state, Update;

			//if need to spin, spin
			if(m_spinWhileMoving)
			{
				Vector3 currentRotation = this.transform.rotation.eulerAngles;
				currentRotation.z+= m_spinDegreeVariance*Time.deltaTime;
				//catch exceptions in rotation and change appropriately
				if(currentRotation.z > 360)
				{
					currentRotation.z = 0;
				}
				else if(currentRotation.z < 0 )
				{
					currentRotation.z = 360;
				}

				this.transform.eulerAngles = currentRotation;
			}

			//update motion
			float timeToTravelScreenLength = songLength/m_speed;
			float unitsPerSecond = (Camera.main.orthographicSize*2 * Camera.main.aspect)/timeToTravelScreenLength;

			Vector3 increment = m_moveDirection *unitsPerSecond * Time.deltaTime;

			quadPos = this.gameObject.transform.position;
			quadPos +=increment;
			this.gameObject.transform.position = quadPos;
		}
		else
		{
			if(m_reSpawnTimer < m_reSpawnTime)
			{
				m_reSpawnTimer += Time.deltaTime;
			}
			else
			{
				m_reSpawnTimer = 0;
				m_reSpawning = false;

				RandomizeAndAssignAttributes(true);
			}
		}

		if(m_canScaleWithEnergy && Camera.main.audio.clip != null)
		{
			m_toneBandToDeriveEnergy = Mathf.Clamp(m_toneBandToDeriveEnergy,0,System.Enum.GetNames(typeof(DETECTION_TONE)).Length-1);
			float maxVolume = 100;
			float instantVolume = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.008f)[m_toneBandToDeriveEnergy];
			float scalePercentToSet = m_minScalePercent + ((instantVolume/maxVolume) * (m_maxScalePercent- m_minScalePercent));
			Vector2 scaleToSet = new Vector2(m_currentScale.x * (scalePercentToSet/100),m_currentScale.y * (scalePercentToSet/100));
			this.transform.localScale = scaleToSet;
		}
	}


	public virtual void UpdateRandomizer() 
	{
		Vector3 quadPos;
		Vector2 currentScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2(0,0));

		quadPos = this.gameObject.transform.position;
		
		Vector3 currentScale = this.gameObject.transform.localScale;
		float randomY=0;
		float randomX=0;
		switch(m_direction)
		{
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_LEFT:
		{
			if(quadPos.x + this.renderer.bounds.size.x < currentScreenOffset.x)
			{
				if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
				{
					RandomizeAndAssignAttributes(true);
				}
				else
				{
					RandomizeAndAssignAttributes(false);
				}
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_RIGHT:
		{
			if(quadPos.x + this.renderer.bounds.size.x > ScreenSizeInUnits.x +  currentScreenOffset.x )
			{
				if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
				{
					RandomizeAndAssignAttributes(true);
				}
				else
				{
					RandomizeAndAssignAttributes(false);
				}
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_TOP:
		{
			if(quadPos.y + this.renderer.bounds.size.y > currentScreenOffset.y + ScreenSizeInUnits.y)
			{
				if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
				{
					RandomizeAndAssignAttributes(true);
				}
				else
				{
					RandomizeAndAssignAttributes(false);
				}
			}
		}
			break;
		case RANDOMIZED_MOVE_DIRECTION.DIRECTION_BOTTOM:
		{
			if(quadPos.x + this.renderer.bounds.size.y < currentScreenOffset.y)
			{
				if(AppManager.GetInstance().GetCurrentScene() == GAME_SCENES.SCENE_GAME)
				{
					RandomizeAndAssignAttributes(true);
				}
				else
				{
					RandomizeAndAssignAttributes(false);
				}
			}
		}
			break;
		}
		//if needed to spin, spin
		if(m_spinWhileMoving)
		{
			Vector3 currentRotation = this.transform.rotation.eulerAngles;
			currentRotation.z+= m_spinDegreeVariance*Time.deltaTime;
			//catch exceptions in rotation and change appropriately
			if(currentRotation.z > 360)
			{
				currentRotation.z = 0;
			}
			else if(currentRotation.z < 0 )
			{
				currentRotation.z = 360;
			}
			
			this.transform.eulerAngles = currentRotation;
		}
		//update motion
		quadPos = this.gameObject.transform.position;
		quadPos += m_moveDirection *Time.deltaTime*(m_speed/100);
		this.gameObject.transform.position = quadPos;
		
		
	}
}
