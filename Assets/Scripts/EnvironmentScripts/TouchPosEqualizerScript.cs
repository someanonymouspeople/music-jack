using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class TouchPosEqualizerScript : MonoBehaviour {

	List<JumpingJackScript> m_jumpingJackList;
	public int m_subBandCount;
	public int m_spikeSensitivity;
	public float m_refValue;
	public bool m_direction;
	bool m_prevDirection;
	public bool m_mirror;
	bool m_prevMirror;
	public bool m_colorDir;
	bool m_prevColorDir;
	public bool m_stopAtPlayer;
	bool m_prevStopAtPlayer;
	public bool m_glowMode;
	bool m_prevEqMode;
	public static TouchPosEqualizerScript m_instance;
	

	public static TouchPosEqualizerScript GetInstance()
	{
		return m_instance;
	}
	void Awake()
	{


		if(TouchPosEqualizerScript.GetInstance() != null)
		{
			Destroy(this);
			return;
		}
		m_instance = this;
	}
	void Start () 
	{
		m_prevColorDir = m_colorDir;
		m_prevDirection = m_direction;
		m_prevMirror = m_mirror;
		m_prevStopAtPlayer = m_stopAtPlayer;
		GameObject eq = new GameObject();
		eq.name = "TouchPosEq";
		eq.transform.parent = Camera.main.transform;
		m_jumpingJackList = new List<JumpingJackScript>();
		Sprite[] spritesToPickFrom = Resources.LoadAll<Sprite>("TouchPosEqSprites/EQ");
		for(int i = 0; i<m_subBandCount ; i++)
		{

			GameObject go = Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;
			go.transform.position = new Vector3(-99,-99,0);
			JumpingJackScript tempJack = go.GetComponent<JumpingJackScript>();



			//check and set color
			Color colorToSet = new Color();
			if(m_colorDir)
			{
				colorToSet = Color.Lerp(Color.black, Color.white, 1- ((float)i/(float)m_subBandCount));
			}
			else
			{
				colorToSet = Color.Lerp(Color.black, Color.white, (float)i/(float)m_subBandCount);
			}

			//check and set direction
			float indexToSet =0;

			indexToSet= ((float)i/(float)m_subBandCount) * MusicAnalyzerV2.GetInstance().m_subBandCount ;



			tempJack.SetStopAtPlayer(m_stopAtPlayer);
			tempJack.SetDirection(m_direction);
			tempJack.SetLinkedSubBandIndex(System.Convert.ToInt32(indexToSet));
			tempJack.SetSpikeSensitivity(m_spikeSensitivity);
			tempJack.m_subBandCountToConsider = m_subBandCount;
			tempJack.SetRefValue(m_refValue);
			tempJack.GetComponent<SpriteRenderer>().sprite = spritesToPickFrom[i];

			//go.renderer.sharedMaterial.color = colorToSet;
			go.transform.parent = eq.transform;
			m_jumpingJackList.Add(tempJack);

			//check and spawnMirror
			if(m_mirror)
			{
				GameObject goMir = Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;

				JumpingJackScript tempJackMir = goMir.GetComponent<JumpingJackScript>();
				tempJackMir.SetStopAtPlayer(m_stopAtPlayer);
				tempJackMir.SetLinkedSubBandIndex(System.Convert.ToInt32(indexToSet));
				tempJackMir.SetSpikeSensitivity(m_spikeSensitivity);
				tempJackMir.m_subBandCountToConsider = m_subBandCount;
				tempJackMir.SetRefValue(m_refValue);
				tempJackMir.SetIsMirror(m_mirror);
				tempJackMir.GetComponent<SpriteRenderer>().sprite = spritesToPickFrom[i];
				//goMir.renderer.sharedMaterial.color = colorToSet;
				goMir.transform.parent = eq.transform;
				m_jumpingJackList.Add(tempJackMir);
			}

		}
	
	}

	public void RefreshVisualizer()
	{
		Destroy(GameObject.Find("TouchPosEq"));

		GameObject eq = new GameObject();
		eq.name = "TouchPosEq";
		m_jumpingJackList.Clear();
		Sprite[] spritesToPickFrom;
		if(m_glowMode)
		{
			spritesToPickFrom = Resources.LoadAll<Sprite>("TouchPosEqSprites/EQ_BW_Glow");
		}
		else
		{
			spritesToPickFrom = Resources.LoadAll<Sprite>("TouchPosEqSprites/EQ");
		}

		for(int i = 0; i<m_subBandCount; i++)
		{

			GameObject go = Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;
			JumpingJackScript tempJack = go.GetComponent<JumpingJackScript>();

			//check and set color
			Color colorToSet = new Color();
			if(m_colorDir)
			{
				colorToSet = Color.Lerp(Color.black, Color.white, 1- ((float)i/(float)m_subBandCount));
			}
			else
			{
				colorToSet = Color.Lerp(Color.black, Color.white, (float)i/(float)m_subBandCount);
			}

			//check and set direction
			float indexToSet =0;

				indexToSet= ((float)i/(float)m_subBandCount) * MusicAnalyzerV2.GetInstance().m_subBandCount ;
			tempJack.SetStopAtPlayer(m_stopAtPlayer);
			tempJack.SetDirection(m_direction);
			tempJack.SetLinkedSubBandIndex(System.Convert.ToInt32(indexToSet));
			tempJack.SetSpikeSensitivity(m_spikeSensitivity);
			tempJack.m_subBandCountToConsider = m_subBandCount;
			tempJack.SetRefValue(m_refValue);
			tempJack.GetComponent<SpriteRenderer>().sprite = spritesToPickFrom[i];
			//go.renderer.sharedMaterial.color = colorToSet;

			go.transform.parent = eq.transform;
			m_jumpingJackList.Add(tempJack);

			//check and spawnMirror
			if(m_mirror)
			{
				GameObject goMir = Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;
				JumpingJackScript tempJackMir = goMir.GetComponent<JumpingJackScript>();
				tempJackMir.SetStopAtPlayer(m_stopAtPlayer);
				tempJackMir.SetDirection(m_direction);
				tempJackMir.SetLinkedSubBandIndex(System.Convert.ToInt32(indexToSet));
				tempJackMir.SetSpikeSensitivity(m_spikeSensitivity);
				tempJackMir.m_subBandCountToConsider = m_subBandCount;
				tempJackMir.SetRefValue(m_refValue);
				tempJackMir.SetIsMirror(m_mirror);
				tempJackMir.GetComponent<SpriteRenderer>().sprite = spritesToPickFrom[i];
				//goMir.renderer.sharedMaterial.color = colorToSet;
				goMir.transform.parent = eq.transform;
				m_jumpingJackList.Add(tempJackMir);
			}

		}
	}


	// Update is called once per frame
	public void UpdateTouchPosEqualizer () 
	{

		if(m_glowMode != m_prevEqMode)
		{
			RefreshVisualizer();
		}
		if(Camera.main.audio.clip != null)
		{

			if(m_direction != m_prevDirection || m_colorDir != m_prevColorDir || m_mirror != m_prevMirror || m_prevStopAtPlayer != m_stopAtPlayer)
			{
				RefreshVisualizer();
				m_prevStopAtPlayer = m_stopAtPlayer;
				m_prevColorDir = m_colorDir;
				m_prevDirection = m_direction;
				m_prevMirror = m_mirror;
			}
			float[] instantVolumeArray = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfSubBands(true,m_refValue);
			float[] averageVolumeArray = MusicAnalyzerV2.GetInstance().GetAverageVolumeOfSubBands(true,m_refValue);
			for(int i = 0; i<MusicAnalyzerV2.GetInstance().m_subBandCount; i++)
			{
				m_jumpingJackList[i].UpdateInPlace(instantVolumeArray[i],averageVolumeArray[i]);
			}
		}

		m_prevEqMode= m_glowMode;
	
	}
}
