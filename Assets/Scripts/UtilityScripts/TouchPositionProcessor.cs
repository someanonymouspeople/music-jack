﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TOUCHPOS_DRAW_MODE
{
	DRAW_MODE_LINE,
	DRAW_MODE_SQUIGGLY_LINE,
	DRAW_MODE_BARS,
	DRAW_MODE_DUST
}
public class TouchPositionProcessor : MonoBehaviour 
{
	//public TOUCHPOS_DRAW_MODE m_mode;
	public static TouchPositionProcessor m_instance;
	GameObject m_touchPositionIndicator;
	public float m_minDistance;
	//public bool m_optimizeTerrain;
	//int m_currentSubBandIndex;
	//int m_indexSwitch;
	//public float m_maxEffectValue;
	//public bool m_reverseTerrainColor;
	void Awake()
	{
		//m_indexSwitch = 1;
		//m_currentSubBandIndex= 2;
		if(TouchPositionProcessor.GetInstance())
		{
			Destroy(this);
			return;
		}
		m_instance = this;
		m_minDistance = 0.04f;
		//m_minDistance = SmoothenedTouchPositionStorer.GetInstance().m_minDistanceBetweenStore;
	}

	public static TouchPositionProcessor GetInstance()
	{

		return m_instance;
	}


//	public void DrawParticleAt(Vector3 pos,float life)
//	{
////		switch(m_mode)
////		{
////		case TOUCHPOS_DRAW_MODE.DRAW_MODE_BARS:
////		{
////			m_minDistance = 0.1f;
////			GameObject go = GameObject.Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;
////			go.transform.position = pos;
////			//go.GetComponent<TouchPosParticleScript>().SetLifeTime(life);
////			//go.GetComponent<JumpingJackScript>().SetLinkedSubBandIndex(m_currentSubBandIndex);
////			go.GetComponent<JumpingJackScript>().SetMaxJumpInUnits(m_maxEffectValue);
////			go.GetComponent<JumpingJackScript>().SetTerrainGradientBool(m_reverseTerrainColor);
////			m_currentSubBandIndex++;
////			if(m_currentSubBandIndex == MusicAnalyzerV2.GetInstance().m_subBandCount)
////			{
////				m_currentSubBandIndex = 0;
////			}
////		}
////			break;
////		case TOUCHPOS_DRAW_MODE.DRAW_MODE_DUST:
////		{
////			m_minDistance = 0.1f;
////			GameObject go = GameObject.Instantiate(Resources.Load("GameObjects/Utilities/JumpingJack")) as GameObject;
////			go.transform.position = pos;
////			//go.GetComponent<TouchPosParticleScript>().SetLifeTime(life);
////			//go.GetComponent<JumpingJackScript>().SetLinkedSubBandIndex(m_currentSubBandIndex);
////			go.GetComponent<JumpingJackScript>().SetMaxJumpInUnits(m_maxEffectValue);
////			go.GetComponent<JumpingJackScript>().m_posMode = true;
////			m_currentSubBandIndex++;
////			if(m_currentSubBandIndex == MusicAnalyzerV2.GetInstance().m_subBandCount)
////			{
////				m_currentSubBandIndex = 0;
////			}
////		}
////			break;
////		case TOUCHPOS_DRAW_MODE.DRAW_MODE_SQUIGGLY_LINE:
////		{
////			m_minDistance = 0.1f;
////			GameObject go = GameObject.Instantiate(Resources.Load("GameObjects/Utilities/TouchParticle")) as GameObject;
////
////			go.GetComponent<TouchPosParticleScript>().SetLifeTime(life);
////			m_currentSubBandIndex += 1*m_indexSwitch;
////			if(m_currentSubBandIndex == 1 || m_currentSubBandIndex == MusicAnalyzerV2.GetInstance().m_subBandCount -1)
////			{
////				m_indexSwitch *= -1;
////			}
////
////			pos.y += (MusicAnalyzerV2.GetInstance().GetInstantVolumeOfSubBand(m_currentSubBandIndex,0.01f)/100.0f) * m_maxEffectValue;
////			go.transform.position = pos;
////
////		}
////			break;
////		case TOUCHPOS_DRAW_MODE.DRAW_MODE_LINE:
////		{
////			m_minDistance = 0.1f;
////			GameObject go = GameObject.Instantiate(Resources.Load("GameObjects/Utilities/TouchParticle")) as GameObject;
////			go.transform.position = pos;
////			go.GetComponent<TouchPosParticleScript>().SetLifeTime(life);
////		}
////			break;
////
////		
////		}
//	}



	public List<Vector3> SmoothenBetweenAndAdd(Vector3 posBehind,Vector3 posForward)
	{
		List<Vector3> addedVecs = new List<Vector3>();
		float distBetween = (posForward - posBehind).magnitude;
		
		if(distBetween > m_minDistance)
		{
			float lerpIncrement = 1f/(distBetween/m_minDistance);
			int indexIncrement = 0;
			for(float currentLerpLevel = 0; currentLerpLevel <1; currentLerpLevel+= lerpIncrement)
			{
				Vector3 vectorToAdd = Vector3.Lerp(posBehind,posForward,currentLerpLevel);
				SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList().Insert(SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList().Count-1,vectorToAdd);
				addedVecs.Add(vectorToAdd);
			}

		}
		return addedVecs;
	}


	public void SortElementsOnX()
	{
		List<Vector3> tempList = new List<Vector3>();
		tempList = SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList();
		tempList.Sort((a,b) => a.x.CompareTo(b.x));
		SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList().Sort((a, b) => a.x.CompareTo(b.x));
	}
	public void OptimizeElements()
	{
		float minCamFieldX = Camera.main.ScreenToWorldPoint(new Vector2()).x;
		List<Vector3> listToOptimize = SmoothenedTouchPositionStorer.GetInstance().GetModifiableSmoothenedPositionList();
		listToOptimize.RemoveAll((x)=>x.x<minCamFieldX);

	}

}
