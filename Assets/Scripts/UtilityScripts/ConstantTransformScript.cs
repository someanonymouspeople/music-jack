﻿using UnityEngine;
using System.Collections;

public enum CONST_TRANFORM_TYPE
{
	CONST_TRANSFORM_SCALE,
	CONST_TRANSFORM_ROTATE,
	CONST_TRANSFORM_TRANSLATE
}
public class ConstantTransformScript : MonoBehaviour {

	public CONST_TRANFORM_TYPE m_type;
	public float m_scaleIncrementPerCycle;
	public float m_translateIncrementPerCycle;
	public float m_rotationIncrementPerCycle;

	public Vector3 m_axesToIncrement;
	public Vector3 m_startScale;
	public Vector3 m_startRot;
	public Vector3 m_startLocalPos;
	float m_cycleTime;
	public bool m_shouldFade;
	public int m_cyclesToFade;

	public void SetCyclesToFade(int cycleCount)
	{
		m_cyclesToFade = cycleCount;
	}
	void Awake () 
	{
		if(m_cyclesToFade ==0)
		{
			m_cyclesToFade = 1;
		}
		m_startScale = this.transform.localScale;
		m_startRot = this.transform.eulerAngles;
		m_startLocalPos = this.transform.localPosition;
		if(m_scaleIncrementPerCycle == 0)
		{
			m_scaleIncrementPerCycle = 1;
		}
		if(m_translateIncrementPerCycle == 0)
		{
			m_translateIncrementPerCycle = Vector2.Distance(Camera.main.ScreenToWorldPoint(Vector2.zero), Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.orthographicSize*2 * Camera.main.aspect,0)));
		}
			if(m_rotationIncrementPerCycle == 0)
		{
				m_rotationIncrementPerCycle = 360;
		}

		if(m_axesToIncrement.x > 1)
		{
			m_axesToIncrement.x = 1;
		}
		if(m_axesToIncrement.y > 1)
		{
			m_axesToIncrement.y = 1;
		}
		if(m_axesToIncrement.z > 1)
		{
			m_axesToIncrement.z = 1;
		}

		m_cycleTime = 60;

	}

	public void Reset()
	{
		this.transform.localScale = m_startScale;
		this.transform.eulerAngles = m_startRot;
		this.transform.localPosition = m_startLocalPos;
	}
	
	public void SetCycleTime(float cycleTime)
	{
		m_cycleTime = cycleTime;
	}

	public void Update () 
	{
		switch(m_type)
			{
				case CONST_TRANFORM_TYPE.CONST_TRANSFORM_SCALE:
				{
				Vector3 curScale = this.transform.localScale;
				curScale.x += m_scaleIncrementPerCycle/m_cycleTime * Time.deltaTime;
				curScale.y += m_scaleIncrementPerCycle/m_cycleTime * Time.deltaTime;
				curScale.z += m_scaleIncrementPerCycle/m_cycleTime * Time.deltaTime;

				curScale.x *= m_axesToIncrement.x;
				curScale.y *= m_axesToIncrement.y;
				curScale.z *= m_axesToIncrement.z;
				this.transform.localScale = curScale;
					if(m_shouldFade)
					{
						
					float m_currentAlpha =this.renderer.material.color.a;
					m_currentAlpha -= 1/(m_cycleTime*m_cyclesToFade) * Time.deltaTime;
					
					if(m_currentAlpha >= 0)
					{
					this.renderer.material.color = new Color(1,1,1,m_currentAlpha);
					}
					else
					{
						m_shouldFade = false;
					}
					}
				}
					break;
				case CONST_TRANFORM_TYPE.CONST_TRANSFORM_ROTATE:
				{
				Vector3 curRot = this.transform.eulerAngles;
				curRot.x += m_rotationIncrementPerCycle/m_cycleTime * Time.deltaTime;
			curRot.y += m_rotationIncrementPerCycle/m_cycleTime * Time.deltaTime;
			curRot.z += m_rotationIncrementPerCycle/m_cycleTime * Time.deltaTime;

			curRot.x *= m_axesToIncrement.x;
			curRot.y *= m_axesToIncrement.y;
			curRot.z *= m_axesToIncrement.z;
				this.transform.Rotate(curRot);
				}
					break;

				case CONST_TRANFORM_TYPE.CONST_TRANSFORM_TRANSLATE:
				{
			Vector3 curPos = this.transform.localPosition;
				curPos.x += m_translateIncrementPerCycle/m_cycleTime * Time.deltaTime;
				curPos.y += m_translateIncrementPerCycle/m_cycleTime * Time.deltaTime;
				curPos.z += m_translateIncrementPerCycle/m_cycleTime * Time.deltaTime;
			curPos.x *= m_axesToIncrement.x;
			curPos.y *= m_axesToIncrement.y;
			curPos.z *= m_axesToIncrement.z;
			this.transform.localPosition = curPos;
				}
					break;
			}

	}


}
