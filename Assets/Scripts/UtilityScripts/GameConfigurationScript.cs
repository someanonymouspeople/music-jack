﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;

public class GameConfigurationScript : MonoBehaviour {


	public delegate bool DirectorySetterManagerCallBack(string path);
	GameObject m_dialogeBox;
	bool m_attemptingLogin;
	bool m_directorySet;

	IEnumerator CouldNotLogInInvoke()
	{
		Debug.Log ("Game configuration could not login invoked");
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Unable to Login \nFacebook servers seem down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		InitFirstDB();
		Debug.Log ("Game config DB reinited");
	}
	IEnumerator FBActiveInvoke()
	{
		Debug.Log ("New Share button FBActive invoked");

		m_attemptingLogin = true;
		FB.Login("public_profile,user_friends,publish_actions",CustomConnectedButtonCallBack);

		while(m_attemptingLogin)
		{
			yield return null;
		}

		if(FB.IsLoggedIn)
		{
			GameConfigurationHolderScript.GetInstance().m_guestMode = false;
			Application.LoadLevel("MainMenuScene");
		}
		else
		{
			StartCoroutine("CouldNotLogInInvoke");
		}

	}

	void CustomConnectedButtonCallBack(FBResult result)
	{
		Debug.Log ("Game configuration CustomShareButtonCallBack invoked");
		
		m_attemptingLogin = false;
		
		
	}
	IEnumerator NetInactiveInvoke()
	{
		Debug.Log ("Game configuration NetInactiveInvoked");

		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Internet down\n please connect and retry",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		//Reinit
		InitFirstDB();

	}

	IEnumerator AsYourselfInvoke()
	{
		Destroy(m_dialogeBox);
		Debug.Log ("Game configuration AsYourselfInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Initializing game",0);
		dialogueBox.GetComponentInChildren<TextMesh>().characterSize = 0.045f;
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		Debug.Log ("ConnectedFiller button is clicked");
		
		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
		
		if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
		{
			Debug.Log ("ConnectedFiller button is connected to internet");
			
			//GPG
			//Find and set the playing platform
			PlayGamesPlatform.Activate();
			
			//enable debug log
			PlayGamesPlatform.DebugLogEnabled = true;
			
			//Login
			Social.localUser.Authenticate((bool success) => {
				if(success)
				{
					Debug.Log("Logged in to google play "+Social.localUser.userName);
				}
				else
				{
					Debug.Log("Coudnt log in to google play");
				}
				
			});
			
			
			
			result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
			
			if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
			{
				Debug.Log ("ConnectedFiller button FB active");
				Destroy(m_dialogeBox);
				StartCoroutine("FBActiveInvoke");
				
			}
			else
			{
				Destroy(m_dialogeBox);
				StartCoroutine("CouldNotLogInInvoke");
			}
		}
		else
		{
			Destroy(m_dialogeBox);
			StartCoroutine("NetInactiveInvoke");
		}
		return true;
	}

	bool ConnectedFillerCallBack(MusicJunkieDialogBoxScript script)
	{
		StartCoroutine("AsYourselfInvoke");
		return true;
	}

	IEnumerator GuestFillerInvoke()
	{
		Destroy(m_dialogeBox);
		Debug.Log ("Game configuration GuestFillerInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Initializing game \n as guest",0);
		dialogueBox.GetComponentInChildren<TextMesh>().characterSize = 0.045f;
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		Application.LoadLevel("MainMenuScene");
	}

	bool GuestFillerCallBack(MusicJunkieDialogBoxScript script)
	{
		StartCoroutine("GuestFillerInvoke");
		return true;
	}

	void Awake()
	{
		m_directorySet = false;
	}

	void LoginCallBack(GameObject button)
	{
		StartCoroutine("AsYourselfInvoke");
	}

	void GuestCallBack(GameObject button)
	{
		StartCoroutine("GuestFillerInvoke");
	}

	void InitFirstDB()
	{
		m_dialogeBox = GameObject.Instantiate(Resources.Load("UI/LaunchPad")) as GameObject;

		CallBackButton[] buttonArray = new CallBackButton[2];
		int index = 0;
		foreach(CallBackButton button in m_dialogeBox.GetComponentsInChildren<CallBackButton>())
		{
			buttonArray[index] = button;
			index ++;
		}
		buttonArray[1].AssignCallBack(LoginCallBack);
		buttonArray[0].AssignCallBack(GuestCallBack);

	}

	bool CustomDirectorySetterManagerCallBack(string path)
	{
		if(path != "NotSet")
		{
			GameConfigurationHolderScript.GetInstance().SetSongDataPath(path);
			m_directorySet = true;
		}
		else
		{
			m_directorySet = false;
		}
		return m_directorySet;
	}

	IEnumerator FirstRoutine()
	{
		GameObject directorySetter = GameObject.Instantiate(Resources.Load("UI/DirectorySetterPrefab")) as GameObject;
		directorySetter.GetComponent<DirectorySetterManagerScript>().SetCallBack(CustomDirectorySetterManagerCallBack);
		while(!m_directorySet)
		{
			yield return null;
		}
		Destroy(directorySetter);
		InitFirstDB();
	}

	void Start () 
	{
		if(GameConfigurationHolderScript.GetInstance().m_firstTime )
		{
			StartCoroutine("FirstRoutine");
		}
		else if(GameConfigurationHolderScript.GetInstance().m_guestMode)
		{
			InitFirstDB();
		}
		else
		{
			ConnectedFillerCallBack(null);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
