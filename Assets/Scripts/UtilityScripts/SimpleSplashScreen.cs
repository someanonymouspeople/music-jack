﻿using UnityEngine;
using System.Collections;

public enum SPLASH_STATES
{
	STATE_FADING_IN,
	STATE_HANGING_AROUND,
	STATE_FADING_OUT,
	STATE_SWITCHING,
	STATE_DONE
}
public class SimpleSplashScreen : MonoBehaviour {

	public Sprite[] m_splashScreens;
	public int m_activeSplashIndex;
	public float m_fadeInOutTime;
	float m_commonTimer;
	public float m_hangTime;
	public SPLASH_STATES m_state;
	GAME_SCENES m_sceneToSwitchTo;

	void CustomInitComplete()
	{
		Debug.Log("FB init complete");
	}
	static void CustomHideUnity(bool checkFlag)
	{
		Debug.Log ("Unity hiding");
		if(!checkFlag)
		{
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}
	}
	void Awake()
	{
		if(m_splashScreens.Length<1)
		{
			Debug.Log("no splash screens added... moving on");
			this.SwitchState(SPLASH_STATES.STATE_DONE);
		}
		m_activeSplashIndex = 0;
//		for(int i =0;i<m_splashScreensPrefabRefs.Length;i++)
//		{
//			m_splashScreensPrefabRefs[i] = GameObject.Instantiate(m_splashScreensPrefabRefs[i]);
//			m_splashScreensPrefabRefs[i].renderer.material.color= new Color(1,1,1,0);
//		}
		this.GetComponent<SpriteRenderer>().sprite = m_splashScreens[m_activeSplashIndex];
			
		FB.Init(CustomInitComplete,CustomHideUnity);
		
		m_state = SPLASH_STATES.STATE_FADING_IN;
	}

	void UpdateFadingIn()
	{
		if(m_commonTimer< m_fadeInOutTime)
		{
			m_commonTimer += Time.deltaTime;
			float valueToSet = m_commonTimer/m_fadeInOutTime;
			Color col = this.gameObject.renderer.material.color;
			col.a = Mathf.Lerp(0,1,valueToSet);
			this.gameObject.renderer.material.color = col;
		}
		else
		{
			SwitchState(SPLASH_STATES.STATE_HANGING_AROUND);
		}
	}

	void UpdateHangingAround()
	{
		if(m_commonTimer< m_hangTime)
		{
			m_commonTimer += Time.deltaTime;
		}
		else
		{
			SwitchState(SPLASH_STATES.STATE_FADING_OUT);
		}
	}

	void UpdateFadingOut()
	{
		if(m_commonTimer< m_fadeInOutTime)
		{
			m_commonTimer += Time.deltaTime;
			float valueToSet = m_commonTimer/m_fadeInOutTime;
			Color col = this.gameObject.renderer.material.color;
			col.a = Mathf.Lerp(1,0,valueToSet);
			this.gameObject.renderer.material.color = col;
		}
		else
		{
			SwitchState(SPLASH_STATES.STATE_SWITCHING);
		}
	}

	void SwitchState(SPLASH_STATES stateToSet)
	{
		switch(stateToSet)
		{
		case SPLASH_STATES.STATE_FADING_IN:
		{
			this.GetComponent<SpriteRenderer>().material.color = new Color(1,1,1,0);
			m_commonTimer = 0;
		}
			break;
		case SPLASH_STATES.STATE_HANGING_AROUND:
		{
			this.GetComponent<SpriteRenderer>().material.color = new Color(1,1,1,1);
			m_commonTimer = 0;
		}
			break;
		case SPLASH_STATES.STATE_FADING_OUT:
		{
			m_commonTimer = 0;
		}
			break;
		case SPLASH_STATES.STATE_SWITCHING:
		{
			if(m_activeSplashIndex < m_splashScreens.Length-1)
			{
				m_activeSplashIndex++;
				this.GetComponent<SpriteRenderer>().sprite = m_splashScreens[m_activeSplashIndex];
				stateToSet = SPLASH_STATES.STATE_FADING_IN;
				this.GetComponent<SpriteRenderer>().material.color = new Color(1,1,1,0);
				m_commonTimer = 0;
			}
			else
			{
				SwitchState(SPLASH_STATES.STATE_DONE);
			}
		}
			break;
		case SPLASH_STATES.STATE_DONE:
		{
			this.enabled = false;
			this.gameObject.AddComponent<GameConfigurationScript>();
			Destroy (this);
			//Application.LoadLevel("MainMenuScene");
		}
			break;
		}
		m_state = stateToSet;
	}


	public void Update()
	{
		switch (m_state)
		{
		case SPLASH_STATES.STATE_FADING_IN:
		{
			UpdateFadingIn();
		}
			break;
		case SPLASH_STATES.STATE_HANGING_AROUND:
		{
			UpdateHangingAround();
		}
			break;
		case SPLASH_STATES.STATE_FADING_OUT:
		{
			UpdateFadingOut();
		}
			break;
		
		}

	}
}
