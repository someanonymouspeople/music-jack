﻿using UnityEngine;
using System.Collections;
using System.Net;

public enum NETWORK_CONNECTION_RESULT
{
	ERROR,
	LINK_ACTIVE,
	LINK_INACTIVE,
	NET_ACTIVE,
	NET_INACTIVE
};

public class NetworkConnectionDelegate 
{
	
	public static NetworkConnectionDelegate m_instance;
	float m_pingTimer = 0;
	int m_pingTime = 3;

	public static NetworkConnectionDelegate GetInstance()
	{
		if(m_instance != null)
		{
			return m_instance;
		}
		else
		{
			m_instance = new NetworkConnectionDelegate();
			return m_instance;
		}
	}

	public NETWORK_CONNECTION_RESULT IsLinkActive(string link)
	{
		Debug.Log("Share Button Starting is Link active");
		
		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(link);
		req.Timeout = 3000;
		try
		{
			ServicePointManager.ServerCertificateValidationCallback = (p1, p2, p3, p4) => true;
			Debug.Log("Share Button requesting HTML response");
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					Debug.Log("Share Button Link active");
					result = NETWORK_CONNECTION_RESULT.LINK_ACTIVE;
					
				}
			}
		}
		catch
		{
			Debug.Log("Share Button Link inactive");
			result = NETWORK_CONNECTION_RESULT.LINK_INACTIVE;
		}
		
		return result;

	}

	public NETWORK_CONNECTION_RESULT IsConnectedToInternet()
	{
		Debug.Log("Share Button Starting is connected to internet");

		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://google.com");
		req.Timeout = 3000;
		try
		{
			ServicePointManager.ServerCertificateValidationCallback = (p1, p2, p3, p4) => true;
			Debug.Log("Share Button requesting HTML response");
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				Debug.Log("Share Buttons HTML response recieved");
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					Debug.Log("Share Buttons Net active");
					result = NETWORK_CONNECTION_RESULT.NET_ACTIVE;

				}
			}
		}
		catch
		{
			Debug.Log("Share Buttons Net inactive");
			result = NETWORK_CONNECTION_RESULT.NET_INACTIVE;
		}

		return result;

	}
}
