﻿using UnityEngine;
using System.Collections;

public class LoadingScreenPicViewer : MonoBehaviour 
{
	public static LoadingScreenPicViewer m_instance;
	public static LoadingScreenPicViewer GetInstance()
	{
		return m_instance;
	}
	public Sprite[] m_screenOptions;
	public GameObject[] m_spriteRendererHolders;
	int m_goHidden;
	int m_goDisplaying;
	public bool m_isStable = false;

	public IEnumerator SwitchScreen(int optionNumber)
	{
		m_isStable = false;
		foreach(Task t in MJAssetLoader.GetInstance().m_runningTasksHandleList)
		{
			t.Pause();
		}
		optionNumber = Mathf.Clamp(optionNumber,0,m_screenOptions.Length-1);
		Sprite spriteToSwitchWith = m_screenOptions[optionNumber];
		m_spriteRendererHolders[m_goHidden].GetComponent<SpriteRenderer>().sprite = spriteToSwitchWith;
		bool isThere = false;
		while(!isThere)
		{
			if(m_spriteRendererHolders[m_goHidden].gameObject == null)
			{
				yield break;
			}
			m_spriteRendererHolders[m_goHidden].transform.position = Vector3.Lerp(m_spriteRendererHolders[m_goHidden].transform.position,new Vector3(0,0,m_spriteRendererHolders[m_goHidden].transform.position.z),0.08f);
			m_spriteRendererHolders[m_goDisplaying].transform.position = Vector3.Lerp(m_spriteRendererHolders[m_goDisplaying].transform.position, new Vector3(-(Camera.main.orthographicSize*2*Camera.main.aspect),0,m_spriteRendererHolders[m_goHidden].transform.position.z),0.08f);
			if(JunkieScript.AreApproximateBy(m_spriteRendererHolders[m_goHidden].transform.position.x,0,0.02f))
			{
				isThere = true;
			}
			yield return null;
		}
		m_spriteRendererHolders[m_goHidden].transform.position = new Vector3(0,0,m_spriteRendererHolders[m_goHidden].transform.position.z);
		m_spriteRendererHolders[m_goDisplaying].transform.position = new Vector3(Camera.main.orthographicSize*2*Camera.main.aspect,0,m_spriteRendererHolders[m_goDisplaying].transform.position.z);
		int temp = m_goDisplaying;
		m_goDisplaying = m_goHidden;
		m_goHidden = temp;
		m_isStable = true;
		foreach(Task t in MJAssetLoader.GetInstance().m_runningTasksHandleList)
		{
			t.Unpause();
		}
		yield break;

	}

	public void Awake()
	{
		m_instance = this;
		m_isStable = true;
		m_spriteRendererHolders = new GameObject[2];

		int goCounter = 0;
		for(int i =0 ; i< m_spriteRendererHolders.Length;i++)
		{
			m_spriteRendererHolders[i] = new GameObject();
			SpriteRenderer rend = m_spriteRendererHolders[i].gameObject.AddComponent<SpriteRenderer>();
			rend.sortingLayerName = "HUD";
			rend.sortingOrder = 1;
			m_spriteRendererHolders[i].gameObject.transform.position = new Vector3(0+(goCounter*(Camera.main.orthographicSize*2 * Camera.main.aspect)),0,-5);
			goCounter++;
		}
		int randomScreenToDisp = Random.Range(0,(m_screenOptions.Length-1 * 10)+9)/10;
		m_spriteRendererHolders[0].GetComponent<SpriteRenderer>().sprite = m_screenOptions[randomScreenToDisp];
		m_goDisplaying = 0;
		m_goHidden = 1;
	}
}
