﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//
//
//
//public class SubBand
//{
//	public SubBand(int elementSize)
//	{
//		m_FFTElementArray = new FFTOutputElement[elementSize];
//		m_bandEndFreq = 0;
//		m_bandEndFreq = 0;
//		m_averageVolume = 0;
//	}
//
//	public void CalculateAverageVolume()
//	{
//		float averageVolume = 0;
//		foreach (FFTOutputElement f in m_FFTElementArray)
//		{
//			averageVolume += f.m_magnitude;
//		}
//		averageVolume /= m_FFTElementArray.Length;
//		m_averageVolume = averageVolume; 
//	}
//
//	public FFTOutputElement[] m_FFTElementArray;
//	public int m_bandStartFreq;
//	public int m_bandEndFreq;
//	public float m_averageVolume;
//}
//
//
//public class FFTTimeLineElement
//{
//	public FFTTimeLineElement(int subBandSize)
//	{
//		m_subBands = new SubBand[subBandSize];
//		m_timeStamp = 0;
//	}
//	
//	public SubBand[] m_subBands;
//	public float m_timeStamp;
//	public float m_energy;
//}
//
//
//public class AudioToneBufferElement
//{
//	public AudioToneBufferElement()
//	{
//		m_subBandArrayList = new List<float[,]>();
//	}
//
//	public List<float[,]> m_subBandArrayList;
//
//}
//
//public class MusicAnalyzer 
//{
//	
//	//global static function
//
//	public static int GetElementCount<T>(T[,] arrayToGetFor,int firstDimensionToCheckIn,int maxCount,bool dontCountIfZero = true)
//	{
//		//fill another array with all elements in that dimension
//		int counter = 0;
//		bool flag;
//		for(int i = 0; i< maxCount; i++)
//		{
//			if(dontCountIfZero)
//			{
//				T elementToCheckFor =arrayToGetFor[firstDimensionToCheckIn,i];
//				if(elementToCheckFor != null)
//				{
//					if(elementToCheckFor is float)
//					{
//						float temp = (float)System.Convert.ChangeType(elementToCheckFor,typeof(float));
//						if(temp != 0)
//						{
//							counter ++;
//						}
//					}
//					else if(elementToCheckFor is int)
//					{
//						int temp = (int)System.Convert.ChangeType(elementToCheckFor,typeof(int));
//						if(temp != 0)
//						{
//							counter ++;
//						}
//					}
//					else
//					{
//						counter++;
//					}
//				}
//			}
//			else
//			{
//				T elementToCheckFor =arrayToGetFor[firstDimensionToCheckIn,i];
//				if(elementToCheckFor != null)
//				{
//					counter ++;
//				}
//			}
//		}
//		return counter;
//	}
//
//	public static T[] GetRange<T>(T[] arrayToGetFor,int startIndex, int endIndex)
//	{
//		if(endIndex < startIndex)
//		{
//			Debug.Log("End index lesser than start,Inappropriate parameters");
//			return null;
//		}
//		
//		if(endIndex > arrayToGetFor.Length-1)
//		{
//			endIndex = arrayToGetFor.Length-1;
//		}
//		if(startIndex < 0)
//		{
//			startIndex = 0;
//		}
//		if(endIndex - startIndex > arrayToGetFor.Length)
//		{
//			Debug.Log("Range exceeds size of array, Inappropriate parameters");
//			return null;
//		}
//		
//		T [] arrayToReturn = new T[endIndex - startIndex];
//		int counter = 0;
//		for(int i = startIndex; i<endIndex; i++)
//		{
//			arrayToReturn[counter] = arrayToGetFor[i];
//			counter ++;
//		}
//		return arrayToReturn;
//		
//	}
//
//	//variables
//	public int m_decibelOffset;
//	public int m_subBandCount;
//	public float m_minTimeBetweenEachBeat;
//	public float m_beatTimer;
//	public static MusicAnalyzer m_instance;
//	public float[] m_beatTimers;
//
//	public float[,] m_instantVolumeHistoryBuffer;
//	public float[,] averageVolumeBuffer;
//	public float[,] m_volumeVarianceHistoryBuffer;
//
//	public AudioToneBufferElement[] m_audioToneVolumeHistoryBuffer;
//	public int m_energyCountInHistory;
//
//	public double m_beatThreshold;
//	public float m_beatSensitivityIncrement;
//	public float m_beatSensitivityDecrement;
//	public float m_minBeatThreshold;
//	public float m_maxBeatThreshold;
//	public float m_beatThresholdSensitivityTime;
//	bool m_sortByAudioTone;
//	//methods
//
//	public static MusicAnalyzer GetInstance()
//	{
//		if(m_instance != null)
//		{
//			return m_instance;
//		}
//
//		m_instance = new MusicAnalyzer();
//		m_instance.Start();
//		return m_instance;
//
//	}
//	
//	void Start () 
//	{
//		m_sortByAudioTone = true;
//
//		m_decibelOffset = 203;
//		if(m_sortByAudioTone == false)
//		{
//			if(m_subBandCount == 0)
//			{
//				m_subBandCount = 5;
//			}
//		}
//		else
//		{
//			if(m_subBandCount == 0)
//			{
//				m_subBandCount = 64;
//			}
//		}
//		if(m_energyCountInHistory-1 <0)
//		{
//			m_energyCountInHistory = 43;
//		}
//
//		m_subBandCount = Mathf.Clamp(m_subBandCount,3,128);
//		m_energyCountInHistory = Mathf.Clamp(m_energyCountInHistory,5,43);
//		m_instantVolumeHistoryBuffer = new float[m_subBandCount, m_energyCountInHistory];
//
//		if(!m_sortByAudioTone)
//		{
//			m_beatThresholdSensitivityTime = 0.2f;
//			m_maxBeatThreshold = 1.4f;
//			m_minBeatThreshold = 1.05f;
//			m_beatSensitivityDecrement = 0.01f;
//			m_beatSensitivityIncrement = 0.02f;
//			m_beatTimer = 0;
//			m_minTimeBetweenEachBeat = 0.0f;
//			m_beatThreshold = m_maxBeatThreshold;
//			averageVolumeBuffer = new float[m_subBandCount,1];
//			m_volumeVarianceHistoryBuffer = new float[m_subBandCount,m_energyCountInHistory];
//		}
//		else
//		{
//			m_beatThreshold = 1.25f;
//			m_minTimeBetweenEachBeat = 0.2f;
//			m_audioToneVolumeHistoryBuffer = new AudioToneBufferElement[5];
//			averageVolumeBuffer = new float[5,1];
//			m_beatTimers = new float[5];
//			for(int i = 0; i< m_beatTimers.Length ; i++)
//			{
//				m_beatTimers[i] = 0;
//			}
//			m_volumeVarianceHistoryBuffer = new float[5,m_energyCountInHistory];
//		}
//	}
//
//	public float[] ProcessAndGetSingleSampleArray(AudioClip audio)
//	{
//		float[] rawSamples = new float[audio.samples * audio.channels];
//		audio.GetData(rawSamples,0);
//
//		float[] samplesToConsider = null;
//
//		if(audio.channels == 2)
//		{
//			samplesToConsider = new float[rawSamples.Length/2]; 
//			Debug.Log("Stereo song detected, Analyzing...");
//			float [] leftChannel = new float[rawSamples.Length/2];
//			float [] rightChannel = new float[rawSamples.Length/2];
//			
//			//split left and right channels
//			int counter = 0;
//			for(int i =0; i<rawSamples.Length; i+=2)
//			{
//				leftChannel[counter] = rawSamples[i];
//				rightChannel[counter] = rawSamples[i+1];
//				counter++;
//			}
//			
//			//average left and right channels to get a single sample array
//			for(int i = 0;i <samplesToConsider.Length; i++)
//			{
//				samplesToConsider[i] = (leftChannel[i] + rightChannel[i])/2;
//			}
//
//			
//		}
//		else if(audio.channels == 1)
//		{
//			samplesToConsider = new float[rawSamples.Length]; 
//			Debug.Log("Mono song detected, Analyzing...");
//			samplesToConsider = rawSamples;
//		}
//		return samplesToConsider;
//	}
//
//
//	public FFTTimeLineElement[] GetPreProcessedFrequencyTimeLine(float[] samplesToConsider,AudioClip audio)
//	{
//		CustomComplex[] complexConvert = new CustomComplex[samplesToConsider.Length];
//		
//		for(int i = 0; i< samplesToConsider.Length; i++)
//		{
//			float w = -2* Mathf.PI *(float)i/(float)samplesToConsider.Length;
//			complexConvert[i] = new CustomComplex(Mathf.Cos(w),Mathf.Sin(w));
//		}
//		
//		
//		//FFT ATTEMPT
//		//garbage collector will handle the new lists being generated
//		
//		FFTTimeLineElement[] timeLine = new FFTTimeLineElement[samplesToConsider.Length/1024];
//		int subBandFrequencyIncrement = 22000/m_subBandCount;
//		FFT2 FFTHelper = new FFT2();
//
//		//for(int i = 0; i< 1024; i+= 1024)
//		for(int i = 0; i< samplesToConsider.Length - (audio.frequency*100); i+= 1024)
//		{
//			//Init analyser
//			
//			//Make a list of customFFTElements to hold the FFT outputs beng generated
//			List<CustomFFTElement> FFTResultArray = new List<CustomFFTElement>();
//			
//			FFTHelper.init((uint)Mathf.Log(1024,2));
//			FFTHelper.run(GetRange(complexConvert,i,i+1024));
//			
//			CustomFFTElement[] result = FFTHelper.GetResult();
//			foreach(CustomFFTElement ffte in result)
//			{
//				FFTResultArray.Add(ffte);
//			}
//			
//			//Process FFTOutput
//			SubBand[] subBandArray = new SubBand[m_subBandCount];
//			
//			//Split FFT output into frequency subbands
//			//22000 = max frequency that can be detected by the human ear
//			
//			int currentSubBandStartFrequency = 0;
//			for(int j =0; j<m_subBandCount ; j++)
//			{
//				List<FFTOutputElement> m_decipheredResult = new List<FFTOutputElement>();
//				m_decipheredResult = FFTHelper.DecipherFFT(FFTResultArray.ToArray(),audio.frequency,currentSubBandStartFrequency,currentSubBandStartFrequency + subBandFrequencyIncrement);
//				FFTOutputElement[] tempArray = new FFTOutputElement[m_decipheredResult.Count];
//				tempArray = m_decipheredResult.ToArray();
//
//				SubBand tempSubBand = new SubBand(m_decipheredResult.Count);
//				tempSubBand.m_FFTElementArray = tempArray;
//				tempSubBand.m_bandStartFreq = j*subBandFrequencyIncrement;
//				tempSubBand.m_bandEndFreq = tempSubBand.m_bandStartFreq + subBandFrequencyIncrement;
//				//tempSubBand.CalculateAverageVolume();
//
//				subBandArray[j] = tempSubBand;
//				currentSubBandStartFrequency += subBandFrequencyIncrement;
//			}
//			
//			//Store processed outputs in a timeLine to decipher and generate tracks
//			FFTTimeLineElement tempTimeLineElement = new FFTTimeLineElement(subBandArray.Length);
//			tempTimeLineElement.m_subBands = subBandArray;
//			tempTimeLineElement.m_timeStamp = (((float)i)/audio.samples) * audio.length;
//			
//			timeLine[i/1024] = tempTimeLineElement;
//			
//		}
//
//		return timeLine;
//	}
//
////	public MinMax GetMinMaxVolume(AudioSource source)
////	{
////		float minEnergy = 0;
////		float maxEnergy = 0;
////		MinMax minMax = new MinMax();
////
////
////		FFT2 FFTHelper = new FFT2();
////		FFTHelper.init((uint)Mathf.Abs(Mathf.Log(source.clip.samples,2)));
////
////		float[] rawSamples = ProcessAndGetSingleSampleArray(source.clip);
////		//source.clip.GetData(rawSamples,0);
////
////		CustomComplex[] complexConvert = new CustomComplex[rawSamples.Length];
////		
////		for(int i = 0; i< rawSamples.Length; i++)
////		{
////
////			//float w = -2* Mathf.PI *(float)i/(float)rawSamples.Length;
////			float timeStamp = 0;
////			float real = rawSamples[i]/32768;
////			float imag = 0;
////			complexConvert[i] = new CustomComplex(real,imag,timeStamp);
////		}
////
////
////		FFTHelper.run(complexConvert);
////		CustomFFTElement[] FFTResult = FFTHelper.GetResult();
////		FFTHelper.GetMagnitudeFromFFT(FFTResult);
//////		float[] sampleArray = ProcessAndGetSingleSampleArray(source.clip);
//////
//////		List<float> energyData = new List<float>();
//////		for(int i = 0; i< sampleArray.Length; i++)
//////		{
//////			energyData.Add(sampleArray[i]);
//////		}
//////		//ascending order
//////		energyData.Sort((x,y)=> x.CompareTo(y));
//////
//////		minEnergy = energyData[0];
//////
//////		maxEnergy = energyData[energyData.Count-1];
////
////		minMax.m_min = minEnergy;
////		minMax.m_max = maxEnergy;
////		return minMax;
////	}
//
//	public float GetAverageVolume()
//	{
//		float value = 0;
//		if(!m_sortByAudioTone)
//		{
//			for(int i =0; i<m_subBandCount; i++)
//			{
//				value += averageVolumeBuffer[i,0];
//			}
//			value/= m_subBandCount;
//		}
//		else
//		{
//			for(int i =0; i<5 ;i++)
//			{
//				value += averageVolumeBuffer[i,0];
//			}
//			value /= 5;
//		}
//		return value;
//
//	}
//
//
//	public float GetInstantVolume()
//	{
//		float value = 0;
//		if(m_sortByAudioTone)
//		{
//
//			for(int i = 0; i<m_audioToneVolumeHistoryBuffer.Length; i++)
//			{
//				float volumeThisElement = 0;
//				int counter = 0;
//				for(int j = 0; j <m_audioToneVolumeHistoryBuffer[i].m_subBandArrayList.Count; j++)
//				{
//					int elementCount = GetElementCount(m_audioToneVolumeHistoryBuffer[i].m_subBandArrayList[j],0,m_energyCountInHistory);
//					if(elementCount>0)
//					{
//					volumeThisElement += m_audioToneVolumeHistoryBuffer[i].m_subBandArrayList[j][0,elementCount-1];
//					counter ++;
//					}
//				}
//				volumeThisElement/= counter;
//
//				value += volumeThisElement;
//			}
//			value/= m_audioToneVolumeHistoryBuffer.Length;
//
//		}
//		else
//		{
//
//			for(int i =0; i<m_subBandCount ; i++)
//			{
//				int elementCount = GetElementCount(m_instantVolumeHistoryBuffer,i,m_energyCountInHistory);
//				if(elementCount>0)
//				{
//				value+= m_instantVolumeHistoryBuffer[i,elementCount-1];
//				}
//			}
//			value/= m_subBandCount;
//		}
//		return value;
//	}
//
//	private float[] SortByAudioToneAndReturnInstantEnergy()
//	{
//		float[] audioToneInstantBuffer = new float[5];
//		AudioToneBufferElement tempAudioToneBufferElement = new AudioToneBufferElement();
//		float m_prevConsideredFrequency=0;
//		for(int i = 0; i< m_subBandCount ; i++)
//		{
//			//check according to the frequency they contain and fill the buffer
//			float frequencyTheSubBandContains = (22050/m_subBandCount) * (i);
//			
//			float[,] tempVolumeBuffer = new float[1,m_energyCountInHistory];
//			
//			if(frequencyTheSubBandContains < 250)
//			{
//				for(int j= 0; j< m_energyCountInHistory; j++)
//				{
//					tempVolumeBuffer[0,j] = m_instantVolumeHistoryBuffer[i,j];
//				}
//			}
//			else if (frequencyTheSubBandContains < 512)
//			{
//				if(m_prevConsideredFrequency < 250)
//				{
//					m_audioToneVolumeHistoryBuffer[0] = tempAudioToneBufferElement;
//					float value= 0;
//					for(int k = 0; k<tempAudioToneBufferElement.m_subBandArrayList.Count; k++)
//					{
//						value += tempAudioToneBufferElement.m_subBandArrayList[k][0,GetElementCount(tempAudioToneBufferElement.m_subBandArrayList[k],0,m_energyCountInHistory)-1];
//					}
//					value /= tempAudioToneBufferElement.m_subBandArrayList.Count;
//					audioToneInstantBuffer[0] = value;
//					tempAudioToneBufferElement = new AudioToneBufferElement();
//				}
//				for(int j= 0; j< m_energyCountInHistory; j++)
//				{
//					tempVolumeBuffer[0,j] = m_instantVolumeHistoryBuffer[i,j];
//				}
//			}
//			else if (frequencyTheSubBandContains < 3200)
//			{
//				if(m_prevConsideredFrequency < 512)
//				{
//					m_audioToneVolumeHistoryBuffer[1] = tempAudioToneBufferElement;
//					float value= 0;
//					for(int k = 0; k<tempAudioToneBufferElement.m_subBandArrayList.Count; k++)
//					{
//						value += tempAudioToneBufferElement.m_subBandArrayList[k][0,GetElementCount(tempAudioToneBufferElement.m_subBandArrayList[k],0,m_energyCountInHistory)-1];
//					}
//					value /= tempAudioToneBufferElement.m_subBandArrayList.Count;
//					audioToneInstantBuffer[1] = value;
//					tempAudioToneBufferElement = new AudioToneBufferElement();
//				}
//				for(int j= 0; j< m_energyCountInHistory; j++)
//				{
//					tempVolumeBuffer[0,j] = m_instantVolumeHistoryBuffer[i,j];
//				}
//			}
//			else if (frequencyTheSubBandContains < 5200)
//			{
//				if(m_prevConsideredFrequency < 3200)
//				{
//					m_audioToneVolumeHistoryBuffer[2] = tempAudioToneBufferElement;
//					float value= 0;
//					for(int k = 0; k<tempAudioToneBufferElement.m_subBandArrayList.Count; k++)
//					{
//						value += tempAudioToneBufferElement.m_subBandArrayList[k][0,GetElementCount(tempAudioToneBufferElement.m_subBandArrayList[k],0,m_energyCountInHistory)-1];
//					}
//					value /= tempAudioToneBufferElement.m_subBandArrayList.Count;
//					audioToneInstantBuffer[2] = value;
//					tempAudioToneBufferElement = new AudioToneBufferElement();
//				}
//				for(int j= 0; j< m_energyCountInHistory; j++)
//				{
//					tempVolumeBuffer[0,j] = m_instantVolumeHistoryBuffer[i,j];
//				}
//			}
//			else
//			{
//				if(m_prevConsideredFrequency < 5200)
//				{
//					m_audioToneVolumeHistoryBuffer[3] = tempAudioToneBufferElement;
//					float value= 0;
//					for(int k = 0; k<tempAudioToneBufferElement.m_subBandArrayList.Count; k++)
//					{
//						value += tempAudioToneBufferElement.m_subBandArrayList[k][0,GetElementCount(tempAudioToneBufferElement.m_subBandArrayList[k],0,m_energyCountInHistory)-1];
//					}
//					value /= tempAudioToneBufferElement.m_subBandArrayList.Count;
//					audioToneInstantBuffer[3] = value;
//					tempAudioToneBufferElement = new AudioToneBufferElement();
//				}
//				for(int j= 0; j< m_energyCountInHistory; j++)
//				{
//					tempVolumeBuffer[0,j] = m_instantVolumeHistoryBuffer[i,j];
//				}
//			}
//			
//			
//			m_prevConsideredFrequency = frequencyTheSubBandContains;
//			tempAudioToneBufferElement.m_subBandArrayList.Add(tempVolumeBuffer);
//			if(i == m_subBandCount-1)
//			{
//				m_audioToneVolumeHistoryBuffer[4] = tempAudioToneBufferElement;
//				float value= 0;
//				for(int k = 0; k<tempAudioToneBufferElement.m_subBandArrayList.Count; k++)
//				{
//					value += tempAudioToneBufferElement.m_subBandArrayList[k][0,GetElementCount(tempAudioToneBufferElement.m_subBandArrayList[k],0,m_energyCountInHistory)-1];
//				}
//				value /= tempAudioToneBufferElement.m_subBandArrayList.Count;
//				audioToneInstantBuffer[4] = value;
//			}
//		}
//
//		return audioToneInstantBuffer;
//	}
//	
//	public bool CheckForBeats(AudioSource source,GameplayScript.BeatCallBack callBack = null, bool ColorBlind = false)
//	{
//		
//		// NOT HACK but needs optimization
//		if(ColorBlind)
//		{
//			//Get samples
//			float[] stereoData = new float[1024];
//			if(source.clip.channels == 2)
//			{
//				float[] leftChannelData = new float[1024];
//				float[] rightChannelData = new float[1024];
//				
//				source.GetOutputData(leftChannelData,0);
//				source.GetOutputData(rightChannelData,0);
//				
//				for(int i = 0; i< 1024; i++)
//				{
//					stereoData[i] = leftChannelData[i] + rightChannelData[i];
//				}
//			}
//			else if (source.clip.channels == 1)
//			{
//				source.GetOutputData(stereoData,0);
//			}
//			
//			//Process samples
//			float sumOfSquares = 0 ;
//			foreach (float f in stereoData)
//			{
//				float value = f*f;
//				sumOfSquares += value;
//			}
//			
//			float average = sumOfSquares/ 1024;
//			float RMS = Mathf.Sqrt(average);
//			float volumeInDecibels = 20 * Mathf.Log10(RMS) + m_decibelOffset;
//			
//			Debug.Log("CurrentVolume :-" +volumeInDecibels);
//
//			//TO IMPLEMENT :- volume based beat detection
//			return false;
//		}
//		else
//		{
//			float[] stereoData = new float[1024];
//			if(source.clip.channels == 2)
//			{
//				float[] leftChannelData = new float[1024];
//				float[] rightChannelData = new float[1024];
//				
//				source.GetSpectrumData(leftChannelData,0,FFTWindow.BlackmanHarris);
//				source.GetSpectrumData(rightChannelData,0,FFTWindow.BlackmanHarris);
//				
//				for(int i = 0; i< 1024; i++)
//				{
//					stereoData[i] = leftChannelData[i] + rightChannelData[i];
//				}
//			}
//			else if (source.clip.channels == 1)
//			{
//				source.GetSpectrumData(stereoData,0,FFTWindow.BlackmanHarris);
//			}
//			
//			//Process samples
//		
//			//split to frequency bands
//			int subBandIncrement = 22000/m_subBandCount;
//			
//			float[,] distributedValues = new float[m_subBandCount,1024/m_subBandCount];
//			
//			for(int i = 0; i< m_subBandCount; i++)
//			{
//				for(int j = 0; j< 1024/m_subBandCount; j++)
//				{
//					if(i==0 && j==0)
//					{
//
//					}
//					else if(i==0 && j==1)
//					{
//						distributedValues[i,0] = stereoData[(1024/m_subBandCount)*i + j];
//						distributedValues[i,1] = stereoData[(1024/m_subBandCount)*i + j];
//					}
//					else
//					{
//						distributedValues[i,j] = stereoData[(1024/m_subBandCount)*i + j];
//					}
//				}
//			}
//
//			//to aid calculation furhter done logic, we save the volumes calculated this update
//			float[] volumesThisUpdate = new float[m_subBandCount];
//			float[] audioToneInstantBuffer = new float[5];
//			//calculate and store volumes of all frequency bands in historyBuffer
//			for(int i=0; i<m_subBandCount; i++)
//			{
//				float sumOfSquares = 0 ;
//				for (int j = 0; j<1024/m_subBandCount; j++)
//				{
//					float value = distributedValues[i,j];
//					value *= value;
//					sumOfSquares += value;
//				}
//				
//				float average = sumOfSquares/ (1024/m_subBandCount);
//				float RMS = Mathf.Sqrt(average);
//				float volumeInDecibels = ( 20 * Mathf.Log10(RMS)) + m_decibelOffset;
//
//
//				volumesThisUpdate[i] = volumeInDecibels;
//
//				int elementCount = GetElementCount<float>(m_instantVolumeHistoryBuffer,i,m_energyCountInHistory) ;
//				if(elementCount < m_energyCountInHistory)
//				{
//					m_instantVolumeHistoryBuffer[i,elementCount] = volumeInDecibels;
//				}
//				else
//				{
//					//if full shift elements one step left and fill at end
//					for(int k = 0; k<m_energyCountInHistory -1; k++)
//					{
//						m_instantVolumeHistoryBuffer[i,k] = m_instantVolumeHistoryBuffer[i,k+1];
//					}
//					m_instantVolumeHistoryBuffer[i,m_energyCountInHistory-1] = volumeInDecibels;
//				}
//
//			}
//
//			//increment individual beat timers
//			if(m_sortByAudioTone)
//			{
//				for(int i = 0;i<m_beatTimers.Length ; i++)
//				{
//					if(m_beatTimers[i] <= m_minTimeBetweenEachBeat)
//					{
//						m_beatTimers[i] += Time.deltaTime;
//					}
//				}
//			
//				//sortThrough the instantVolumeHistoryBuffer and fill the audioToneVolumeBuffer and instantVolumeHistoryBuffer 
//				audioToneInstantBuffer = SortByAudioToneAndReturnInstantEnergy();
//
//
//
//				//Check for beats
//
//				float beatVolume = 0;
//				int band = 0;
//				//Analyze the audioTone volume buffer and return true if a beat is found
//				for(int i =0 ;i< 5; i++)
//				{
//
//					//5 for 5 categories of tones
//
//					float averageValueAmongAudioTone = 0;
//
//					foreach(float[,] array in m_audioToneVolumeHistoryBuffer[i].m_subBandArrayList)
//					{
//						int elementCount = GetElementCount<float>(array,0,m_energyCountInHistory);
//						float value = 0;
//						for(int currentElement= 0; currentElement < elementCount; currentElement++)
//						{
//							value += array[0,currentElement];
//						}
//						float averageValue = value/elementCount;
//						averageValueAmongAudioTone += averageValue;
//
//					}
//					averageValueAmongAudioTone/= m_audioToneVolumeHistoryBuffer[i].m_subBandArrayList.Count;
//
//					averageVolumeBuffer[i,0] = averageValueAmongAudioTone;
//					float volumeVariance =0;
//					volumeVariance = Mathf.Pow((audioToneInstantBuffer[i] - averageValueAmongAudioTone),2);
//					m_beatThreshold = (-0.0025714 * volumeVariance) + 1.542857;
//
//					//compare average energy
//					double valueToCheck = averageValueAmongAudioTone * m_beatThreshold;
//					if(audioToneInstantBuffer[i] >= valueToCheck)
//					{
//						if(m_beatTimers[i]>m_minTimeBetweenEachBeat )
//						{
//							if(callBack != null)
//							{
//								if(audioToneInstantBuffer[i]> beatVolume)
//								{
//									beatVolume = audioToneInstantBuffer[i];
//									band = i;
//								}
//							}
//							else
//							{
//								//Debug.Log("NO CALLBACK ASSIGNED");
//							}
//							m_beatTimers[i] = 0;
//							//beatDetected = true;
//						}
//						
//					}
//
//
//				}
//
//				//feed the visualizer data so that visualization takes place
//				//MusicVisualizer.GetInstance().FeedData(m_instantVolumeHistoryBuffer);
//				
//				if(beatVolume>0)
//				{
//					callBack((DETECTION_TONE)band);
//					return true;
//				}
//				
//
//				//if no beat return false
//				return false;
//
//			}
//			else
//			{
//				//analyze the volume buffer and return true if beat is found
//
//				if(m_beatTimer <= m_minTimeBetweenEachBeat)
//				{
//					m_beatTimer+= Time.deltaTime;
//				}
//				float beatTriggeredValue = 0;
//				int band = 0;
//				for(int i = 0; i<m_subBandCount; i++)
//				{
//					// compute average energy 
//					float value = 0;
//					int elementCount = GetElementCount<float>(m_instantVolumeHistoryBuffer,i,m_energyCountInHistory);
//					for(int j = 0; j<elementCount; j++)
//					{
//						value += m_instantVolumeHistoryBuffer[i,j];
//					}
//					float averageValue = value/elementCount;
//					averageVolumeBuffer[i,0] = averageValue;
//					//compare average energy
//					float volumeVariance =0;
//					volumeVariance = Mathf.Pow((m_instantVolumeHistoryBuffer[i,elementCount-1] - averageValue),2);
//					//m_beatThreshold = 1.3f;
//
//					double valueToCheck = averageValue * m_beatThreshold;
//
//					if(volumesThisUpdate[i] >= valueToCheck)
//					{
//						if(m_beatTimer>m_minTimeBetweenEachBeat)
//						{
//							if(volumesThisUpdate[i] > beatTriggeredValue)
//							{
//								beatTriggeredValue = volumesThisUpdate[i];
//								band = i;
//							}
//
//						}
//					}
//
//						
//				}
//
//				//feed the visualizer data so that visualization takes place
//				//MusicVisualizer.GetInstance().FeedData(m_instantVolumeHistoryBuffer);
//
//				//if beat triggered
//				if(m_beatTimer > m_minTimeBetweenEachBeat)
//				{
//				if(beatTriggeredValue != 0)
//				{
//					Debug.Log("THUMP! triggered by volume :- "+ beatTriggeredValue);
//					if(m_beatThreshold < m_maxBeatThreshold)
//					{
//						m_beatThreshold += m_beatSensitivityDecrement;
//					}
//					callBack((DETECTION_TONE)Random.Range(0,5));
//						m_beatTimer = 0;
//					return true;
//				}
//				}
//
//				if(m_beatTimer > m_minTimeBetweenEachBeat)
//				{
//				m_beatTimer+= Time.deltaTime;
//				}
//				if(m_beatTimer > m_beatThresholdSensitivityTime)
//				{
//					m_beatTimer = m_minTimeBetweenEachBeat;
//					if(m_beatThreshold>m_minBeatThreshold)
//					{
//					m_beatThreshold-= m_beatSensitivityIncrement;
//					}
//				}
//				// if no beats found
//				return false;
//			}
//
//
//
//		}
//	}
//
//	//INCOMPLETE
////	public List<float> GetBeatPositions(AudioClip audio,AudioSource audioSource)
////	{
////
////		List<float> beatTimeLine = new List<float>();
////		
////		float[] samplesToConsider;
////		//check if mono or stereo
////		if(audio.channels > 2 || audio.channels <1)
////		{
////			Debug.Log("Unsupported song format detected, terminating analysis....");
////			return null;
////		}
////
////
////		//FFTTimeLineElement[] timeLine;
////		//get samples
////		//samplesToConsider = ProcessAndGetSingleSampleArray(audio);
////		//Get pre processed deciphered FFT outputs with timeStamps
////		//timeLine = GetPreProcessedFrequencyTimeLine(samplesToConsider, audio);
////
////		//process the timeLineEvents to detect beat
////
////
////
////		return beatTimeLine;
////	}
//
//}
