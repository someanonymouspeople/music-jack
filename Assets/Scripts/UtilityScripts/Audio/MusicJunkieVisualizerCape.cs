﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicJunkieVisualizerCape : MonoBehaviour {

	public bool m_variableLength;
	public int m_swayContactPoints;
	public float m_localSwayInUnits;
	public bool m_changeSwayWithSpeed;
	public float m_swayLerpSpeed;
	public float m_maxSwayLerpSpeed;
	public float m_minSwayLerpSpeed;
	public float m_maxBands;
	public float m_minBands;
	Vector2[] m_localStartPos;
	float m_currentBands;
	int m_currentSwayDirection;

	int m_subBandCount = 0;
	float m_decibelToUnitScale;
	public GameObject[,] m_visualAidArray;
	GameObject m_visualizer;
	
	public static MusicJunkieVisualizerCape m_instance;

	public void Awake()
	{
		m_currentSwayDirection = 1;
		if(m_maxSwayLerpSpeed == 0)
		{
			m_maxSwayLerpSpeed = 0.1f;
		}
		if(m_minSwayLerpSpeed == 0)
		{
			m_minSwayLerpSpeed = 0.02f;
		}
		if(m_swayLerpSpeed == 0)
		{
			m_swayLerpSpeed = m_maxSwayLerpSpeed/2;
		}

		if(m_maxBands == 0)
		{
			m_maxBands = 10;
		}
		if(m_minBands == 0)
		{
			m_minBands = 5;
		}
		if(MusicJunkieVisualizerCape.GetInstance() != null)
		{
			Destroy (this);
			return;
		}
		Init();
		m_instance = this;
	}

	public static MusicJunkieVisualizerCape GetInstance()
	{

		return m_instance;
		
	}

//	public void DestroyCape()
//	{
//		if(m_visualAidArray != null)
//		{
//			foreach(GameObject go in m_visualAidArray)
//			{
//				Destroy(go);
//			}
//		}
//		m_visualAidArray = null;
//	}
	
	public void Init()
	{ 

		if(m_visualAidArray == null)
		{
			switch(MusicAnalyzerV2.GetInstance().m_mode)
			{
			case BEAT_DETECTION_MODES.FREQUENCY_BASED:
			{
				m_visualizer = new GameObject();
				m_visualizer.name = "Visualizer";
				Vector2 cameraSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
				m_decibelToUnitScale = 30;
				m_subBandCount = MusicAnalyzerV2.GetInstance().m_subBandCount;
				m_visualAidArray = new GameObject[m_subBandCount,(int)m_maxBands];
				m_localStartPos = new Vector2[m_subBandCount];
				for(int i = 0; i<m_subBandCount; i++)
				{
					
					for(int j =0; j<m_maxBands; j++)
					{

						m_visualAidArray[i,j] = GameObject.Instantiate(Resources.Load("UI/VisualizerVisualAid")) as GameObject;
						m_visualAidArray[i,j].name = "FreqBand"+i* (22050/m_subBandCount);
						//m_visualAidArray[i,j].transform.localScale = new Vector3(0.5f,0.1f,0.1f);
						m_visualAidArray[i,j].transform.localPosition = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f * i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),0);
						m_visualAidArray[i,j].transform.parent = m_visualizer.transform;
						m_visualAidArray[i,j].renderer.material.color = Color.Lerp(Color.blue,Color.red,(float)i/(float)m_subBandCount);
						

						if(j == 0)
						{
							m_localStartPos[i] = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f* i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),0);
						}
					}
				}
				m_visualizer.transform.parent = Camera.main.transform;
			}
				break;
			case BEAT_DETECTION_MODES.TONE_BASED:
			{
				m_visualizer = new GameObject();
				m_visualizer.name = "Visualizer";
				Vector2 cameraSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
				m_decibelToUnitScale = 30;
				m_subBandCount = 5;
				int length = System.Enum.GetNames(typeof (DETECTION_TONE)).Length - 1;
				m_visualAidArray = new GameObject[length,(int)m_maxBands];
				m_localStartPos = new Vector2[length];
				Sprite[] spritesToPickFrom = Resources.LoadAll<Sprite>("UI/VisualArraySheet");
				for(int i = 0; i<length; i++)
				{
					Color colorToSet = Color.Lerp(Color.white,Color.black,(float)i/(float)m_subBandCount);

					for(int j =0; j<m_maxBands; j++)
					{

						m_visualAidArray[i,j] = GameObject.Instantiate(Resources.Load("UI/VisualizerVisualAid")) as GameObject;
						m_visualAidArray[i,j].gameObject.renderer.enabled = false;
						m_visualAidArray[i,j].name = ""+(DETECTION_TONE)i +" "+ j;
						//m_visualAidArray[i,j].transform.localScale = new Vector3(0.5f,0.1f,0.1f);
						m_visualAidArray[i,j].transform.localPosition = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f* i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),100);

						m_visualAidArray[i,j].transform.parent = m_visualizer.transform;
						m_visualAidArray[i,j].GetComponent<SpriteRenderer>().sprite = spritesToPickFrom[i];
						//m_visualAidArray[i,j].renderer.material.color = colorToSet;
						
						m_visualAidArray[i,j].renderer.sortingLayerName = "GamePlay";
						m_visualAidArray[i,j].renderer.sortingOrder = 0;
						if(j == 0)
						{
							m_localStartPos[i] = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f* i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),0);
						}
					}
				}
				m_visualizer.transform.parent = GameplayScript.GetInstance().GetHero().transform;

			}
				break;
			default:
			{
				Debug.Log("Unable to visualize current mode of beat detection");
			}
				break;
			}
			
		}
		
		
	}
	
	public GameObject GetVisualizer()
	{
		if(m_visualizer != null)
		{
			return m_visualizer;
		}
		return null;
	}
	
	public void Refresh()
	{ 
		if(m_visualAidArray != null)
		{

			List<GameObject> toRemoveList = new List<GameObject>();
			foreach(GameObject go in m_visualAidArray)
			{
				if(go != null)
				{
					toRemoveList.Add(go);
				}
			}
			
			for(int i =0; i<toRemoveList.Count; i++)
			{
				toRemoveList[i].transform.position = new Vector2(-999,0);
				toRemoveList[i].renderer.enabled = false;
				Object.Destroy(toRemoveList[i].gameObject);
			}
			m_visualAidArray = null;
			Destroy(GameplayScript.GetInstance().GetHeroScript().gameObject.transform.FindChild("Visualizer").gameObject);
			m_visualAidArray = null;
		}
	}
	
	public void UpdateCape()
	{
		 
		float maxVolume = 100;
		float relativeSpeed = GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed();
		if(m_changeSwayWithSpeed)
		{
		m_swayLerpSpeed = m_minSwayLerpSpeed + (relativeSpeed*m_maxSwayLerpSpeed);
		}
		//calculate and render cape length 
		switch(MusicAnalyzerV2.GetInstance().m_mode)
		{
		case BEAT_DETECTION_MODES.FREQUENCY_BASED:
		{
			
		}
			break;
		case BEAT_DETECTION_MODES.TONE_BASED:
		{
			m_currentBands = m_minBands + (relativeSpeed*(m_maxBands-m_minBands));
			float countToUnlock = 0;
			float[] volumeArray = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.015f);
			float[] averageVolumeArray = MusicAnalyzerV2.GetInstance().GetAverageVolumeOfToneBands(0.015f);
			float[] diffArray = new float[5];
			for(int i = 0; i<5; i++)
			{
				diffArray[i] = volumeArray[i] - averageVolumeArray[i];
				if(diffArray[i] < 0)
				{
					diffArray[i] = 0;
				}
				volumeArray[i] += diffArray[i] * 3;
			}
			for(int i = 0; i< volumeArray.Length; i++)
			{
				countToUnlock = (volumeArray[i]/60)*m_currentBands;
				for(int j = 0; j<m_maxBands; j++)
				{
					if(j <countToUnlock)
					{
						m_visualAidArray[i,j].renderer.enabled = true;
					}
					else
					{
						m_visualAidArray[i,j].renderer.enabled = false;
					}
				}
			}
		}
			break;
		default:
		{
			Debug.Log("Unable to visualize current mode of beat detection");
		}
			break;
		}

		//calculate and set the sway of the cape
		// y because the cape is held against the wind horizontally
//		float lengthOfCape = 0; 
//		if(m_variableLength)
//		{
//			lengthOfCape = m_visualAidArray[0,0].renderer.bounds.size.y * 1.25f * m_currentBands;
//		}
//		else
//		{
//			lengthOfCape = m_visualAidArray[0,0].renderer.bounds.size.y * 1.25f * m_maxBands;
//		}
//
//		float[] swayContactPointsRespectToLength = new float[m_swayContactPoints];
//		for(int i =0; i<m_swayContactPoints; i++)
//		{
//			float value = (lengthOfCape/(m_swayContactPoints+1));
//			value *=  (i+1);
//			swayContactPointsRespectToLength[i] = value;
//		}
//		int currentSwayPoint = 0;
//
//		// find bars that are on the sway points
//		int[] barIndexesOnSwayPoints = new int[m_swayContactPoints];
//		int firstBar = (int)(m_maxBands/(m_swayContactPoints + 1));
//		int barPosIncrement = (int)(m_maxBands/m_swayContactPoints);
//		barIndexesOnSwayPoints[0] = firstBar;
//		if(m_swayContactPoints > 1)
//		{
//			int counter = 1;
//
//			for(int i =firstBar+barPosIncrement; i<m_maxBands; i+= barPosIncrement)
//			{
//				barIndexesOnSwayPoints[counter] = i;
//				counter++;
//			}
//		}
//
//
//		//sway the primaryContact bars
//
//		for(int band = 0 ; band<m_maxBands; band++)
//		{
//			for(int m_swayBarIndex =0 ;m_swayBarIndex <barIndexesOnSwayPoints.Length; m_swayBarIndex++)
//			{
//				if(band == barIndexesOnSwayPoints[m_swayBarIndex])
//				{
//					for(int i =0 ; i <m_subBandCount; i++)
//					{
//
//						Vector2 barPos = m_visualAidArray[i,band].transform.localPosition;
//						float startPosX = m_localStartPos[i].x;
//
//						if(barPos.x > startPosX +( m_localSwayInUnits ) )
//						{
//							m_currentSwayDirection *= -1;
//							barPos.x = startPosX + (m_localSwayInUnits) -  (m_swayLerpSpeed * Time.deltaTime);
//							m_visualAidArray[i,band].transform.localPosition = barPos;
//		
//						}
//						else if (barPos.x < startPosX - (m_localSwayInUnits))
//						{
//							m_currentSwayDirection *= -1;
//							barPos.x = startPosX - (m_localSwayInUnits)  +  (m_swayLerpSpeed * Time.deltaTime);
//							m_visualAidArray[i,band].transform.localPosition = barPos;
//						}
//						else
//						{
//							barPos.x = Mathf.Lerp(barPos.x,m_localStartPos[i].x +((m_localSwayInUnits + 0.5f) * m_currentSwayDirection),m_swayLerpSpeed);//m_swayLerpSpeed * Time.deltaTime * m_currentSwayDirection;
//							m_visualAidArray[i,band].transform.localPosition = barPos;
//						}
//					}
//				}
//				else
//				{
//					if(band < barIndexesOnSwayPoints[0])
//					{
//						if(band < 1)
//						{
//							for(int i = 0 ; i <m_subBandCount; i++)
//							{
//								Vector2 barPos = m_visualAidArray[i,band].transform.localPosition;
//								float startPosX = m_localStartPos[2].x;
//								barPos.x = Mathf.Lerp(barPos.x,m_localStartPos[2].x +(((m_localSwayInUnits) + 0.5f) * m_currentSwayDirection),m_swayLerpSpeed * ((float)1/(float)barPosIncrement));//m_swayLerpSpeed * Time.deltaTime * m_currentSwayDirection;
//								m_visualAidArray[i,band].transform.localPosition = barPos;
//								
//							}
//						}
//						else
//						{
//							for(int i = 0 ; i <m_subBandCount; i++)
//							{
//								Vector2 barPos = m_visualAidArray[i,band].transform.localPosition;
//								float startPosX = 0;
//								if(i<m_subBandCount/2)
//								{
//									if(i!=0)
//									{
//										startPosX = m_localStartPos[2].x - ((((float)i/(float)(m_subBandCount-1)) * (m_localSwayInUnits* ((float)band/(float)(barIndexesOnSwayPoints[0]-1)))));
//									}
//									else
//									{
//										startPosX = m_localStartPos[2].x - (m_localSwayInUnits * ((float)band/(float)(barIndexesOnSwayPoints[0]-1)));
//									}
//								}
//								else if(i >m_subBandCount/2)
//								{
//									startPosX = m_localStartPos[2].x + ((((float)i/(float)(m_subBandCount-1)) * (m_localSwayInUnits* ((float)band/(float)(barIndexesOnSwayPoints[0]-1)))));
//								}
//								else 
//								{
//									startPosX = m_localStartPos[2].x ;
//								}
////
//								
//								//startPosX = m_localStartPos[2].x;
//								barPos.x = Mathf.Lerp(barPos.x,startPosX +((m_localSwayInUnits + 0.5f) * m_currentSwayDirection),m_swayLerpSpeed * ((float)band/(float)barPosIncrement));//m_swayLerpSpeed * Time.deltaTime * m_currentSwayDirection;
//								m_visualAidArray[i,band].transform.localPosition = barPos;
//								
//							}
//						}
//					}
//					else
//					{
//						for(int i = 0 ; i <m_subBandCount; i++)
//						{
//							Vector2 barPos = m_visualAidArray[i,band].transform.localPosition;
//							float startPosX = m_localStartPos[i].x;
//							barPos.x = Mathf.Lerp(barPos.x,m_localStartPos[i].x +((m_localSwayInUnits + 0.5f) * m_currentSwayDirection),m_swayLerpSpeed * ((float)band/(float)barPosIncrement) - m_swayBarIndex);//m_swayLerpSpeed * Time.deltaTime * m_currentSwayDirection;
//							m_visualAidArray[i,band].transform.localPosition = barPos;
//							
//						}
//					}
//				}
//
//			}
//		}
//
//


//		for(int bars = 0; bars< m_maxBands; bars ++)
//		{
//			float positionOfCurrentBar = bars * m_visualAidArray[0,0].renderer.bounds.size.y * 1.25f;
//			float distanceToNextSwayPoint = swayContactPointsRespectToLength[currentSwayPoint] - positionOfCurrentBar;
//			float relativeDistanceToSwayPoint = positionOfCurrentBar / swayContactPointsRespectToLength[currentSwayPoint];
//			if(relativeDistanceToSwayPoint > 1 && m_swayContactPoints > 1 && currentSwayPoint +1 >swayContactPointsRespectToLength.Length)
//			{
//				if(relativeDistanceToSwayPoint < 2)
//				{
//					relativeDistanceToSwayPoint = 1 - relativeDistanceToSwayPoint;
//				}
//				else
//				{
//					currentSwayPoint++;
//					relativeDistanceToSwayPoint =positionOfCurrentBar / swayContactPointsRespectToLength[currentSwayPoint];
//				}
//			}
//			float capeSwayIncrement = m_currentSwayDirection * m_swayLerpSpeed * Time.deltaTime * relativeDistanceToSwayPoint;
//
//			for(int bands = 0; bands < m_subBandCount; bands ++)
//			{
//				// sway
//				Vector2 barPos = m_visualAidArray[bands,bars].transform.localPosition;
//				float startPosX = (m_visualAidArray[0,0].renderer.bounds.size.x *1.25f* bands) - (m_subBandCount/2 * m_visualAidArray[0,0].renderer.bounds.size.x);
//				if(barPos.x > startPosX +( m_localSwayInUnits ) )
//				{
//					m_currentSwayDirection *= -1;
//					barPos.x = startPosX + (m_localSwayInUnits) ;
//					m_visualAidArray[bands,bars].transform.localPosition = barPos;
//
//				}
//				else if (barPos.x < startPosX - (m_localSwayInUnits))
//				{
//					m_currentSwayDirection *= -1;
//					barPos.x = startPosX - (m_localSwayInUnits);
//					m_visualAidArray[bands,bars].transform.localPosition = barPos;
//				}
//				else
//				{
//					barPos.x += capeSwayIncrement;
//					m_visualAidArray[bands,bars].transform.localPosition = barPos;
//				}
//			}
//		}

		///////
		/// 
		/// 

		
		
	}
	
}
