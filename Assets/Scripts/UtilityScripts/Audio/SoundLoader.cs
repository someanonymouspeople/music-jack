﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;




public class SoundLoader : MonoBehaviour 
{
	string m_path = "";
#if UNITY_STANDALONE
	public string[] m_supportedExtensions = {"ogg","wav"};
#endif
#if UNITY_ANDROID
	string[] m_supportedExtensions = {"wav","mp3","ogg","aac"};
#endif
#if UNITY_IPHONE
	string[] m_supportedExtensions = {"ogg","mp3"};
#endif
	public List<FileInfo> m_soundFiles;
	AudioSource m_source;
	List<AudioClip> m_loadedClipList;
	public void Start()
	{
		m_soundFiles = new List<FileInfo>();
		m_loadedClipList = new List<AudioClip>();
			
	}

	public void SetSource(AudioSource source)
	{
		m_source = source;
	}

	public List<FileInfo> GetSoundFileData()
	{
		return m_soundFiles;
	}

	bool IsValidFileType(string filename)
	{
		foreach(string ext in m_supportedExtensions)
		{
			if(filename.IndexOf(ext) > -1) 
			{
				return true;
			}
		}
		return false;
	}

	public void LoadAllAudioFiles(string path)
	{
		//m_path = path;
		m_soundFiles.Clear();
		m_path = path;
		////////////////////recursive search/////////////////////
		try
		{
			DirectoryInfo rootDirInfo = new DirectoryInfo(m_path);
			foreach (FileInfo file in rootDirInfo.GetFiles())
			{
				if(IsValidFileType(file.FullName))
				{
					m_soundFiles.Add(file);
					CheckAndLoadAudioFile(m_soundFiles.IndexOf(file));
				}
			}
			
			foreach (string directory in Directory.GetDirectories(m_path))
			{
				DirectoryInfo dirInfo = new DirectoryInfo(directory);
				LoadAllAudioFileData(directory);
			}
		}
		catch (System.Exception excpt)
		{
			Debug.Log(excpt.Message);
		}


	}

	public void LoadAllAudioFileData(string path)
	{
		Debug.Log("Populating with path "+path);
		m_soundFiles.Clear();
		m_path = path;
		////////////////////recursive search/////////////////////
		try
		{
			DirectoryInfo rootDirInfo = new DirectoryInfo(m_path);
			foreach (FileInfo file in rootDirInfo.GetFiles())
			{
				if(IsValidFileType(file.FullName))
				{
					m_soundFiles.Add(file);
				}
			}

			foreach (string directory in Directory.GetDirectories(m_path))
			{
				DirectoryInfo dirInfo = new DirectoryInfo(directory);
				LoadAllAudioFileData(directory);
			}
		}
		catch (System.Exception excpt)
		{
			Debug.Log(excpt.Message);

		}
		
		////////////////////recursive search/////////////////////

		
	}

	public AudioClip CheckAudioFile(int index)
	{
		string filePath = m_soundFiles[index].FullName;
		string[] pathParts = filePath.Split('\\');
		if(m_loadedClipList.Count>0)
		{
			foreach(AudioClip audio in m_loadedClipList)
			{
				if(audio.name == pathParts[pathParts.Length -1])
				{
					return audio;
					
				}
			}
		}
		return null;
	}

	public void CheckAndLoadAudioFile(int index)
	{
		string filePath = m_soundFiles[index].FullName;
		string[] pathParts = filePath.Split('\\');
		if(m_loadedClipList.Count>0)
		{
		foreach(AudioClip audio in m_loadedClipList)
		{
			if(audio.name == pathParts[pathParts.Length -1])
			{
				Debug.Log("file already loaded");

			}
		}
		}
		Debug.Log("loading audio file");

		StartCoroutine(LoadAudioFileInThread(m_soundFiles[index].FullName));
	}

	public string GetAudioFilePath(int soundfileIndex)
	{
		Mathf.Clamp(soundfileIndex,0,m_soundFiles.Count);

		int endIndex = 0;
		if(m_soundFiles.Count>0)
		{
			return m_soundFiles[soundfileIndex].FullName;
		}
		return null;
	}

	public AudioClip GetAudioFile(int soundFileIndex)
	{
		int endIndex = 0;
		if(m_loadedClipList.Count>0)
		{
			foreach(AudioClip audio in m_loadedClipList)
			{
				if(audio.name.Contains(m_soundFiles[soundFileIndex].Name))
				{
					return audio;
				}
				endIndex++;
			}
		}
		Debug.Log("Getting audio file "+m_soundFiles[soundFileIndex].FullName);
		LoadAudioFile(m_soundFiles[soundFileIndex].FullName);

		for(int i = 0; i< m_loadedClipList.Count;i++)
		{
			Debug.Log("Current comparison index "+i);
			Debug.Log("Comparing "+m_soundFiles[soundFileIndex].Name+" To "+m_loadedClipList[i].name);
			if(m_loadedClipList[i].name.Contains(m_soundFiles[soundFileIndex].Name))
			{
				Debug.Log("Clip found in loaded clip list");
				return m_loadedClipList[i];
			}
		}
		Debug.Log("Clip not found in loaded clip list");
		return null;
	}

	bool LoadAudioFile(string filePath)
	{
		//initialize www object to load file
		Debug.Log(filePath);
		WWW audioLoader = new WWW("file://"+filePath);
		
		while(!audioLoader.isDone)
		{
			//Debug.Log("loading audio file in main thread");
		}

		//else 
		//save in list for later access
		AudioClip audioClip = audioLoader.GetAudioClip(false);
		string[] pathParts = filePath.Split('\\');
		audioClip.name = pathParts[pathParts.Length - 1];

		if(audioClip.isReadyToPlay)
		{
			Debug.Log(audioClip.length);
			Debug.Log(audioClip.name);
			Debug.Log("MusicJunkie successfully loaded music "+ audioClip.name);
		}
		else
		{
			Debug.Log("load unsuccessful " + audioLoader.error);
		}

		m_loadedClipList.Add(audioClip);
		for(int i=0;i<m_loadedClipList.Count; i++)
		{
			Debug.Log("LoadedClipList index "+i+" Contains :- "+m_loadedClipList[i].name);
		}



		return true;
	}
	
	public IEnumerator LoadAudioFileInThread(string filePath)
	{
		string[] pathParts = filePath.Split('\\');

		//initialize www object to load life
		WWW audioLoader = new WWW("file://"+filePath);

		while(!audioLoader.isDone)
		{
			yield return null;
		}
		AudioClip audioClip = audioLoader.GetAudioClip(false);
		Debug.Log("MusicJunkie successfully loaded music "+ audioClip.name );

		//save in list for later access
		audioClip.name = pathParts[pathParts.Length - 1];
		m_loadedClipList.Add(audioClip);
	}

	public IEnumerator LoadAudioFileInThread(int index)
	{
		string filePath = GetAudioFilePath(index);
		string[] pathParts = filePath.Split('\\');
		
		//initialize www object to load life
		WWW audioLoader = new WWW("file://"+filePath);
		
		while(!audioLoader.isDone)
		{
			yield return null;
		}
		AudioClip audioClip = audioLoader.GetAudioClip(false);
		Debug.Log("MusicJunkie successfully loaded music "+ audioClip.name );
		
		//save in list for later access
		audioClip.name = pathParts[pathParts.Length - 1];
		m_loadedClipList.Add(audioClip);
	}



}
