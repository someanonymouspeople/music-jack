using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public enum BEAT_DETECTION_MODES
{
	NONE,
	SIMPLE,
	FREQUENCY_BASED,
	TONE_BASED
}


public enum DETECTION_TONE
{
	NONE = -1,
	BASS,
	HIGH_BASS,
	MID,
	HIGH_MID,
	TREBLE
}

public class BeatEvent
{

	public BeatEvent()
	{
		m_instantEnergy = 0;
		m_averageEnergy = 0;
		DETECTION_TONE m_beatTone = DETECTION_TONE.NONE;
		m_subBandDetectedIn = 0;

	}


	public float m_instantEnergy;
	public float m_averageEnergy;
	public float m_crossedBeatThreshold;
	public DETECTION_TONE m_beatTone;
	public short m_subBandDetectedIn;

}

public class BeatTimeLineEvent
{
	public BeatTimeLineEvent()
	{
		m_beatEvent = new BeatEvent();
		m_timeStamp = 0;
		m_spawnedIndicator = false;

	}

	public float m_instantEnergyOfSong;
	public float m_averageEnergyOfSong;
	public BeatEvent m_beatEvent;
	public float m_timeStamp;


	public bool m_spawnedIndicator;
}

public class CustomComplex
{
	public CustomComplex()
	{
		re = 0;
		imag = 0;
	}
	public CustomComplex(double real, double imaginary)
	{
		re = real;
		imag = imaginary;
		
	}
	public double re = 0.0;     // re component
	public double imag = 0.0;     // imag component
	
}


public class EnergyHistoryElement
{
	public EnergyHistoryElement()
	{

	}
	public EnergyHistoryElement(float channelOrSubBand,float energy)
	{
		m_channelOrSubBand = channelOrSubBand;
		m_energy = energy;
	}

	public float m_channelOrSubBand;
	public float m_energy;
}

public class SubBandEnergyElement
{
	public SubBandEnergyElement()
	{
		m_energyBuffer = new Queue<float>();
		m_upperFrequency=0;
		m_subBandCount = 0;
		m_tone = DETECTION_TONE.BASS;
	}

	public float GetAverageEnergyOfSubBand()
	{
		float value = 0;
		if(m_energyBuffer.Count > 0)
		{
			foreach(float f in m_energyBuffer)
			{
				value+= f;
			}
			value /= m_energyBuffer.Count;
		}
		return value;
	}
	
	public Queue<float> m_energyBuffer;
	public float m_upperFrequency;
	public float m_subBandCount;
	public DETECTION_TONE m_tone;
}

public class MusicAnalyzerV2: MonoBehaviour 
{
	public double[] m_audioData;
	public bool m_switchModeOnTheFly;
	public bool m_willBeUsedDuringRunTime;
	bool m_runTimeDetectionDone;
	public static MusicAnalyzerV2 m_instance = null;
	public List<SubBandEnergyElement> m_toneEnergyHistoryBuffer;
	public Queue<EnergyHistoryElement> m_instantEnergyHistoryBuffer;
	public List<SubBandEnergyElement> m_subBandEnergyHistoryBuffer;

	public List<SubBandEnergyElement> m_runTimeToneEnergyHistoryBuffer;
	public Queue<EnergyHistoryElement> m_runTimeInstantEnergyHistoryBuffer;
	public List<SubBandEnergyElement> m_runTimeSubBandEnergyHistoryBuffer;


	public float[] m_averageEnergyOfToneBands;
	public AudioSource m_source;
	public BEAT_DETECTION_MODES m_mode;
	public float[] m_baseBeatThresholds;
	public float m_beatThresholdModifier;
	public float m_beatThreshold;
	public List<BeatTimeLineEvent> m_calculatedBeatTimeLine;
	public List<BeatTimeLineEvent> m_accumulatedBeats;
	public List<BeatTimeLineEvent> m_prevAccumulatedBeats;
	public List<YPositionedBeatTimeLineEvent> m_postProcessedBeatTimeLine = new List<YPositionedBeatTimeLineEvent>();
	public float m_maxVolume;

	GameplayScript.BeatCallBack m_callBack;
	public int m_subBandCount;
	public bool[] m_checkFilters = new bool[5];
	public float m_energyVariance;
	int m_startIndexOfBassSamples = 0;
	int m_startIndexOfHighBassSamples = 0;
	int m_startIndexOfMidSamples = 0;
	int m_startIndexOfHighMidSamples = 0;
	int m_startIndexOfTrebleSamples = 0;
	public float m_instantEnergy;
	public float m_averageEnergy;
	public float m_postProcessSpawnTime;
	public int m_samplesProcessedThisSong;

	SPAWN_LANE m_prevConsideredLane;

	public void ClearTimeLines()
	{
		m_prevConsideredLane = SPAWN_LANE.LANE_THREE;
		if(m_calculatedBeatTimeLine != null)
		{
		m_calculatedBeatTimeLine.Clear();
		}
		if(m_postProcessedBeatTimeLine != null)
		{
		m_postProcessedBeatTimeLine.Clear();
		}
	}

	public void Awake()
	{

		if(m_postProcessSpawnTime==0)
		{
			m_postProcessSpawnTime = 0.2f;
		}

		if(MusicAnalyzerV2.GetInstance() != null)
		{
			Destroy(this);
			return;
		}

		m_instance = this;
		Init();
	}

	public void Init()
	{
		m_audioData = new double[1024];
		m_runTimeToneEnergyHistoryBuffer =new List<SubBandEnergyElement>();
		m_runTimeInstantEnergyHistoryBuffer = new Queue<EnergyHistoryElement>();
		m_runTimeSubBandEnergyHistoryBuffer = new List<SubBandEnergyElement>();

		m_calculatedBeatTimeLine = new List<BeatTimeLineEvent>();
		m_accumulatedBeats = new List<BeatTimeLineEvent>();
		m_prevAccumulatedBeats= new List<BeatTimeLineEvent>();
		m_averageEnergyOfToneBands = new float[System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1];
		bool[] m_tempCheck = new bool[5];
		if(m_checkFilters.Length >5)
		{
			for(int i =0; i<5; i++)
			{
				m_tempCheck[i] = m_checkFilters[i];
			}
			m_checkFilters = new bool[5];
			m_checkFilters = m_tempCheck;
		}
		else if (m_checkFilters.Length <5)
		{
			m_tempCheck = m_checkFilters;
			for(int i=m_checkFilters.Length-1; i <5 ;i++)
			{
				m_tempCheck[i] = true;
			}
			m_checkFilters = new bool[5];
			m_checkFilters = m_tempCheck;
		}

		float[] m_tempThresholdCheck = new float[5];
		if(m_baseBeatThresholds.Length >5)
		{
			for(int i =0; i<5; i++)
			{
				m_tempThresholdCheck[i] = m_baseBeatThresholds[i];
			}
			m_baseBeatThresholds = new float[5];
			m_baseBeatThresholds = m_tempThresholdCheck;
		}
		else if (m_baseBeatThresholds.Length <5)
		{
			m_tempThresholdCheck = m_baseBeatThresholds;
			for(int i=m_baseBeatThresholds.Length-1; i <5 ;i++)
			{
				m_tempThresholdCheck[i] = 3.5f;
			}
			m_baseBeatThresholds = new float[5];
			m_baseBeatThresholds = m_tempThresholdCheck;
		}


		if(m_subBandCount == 0)
		{
			m_subBandCount = 64;
		}
		Mathf.Clamp(m_subBandCount,1,128);

		m_instantEnergyHistoryBuffer = new Queue<EnergyHistoryElement>(43);
	}

	public List<YPositionedBeatTimeLineEvent> GetPostProcessedBeatTimeLine()
	{
		if(m_postProcessedBeatTimeLine != null)
		{
			return m_postProcessedBeatTimeLine;

		}
		else
		{
			Debug.Log("postProcessNotCalled");

		}
		return null;
	}

	public static MusicAnalyzerV2 GetInstance()
	{
		return m_instance;
	}

	public void SetAudioSource(AudioSource source)
	{
		//to establish code flow and grant control to the user
		m_source = source;
	}
	
	public void SetDetectionMode(BEAT_DETECTION_MODES mode)
	{	
		if(!m_switchModeOnTheFly)
		{
			//to establish code flow and grant control to the user
			switch(mode)
			{
			case BEAT_DETECTION_MODES.SIMPLE:
			{
				m_instantEnergyHistoryBuffer = new Queue<EnergyHistoryElement>(43);
			}
				break;
				
			case BEAT_DETECTION_MODES.FREQUENCY_BASED:
			{
				m_subBandEnergyHistoryBuffer = new List<SubBandEnergyElement>(m_subBandCount);
				//initialize the energyElements
				for(int i = 0; i<m_subBandCount; i++)
				{
					SubBandEnergyElement tempElement = new SubBandEnergyElement();
					m_subBandEnergyHistoryBuffer.Add(tempElement);
				}
			}
				break;
			case BEAT_DETECTION_MODES.TONE_BASED:
			{

				m_toneEnergyHistoryBuffer = new List<SubBandEnergyElement>(System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1);
				for(int i=0; i<System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1;i++)
				{
					SubBandEnergyElement tempElement = new SubBandEnergyElement();
					m_toneEnergyHistoryBuffer.Add(tempElement);
				}


			}
				break;
			}
		}
		else
		{
			m_instantEnergyHistoryBuffer = new Queue<EnergyHistoryElement>(43);
			m_runTimeInstantEnergyHistoryBuffer = new Queue<EnergyHistoryElement>(43);
			m_subBandEnergyHistoryBuffer = new List<SubBandEnergyElement>(m_subBandCount);
			m_runTimeSubBandEnergyHistoryBuffer = new List<SubBandEnergyElement>(m_subBandCount);

			//initialize the energyElements
			for(int i = 0; i<m_subBandCount; i++)
			{
				SubBandEnergyElement tempElement = new SubBandEnergyElement();
				m_subBandEnergyHistoryBuffer.Add(tempElement);
				SubBandEnergyElement tempRunTimeElement = new SubBandEnergyElement();
				m_runTimeSubBandEnergyHistoryBuffer.Add(tempRunTimeElement);
			}
			m_toneEnergyHistoryBuffer = new List<SubBandEnergyElement>(System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1);
			m_runTimeToneEnergyHistoryBuffer = new List<SubBandEnergyElement>(System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1);
			for(int i=0; i<System.Enum.GetNames(typeof(DETECTION_TONE)).Length -1;i++)
			{
				SubBandEnergyElement tempElement = new SubBandEnergyElement();
				m_toneEnergyHistoryBuffer.Add(tempElement);
				SubBandEnergyElement tempRunTimeElement = new SubBandEnergyElement();
				m_runTimeToneEnergyHistoryBuffer.Add(tempRunTimeElement);
			}
		}
		m_mode = mode;
	}


	private float[] GetAudioSamples(int sampleSize)
	{
		float[] audioSamples;
		int sampleCount = 0;
		int offsetInSamples = m_source.timeSamples;
		float numChannels = m_source.clip.channels;
		if(numChannels==2)
		{
			//88200 samples to make up 44100 samples on each audio channel
			sampleCount = sampleSize * 2;
		}
		else if(numChannels ==1)
		{
			//44100 samples because the currently playing audio is mono
			sampleCount = sampleSize;
			
		}
		audioSamples = new float[sampleCount];
		if(offsetInSamples + 1024 > m_source.clip.samples)
		{

		}
		else
		{
			m_source.clip.GetData(audioSamples,offsetInSamples);
		}
		return audioSamples;
	}

	public bool SimpleBeatCheck(GameplayScript.BeatCallBack callBack, out float outInstantEnergy, out float outAverageEnergy)
	{
		outInstantEnergy = 0;
		outAverageEnergy = 0;
		return true;
	}

	public DETECTION_TONE GetToneForFrequency(float frequency)
	{
		if(frequency < 100)
		{
			return DETECTION_TONE.BASS;
		}
		else if(frequency < 400)
		{
			return DETECTION_TONE.HIGH_BASS;
		}
		else if(frequency <1200)
		{
			return DETECTION_TONE.MID;
		}
		else if(frequency < 5200)
		{
			return DETECTION_TONE.HIGH_MID;
		}
		else
		{
			return DETECTION_TONE.TREBLE;
		}
		return DETECTION_TONE.BASS;
	}
	public float[] GetAverageVolumeOfToneBands(float refValue)
	{
		float[] averageTempArray = GetAverageEnergyOfToneBands();
		for(int i =0 ;i< 5;i++)
		{
			if(averageTempArray[i] > 0)
			{
				averageTempArray[i] = 20 * (Mathf.Log10(Mathf.Sqrt(averageTempArray[i])/refValue));
			}
			if(averageTempArray[i] < 0)
			{
				averageTempArray[i] = 0;
			}
		}
		return averageTempArray;
	}
	
	public float[] GetInstantVolumeOfToneBands(float refValue)
	{
		float[] instantTempArray = new float[5];
		for(int i =0 ;i< 5;i++)
		{
			instantTempArray[i] = 0;
			if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count>0)
			{
				instantTempArray[i] = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray()[m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count-1];
				if(instantTempArray[i] > 0)
				{
					instantTempArray[i] = 20 * (Mathf.Log10(Mathf.Sqrt(instantTempArray[i])/refValue));
				}
				if(instantTempArray[i] < 0)
				{
					instantTempArray[i] = 0;
				}
			}
		}
		return instantTempArray;
	}

	public float[] GetInstantVolumeOfSubBands(bool runtime,float refValue)
	{
		float[] instantTempArray = new float[m_subBandCount];
		for(int i =0 ;i< m_subBandCount;i++)
		{
			instantTempArray[i] = 0;
			if(runtime)
			{
				if(m_runTimeSubBandEnergyHistoryBuffer[i].m_energyBuffer.Count>0)
				{
					instantTempArray[i] = m_runTimeSubBandEnergyHistoryBuffer[i].m_energyBuffer.Last();// m_runTimeSubBandEnergyHistoryBuffer[i].m_energyBuffer.ToArray()[m_runTimeSubBandEnergyHistoryBuffer[i].m_energyBuffer.Count-1];
				}
			}
			else
			{
				if(m_subBandEnergyHistoryBuffer[i].m_energyBuffer.Count>0)
				{
					instantTempArray[i] = m_subBandEnergyHistoryBuffer[i].m_energyBuffer.Last();//.ToArray()[m_subBandEnergyHistoryBuffer[i].m_energyBuffer.Count-1];
				}

			}

			if(instantTempArray[i] > 0)
			{
				instantTempArray[i] = 20 * (Mathf.Log10(Mathf.Sqrt(instantTempArray[i])/refValue));
			}
			if(instantTempArray[i] < 0)
			{
				instantTempArray[i] = 0;
			}
		}

		return instantTempArray;
	}

	public float[] GetAverageVolumeOfSubBands(bool runTime,float refValue)
	{
		float[] averageTempArray = GetAverageEnergyOfSubBands(runTime);
		for(int i =0 ;i< m_subBandCount;i++)
		{
			if(averageTempArray[i] > 0)
			{
				averageTempArray[i] = 20 * (Mathf.Log10(Mathf.Sqrt(averageTempArray[i])/refValue));
			}
			if(averageTempArray[i] < 0)
			{
				averageTempArray[i] = 0;
			}
		}
		return averageTempArray;
	}

	public float GetInstantVolumeOfSubBand(int subBandNumber,float refValue)
	{
		if(Camera.main.audio.clip != null && m_subBandEnergyHistoryBuffer[subBandNumber].m_energyBuffer.ToArray().Length >0)
		{
			if(subBandNumber < m_subBandCount && subBandNumber >=0)
			{
				int numberToCheck = m_subBandEnergyHistoryBuffer[subBandNumber].m_energyBuffer.Count-1;
				//numberToCheck = Mathf.Clamp(numberToCheck,0,43);
				float instantVolume = m_subBandEnergyHistoryBuffer[subBandNumber].m_energyBuffer.ToArray()[numberToCheck];
				instantVolume = 20 * (Mathf.Log10(Mathf.Sqrt(instantVolume)/refValue));

				if(instantVolume < 0)
				{
					instantVolume = 0;
				}
				return instantVolume;
			}
			Debug.Log("invalid subband number "+ subBandNumber);
			return 0;
		}
		//Debug.Log("no audio playing yet");
		return 0;
	}

	public float HammingWindow(int n, int N) 
	{
		return 0.54f - 0.46f * (float) Mathf.Cos((2 * Mathf.PI * n) / (N - 1));
	}
	public float BlackmanHarrisWindow(int n, int N)
	{
		return  0.35875f - (0.48829f * (float)Mathf.Cos((2 * Mathf.PI * n)/(N-1))) + (0.14128f * (float)Mathf.Cos((4 * Mathf.PI * n)/(N-1))) - (0.01168f * (float)Mathf.Cos((6 * Mathf.PI * n)/(N-1)));
	}


	public IEnumerator CalculateBeatTimeLine(AudioClip clip,BEAT_DETECTION_MODES mode,bool postProcess,bool yieldToMainThread = false,int timePast = 0, int secondsToCalcFor = 0, int sampleWindow = 1024)
	{
		if(timePast == 0)
		{
			ClearTimeLines();
		}

		List<BeatTimeLineEvent> beatTimeLine = null;
		if(clip != null)
		{
			if(clip.isReadyToPlay)
			{

				beatTimeLine = new List<BeatTimeLineEvent>();
				float timeToCalcFor = 0;
				if(secondsToCalcFor == 0)
				{
					timeToCalcFor = clip.length;
				}
				else
				{
					timeToCalcFor = secondsToCalcFor;
				}

				int sampleRate = clip.frequency;
				int samplesPast = 0;
				if(timePast != 0)
				{
					samplesPast = timePast* sampleRate;
				}
				
				//iterate throught the song and find all beats

				float samplesToIterateThrough = timeToCalcFor * sampleRate;
				float audioSamplesToIterateThrough = samplesPast+ samplesToIterateThrough;
				if(audioSamplesToIterateThrough > clip.length * sampleRate)
				{
					audioSamplesToIterateThrough = clip.length * sampleRate;
				}
				int sampleWindowCount = sampleWindow * clip.channels; 
				
				float averageEnergy;
				float instantEnergy;
			

				List<BeatEvent> outBeatEvents;
				float[] audioSamplesToGetFor = new float[sampleWindowCount];
				for(int sampleIndex = samplesPast;sampleIndex< audioSamplesToIterateThrough; sampleIndex += sampleWindowCount)
				{

					switch(mode)
					{
					case BEAT_DETECTION_MODES.SIMPLE:
					{
						
					}
						break;
					case BEAT_DETECTION_MODES.FREQUENCY_BASED:
					{
						
					}
						break;
					case BEAT_DETECTION_MODES.TONE_BASED:
					{

						clip.GetData(audioSamplesToGetFor,sampleIndex);
						bool beatDetected = ToneBasedBeatCheck(audioSamplesToGetFor,clip.channels,out instantEnergy,out averageEnergy,out outBeatEvents);
						if(beatDetected)
						{
							for(int i = 0; i< outBeatEvents.Count; i++)
							{
								BeatTimeLineEvent temp = new BeatTimeLineEvent();
								temp.m_instantEnergyOfSong = instantEnergy;
								temp.m_averageEnergyOfSong = averageEnergy;
								temp.m_beatEvent = outBeatEvents[i];
								temp.m_timeStamp = (((float)sampleIndex + ((float)sampleWindowCount/2)) /clip.samples) * clip.length;
								beatTimeLine.Add(temp);
							}
						}
						if(m_willBeUsedDuringRunTime)
						{
							m_samplesProcessedThisSong = sampleIndex;
						}
					}
						break;
					default:
					{
						
					}
						break;
					}

					//clear allocated memory for new samples next iteration
					for(int i =0; i<sampleWindowCount; i++)
					{
						audioSamplesToGetFor[i] = 0;
					}

					if(yieldToMainThread)
					{
						//yield return new WaitForSeconds(0.5f);
						yield return null;
					}


				}
			}
		}
		else
		{
			Debug.Log("Clip not ready to process");
			yield break;
		}


		m_calculatedBeatTimeLine = beatTimeLine;
			
		//postProcess the beats
		if(postProcess)
		{
			CustomPostProcessBeats(ref m_calculatedBeatTimeLine);
			if(!yieldToMainThread)
			{
				m_postProcessedBeatTimeLine = InjectYPositions(m_calculatedBeatTimeLine);
			}
			else
			{
				m_postProcessedBeatTimeLine.AddRange(InjectYPositions(m_calculatedBeatTimeLine));
			}

		}

	}

	public SPAWN_LANE GetRandomSpawnLane(SPAWN_LANE lastSpawn, bool isLockedToNearest,int laneCount = 1)
	{
		int m_numberOfLanes = System.Enum.GetNames(typeof(SPAWN_LANE)).Length;
		if(isLockedToNearest)
		{
			int laneBehindPrev = (int)lastSpawn - laneCount;
			if(laneBehindPrev < 1)
			{
				laneBehindPrev = 1;
			}
			int laneAheadOfPrev = (int)lastSpawn + laneCount;
			if(laneAheadOfPrev > m_numberOfLanes)
			{
				laneAheadOfPrev = m_numberOfLanes;
			}
			int laneChecker = Random.Range(laneBehindPrev * 10, (laneAheadOfPrev * 10 + 9))/10;
			
			return (SPAWN_LANE)laneChecker;
			
			
		}
		else
		{
			int laneChecker = Random.Range(0,m_numberOfLanes*10)/10;
			
			return (SPAWN_LANE)laneChecker;
			
		}
	}

	public List<YPositionedBeatTimeLineEvent> InjectYPositions(List<BeatTimeLineEvent> beatTimeLine)
	{
		Vector2 camScreenOffset = Camera.main.ScreenToWorldPoint(new Vector2());
		List<YPositionedBeatTimeLineEvent> listToReturn = new List<YPositionedBeatTimeLineEvent>();

		foreach(BeatTimeLineEvent ev in beatTimeLine)
		{
			YPositionedBeatTimeLineEvent yPosedBeatTimeLineEvent = new YPositionedBeatTimeLineEvent(ev);
			SPAWN_LANE randomLane = GetRandomSpawnLane(m_prevConsideredLane,true);
			float yToSet = (camScreenOffset.y + 1.5f) +((0.75f) *(int)randomLane);
			yPosedBeatTimeLineEvent.m_beatYPos = yToSet;
			listToReturn.Add(yPosedBeatTimeLineEvent);
			m_prevConsideredLane = randomLane;
		}
		return listToReturn;
	}

		

	public List<BeatTimeLineEvent> GetCalculatedBeatTimeLine()
	{
		if(m_calculatedBeatTimeLine != null)
		{
			return m_calculatedBeatTimeLine;
		}
		else
		{
			Debug.Log("CalculateBeatTimeLine not called yet");
		}
		return null;
	}



	public bool ToneBasedBeatCheck(float[] inputAudioSamples,int channels, out float outInstantEnergy, out float outAverageEnergy,out List<BeatEvent> outBeatEvents)
	{
		bool beatDetected = false;
		outInstantEnergy = 0;
		outAverageEnergy = 0;
		outBeatEvents = new List<BeatEvent>();
		//retrieve audio samples
		int numChannels = channels;
		float[] audioSamples = inputAudioSamples;

		//average values if stereo and convertToComplex(NOTE :- iterations 1024)
	
		double[] m_audioData = new double[1024];

		if(numChannels ==2)
		{

			for(int i =0; i< 1024;i++)
			{
				//m_audioData averaging
				m_audioData[i] = (double)((audioSamples[i] + audioSamples[i+1])/2);
			}
			
		}
		else if(numChannels == 1)
		{
			for(int i =0; i< 1024;i++)
			{
				//m_audioData averaging
				m_audioData[i] = (double)audioSamples[i];
			}

		}
		
		//Generate FFT from complexConvert
		LomontFFT FFTHelper2 = new LomontFFT();
		FFTHelper2.RealFFT(ref m_audioData,true);


		int index = 0;
		for(int i = 0; i<m_audioData.Length/2; i++)
		{
			

			m_audioData[i] = m_audioData[index] * HammingWindow(i,1024);
			m_audioData[i + 1] = m_audioData[index + 1] * HammingWindow(i,1024);

			index += 2;
			
		}
		
		// the generated FFT consists of 512 results as opposed to the 1024 we are supposed to get from the input 
		// owing to the fact that the other 512 are simply mirrors of this one
		
		// compute the modulus of the result and categorize by tone (NOTE:- iterations 512)
		// also compute the instant energy and store it locally
		// sacrificing code flow for optimization
		

		float bassEnergiesCount = 0;
		float highBassEnergiesCount = 0;
		float midEnergiesCount = 0;
		float highMidEnergiesCount = 0;
		float trebleEnergiesCount = 0;
		
		float instantBassBandEnergy= 0;
		float instantHighBassBandEnergy= 0;
		float instantMidBandEnergy= 0;
		float instantHighMidBandEnergy= 0;
		float instantTrebleBandEnergy= 0;
		
		int subBandCounter = 0;
		float instantEnergyThisSubBand = 0;
		float modulusOfFFtThisIteration =0;
		for(int i=0; i<m_audioData.Length/2; i+=2)
		{
			modulusOfFFtThisIteration = Mathf.Pow((float)m_audioData[i],2)+Mathf.Pow((float)m_audioData[i+1],2);

			
			DETECTION_TONE tone = GetToneForFrequency((i) * 22050/(m_audioData.Length/2));
			switch(tone)
			{
			case DETECTION_TONE.BASS:
			{

				bassEnergiesCount++;
				instantBassBandEnergy += modulusOfFFtThisIteration;
			}
				break;
			case DETECTION_TONE.HIGH_BASS:
			{

				highBassEnergiesCount++;
				instantHighBassBandEnergy += modulusOfFFtThisIteration;
			}
				break;
				
			case DETECTION_TONE.MID:
			{

				midEnergiesCount++;
				instantMidBandEnergy += modulusOfFFtThisIteration;
			}
				break;
				
			case DETECTION_TONE.HIGH_MID:
			{

				highMidEnergiesCount++;
				instantHighMidBandEnergy += modulusOfFFtThisIteration;
			}
				break;
				
			case DETECTION_TONE.TREBLE:
			{

				trebleEnergiesCount++;
				instantTrebleBandEnergy += modulusOfFFtThisIteration;
			}
				break;
			}
			
			//also fill in subbandVolumeHistoryBuffer in anticipation of later use
			if((i+1)% (m_audioData.Length/2)/m_subBandCount == 0)
			{
				instantEnergyThisSubBand/= (m_audioData.Length/2)/m_subBandCount;
				
				if(m_subBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Count< 43)
				{
					m_subBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Enqueue(instantEnergyThisSubBand);
				}
				else
				{
					m_subBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Dequeue();
					m_subBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Enqueue(modulusOfFFtThisIteration);
					m_subBandEnergyHistoryBuffer[subBandCounter].m_subBandCount = m_subBandCount;
					m_subBandEnergyHistoryBuffer[subBandCounter].m_tone = tone;
					m_subBandEnergyHistoryBuffer[subBandCounter].m_upperFrequency = subBandCounter * (22050/m_subBandCount);
				}
				subBandCounter ++;
			}
			else
			{
				instantEnergyThisSubBand += modulusOfFFtThisIteration;
			}
		}
		
		//calculate average energies in the different bands (Note:- iterations 512)
		instantBassBandEnergy/= bassEnergiesCount;
		instantHighBassBandEnergy/= highBassEnergiesCount;
		instantMidBandEnergy/= midEnergiesCount;
		instantHighMidBandEnergy/= highMidEnergiesCount;
		instantTrebleBandEnergy/= trebleEnergiesCount;
		
		
		float instantEnergyOfMusic = instantBassBandEnergy+ instantHighBassBandEnergy+ instantMidBandEnergy + instantHighMidBandEnergy + instantTrebleBandEnergy;
		instantEnergyOfMusic /= 5;
		outInstantEnergy = instantEnergyOfMusic;
		
		float averageEnergyOfMusic = 0; 
		
		//store in their respective tone buffers and check for beats(NOTE:- iterations 5* (2-250) and this can be made better(MINDEFFED >:( ) )
		float currentInstantEnergyToCheckFor = 0;
		float averageEnergyOfToneBand = 0;
		for(int i = 0; i<m_toneEnergyHistoryBuffer.Count; i++)
		{
			float instantEnergyOfToneBand = 0;
			switch(i)
			{
			case 0:
			{
				instantEnergyOfToneBand= instantBassBandEnergy;
				if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantBassBandEnergy);
					return false;
				}
				else
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantBassBandEnergy);
					
					
					//get instant values of the previous second
					float[] energyBufferArray = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
					
					//average the instant energy values
					averageEnergyOfToneBand = instantBassBandEnergy;
					for(int k=0; k<energyBufferArray.Length -1; k++)
					{
						averageEnergyOfToneBand += energyBufferArray[k];
					}
					averageEnergyOfToneBand /= energyBufferArray.Length;
					currentInstantEnergyToCheckFor = instantBassBandEnergy;
					averageEnergyOfMusic += averageEnergyOfToneBand;
				}
			}
				break;
			case 1:
			{
				instantEnergyOfToneBand= instantHighBassBandEnergy;
				if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighBassBandEnergy);
					return false;
				}
				else
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighBassBandEnergy);
					
					//Check for beats
					//get instant values of the previous second
					float[] energyBufferArray = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
					
					//average the instant energy values
					averageEnergyOfToneBand = instantHighBassBandEnergy;
					for(int k=0; k<energyBufferArray.Length -1; k++)
					{
						averageEnergyOfToneBand += energyBufferArray[k];
					}
					averageEnergyOfToneBand /= energyBufferArray.Length;
					currentInstantEnergyToCheckFor = instantHighBassBandEnergy;
					averageEnergyOfMusic += averageEnergyOfToneBand;
				}
				
			}
				break;
			case 2:
			{
				instantEnergyOfToneBand= instantMidBandEnergy;
				if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantMidBandEnergy);
					return false;
				}
				else
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantMidBandEnergy);
					
					//get instant values of the previous second
					float[] energyBufferArray = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
					
					//average the instant energy values
					averageEnergyOfToneBand = instantMidBandEnergy;
					for(int k=0; k<energyBufferArray.Length -1; k++)
					{
						averageEnergyOfToneBand += energyBufferArray[k];
					}
					averageEnergyOfToneBand /= energyBufferArray.Length;
					currentInstantEnergyToCheckFor = instantMidBandEnergy;
					averageEnergyOfMusic += averageEnergyOfToneBand;
				}
				
			}
				break;
			case 3:
			{
				instantEnergyOfToneBand= instantHighMidBandEnergy;
				if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighMidBandEnergy);
					return false;
				}
				else
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighMidBandEnergy);
					
					//get instant values of the previous second
					float[] energyBufferArray = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
					
					//average the instant energy values
					averageEnergyOfToneBand = instantHighMidBandEnergy;
					for(int k=0; k<energyBufferArray.Length -1; k++)
					{
						averageEnergyOfToneBand += energyBufferArray[k];
					}
					averageEnergyOfToneBand /= energyBufferArray.Length;
					currentInstantEnergyToCheckFor = instantHighMidBandEnergy;
					averageEnergyOfMusic += averageEnergyOfToneBand;
				}
			}
				break;
				
			case 4:
			{
				instantEnergyOfToneBand= instantTrebleBandEnergy;
				if(m_toneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantTrebleBandEnergy);
					return false;
				}
				else
				{
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_toneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantTrebleBandEnergy);
					
					//get instant values of the previous second
					float[] energyBufferArray = m_toneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
					
					//average the instant energy values
					averageEnergyOfToneBand = instantTrebleBandEnergy;
					for(int k=0; k<energyBufferArray.Length -1; k++)
					{
						averageEnergyOfToneBand += energyBufferArray[k];
					}
					averageEnergyOfToneBand /= energyBufferArray.Length;
					currentInstantEnergyToCheckFor = instantTrebleBandEnergy;
					averageEnergyOfMusic += averageEnergyOfToneBand;
				}
			}
				break;
			}
			m_averageEnergyOfToneBands[i] = averageEnergyOfToneBand;
			averageEnergyOfMusic /= m_toneEnergyHistoryBuffer.Count;
			outAverageEnergy = averageEnergyOfMusic;
			//Check for beats
			if(m_checkFilters[i])
			{
				m_energyVariance= currentInstantEnergyToCheckFor - averageEnergyOfToneBand;
				//m_energyVariance *= 10;
				
				m_beatThresholdModifier = (-0.005f * m_energyVariance) * Mathf.Pow(10,5);
				m_beatThreshold = m_beatThresholdModifier + m_baseBeatThresholds[i];
				//m_beatThreshold = m_baseBeatThresholds[i];
				//compare with currentInstantEnergyValue
				if(currentInstantEnergyToCheckFor > averageEnergyOfToneBand * m_beatThreshold)
				{
					BeatEvent tempEvent = new BeatEvent();
					tempEvent.m_beatTone = (DETECTION_TONE)i;
					tempEvent.m_averageEnergy = averageEnergyOfToneBand;
					tempEvent.m_instantEnergy = instantEnergyOfToneBand;
					tempEvent.m_crossedBeatThreshold = m_beatThreshold;

					outBeatEvents.Add(tempEvent);
					//callBack(toneFoundIn);
					//Debug.Log("THUMP!! via BeatDetectionMode "+m_mode+" at tone band "+(DETECTION_TONE)i);
					beatDetected = true;
					
				}
			}
			
			
		}
		
		if(beatDetected)
		{
			return true;
		}
		return false;
	}



	public float GetInstantEnergy()
	{
		return m_instantEnergy;
	}
	public float GetAverageEnergy()
	{
		return m_averageEnergy;
	}
	public float GetInstantVolume()
	{
		return 20 * Mathf.Log10(m_instantEnergy) + 203;
	}

	public float GetAverageVolume()
	{
		return 20* Mathf.Log10(m_averageEnergy) + 203;
	}


	public float GetAverageEnergyOfSubBand(int subBand)
	{
		Mathf.Clamp(subBand,0,63);
		return m_subBandEnergyHistoryBuffer[subBand].GetAverageEnergyOfSubBand();
	}

	public float[] GetAverageEnergyOfToneBands()
	{
		float[] averageTempArray = new float[5];
		for(int i =0 ;i< 5;i++)
		{
			averageTempArray[i] = 0;
			averageTempArray[i] = m_averageEnergyOfToneBands[i];
		}
		return averageTempArray;
	}
	public float GetAverageEnergyOfToneBand(DETECTION_TONE tone)
	{
		if(m_mode == BEAT_DETECTION_MODES.TONE_BASED)
		{
		return m_averageEnergyOfToneBands[(int)tone];
		}
		Debug.Log ("average volume request invalid, mode not TONE_BASED");
		return 0;
	}

	public float[] GetAverageEnergyOfSubBands(bool runTime)
	{
		float[] averageTempArray = new float[m_subBandCount];
		for(int i =0 ;i< m_subBandCount;i++)
		{
			averageTempArray[i] = 0;
			if(runTime)
			{
				averageTempArray[i] = m_runTimeSubBandEnergyHistoryBuffer[i].GetAverageEnergyOfSubBand();
			}
			else
			{
			averageTempArray[i] = m_subBandEnergyHistoryBuffer[i].GetAverageEnergyOfSubBand();
			}
		}
		return averageTempArray;
	}

	
	public void ProcessSong(out float outInstantEnergy, out float outAverageEnergy,ref double[] preAllocatedArrayToUse)
	{

		outInstantEnergy = 0;
		outAverageEnergy = 0;
	
		//retrieve audio samples
		int numChannels = m_source.clip.channels;
		float[] audioSamples = GetAudioSamples(1024);
		
		//average values if stereo and convertToComplex(NOTE :- iterations 1024)


		//double[] audioData = new double[1024];
		if(numChannels ==2)
		{
			for(int i =0; i< 1024;i++)
			{
				//audioData averaging
				preAllocatedArrayToUse[i] =(double)(audioSamples[i] + audioSamples[i+1])/2;
			}
			
		}
		else if(numChannels == 1)
		{
			//complexConversion
			for(int i =0; i< 1024;i++)
			{
				//audioData averaging
				preAllocatedArrayToUse[i] =(double)audioSamples[i];
			}
		}
		
		//Generate FFT from complexConvert
		LomontFFT FFTHelper2 = new LomontFFT();
		FFTHelper2.RealFFT(ref preAllocatedArrayToUse,true);
		//CustomFFTElement[] FFTResult = new CustomFFTElement[audioData.Length/2];
		int index = 0;
		for(int i = 0; i<preAllocatedArrayToUse.Length/2; i++)
		{
			

			preAllocatedArrayToUse[i] = preAllocatedArrayToUse[index] * HammingWindow(i,1024);
			preAllocatedArrayToUse[i+1] = preAllocatedArrayToUse[index + 1] * HammingWindow(i,1024);

			index += 2;
			
		}


		
		// the generated FFT consists of 512 results as opposed to the 1024 we are supposed to get from the input 
		// owing to the fact that the other 512 are simply mirrors of this one
		
		// compute the modulus of the result and categorize by tone (NOTE:- iterations 512)
		// also compute the instant energy and store it locally
		// sacrificing code flow for optimization
		
		float modulusOfFFTThisIteration = 0;

		float bassEnergiesCount = 0;
		float highBassEnergiesCount = 0;
		float midEnergiesCount =0;
		float highMidEnergiesCount = 0;
		float trebleEnergiesCount = 0;
		
		float instantBassBandEnergy= 0;
		float instantHighBassBandEnergy= 0;
		float instantMidBandEnergy= 0;
		float instantHighMidBandEnergy= 0;
		float instantTrebleBandEnergy= 0;
		
		int subBandCounter = 0;
		float instantEnergyThisSubBand = 0;
		for(int i=0; i<preAllocatedArrayToUse.Length/2; i++)
		{
			modulusOfFFTThisIteration = Mathf.Pow((float)preAllocatedArrayToUse[i],2)+Mathf.Pow((float)preAllocatedArrayToUse[i+1],2);

			DETECTION_TONE tone = GetToneForFrequency((i) * 22050/(preAllocatedArrayToUse.Length/2));
			switch(tone)
			{
			case DETECTION_TONE.BASS:
			{
				bassEnergiesCount++;
				instantBassBandEnergy += modulusOfFFTThisIteration;
			}
				break;
			case DETECTION_TONE.HIGH_BASS:
			{
				highBassEnergiesCount++;
				instantHighBassBandEnergy += modulusOfFFTThisIteration;
			}
				break;
				
			case DETECTION_TONE.MID:
			{
				midEnergiesCount++;
				instantMidBandEnergy += modulusOfFFTThisIteration;
			}
				break;
				
			case DETECTION_TONE.HIGH_MID:
			{
				highMidEnergiesCount++;
				instantHighMidBandEnergy += modulusOfFFTThisIteration;
			}
				break;
				
			case DETECTION_TONE.TREBLE:
			{
				trebleEnergiesCount++;
				instantTrebleBandEnergy += modulusOfFFTThisIteration;
			}
				break;
			}
			
			//also fill in subbandVolumeHistoryBuffer in anticipation of later use
			if((i+1)% (preAllocatedArrayToUse.Length/2)/m_subBandCount == 0)
			{
				instantEnergyThisSubBand/= (preAllocatedArrayToUse.Length/2)/m_subBandCount;
				
				if(m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Count< 43)
				{
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Enqueue(instantEnergyThisSubBand);
				}
				else
				{
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Dequeue();
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_energyBuffer.Enqueue(modulusOfFFTThisIteration);
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_subBandCount = m_subBandCount;
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_tone = tone;
					m_runTimeSubBandEnergyHistoryBuffer[subBandCounter].m_upperFrequency = subBandCounter * (22050/m_subBandCount);
				}
				subBandCounter ++;
			}
			else
			{
				instantEnergyThisSubBand += modulusOfFFTThisIteration;
			}
		}
		
		//calculate average energies in the different bands (Note:- iterations 512)
		instantBassBandEnergy/= bassEnergiesCount;
		instantHighBassBandEnergy/= highBassEnergiesCount;
		instantMidBandEnergy/= midEnergiesCount;
		instantHighMidBandEnergy/= highMidEnergiesCount;
		instantTrebleBandEnergy/= trebleEnergiesCount;
		
		
		float instantEnergyOfMusic = instantBassBandEnergy+ instantHighBassBandEnergy+ instantMidBandEnergy + instantHighMidBandEnergy + instantTrebleBandEnergy;
		instantEnergyOfMusic /= 5;
		outInstantEnergy = instantEnergyOfMusic;
		
		float averageEnergyOfMusic = 0; 
		
		//store in their respective tone buffers and check for beats(NOTE:- iterations 5* (2-250) and this can be made better(MINDEFFED >:( ) )
		float currentInstantEnergyToCheckFor = 0;
		float averageEnergyOfToneBand = 0;
		for(int i = 0; i<m_runTimeToneEnergyHistoryBuffer.Count; i++)
		{
			switch(i)
			{
				case 0:
				{
				if(m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
					{
					m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantBassBandEnergy);
						
					}
					else
					{
					m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
					m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantBassBandEnergy);
						
						
						//get instant values of the previous second
						float[] energyBufferArray = m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();

						//average the instant energy values
						averageEnergyOfToneBand = instantBassBandEnergy;
						for(int k=0; k<energyBufferArray.Length -1; k++)
						{
							averageEnergyOfToneBand += energyBufferArray[k];
						}
						averageEnergyOfToneBand /= energyBufferArray.Length;
						currentInstantEnergyToCheckFor = instantBassBandEnergy;
						averageEnergyOfMusic += averageEnergyOfToneBand;
					}
				}
					break;
				case 1:
				{
					if(m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighBassBandEnergy);
						
					}
					else
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighBassBandEnergy);
						
						//Check for beats
						//get instant values of the previous second
						float[] energyBufferArray = m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
						
						//average the instant energy values
						averageEnergyOfToneBand = instantHighBassBandEnergy;
						for(int k=0; k<energyBufferArray.Length -1; k++)
						{
							averageEnergyOfToneBand += energyBufferArray[k];
						}
						averageEnergyOfToneBand /= energyBufferArray.Length;
						currentInstantEnergyToCheckFor = instantHighBassBandEnergy;
						averageEnergyOfMusic += averageEnergyOfToneBand;
					}
					
				}
					break;
				case 2:
				{
					if(m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantMidBandEnergy);
						
					}
					else
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantMidBandEnergy);
						
						//get instant values of the previous second
						float[] energyBufferArray = m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
						
						//average the instant energy values
						averageEnergyOfToneBand = instantMidBandEnergy;
						for(int k=0; k<energyBufferArray.Length -1; k++)
						{
							averageEnergyOfToneBand += energyBufferArray[k];
						}
						averageEnergyOfToneBand /= energyBufferArray.Length;
						currentInstantEnergyToCheckFor = instantMidBandEnergy;
						averageEnergyOfMusic += averageEnergyOfToneBand;
					}
					
				}
					break;
				case 3:
				{
					if(m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighMidBandEnergy);
						
					}
					else
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantHighMidBandEnergy);
						
						//get instant values of the previous second
						float[] energyBufferArray = m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
						
						//average the instant energy values
						averageEnergyOfToneBand = instantHighMidBandEnergy;
						for(int k=0; k<energyBufferArray.Length -1; k++)
						{
							averageEnergyOfToneBand += energyBufferArray[k];
						}
						averageEnergyOfToneBand /= energyBufferArray.Length;
						currentInstantEnergyToCheckFor = instantHighMidBandEnergy;
						averageEnergyOfMusic += averageEnergyOfToneBand;
					}
				}
					break;
					
				case 4:
				{
					if(m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Count < 42)
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantTrebleBandEnergy);
						
					}
					else
					{
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Dequeue();
						m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.Enqueue(instantTrebleBandEnergy);
						
						//get instant values of the previous second
						float[] energyBufferArray = m_runTimeToneEnergyHistoryBuffer[i].m_energyBuffer.ToArray();
						
						//average the instant energy values
						averageEnergyOfToneBand = instantTrebleBandEnergy;
						for(int k=0; k<energyBufferArray.Length -1; k++)
						{
							averageEnergyOfToneBand += energyBufferArray[k];
						}
						averageEnergyOfToneBand /= energyBufferArray.Length;
						currentInstantEnergyToCheckFor = instantTrebleBandEnergy;
						averageEnergyOfMusic += averageEnergyOfToneBand;
					}
				}
					break;
			}

			// fill helper variables
			m_averageEnergyOfToneBands[i] = averageEnergyOfToneBand;
			averageEnergyOfMusic /= m_runTimeToneEnergyHistoryBuffer.Count;
			outAverageEnergy =  averageEnergyOfMusic;
			
		}
	}
		

//	public bool CheckForBeats(GameplayScript.BeatCallBack callBack, out float outInstantEnergy, out float outAverageEnergy, out DETECTION_TONE outDetectedTone)
//	{
//		bool beatDetected = false;
//		float instantEnergy = 0;
//		float averageEnergy = 0;
//		DETECTION_TONE detectedTone = DETECTION_TONE.NONE;
//		if(m_source == null)
//		{
//			//Debug.Log("Source not set. Please set source befor attempting to check for beats");
//			beatDetected = false;
//		}
//
//		if(!m_source.isPlaying)
//		{
//			//Debug.Log("the source is not playing back any audio. The beat detection will not work");
//			beatDetected = false;
//		}
//
//		switch(m_mode)
//		{
//		case BEAT_DETECTION_MODES.SIMPLE:
//		{
//			beatDetected = SimpleBeatCheck(callBack, out instantEnergy, out averageEnergy);
//		}
//			break;
//		
//		case BEAT_DETECTION_MODES.FREQUENCY_BASED:
//		{
//			beatDetected = FrequencyBasedBeatCheck(callBack, out instantEnergy, out averageEnergy);
//		}
//			break;
//
//		case BEAT_DETECTION_MODES.TONE_BASED:
//		{
//			beatDetected = ToneBasedBeatCheck(callBack, out instantEnergy, out averageEnergy, out detectedTone);
//		}
//			break;
//		
//		}
//		outInstantEnergy = instantEnergy;
//		outAverageEnergy = averageEnergy;
//		m_instantEnergy = instantEnergy;
//		m_averageEnergy = averageEnergy;
//		outDetectedTone = detectedTone;
//
//		if(beatDetected)
//		{
//			return true;
//		}
//		return false;
//	}

	public void CustomPostProcessBeats(ref List<BeatTimeLineEvent> beatTimeLine)
	{
		beatTimeLine.RemoveAll((x) => x.m_timeStamp < 3);
		//return beatTimeLine;
		List<BeatTimeLineEvent> toRemoveList = new List<BeatTimeLineEvent>();
		
		//split the beatTimeLine based on bands
		List<BeatTimeLineEvent> bassBeatTimeLine = beatTimeLine.FindAll(x=> x.m_beatEvent.m_beatTone == DETECTION_TONE.BASS);
		List<BeatTimeLineEvent> highBassBeatTimeLine = beatTimeLine.FindAll(x=> x.m_beatEvent.m_beatTone == DETECTION_TONE.HIGH_BASS);
		List<BeatTimeLineEvent> midBeatTimeLine = beatTimeLine.FindAll(x=> x.m_beatEvent.m_beatTone == DETECTION_TONE.MID);
		List<BeatTimeLineEvent> highMidBeatTimeLine = beatTimeLine.FindAll(x=> x.m_beatEvent.m_beatTone == DETECTION_TONE.HIGH_MID);
		List<BeatTimeLineEvent> trebleBeatTimeLine = beatTimeLine.FindAll(x=> x.m_beatEvent.m_beatTone == DETECTION_TONE.TREBLE);
		

//		Debug.Log("Bass beats PREProcess "+ bassBeatTimeLine.Count);
//		
//		Debug.Log("High bass beats PREProcess "+ highBassBeatTimeLine.Count);
//		
//		Debug.Log("Mid beats PREProcess "+ midBeatTimeLine.Count);
//		
//		Debug.Log("High mid beats PREProcess "+ highMidBeatTimeLine.Count);
//		
//		Debug.Log("Treble beats PREProcess "+ trebleBeatTimeLine.Count);
		
		
		//declare an int to store the index of the prevConsidered beat index
		int prevCheckedBeatIndex = 0;
		
		//process bass beats
		for(int i = 1; i<bassBeatTimeLine.Count; i++)
		{
			
			if(bassBeatTimeLine[i].m_timeStamp - bassBeatTimeLine[prevCheckedBeatIndex].m_timeStamp < m_postProcessSpawnTime)
			{
				toRemoveList.Add(bassBeatTimeLine[i]);
			}
			else
			{
				prevCheckedBeatIndex = i;
			}
			
		}
		
		prevCheckedBeatIndex = 0;
		//process high bass beats
		for(int i = 1; i<highBassBeatTimeLine.Count; i++)
		{
			
			if(highBassBeatTimeLine[i].m_timeStamp - highBassBeatTimeLine[prevCheckedBeatIndex].m_timeStamp < m_postProcessSpawnTime)
			{
				toRemoveList.Add(highBassBeatTimeLine[i]);
			}
			else
			{
				prevCheckedBeatIndex = i;
			}
			
		}
		
		prevCheckedBeatIndex = 0;
		// process mid beats
		for(int i = 1; i<midBeatTimeLine.Count; i++)
		{
			
			if(midBeatTimeLine[i].m_timeStamp - midBeatTimeLine[prevCheckedBeatIndex].m_timeStamp < m_postProcessSpawnTime)
			{
				toRemoveList.Add(midBeatTimeLine[i]);
			}
			else
			{
				prevCheckedBeatIndex = i;
			}
			
		}
		
		prevCheckedBeatIndex = 0;
		//process high mid beats
		for(int i = 1; i<highMidBeatTimeLine.Count; i++)
		{
			
			if(highMidBeatTimeLine[i].m_timeStamp - highMidBeatTimeLine[prevCheckedBeatIndex].m_timeStamp < m_postProcessSpawnTime)
			{
				toRemoveList.Add(highMidBeatTimeLine[i]);
			}
			else
			{
				prevCheckedBeatIndex = i;
			}
			
		}
		
		prevCheckedBeatIndex = 0;
		//process treble beats
		for(int i = 1; i<trebleBeatTimeLine.Count; i++)
		{
			
			if(trebleBeatTimeLine[i].m_timeStamp - trebleBeatTimeLine[prevCheckedBeatIndex].m_timeStamp < m_postProcessSpawnTime)
			{
				toRemoveList.Add(trebleBeatTimeLine[i]);
			}
			else
			{
				prevCheckedBeatIndex = i;
			}
			
		}
		
		foreach(BeatTimeLineEvent ev in toRemoveList)
		{
			switch(ev.m_beatEvent.m_beatTone)
			{
			case DETECTION_TONE.BASS:
			{
				bassBeatTimeLine.Remove(ev);
			}
				break;
			case DETECTION_TONE.HIGH_BASS:
			{
				highBassBeatTimeLine.Remove(ev);
			}
				break;
			case DETECTION_TONE.MID:
			{
				midBeatTimeLine.Remove(ev);
			}
				break;
			case DETECTION_TONE.HIGH_MID:
			{
				highMidBeatTimeLine.Remove(ev);
			}
				break;
			case DETECTION_TONE.TREBLE:
			{
				trebleBeatTimeLine.Remove(ev);
			}
				break;
			}
			
		}
		
		List<BeatTimeLineEvent> processedBeatTimeLine = new List<BeatTimeLineEvent>();
		processedBeatTimeLine.AddRange(bassBeatTimeLine);
		//.Log("Bass beats POSTProcess "+ bassBeatTimeLine.Count);
		processedBeatTimeLine.AddRange(highBassBeatTimeLine);
		//Debug.Log("High bass beats POSTProcess "+ highBassBeatTimeLine.Count);
		processedBeatTimeLine.AddRange(midBeatTimeLine);
		//Debug.Log("Mid beats POSTProcess "+ midBeatTimeLine.Count);
		processedBeatTimeLine.AddRange(highMidBeatTimeLine);
		//Debug.Log("High mid beats POSTProcess "+ highMidBeatTimeLine.Count);
		processedBeatTimeLine.AddRange(trebleBeatTimeLine);
		//Debug.Log("Treble beats POSTProcess "+ trebleBeatTimeLine.Count);
		
		processedBeatTimeLine.Sort((x,y) => x.m_timeStamp.CompareTo(y.m_timeStamp));
		
		
		toRemoveList.Clear();

		
		//prioritize stragglers amidst the spawnTime window
		for(int i =0 ;i < processedBeatTimeLine.Count;i ++)
		{
			BeatTimeLineEvent beatToConsider = processedBeatTimeLine[i];
			List<BeatTimeLineEvent> beatsInBetween = processedBeatTimeLine.FindAll((x)=> (x.m_timeStamp >= beatToConsider.m_timeStamp) && (x.m_timeStamp < beatToConsider.m_timeStamp + 0.5f));
			beatsInBetween.Add(beatToConsider);
			if(beatsInBetween.Count > 1)
			{
				DETECTION_TONE toneOfHighestPriority = DETECTION_TONE.TREBLE;
				
				// find tone of highestPriority
				for(int j = 0; j< beatsInBetween.Count ; j++)
				{
					if((int)beatsInBetween[j].m_beatEvent.m_beatTone < (int)toneOfHighestPriority)
					{
						toneOfHighestPriority = beatsInBetween[j].m_beatEvent.m_beatTone;
					}
				}
				//add other tones to the toRemoveList
				foreach(BeatTimeLineEvent ev in beatsInBetween)
				{
					if(ev.m_beatEvent.m_beatTone != toneOfHighestPriority)
					{
						processedBeatTimeLine.Remove(ev);
					}
				}
			}
			
		}

		//Debug.Log("postProcessedBeatCount "+ processedBeatTimeLine.Count );
		beatTimeLine = processedBeatTimeLine;
		//return processedBeatTimeLine;
	}
}
