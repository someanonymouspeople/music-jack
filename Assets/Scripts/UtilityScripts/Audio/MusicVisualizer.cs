﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicVisualizer {

	float m_maxBands;
	float m_minBands;
	float m_currentBands;
	int m_subBandCount = 0;
	float m_decibelToUnitScale;
	GameObject[,] m_visualAidArray;
	GameObject m_visualizer;

	public static MusicVisualizer m_instance;

	public static MusicVisualizer GetInstance()
	{
		if(m_instance != null)
		{
			m_instance.Init();
			return m_instance;
		}

		m_instance = new MusicVisualizer();
		m_instance.Init();
		return m_instance;

	}

	public void Init()
	{ 
		if(m_maxBands == 0)
		{
			m_maxBands = 15;
		}
		if(m_minBands == 0)
		{
			m_minBands = 5;
		}
		if(m_visualAidArray == null)
		{
			switch(MusicAnalyzerV2.GetInstance().m_mode)
			{
				case BEAT_DETECTION_MODES.FREQUENCY_BASED:
				{
					m_visualizer = new GameObject();
					m_visualizer.name = "Visualizer";
					Vector2 cameraSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
					m_decibelToUnitScale = 30;
					m_subBandCount = MusicAnalyzerV2.GetInstance().m_subBandCount;
				m_visualAidArray = new GameObject[m_subBandCount,(int)m_maxBands];
					for(int i = 0; i<m_subBandCount; i++)
					{
						
						for(int j =0; j<m_maxBands; j++)
						{
							m_visualAidArray[i,j] = GameObject.Instantiate(Resources.Load("UI/VisualizerVisualAid")) as GameObject;
							m_visualAidArray[i,j].name = "FreqBand"+i* (22050/m_subBandCount);
							//m_visualAidArray[i,j].transform.localScale = new Vector3(0.5f,0.1f,0.1f);
							m_visualAidArray[i,j].transform.localPosition = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f * i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),0);
							m_visualAidArray[i,j].transform.parent = m_visualizer.transform;
							m_visualAidArray[i,j].renderer.material.color = Color.Lerp(Color.blue,Color.red,(float)i/(float)m_subBandCount);
							
							m_visualAidArray[i,j].renderer.sortingLayerName = "GamePlay";
							m_visualAidArray[i,j].renderer.sortingOrder = 0;
							
						}
					}
					m_visualizer.transform.parent = Camera.main.transform;
				}
				break;
			case BEAT_DETECTION_MODES.TONE_BASED:
				{
					m_visualizer = new GameObject();
					m_visualizer.name = "Visualizer";
					Vector2 cameraSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width,Screen.height));
					m_decibelToUnitScale = 30;
					m_subBandCount = 5;
				m_visualAidArray = new GameObject[m_subBandCount,(int)m_maxBands];
					for(int i = 0; i<m_subBandCount; i++)
					{
						Color colorToSet = Color.Lerp(Color.white,Color.black,(float)i/(float)m_subBandCount);
						for(int j =0; j<m_maxBands; j++)
						{
							m_visualAidArray[i,j] = GameObject.Instantiate(Resources.Load("UI/VisualizerVisualAid")) as GameObject;
							m_visualAidArray[i,j].name = ""+(DETECTION_TONE)i +" "+ j;
							//m_visualAidArray[i,j].transform.localScale = new Vector3(0.5f,0.1f,0.1f);
							m_visualAidArray[i,j].transform.localPosition = new Vector3((m_visualAidArray[i,j].renderer.bounds.size.x *1.25f* i) - (m_subBandCount/2 * m_visualAidArray[i,j].renderer.bounds.size.x),cameraSize.y/2 + (j*m_visualAidArray[i,j].renderer.bounds.size.y *2),0);
							m_visualAidArray[i,j].transform.parent = m_visualizer.transform;
							m_visualAidArray[i,j].renderer.material.color = colorToSet;
							
							m_visualAidArray[i,j].renderer.sortingLayerName = "GamePlay";
							m_visualAidArray[i,j].renderer.sortingOrder = 0;

							
						}
					}
					m_visualizer.transform.parent = Camera.main.transform;
				}
				break;
			default:
			{
				Debug.Log("Unable to visualize current mode of beat detection");
			}
				break;
			}
		
		}


	}

	public GameObject GetVisualizer()
	{
		if(m_visualizer != null)
		{
			return m_visualizer;
		}
		return null;
	}

	public void Clear()
	{ 
		if(m_visualAidArray != null)
		{
			List<GameObject> toRemoveList = new List<GameObject>();
			foreach(GameObject go in m_visualAidArray)
			{
				if(go != null)
				{
				toRemoveList.Add(go);
				}
			}

			for(int i =0; i<toRemoveList.Count; i++)
			{
				Object.Destroy(toRemoveList[i].gameObject);
			}
			m_visualAidArray = null;
		}
	}

	public void FeedData()
	{
		float maxVolume = 100;
		switch(MusicAnalyzerV2.GetInstance().m_mode)
		{
		case BEAT_DETECTION_MODES.FREQUENCY_BASED:
		{

		}
			break;
		case BEAT_DETECTION_MODES.TONE_BASED:
		{
			m_currentBands = m_minBands + (GameplayScript.GetInstance().GetHeroScript().GetRelativeSpeed()*(m_maxBands-m_minBands));
			float countToUnlock = 0;
			float[] volumeArray = MusicAnalyzerV2.GetInstance().GetInstantVolumeOfToneBands(0.008f);
			float[] averageArray = MusicAnalyzerV2.GetInstance().GetAverageEnergyOfToneBands();
			for(int i = 0; i< m_subBandCount; i++)
			{
				countToUnlock = (volumeArray[i]/maxVolume)*m_currentBands;
				for(int j = 0; j<m_maxBands; j++)
				{
					if(j <countToUnlock)
					{
						m_visualAidArray[i,j].renderer.enabled = true;
					}
					else
					{
						m_visualAidArray[i,j].renderer.enabled = false;
					}
				}
			}
		}
			break;
		default:
		{
			Debug.Log("Unable to visualize current mode of beat detection");
		}
			break;
		}


	}

}
