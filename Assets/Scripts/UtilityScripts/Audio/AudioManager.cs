﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AUDIO_TAG
{
	TAG_DRUM_HIT,
	TAG_DRUM_MISS,
	TAG_DRUM_INGAME,
	TAG_HERO_HIT,
	TAG_HERO_STUMBLE,
	TAG_OBSTACLE_HIT,
	TAG_HERO_DRUM_HIT,
	TAG_HERO_DRUM_MISS
};

public class AudioManager : SoundLoader 
{

	public static AudioManager m_instance;
	List<AudioSource> m_playingSourceReferences;

	public List<AudioClip> m_sfxList;

	public static AudioManager GetInstance()
	{
		return m_instance;
	}

	void Awake()
	{

		if(AudioManager.GetInstance()!= null)
		{
			Destroy (this.gameObject);
			return;
		}
		m_playingSourceReferences = new List<AudioSource>();
		m_instance = this;
	}

	public AudioClip GetSFX(string clipName)
	{
		if(m_sfxList!= null)
		{
			if(m_sfxList.Count>0)
			{
				return m_sfxList.Find((x)=> x.name == clipName);
			}
			Debug.Log("sfx clip not found");
			return null;
		}
		Debug.Log("SFX List not initialized");
		return null;
	}
	public void PauseAllAudio()
	{
		CheckAndReleaseUnusedReferences();
		if(m_playingSourceReferences.Count>0)
		{
			for(int i =0 ; i<m_playingSourceReferences.Count; i++)
			{
				if(m_playingSourceReferences[i].isPlaying)
				{
					m_playingSourceReferences[i].Pause();

				}
			}
		}
	}

	public void StopAllAudio()
	{
		PauseAllAudio();
		CheckAndReleaseUnusedReferences();

	}

	public void ResumeAllAudio()
	{

		if(m_playingSourceReferences.Count >0)
		{
		for(int i = 0; i< m_playingSourceReferences.Count; i++)
		{
			if(!m_playingSourceReferences[i].isPlaying)
			{
				m_playingSourceReferences[i].Play();
			}
		}
			CheckAndReleaseUnusedReferences();
		}
	}

	public void ReleaseReferences()
	{
		m_playingSourceReferences.RemoveAll(item => item == null);
		
		if(m_playingSourceReferences.Count>0)
		{
			for(int i =0; i <m_playingSourceReferences.Count; i++)
			{
				Debug.Log(m_playingSourceReferences[i]);
				m_playingSourceReferences[i] = null;
				m_playingSourceReferences.RemoveAt(i);
			}
		}
	}

	public void CheckAndReleaseUnusedReferences()
	{
		m_playingSourceReferences.RemoveAll(item => item == null);


		for(int i =0; i <m_playingSourceReferences.Count; i++)
		{

				if(!m_playingSourceReferences[i].isPlaying)
				{
					Debug.Log(m_playingSourceReferences[i]);
					m_playingSourceReferences[i].clip = null;
					m_playingSourceReferences[i] = null;
					m_playingSourceReferences.RemoveAt(i);
				}

		}

	}

	public AudioClip GetClipPlayingAt(AudioSource source)
	{
		if(m_playingSourceReferences.Count>0 && source.isPlaying)
		{
			for(int i=0;i<m_playingSourceReferences.Count;i++)
			{
				if(m_playingSourceReferences[i]==source)
				{
					Debug.Log("Returning clip "+source.clip.name+" playing at "+source.name);
					return source.clip;
				}
			}
		}
		Debug.Log("AudioSource not playing any clip");
		return null;
	}


	public void SetBackGroundVolume(float volume)
	{
		volume = Mathf.Clamp(volume,0,100);
		AppManager.GetInstance().SetBackGroundVolume(volume);
		Camera.main.GetComponent<AudioSource>().volume = AppManager.GetInstance().GetBackGroundMusicVolume()/100.0f;
	}

	public void SetAudioEffectVolume(float volume)
	{	
		volume = Mathf.Clamp(volume,0,100);
		AppManager.GetInstance().SetAudioEffectVolume(volume);
		Camera.main.transform.FindChild("SoundEffectPlayer").GetComponent<AudioSource>().volume = AppManager.GetInstance().GetAudioEffectVolume()/100.0f;
	}

	public float GetBackGroundVolume()
	{
		return AppManager.GetInstance().GetBackGroundMusicVolume();

	}
	
	public float GetAudioEffectVolume()
	{
		return AppManager.GetInstance().GetAudioEffectVolume();
	}

	void Start()
	{
		base.Start();
		string stringToLoadFrom = GameConfigurationHolderScript.GetInstance().GetSongDataPath();
//#if UNITY_ANDROID
//		LoadAllAudioFileData(stringToLoadFrom"../sdcard/Music/");
//		//LoadAllAudioFileData("C:/AudioFilesToLoad/");
//#elif UNITY_IPHONE
//		LoadAllAudioFileData("../var/root/Media/iTunes_Control/Music/");
//#elif UNITY_STANDALONE
//		LoadAllAudioFileData("C:/AudioFilesToLoad/");
		LoadAllAudioFileData(stringToLoadFrom+"/");
//#endif
	}
	public bool ContainsData()
	{
		if(m_soundFiles.Count>0)
		{
			return true;
		}
		return false;
	}
	public void PlayAtSource(AudioSource source,AudioClip clip,float volume)
	{
		if(clip)
		{
			//Debug.Log("clip exists");
			//Debug.Log(clip.name);
		}
		else
		{
			Debug.Log("no clip");
			return;
		}
		if(!clip.isReadyToPlay)
		{
			Debug.Log("Music not ready to play");
		}

		Debug.Log("Music ready to play");

		for(int i = 0; i<m_playingSourceReferences.Count;i++)
		{
			if(m_playingSourceReferences[i] == source)
			{
				if(source.isPlaying)
				{
					source.Stop();
					source.volume = volume/100.0f;
					source.clip = clip;
					source.Play();
					Debug.Log("Ref exists music playing "+clip.name);
					return;
				}
			}
		}

		
		source.volume = volume/100.0f;
		source.clip = clip;
		source.Play();
		Debug.Log("Music playing "+clip.name);

		m_playingSourceReferences.Add(source);
		
	}
	
	public void UpdateAudioManager()
	{

	}

}
