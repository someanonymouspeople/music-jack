﻿using UnityEngine;
using System.Collections;

public class CustomComplexNum
{
	public CustomComplexNum()
	{
		m_realPart = 0;
		m_imagPart = 0;
	}

	public CustomComplexNum(float real,float imag,float time)
	{
		m_realPart = real;
		m_imagPart = imag;
		m_timeStamp = time;
	}

	public float m_realPart;
	public float m_imagPart;
	public float m_timeStamp;
}


public class DFTHelper 
{
	public static DFTHelper m_instance;


	protected DFTHelper()
	{

	}

	public static DFTHelper GetInstance()
	{
		if(m_instance != null)
		{
			return m_instance;
		}
		m_instance = new DFTHelper();
		return m_instance;
	}

//	public CustomComplexNum[] GetFFT (float [] inputTimeDomainSignal, int startOffset, int sampleSizeN)
//	{
//		if(inputTimeDomainSignal.Length == 1)
//		{
//			CCLog("only one element sent, not enough elements to calc ");
//			return null;
//		}
//
//		if(inputTimeDomainSignal.Length %2 != 0)
//		{
//			CCLog("FFT requires input signal lengths that are in the power of 2, exception detected");
//			return null;
//		}
//
//		// split the input signals based on odd and even indices
//		float[] evenIndices = new float[inputTimeDomainSignal.Length/2];
//		float[] oddIndices = new float[inputTimeDomainSignal.Length/2];
//		int evenCounter = 0;
//		int oddCounter = 0;
//		for(int i = 0; i<inputTimeDomainSignal.Length-1; i++)
//		{
//			if(i%2 == 0)
//			{
//				evenIndices[evenCounter] = inputTimeDomainSignal[i];
//				evenCounter++;
//			}
//			else
//			{
//				oddIndices[oddCounter] = inputTimeDomainSignal[i];
//				oddCounter++;
//			}
//		}
//
//	}

//	public static Complex[] fft(Complex[] x) {
//		int N = x.length;
//		
//		// base case
//		if (N == 1) return new Complex[] { x[0] };
//		
//		// radix 2 Cooley-Tukey FFT
//		if (N % 2 != 0) { throw new RuntimeException("N is not a power of 2"); }
//		
//		// fft of even terms
//		Complex[] even = new Complex[N/2];
//		for (int k = 0; k < N/2; k++) {
//			even[k] = x[2*k];
//		}
//		Complex[] q = fft(even);
//		
//		// fft of odd terms
//		Complex[] odd  = even;  // reuse the array
//		for (int k = 0; k < N/2; k++) {
//			odd[k] = x[2*k + 1];
//		}
//		Complex[] r = fft(odd);
//		
//		// combine
//		Complex[] y = new Complex[N];
//		for (int k = 0; k < N/2; k++) {
//			double kth = -2 * k * Math.PI / N;
//			Complex wk = new Complex(Math.cos(kth), Math.sin(kth));
//			y[k]       = q[k].plus(wk.times(r[k]));
//			y[k + N/2] = q[k].minus(wk.times(r[k]));
//		}
//		return y;
//	}


	public CustomComplexNum[] GetDFT(float[] inputTimeDomainSignal,int startOffset, int sampleSize)
	{
		float angle = 0;
		CustomComplexNum[] DFTFragArray = new CustomComplexNum[sampleSize];
		
		for(long bin = 0; bin < sampleSize ; bin ++)
		{
			DFTFragArray[bin] = new CustomComplexNum();
			float songLength = inputTimeDomainSignal.Length/44100;
			DFTFragArray[bin].m_timeStamp = ((startOffset + bin)/inputTimeDomainSignal.Length) * songLength;

			//find, multiply and add all the possible partial sinusoids that can be present
			//within the input signal at the 'bin' index

			//As we know there can be as many amplitudes or partial sinusoids in one input signal 
			//as there are samples in the window we are calculating with
			
			for(long frequency = 0; frequency < sampleSize; frequency++)
			{
				angle = -2* Mathf.PI *(float)bin * (float)frequency/(float)sampleSize;
				
				DFTFragArray[bin].m_realPart += (float)(inputTimeDomainSignal[startOffset+frequency] * Mathf.Cos(angle));
				DFTFragArray[bin].m_imagPart += (float)(inputTimeDomainSignal[startOffset + frequency] * Mathf.Sin(angle));
				 
			}
			
		}

		return DFTFragArray;
	}
	
	public float[] GetFrequencyFromDFT(CustomComplexNum[] inputDFT,int sampleRate)
	{
		float[] frequencyToReturn = new float[inputDFT.Length];
		for(long bin = 0; bin < inputDFT.Length; bin++)
		{
			frequencyToReturn[bin] = bin * sampleRate/(inputDFT.Length);
		}
		return frequencyToReturn;
	}
	
	public float[] GetMagnitudeFromDFT(CustomComplexNum[] inputDFT)
	{
		float[] magToReturn = new float[inputDFT.Length];
		for(long bin = 0; bin < inputDFT.Length; bin++)
		{
			magToReturn[bin] =  Mathf.Sqrt(inputDFT[bin].m_realPart * inputDFT[bin].m_realPart + inputDFT[bin].m_imagPart * inputDFT[bin].m_imagPart);
		
		}
		return magToReturn;
	}
	public float[] GetPhaseFromDFT(CustomComplexNum[] inputDFT)
	{
		float[] phaseToReturn = new float[inputDFT.Length];
		for(long bin = 0; bin < inputDFT.Length; bin++)
		{
			phaseToReturn[bin] = 180 * Mathf.Atan2(inputDFT[bin].m_realPart,inputDFT[bin].m_imagPart)/Mathf.PI - 90;
		}
		return phaseToReturn;
	}

}
