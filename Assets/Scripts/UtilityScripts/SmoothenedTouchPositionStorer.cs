﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class SmoothenedTouchPositionStorer : MonoBehaviour 
{

	public GameObject m_cursor;
	public Vector3 m_lastSpawnedParticlePos;
	public bool m_trackTouch;
	public List<Vector3> m_smoothenedTouchList;
	static SmoothenedTouchPositionStorer m_instance;
	public bool m_shouldDraw;
	public float m_particleLife;
	public float m_distBetweenParticles;
	public int m_elementCount;
	public bool m_optimizeElements;
	public bool m_lockToRight;
	public float m_minDistanceBetweenStore;
	public float m_minDistanceBetweenDraw;
	Vector2 m_prevDrawnParticlePos;
	public float m_yAccuracy;
	public float m_xAccuracy;
	bool m_hasSpawnedFirstParticle;
	List<Vector3> m_smoothedVecsThisCall;
	public float m_distBetweenPlayerAndEndOfScreen;
	void Awake()
	{
	
		m_prevDrawnParticlePos= new Vector2(-999,0);
		m_smoothedVecsThisCall = new List<Vector3>();
		m_hasSpawnedFirstParticle = false;
		m_cursor = GameObject.Instantiate(Resources.Load("GameObjects/Utilities/TouchCursor") )as GameObject;
		m_smoothenedTouchList = new List<Vector3>();
		m_instance = this;
		m_lastSpawnedParticlePos.x = -99;
	}

	public float GetMinDistanceBetweenStore()
	{
		return m_minDistanceBetweenStore;
	}
	public static SmoothenedTouchPositionStorer GetInstance()
	{
		return m_instance;
	}
	
	public bool IsLeftMBHeldDown()
	{
		return Input.GetMouseButton(0);
	}
	
	public bool IsRightMBHeldDown()
	{
		return Input.GetMouseButton(1);
	}
	
	public bool IsMiddleMBHeldDown()
	{
		return Input.GetMouseButton(2);
	}
	
	
	void Start()
	{
		
	}
	
//	public Vector3 GetFirstPosition()
//	{
//		Vector3 pos = new Vector3(0,999);
//		if(m_smoothenedTouchList.Count >0)
//		{
//			pos = m_smoothenedTouchList[0];
//		}
//		return pos;
//		
//	}
//	
//	public Vector3 GetSecondPosition()
//	{
//		Vector3 pos = Vector3.zero;
//		if(m_smoothenedTouchList.Count > 1)
//		{
//			pos = m_smoothenedTouchList[1];
//		}
//		else
//		{
//			pos = GetFirstPosition();
//		}
//		return pos;
//	}
	
	public List<Vector3> GetModifiableSmoothenedPositionList()
	{
		return m_smoothenedTouchList;
	}
	
	public Vector3[] GetSmoothenedPositionListAsArray()
	{
		Vector3[] tempArray = new Vector3[m_smoothenedTouchList.Count];
		m_smoothenedTouchList.CopyTo(tempArray);
		return tempArray;
	}
	
	public void ClearTouchQueue()
	{
		m_smoothenedTouchList.Clear();
	}

//	
//	public void SetProcessedList(List<Vector3> vectorList)
//	{
//		m_smoothenedTouchList.Clear();
//		m_smoothenedTouchList = vectorList;
//	}

	private Vector2 GetPotentialCursorPos(Vector2 mousePos)
	{
		Vector2 posToSetCursor = new Vector2(Mathf.Lerp(m_cursor.transform.position.x,mousePos.x,m_xAccuracy/100),Mathf.Lerp(m_cursor.transform.position.y,mousePos.y,m_yAccuracy/100));
		float orthographicSize = Camera.main.orthographicSize;
		//clamp cursor
		if(posToSetCursor.y < (-orthographicSize) + 1.35f)
		{
			posToSetCursor.y = (-orthographicSize) + 1.35f;
		}
		if(posToSetCursor.y > (orthographicSize) - 1.3f)
		{
			posToSetCursor.y = (orthographicSize - 1.3f);
		}

		return posToSetCursor;
	}

	private void SmoothenedTouchPosStorageLogic(bool isLockedToRight,Vector2 mousePos)
	{
		//List<Vector3> m_smoothedVecsThisCall = new List<Vector3>();
		Vector3 touchPos = m_cursor.transform.position;
//		if(!isLockedToRight)
//		{
//			if(m_hasSpawnedFirstParticle)
//			{
//				m_smoothenedTouchList.Add(touchPos);
//				if(m_lastSpawnedParticlePos.x != 0)
//				{
//					m_distBetweenParticles = (m_lastSpawnedParticlePos - touchPos).magnitude;
//					m_lastSpawnedParticlePos = touchPos;
//				}
//				else
//				{
//					m_lastSpawnedParticlePos = touchPos;
//				}
//				
//				//check if smoothing is required
//				if(m_smoothenedTouchList.Count>1)
//				{
//					//if more that one element in the list smoothen and add positions between the last two points in the list
//					m_smoothedVecsThisCall = TouchPositionProcessor.GetInstance().SmoothenBetweenAndAdd(m_smoothenedTouchList[m_smoothenedTouchList.Count-1],m_smoothenedTouchList[m_smoothenedTouchList.Count-2]);
//				}
//			}
//			else
//			{
//				if(mousePos.x > m_lastSpawnedParticlePos.x)
//				{
//					m_cursor.transform.position = mousePos;
//					
//					m_hasSpawnedFirstParticle = true;
//					m_smoothenedTouchList.Add(mousePos);
//				}
//			}
//			
//		}
//		else
//		{
			if(m_hasSpawnedFirstParticle)
			{
				if(touchPos.x > m_lastSpawnedParticlePos.x + m_minDistanceBetweenStore )
				{
					m_smoothenedTouchList.Add(touchPos);
					if(m_lastSpawnedParticlePos.x != 0)
					{
						m_distBetweenParticles = (m_lastSpawnedParticlePos - touchPos).magnitude;
						m_lastSpawnedParticlePos = touchPos;
					}
					else
					{
						m_lastSpawnedParticlePos = touchPos;
					}
					
					//check if smoothing is required
					if(m_smoothenedTouchList.Count>1)
					{
						m_smoothedVecsThisCall = TouchPositionProcessor.GetInstance().SmoothenBetweenAndAdd(m_smoothenedTouchList[m_smoothenedTouchList.Count-1],m_smoothenedTouchList[m_smoothenedTouchList.Count-2]);
					}
					
				}
			}
			
			else
			{
				if(mousePos.x > m_lastSpawnedParticlePos.x)
				{
					m_cursor.transform.position = mousePos;
					
					m_hasSpawnedFirstParticle = true;
					m_smoothenedTouchList.Add(mousePos);
				}
			}


	}
	public Vector3 GetPositionClosestToX(float x, float epsilon)
	{
		var tempList = from element in m_smoothenedTouchList where (element.x > x-epsilon && element.x < x+epsilon) select element;
		foreach(Vector3 pos in tempList)
		{
			if(JunkieScript.AreApproximateBy(x,pos.x,epsilon))
			{
				return pos;
			}
		}
		return new Vector3();
	}
	

//	private void TouchPosDrawLogic()
//	{
//		if(m_shouldDraw)
//		{
//			List<Vector3> vecsToDraw = new List<Vector3>();
//
//			if(m_smoothenedTouchList.Count > 0)
//			{
//				vecsToDraw.Add(m_smoothenedTouchList[m_smoothenedTouchList.Count-1]);
//			}
//			if(m_smoothedVecsThisCall.Count >0)
//			{
//				vecsToDraw.AddRange(m_smoothedVecsThisCall);
//			}
//
//			vecsToDraw.Sort((a,b) => a.x.CompareTo(b.x));
//
//			foreach(Vector3 pos in vecsToDraw)
//			{
//				if(Vector3.Distance(pos,m_prevDrawnParticlePos) > m_minDistanceBetweenDraw)
//				{
//					//TouchPositionProcessor.GetInstance().DrawParticleAt(pos,m_particleLife);
//					m_prevDrawnParticlePos = pos;
//				}
//			}
//
//			//if vectors to draw exist
//			if(m_smoothenedTouchList.Count >0)
//			{
//				Vector2 lastStoredPos = m_smoothenedTouchList[m_smoothenedTouchList.Count-1];
//
//				if(lastStoredPos.x > m_prevDrawnParticlePos.x + m_minDistanceBetweenDraw)
//				{
//					TouchPositionProcessor.GetInstance().DrawParticleAt(lastStoredPos,m_particleLife);
//					m_prevDrawnParticlePos = lastStoredPos;
//				}
//
//				//TouchPositionProcessor.GetInstance().DrawParticleAt(m_smoothenedTouchList[m_smoothenedTouchList.Count-1],m_particleLife);
//				
//				//if smoothed
//
//				if(m_smoothedVecsThisCall.Count>0)
//				{
//					m_smoothedVecsThisCall.Sort((a,b) => a.x.CompareTo(b.x));
//					Vector2 lastCheckedPos = m_smoothedVecsThisCall[0];
//					foreach(Vector3 pos in m_smoothedVecsThisCall)
//					{
//						//if(pos.x > lastCheckedPos.x + m_minDistanceBetweenDraw)
//						//{
//							TouchPositionProcessor.GetInstance().DrawParticleAt(pos,m_particleLife);
//							lastCheckedPos = pos;
//						//}
//					}
//				}
//			}
//		}
//	}

	public void SetMinDistanceBetweenDraw(float dist)
	{
		m_minDistanceBetweenDraw = dist;
	}
	
	void Update () 
	{

		m_elementCount = m_smoothenedTouchList.Count;
		
		if(m_trackTouch)
		{
			//if(m_optimizeElements)
			//{
				TouchPositionProcessor.GetInstance().OptimizeElements();
				TouchPositionProcessor.GetInstance().SortElementsOnX();
			//}
			if(IsLeftMBHeldDown())
			{
				//set initial values for variables
				Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

				//only consider lerping when the mouse pointer is ahead of the last spawned position
				if(mousePos.x > m_lastSpawnedParticlePos.x + m_minDistanceBetweenStore)
				{
					//Lerps and clamps cursor
					Vector2 posToSetCursor = GetPotentialCursorPos(mousePos);
					m_cursor.transform.position = posToSetCursor;

					//Stores the potential position(if any) in private list m_smoothenedTouchList
					SmoothenedTouchPosStorageLogic(m_lockToRight,mousePos);

					//Draws the elements contained in the private list m_smoothenedTouchList
					//TouchPosDrawLogic();

				}

			}
			else
			{
				//reset private values required for above logic
				m_hasSpawnedFirstParticle = false;
			}
			
		}
	}
}
