﻿using UnityEngine;
using System.Collections;

public class GameConfigurationHolderScript : MonoBehaviour {

	public string m_songDataPath;
	public bool m_guestMode;
	public bool m_firstTime;
	public bool m_shouldPlayInstructions; 
	public static GameConfigurationHolderScript m_instance;
	public static GameConfigurationHolderScript GetInstance()
	{
		return m_instance;
	}

	public bool GetShouldPlayInstructions()
	{
		return m_shouldPlayInstructions;
	}
	public void SetShouldPlayInstructionsBool(bool shouldPlayInstructions)
	{
		m_shouldPlayInstructions = shouldPlayInstructions;
	}
	public void SetSongDataPath(string path)
	{
		m_songDataPath = path;
		Debug.Log("path set as "+m_songDataPath);
	}

	public string GetSongDataPath()
	{
		return m_songDataPath;
	}
	
	// Use this for initialization
	void Start () 
	{
		m_instance = this;
		DontDestroyOnLoad(this.gameObject);
		SaveFileCreatorScript instance = SaveFileCreatorScript.GetInstance();
		if(SaveFileCreatorScript.GetInstance().FileExists("MJPlayerStatData"))
		{
			float guestBoolResult = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","GuestModeBool");
			m_songDataPath = SaveFileCreatorScript.GetInstance().GetStoredString("MJPlayerStatData","SongDataPath");
			if(!m_songDataPath.Contains("NotSet"))
			{
				if(guestBoolResult == 1)
				{
					m_guestMode = true;
				}
				else
				{
					m_guestMode = false;
				}
				m_firstTime = false;
			}
			else
			{
				m_guestMode = true;
				m_firstTime = true;
			}
			float shouldPlayInstructionsResult = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","ShouldPlayInstructionsBool");
			if(shouldPlayInstructionsResult == 1)
			{
				m_shouldPlayInstructions = true;
			}
			else
			{
				m_shouldPlayInstructions = false;
			}
		}
		else
		{
			m_shouldPlayInstructions = true;
			m_guestMode =true;
			m_firstTime = true;
		}
	
	}

}
