﻿using UnityEngine;
using System.Collections;

public class PlayerStatManager 
{
	public static PlayerStatManager m_instance;
	public ACHIEVEMENTS m_achievementsUnlocked;
	public float m_gatesTraversed;
	public float m_highScore;
	public float m_totalTimePlayed;
	public float m_maxTimePlayed;
	public float m_mostGatesTraveledPerSong;
	public int m_junkieLevel;
	public float m_beats;
	public float m_maxBeats;
	public float m_beatIncrementTimer;
	public float m_beatIncrementInterval;
	public System.DateTime m_lastTimePlayed;
	
	public static PlayerStatManager GetInstance()
	{
		if(m_instance!= null)
		{
			return m_instance;
		}
		else
		{
			m_instance = new PlayerStatManager();
			m_instance.InitManager();
			return m_instance;
		}
	}
	// Use this for initialization
	void InitManager () 
	{
		m_beatIncrementInterval = 30;
		//SaveFileCreatorScript.GetInstance().Initialize();
		if(SaveFileCreatorScript.GetInstance().FileExists("MJPlayerStatData"))
		{
			m_junkieLevel = (int)SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","JunkieLevel");
			m_achievementsUnlocked = (ACHIEVEMENTS)SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","AchievementsUnlocked");
			m_gatesTraversed = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","GatesTraversed");
			m_highScore = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","HighScore");
			m_totalTimePlayed = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","TotalTimePlayed");
			m_maxTimePlayed = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","MaxTimePlayed");
			m_mostGatesTraveledPerSong = SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","MostGatesPerSong");
			m_beats= SaveFileCreatorScript.GetInstance().GetStoredFloat("MJPlayerStatData","BeatsAccumulated");
			m_lastTimePlayed = SaveFileCreatorScript.GetInstance().GetStoredDateTime("MJPlayerStatData","LastPlayedTime");
			m_beatIncrementTimer = System.DateTime.Now.Second;
			m_maxBeats = m_junkieLevel;
			CheckAndIncrementBeatsSinceLastGame();
		}
		else
		{
			SaveFileCreatorScript.GetInstance().CreateFile("MJPlayerStatData");
			m_junkieLevel = 5;
			m_maxBeats = m_junkieLevel;
			m_achievementsUnlocked = 0;
			m_gatesTraversed =0;
			m_highScore = 0;
			m_totalTimePlayed = 0;
			m_maxTimePlayed = 0;
			m_mostGatesTraveledPerSong = 0;
			m_beats= 10;
		}


	}

	public void CheckAndSetHighScore(int highScore)
	{
		if(m_highScore < highScore)
		{
			m_highScore = highScore;
		}
	}

	public float GetHighScore()
	{
		return m_highScore;
	}


	public void EndGameSave()
	{
		if(GameConfigurationHolderScript.GetInstance().m_guestMode)
		{
			SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",1,"GuestModeBool");
		}
		else
		{
			SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",0,"GuestModeBool");
		}
		if(GameConfigurationHolderScript.GetInstance().GetShouldPlayInstructions())
		{
			SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",1,"ShouldPlayInstructionsBool");
		}
		else
		{
			SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",0,"ShouldPlayInstructionsBool");
		}
		SaveFileCreatorScript.GetInstance().SaveStringInFile("MJPlayerStatData",GameConfigurationHolderScript.GetInstance().GetSongDataPath(),"SongDataPath");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_junkieLevel,"JunkieLevel");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",(float)m_achievementsUnlocked,"AchievementsUnlocked");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_gatesTraversed,"GatesTraversed");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_highScore,"HighScore");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_totalTimePlayed,"TotalTimePlayed");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_maxTimePlayed,"MaxTimePlayed");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_mostGatesTraveledPerSong,"MostGatesPerSong");
		SaveFileCreatorScript.GetInstance().SaveFloatInFile("MJPlayerStatData",m_beats,"BeatsAccumulated");
		SaveFileCreatorScript.GetInstance().SaveDateTimeInFile("MJPlayerStatData",System.DateTime.Now,"LastPlayedTime");
		SaveFileCreatorScript.GetInstance().CloseFileAndSerializeValues("MJPlayerStatData");
	}

	private void CheckAndIncrementBeatsSinceLastGame()
	{
		System.DateTime currentTime = System.DateTime.Now;
		System.TimeSpan timeElapsed =  currentTime -m_lastTimePlayed;
		int daysPast = timeElapsed.Days;
		int hoursPast = timeElapsed.Hours;
		int minutesPast = timeElapsed.Minutes;
		int secondsPast = timeElapsed.Seconds;
		int totalTimeElapsedInSeconds = daysPast * 86400 + hoursPast *3600 + minutesPast *60 + secondsPast;
		float beatsToAdd = totalTimeElapsedInSeconds/m_beatIncrementInterval;
		IncrementBeats((int)beatsToAdd);
	}
	
	public bool IncrementBeats(int incrementValue)
	{
		if(m_beats + incrementValue<=m_maxBeats)
		{
			m_beats+=incrementValue;
			return true;
		}
		return false;
	}
	public void SetBeats(int value)
	{
		m_beats = value;
		if(m_beats > m_maxBeats)
		{
			m_beats = m_maxBeats;
		}
	}

	public float GetBeats()
	{
		return m_beats;
	}
	public bool DecrementBeats(int decrementValue)
	{
		if(m_beats-decrementValue >= 0)
		{
			m_beats -= decrementValue;
			return true;
		}
		return false;
	}

	public float GetIntervalLeftBeforeIncrement()
	{
		return m_beatIncrementInterval - m_beatIncrementTimer;
	}

	public void CheckAndIncrementBeats()
	{
		if(m_beatIncrementTimer < m_beatIncrementInterval && m_beats <m_maxBeats)
		{
			m_beatIncrementTimer += Time.deltaTime;
		}
		else if(m_beats == m_maxBeats)
		{
			m_beatIncrementTimer = 0;
		}
		else
		{
			IncrementBeats(1);
			m_beatIncrementTimer = 0;
		}
	}
	public void UpdatePlayerManager () 
	{
		CheckAndIncrementBeats();
	
	}
}
