﻿using UnityEngine;
using System.Collections;

public class PostGameMainOverlayScript : MonoBehaviour {

	TextMesh m_levelCompTextMesh;
	TextMesh m_finalScoreTextMesh;

	TextMesh m_maxMultTextMesh;
	TextMesh m_itsAllMineUsedThisRound;
	TextMesh m_cantTouchThisUsedThisRound;

	void Start () 
	{
		//try posting score to the google play leaderboard
		if(!GameConfigurationHolderScript.GetInstance().m_guestMode)
		{	switch(AppManager.GetInstance().GetGameDifficulty())
			{
			case GAME_DIFFICULTY.DIFFICULTY_EASY:
			{
				Social.ReportScore(AppManager.GetInstance().GetFinalScore(),"CgkI-8Lb87ISEAIQAg",(bool success) =>
				                   {
					if(success)
					{
						Debug.Log("Posted score to leaderboard");
					}
					else
					{
						Debug.Log("posting score to leaderboard failed");
					}
				});
			}
				break;
			
			case GAME_DIFFICULTY.DIFFICULTY_MEDIUM:
			{
				Social.ReportScore(AppManager.GetInstance().GetFinalScore(),"CgkI-8Lb87ISEAIQAw",(bool success) =>
				                   {
					if(success)
					{
						Debug.Log("Posted score to leaderboard");
					}
					else
					{
						Debug.Log("posting score to leaderboard failed");
					}
				});
			}
				break;
			case GAME_DIFFICULTY.DIFFICULTY_HARD:
			{
				Social.ReportScore(AppManager.GetInstance().GetFinalScore(),"CgkI-8Lb87ISEAIQBA",(bool success) =>
				                   {
					if(success)
					{
						Debug.Log("Posted score to leaderboard");
					}
					else
					{
						Debug.Log("posting score to leaderboard failed");
					}
				});
			}
				break;
			}


		}
		m_levelCompTextMesh = transform.FindChild("LevelCompDispText").GetComponent<TextMesh>();
		m_finalScoreTextMesh = transform.FindChild("FinalScoreDispText").GetComponent<TextMesh>();
		m_maxMultTextMesh = transform.FindChild("MultDispText").GetComponent<TextMesh>();
		m_itsAllMineUsedThisRound = transform.FindChild("ItsAllMinePowerUpText").GetComponent<TextMesh>();
		m_cantTouchThisUsedThisRound = transform.FindChild("CantTouchThisPowerUpText").GetComponent<TextMesh>();

		m_itsAllMineUsedThisRound.text = ""+AppManager.GetInstance().m_powerOneUsed;
		m_cantTouchThisUsedThisRound.text = ""+AppManager.GetInstance().m_powerTwoUsed;
		m_finalScoreTextMesh.text = ""+AppManager.GetInstance().GetFinalScore();
		m_levelCompTextMesh.text = ""+AppManager.GetInstance().GetLevelCompletionPercent()+"%";
		m_maxMultTextMesh.text = ""+(int)AppManager.GetInstance().GetMaxMult()+"x";
		PlayerStatManager.GetInstance().IncrementBeats(5);
	}
	
}
