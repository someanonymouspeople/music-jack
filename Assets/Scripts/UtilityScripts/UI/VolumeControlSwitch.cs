﻿using UnityEngine;
using System.Collections;

public enum VOLUME_CONTROL_TAG
{
	TAG_MUSIC,
	TAG_EFFECTS
}
public class VolumeControlSwitch : ClickableButtonScript {

	//Vector3 m_minPos;
	GameObject m_barRef;
	float m_barLengthInUnits;
	public bool m_isHeldDown;
	public VOLUME_CONTROL_TAG m_tag;
	public float m_relativeDistFromStart;
	public float m_volume;

	void Start () 
	{
		m_barRef = this.transform.parent.transform.FindChild("Bar").gameObject;
		m_isHeldDown = false;
		m_barLengthInUnits = m_barRef.GetComponent<SpriteRenderer>().bounds.size.x;
		////m_minPos = m_barRef.transform.position;
		//m_minPos.x -= m_barLengthInUnits/2;


		switch(m_tag)
		{
		case VOLUME_CONTROL_TAG.TAG_MUSIC:
		{
			m_volume = AudioManager.GetInstance().GetBackGroundVolume();
		}
			break;
		case VOLUME_CONTROL_TAG.TAG_EFFECTS:
		{
			m_volume = AudioManager.GetInstance().GetAudioEffectVolume();
		}
			break;
		}

		m_relativeDistFromStart = (m_volume/100)* m_barLengthInUnits;
		this.transform.position = new Vector3((m_barRef.transform.position.x - m_barLengthInUnits/2) + m_relativeDistFromStart,m_barRef.transform.position.y,m_barRef.transform.position.z);
		m_relativeDistFromStart = m_relativeDistFromStart * 100;
	}
	

	void Update () 
	{
		this.transform.rotation = this.transform.parent.rotation;
		IsClicked();
		if(m_isEngaged)
		{

			float touchPosX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
			if(touchPosX >= m_barRef.transform.position.x - m_barLengthInUnits/2 && touchPosX < m_barRef.transform.position.x + m_barLengthInUnits/2)
			{
				this.transform.position = new Vector3(touchPosX,m_barRef.transform.position.y,m_barRef.transform.position.z);
				Vector3 posToConsider = m_barRef.transform.position;
				posToConsider.x -= m_barLengthInUnits/2;
				m_relativeDistFromStart = (int)(Vector2.Distance(transform.position,posToConsider)/m_barLengthInUnits * 100);
				switch(m_tag)
				{
					case VOLUME_CONTROL_TAG.TAG_MUSIC:
					{
					AudioManager.GetInstance().SetBackGroundVolume(m_relativeDistFromStart);
					}
					break;
					case VOLUME_CONTROL_TAG.TAG_EFFECTS:
					{
					AudioManager.GetInstance().SetAudioEffectVolume(m_relativeDistFromStart);
					}
					break;
				}
			}


		}
	}
}
