﻿using UnityEngine;
using System.Collections;

public class SongListButton : ClickableButtonScript {

	int m_index;

	public void SetRespectiveSongIndex(int index)
	{
		m_index = index;
	}
	// Update is called once per frame
	void Update () 
	{
		if(IsClicked())
		{
			AppManager.GetInstance().SetSelectedSong(m_index);
			if(!GameObject.Find("SongSelectArrow"))
			{
				GameObject tempArrow = (GameObject)Instantiate(Resources.Load("GameObjects/Utilities/SongSelectArrow"));
				tempArrow.name = "SongSelectArrow";
				tempArrow.transform.parent= this.transform.parent.transform;
			}
			Vector3 posToSetArrow = this.gameObject.transform.position;
			posToSetArrow.x += this.gameObject.renderer.bounds.extents.x;
			GameObject.Find("SongSelectArrow").transform.position = posToSetArrow;
		}
	}
}
