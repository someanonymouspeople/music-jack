﻿using UnityEngine;
using System.Collections;

public class PermaBouncer : MonoBehaviour {


	public float m_power;
	
	public void OnCollisionEnter2D(Collision2D collider)
	{
		if(collider.gameObject.tag == "MenuButton")
		{
			Vector3 collisionNormal = collider.contacts[0].normal;
			this.gameObject.rigidbody2D.AddForce(collisionNormal * m_power);
		}
	}


	void Start()
	{

	}
	void Update () 
	{

	}
}
