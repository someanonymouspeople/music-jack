﻿using UnityEngine;
using System.Collections;

public class SongListPageFlip : ClickableButtonScript {
	
	public bool m_direction;
	
	SongListScript m_songListScript;

	void Start () 
	{
		m_songListScript = this.gameObject.transform.parent.GetComponent<SongListScript>();
	}
	
	void Update () 
	{
		if(IsClicked())
		{
			if(m_direction)
			{
				m_songListScript.SetNextPage();
			}
			else
			{
				m_songListScript.SetPrevPage();
			}

		}
	}
}
