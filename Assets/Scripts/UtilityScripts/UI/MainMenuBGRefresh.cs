﻿using UnityEngine;
using System.Collections;

public class MainMenuBGRefresh : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		if(AppManager.GetInstance().m_prevScene == GAME_SCENES.SCENE_GAME)
		{
			ParallaxingBackGroundManager.GetInstance().RefreshCurrentBackGround();

		}
	}

}
