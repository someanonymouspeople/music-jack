﻿using UnityEngine;
using System.Collections;

public abstract class ClickableButtonScript : MonoBehaviour {
	
//	float m_buttonTimer;
//	public float m_buttonClickTime;

	public bool m_isEngaged;
	public int m_fingerId;
	void Awake () 
	{

	}
	
	public bool IsClicked() 
	{
#if UNITY_STANDALONE
		if(Input.GetMouseButtonDown(0) && !m_isEngaged)
		{
			if(this.gameObject.renderer.bounds.Contains(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y,this.gameObject.transform.position.z)))
			{
				m_isEngaged =true;
				return true;
			}
		}
		else
		{
			if(Input.GetMouseButtonUp(0))
			{
				m_isEngaged = false;
			}
		}


		return false;
#elif UNITY_ANDROID||UNITY_IPHONE

		if (Input.touchCount > 0)
		{
			for(int i =0 ;i<Input.touchCount; i++)
			{
				Touch touch = Input.GetTouch(i);
				Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
				if(touch.phase == TouchPhase.Began)
				{
					if(this.gameObject.renderer.bounds.Contains(new Vector3(touchPos.x,touchPos.y,this.gameObject.transform.position.z)))
					{
						return true;
					}
				}
			}
		}
		return false;
#endif
		
	}
}
