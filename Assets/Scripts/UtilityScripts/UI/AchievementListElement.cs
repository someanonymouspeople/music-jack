﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

public class AchievementListElement 
{		
	[XmlAttribute("name")]
	public string m_achievementName;
	
	[XmlAttribute("desc")]
	public string m_achievementDesc;
	
}

[XmlRoot("achievementList")]
public class AchievementList
{
	[XmlElement("achievement")]
	public List<AchievementListElement> m_achievementElementList;

	public AchievementList()
	{
		m_achievementElementList = new List<AchievementListElement>();
	}
}


