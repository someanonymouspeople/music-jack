﻿using UnityEngine;
using System.Collections;


public delegate bool MJButtonCallback(MusicJunkieDialogBoxScript script);

public class MJUIButtonFiller
{
	public MJUIButtonFiller()
	{

		m_buttonText = "";
		m_buttonCallBack = null;
		m_customButtonSprite = null;
	}
	public MJUIButtonFiller(string buttonText,MJButtonCallback callback)
	{
		m_buttonText = buttonText;
		m_buttonCallBack = callback;
		m_customButtonSprite = null;
	}
	public string m_buttonText;

	public MJButtonCallback m_buttonCallBack;
	public Sprite m_customButtonSprite;
}

public class MJButton
{
	public MJButton()
	{
		m_buttonOverlay = GameObject.Instantiate(Resources.Load("UI/MJButton")) as GameObject;
		//m_buttonOverlay.transform.position = new Vector2();
		m_buttonText = m_buttonOverlay.transform.FindChild("TextMesh").GetComponent<TextMesh>();
	}

	public GameObject m_buttonOverlay;
	public TextMesh m_buttonText;

}

public class MusicJunkieDialogBoxScript : MonoBehaviour {


	MJButton[] m_buttonHandlers;

	public void InitializeDialogBox(string displayText,int numberOfButtons,params MJUIButtonFiller[] buttons)
	{
		this.transform.FindChild("TextMesh").GetComponent<TextMesh>().text = displayText;
		if(numberOfButtons ==0)
		{
			return;
		}
		if(buttons.Length > 6)
		{
			Debug.Log("buttons count too high to place appropriately, please reduce button count and reinit");
			return;
		}
		this.transform.localPosition = new Vector3(this.transform.localPosition.x,this.transform.localPosition.y + renderer.bounds.extents.y,this.transform.localPosition.z);
		//create buttons and link callbacks
		m_buttonHandlers = new MJButton[buttons.Length];
		for(int i =0; i< buttons.Length; i++)
		{
			m_buttonHandlers[i] = new MJButton();
			m_buttonHandlers[i].m_buttonOverlay.GetComponent<MJButtonScript>().m_callBack = buttons[i].m_buttonCallBack;
			m_buttonHandlers[i].m_buttonText.text = buttons[i].m_buttonText;
			if(buttons[i].m_customButtonSprite != null)
			{
				m_buttonHandlers[i].m_buttonOverlay.GetComponent<SpriteRenderer>().sprite = buttons[i].m_customButtonSprite;
			}

		}

		//position buttons
		int rows = (buttons.Length/3) + 1;
		int columns = 1;

		for(int i = 0; i<buttons.Length; i++)
		{
			if(i % 3 == 0 && i != 0)
			{
				columns++;
			}
		}

		float x = 0, y = 0;
		int maxButtonsPerRow = 0;
		if(buttons.Length > 3)
		{
			maxButtonsPerRow = 3;
		}
		else
		{
			maxButtonsPerRow = buttons.Length;
		}

		int buttonsInSecondRow = buttons.Length- maxButtonsPerRow;
		x = this.gameObject.transform.position.x - (m_buttonHandlers[0].m_buttonOverlay.renderer.bounds.extents.x * maxButtonsPerRow-1);
		y = this.gameObject.transform.position.y - (this.gameObject.renderer.bounds.size.y);

		float xIncrement = m_buttonHandlers[0].m_buttonOverlay.renderer.bounds.size.x;//this.gameObject.renderer.bounds.size.x/(rows+1);
		float yIncrement = (this.gameObject.renderer.bounds.size.y * 1)/columns;

		for(int i =0; i<buttons.Length; i++)
		{
			if( i != 0 && i %3 ==0)
			{
				x = this.gameObject.transform.position.x - (m_buttonHandlers[0].m_buttonOverlay.renderer.bounds.extents.x*0.5f * (buttonsInSecondRow -1));
				y -= yIncrement;
			}
			//m_buttonHandlers[i].m_buttonOverlay.gameObject.transform.rotation = Quaternion.identity;
			m_buttonHandlers[i].m_buttonOverlay.transform.position = new Vector3(x,y,this.gameObject.transform.position.z);
			m_buttonHandlers[i].m_buttonOverlay.transform.parent = this.gameObject.transform;
			x+= xIncrement;
		}

	}
	
}
