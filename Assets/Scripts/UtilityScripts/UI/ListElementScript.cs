﻿using UnityEngine;
using System.Collections;

public class ListElementScript : MonoBehaviour {

	public TextMesh m_achievementNameObject;
	public TextMesh m_achievementDescObject;
	public ACHIEVEMENTS m_achievementValue;
	public GameObject m_achievementSpriteObject;
	public bool m_isActivated;

	void Awake()
	{
		m_achievementNameObject = this.transform.FindChild("ListElementName").GetComponent<TextMesh>();
		m_achievementDescObject = this.transform.FindChild("ListElementDesc").GetComponent<TextMesh>();
		m_achievementSpriteObject = this.transform.FindChild("ListElementSprite").gameObject;
	}
	void Start () 
	{

	}

	public void SetActivated(bool isActivated)
	{
		m_isActivated = isActivated;
		m_achievementNameObject.renderer.material.color = new Color(1,0,0);
		m_achievementDescObject.renderer.material.color = new Color(1,0,0);
	}

	public void SetName(string name)
	{
		m_achievementNameObject.text = name;
	}

	public void SetDesc(string desc)
	{
		m_achievementDescObject.text = desc;
	}

	public void SetValue(ACHIEVEMENTS value)
	{
		m_achievementValue = value;
	}

	public void SetSprite(Sprite sprite)
	{
		//m_achievementSpriteObject.AddComponent<>();
	}

	public void SetPos(Vector3 pos)
	{
		this.gameObject.transform.position = pos;
	}

	void Update () 
	{
	
	}
}
