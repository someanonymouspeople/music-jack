﻿using UnityEngine;
using System.Collections;

public class CallBackButton : ClickableButtonScript 
{
	public delegate void CallBackButtonCallBack(GameObject button);

	public CallBackButtonCallBack m_callBack;
	public void AssignCallBack(CallBackButtonCallBack callback)
	{
		m_callBack = callback;
	}

	void Update () 
	{
		if(IsClicked())
		{
			m_callBack.Invoke(this.gameObject);
		}
	}
}
