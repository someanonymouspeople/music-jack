﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuSuspender : MonoBehaviour 
{
	public List<GameObject> m_buttons;
	
	
	public void DisableMenu()
	{
		foreach(MenuButtonScript ms in transform.GetComponentsInChildren<MenuButtonScript>())
		{
			ms.enabled = false;
		}
	}
	
	public void EnableMenu()
	{
		foreach(MenuButtonScript ms in transform.GetComponentsInChildren<MenuButtonScript>())
		{
			ms.enabled = true;
		}
	}
	
	
	public void DestroyMenu()
	{
		for(int i =0;i<m_buttons.Count;i++)
		{
			Destroy(m_buttons[i]);
		}
		
	}
}


//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//public class MenuSuspender : MonoBehaviour 
//{
//	public List<GameObject> m_buttons;
//	public GameObject[] m_boundaryArray = new GameObject[4];
//	public Vector3 m_topLeft;
//	public Vector3 m_topRight;
//	public Vector3 m_bottomLeft;
//	public Vector3 m_bottomRight;
//	public Rect m_boundingBox;
//	public float m_colliderOffSetUnits;
//	public Rect m_pixelBB;
//
//	public float m_power;
//
//	public void DisableMenu()
//	{
//		foreach(MenuButtonScript ms in transform.GetComponentsInChildren<MenuButtonScript>())
//		{
//			ms.enabled = false;
//		}
//	}
//
//	public void EnableMenu()
//	{
//		foreach(MenuButtonScript ms in transform.GetComponentsInChildren<MenuButtonScript>())
//		{
//			ms.enabled = true;
//		}
//	}
//
//	void Awake()
//	{
////		Vector3 tempMin = new Vector3();
////		Vector3 tempMax = new Vector3();
////		m_buttons = new List<GameObject>();
////
////		for(int i =0; i<transform.childCount;i++)
////		{
////			m_buttons.Add(transform.GetChild(i).gameObject);
////
////			if(m_buttons[i].renderer.bounds.min.x < tempMin.x)
////			{
////				tempMin.x = m_buttons[i].renderer.bounds.min.x;
////			}
////			if(m_buttons[i].renderer.bounds.min.y < tempMin.y)
////			{
////				tempMin.y =m_buttons[i].renderer.bounds.min.y;
////			}
////			if(m_buttons[i].renderer.bounds.max.x > tempMax.x)
////			{
////				tempMax.x = m_buttons[i].renderer.bounds.max.x;
////			}
////			if(m_buttons[i].renderer.bounds.max.y > tempMax.y)
////			{
////				tempMax.y = m_buttons[i].renderer.bounds.max.y;
////			}
////		}
////
////		m_bottomLeft = tempMin;
////		m_topRight = tempMax;
////
////		m_topLeft = new Vector3 (m_bottomLeft.x,m_topRight.y,0);
////		m_bottomRight = new Vector3(m_topRight.x,m_bottomLeft.y,0);
////
////		m_boundingBox = new Rect(m_topLeft.x,m_topLeft.y,Vector3.Distance(m_topLeft,m_topRight),Vector3.Distance(m_topRight,m_bottomRight));
////
////		Vector3 pixelTopL = Camera.main.WorldToScreenPoint(m_topLeft);
////		Vector3 pixelTopR = Camera.main.WorldToScreenPoint(m_topRight);
////		Vector3 pixelBotL = Camera.main.WorldToScreenPoint(m_bottomLeft);
////		Vector3 pixelBotR = Camera.main.WorldToScreenPoint(m_bottomRight);
////
////		m_pixelBB = new Rect(pixelTopL.x,pixelTopL.y,pixelTopR.x - pixelTopL.x,pixelTopR.y-pixelBotR.y);
////
////		for(int i =0 ;i<4; i++)
////		{
////			GameObject boundary = Instantiate(Resources.Load("GameObjects/Utilities/Boundary")) as GameObject;
////			m_boundaryArray[i] = boundary;
////			Bounds boundsToSet = new Bounds();
////			Bounds preFabBounds = boundary.renderer.bounds;
////
////
////			Vector3 posToSet = new Vector3();
////
////			switch(i)
////			{
////				case 0:
////				{
////				posToSet = m_topLeft;
////				posToSet.x += Vector3.Distance(m_topRight,m_topLeft)/2;
////				posToSet.y += m_colliderOffSetUnits;
////
////				boundsToSet.size = new Vector3(Vector3.Distance(m_topRight,m_topLeft) +(m_colliderOffSetUnits*2), preFabBounds.size.y/2,0);
////
////				}
////					break;
////				case 1:
////				{
////				posToSet = m_topRight;
////				posToSet.x += m_colliderOffSetUnits;
////				posToSet.y -= Vector3.Distance(m_topRight,m_bottomRight)/2;
////				boundsToSet.size = new Vector3(preFabBounds.size.x/2 , Vector3.Distance(m_topRight,m_bottomRight) +(m_colliderOffSetUnits*2),0);
////				}
////					break;
////				case 2:
////				{
////				posToSet = m_bottomLeft;
////				posToSet.x += Vector3.Distance(m_bottomRight,m_bottomLeft)/2;
////				posToSet.y -= m_colliderOffSetUnits;
////				boundsToSet.size = new Vector3(Vector3.Distance(m_bottomRight,m_bottomLeft) +(m_colliderOffSetUnits*2), preFabBounds.size.y/2,0);
////				}
////					break;
////				case 3:
////				{
////				posToSet = m_bottomLeft;
////				posToSet.x -= m_colliderOffSetUnits;
////				posToSet.y += Vector3.Distance(m_topRight,m_bottomRight)/2;
////				boundsToSet.size = new Vector3(preFabBounds.size.x/2, Vector3.Distance(m_topRight,m_bottomRight) +(m_colliderOffSetUnits*2),0);
////				}
////					break;
////			}
////		 
////			boundary.transform.position = posToSet;
////			boundary.transform.localScale = boundsToSet.size;
////			boundary.collider2D.sharedMaterial = (PhysicsMaterial2D)Resources.Load("Bouncy");
////			boundary.transform.parent = this.gameObject.transform;
////		}
//	}
//	void Start () 
//	{
////		for(int i =0; i<m_buttons.Count;i++)
////		{
////			Vector3 randomDirection;
////			float randX = Random.Range(-1.0f,1.0f);
////			float randY = Random.Range(-1.0f,1.0f);
////
////			randomDirection = new Vector3(randX,randY,0);
////			if(m_buttons[i].rigidbody2D)
////			{
////			m_buttons[i].rigidbody2D.angularDrag = 0;
////			m_buttons[i].rigidbody2D.drag = 0;
////			m_buttons[i].rigidbody2D.gravityScale = 0;
////			m_buttons[i].rigidbody2D.AddForce(randomDirection * m_power);
////			}
////		}
//	}
//
//
//	public void DestroyMenu()
//	{
//		for(int i =0;i<m_buttons.Count;i++)
//		{
//			Destroy(m_buttons[i]);
//		}
//		for(int i = 0; i<m_boundaryArray.Length;i++)
//		{
//			Destroy(m_boundaryArray[i]);
//		}
//	}
//}
