﻿using UnityEngine;
using System.Collections;

public class AchievementPageFlip : ClickableButtonScript {

	public bool m_direction;

	AchievementListScript m_achievementListScript;


	void Start () 
	{
		m_achievementListScript = this.gameObject.transform.parent.GetComponent<AchievementListScript>();
	}

	void Update () 
	{
		if(IsClicked())
		{
			if(m_direction)
			{
				m_achievementListScript.SetNextPage();
			}
			else
			{
				m_achievementListScript.SetPrevPage();
			}
		}
	}
}
