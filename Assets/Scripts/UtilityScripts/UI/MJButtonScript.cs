﻿using UnityEngine;
using System.Collections;

public class MJButtonScript : ClickableButtonScript {

	public MJButtonCallback m_callBack;

	void Update () 
	{
		if(IsClicked())
		{
			if(m_callBack != null)
			{
				m_callBack.Invoke(this.transform.parent.GetComponent<MusicJunkieDialogBoxScript>());
			}
		}
	
	}
}
