﻿using UnityEngine;
using System.Collections;

public class SimplePermaBouncer : MonoBehaviour {

	public float m_rangeInUnits;
	public float m_speed;
	public Vector3 m_goalPos;
	public Vector3 m_centerPos;
	public Vector3 m_currentStartPos;
	public void OnTriggerEnter2D(Collider2D collider)
	{
		NewTargetSequence();
	}

	void Awake()
	{
		if(m_rangeInUnits == 0)
		{
			m_rangeInUnits = 1;
		}

	}

	void Start()
	{
		m_centerPos = this.gameObject.transform.position;
		float minSpeed = 0.1f;
		float maxSpeed = 0.5f;

		NewTargetSequence();
	}

	public static bool IsAtPositionOrPast(Vector3 currentPos,Vector3 startPos,Vector3 goalPos, out float lerpValue)
	{
		lerpValue = 0; 
		float distBetweenCurAndStart = Vector3.Distance(currentPos,startPos);
		float distBetweenGoalAndStart = Vector3.Distance(goalPos,startPos);
		lerpValue = distBetweenCurAndStart/distBetweenGoalAndStart;
		if(lerpValue >= 1)
		{
			return true;
		}
		return false;
	}

	private void NewTargetSequence(Vector3 dirVec = new Vector3())
	{
		m_currentStartPos = this.gameObject.transform.position;
		m_goalPos = new Vector3(Random.Range(m_centerPos.x - m_rangeInUnits, m_centerPos.x + m_rangeInUnits),Random.Range(m_centerPos.y - m_rangeInUnits,m_centerPos.y+ m_rangeInUnits),m_currentStartPos.z);

	}
	
	void Update () 
	{
		float lerpValue = 0;
		if(IsAtPositionOrPast(this.gameObject.transform.position,m_currentStartPos,m_goalPos, out lerpValue))
		{
			NewTargetSequence();
			return;
		}
		else
		{
			Vector3 curPos = this.gameObject.transform.position;
			Vector3 dirVec = (m_goalPos - m_currentStartPos).normalized;
			curPos += m_speed * dirVec * Time.deltaTime;
			this.gameObject.transform.position = curPos;
		}
	}
}
