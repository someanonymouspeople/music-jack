﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
public class FacebookLoginLogout : ClickableButtonScript 
{
	Sprite m_loginSprite;
	Sprite m_logoutSprite;
	bool m_guestMode;
	bool m_attemptingLogin = false;
	// Use this for initialization

	IEnumerator CouldNotLogInInvoke()
	{
		Debug.Log ("LeaderBoard could not login invoked");
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Unable to Login \nFacebook servers seem down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		//re enable
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		Debug.Log ("LeaderBoard DB reinited");
	}
	
	IEnumerator AsYourselfInvoke()
	{
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		Debug.Log ("LeaderBoard AsYourselfInvoked");
		
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Logging in\n to Facebook",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		Debug.Log ("LeaderBoard is clicked");
		
		NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
		result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
		
		if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
		{
			Debug.Log ("LeaderBoard is connected to internet");
			
			//GPG
			//Find and set the playing platform
			PlayGamesPlatform.Activate();
			
			//enable debug log
			PlayGamesPlatform.DebugLogEnabled = true;
			
			//Login
			Social.localUser.Authenticate((bool success) => {
				if(success)
				{
					Debug.Log("Logged in to google play "+Social.localUser.userName);
				}
				else
				{
					Debug.Log("Coudnt log in to google play");
				}
				
			});
			
			
			
			result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
			
			if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
			{
				Debug.Log ("LeaderBoard FB active");
				//Destroy(m_dialogeBox);
				StartCoroutine("FBActiveInvoke");
				
			}
			else
			{
				StartCoroutine("CouldNotLogInInvoke");
			}
		}
		else
		{
			//Destroy(m_dialogeBox);
			StartCoroutine("NetInactiveInvoke");
		}
		return true;
	}
	

	
	IEnumerator CancelledInvoke()
	{
		
		Debug.Log ("LeaderBoard CancelledInvoked");
		//Reinit
		this.enabled = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		yield break;
	}
	


	IEnumerator LogoutInvoke()
	{
		Debug.Log("Logout invoke called");
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));

		//FB.Logout();
		Debug.Log("erasing access token");
		FB.AccessToken = "";
		FB.IsLoggedIn = false;
		FB.UserId = "";
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Logging out of Facebook",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		this.enabled = true;
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		//Reinit
		Debug.Log("Logout invoke finished");
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		
	}
	
	void CustomShareButtonCallBack(FBResult result)
	{
		m_attemptingLogin = false;
	}
	
	
	IEnumerator FBActiveInvoke()
	{
		m_attemptingLogin = true;
		FB.Login("public_profile,user_friends,publish_actions",CustomShareButtonCallBack);

		while(m_attemptingLogin)
		{
			yield return null;
		}
		if(!FB.IsLoggedIn)
		{
			StartCoroutine("FBInactiveInvoke");
		}
		else
		{
			Debug.Log("Logged in to FB");
			GameConfigurationHolderScript.GetInstance().m_guestMode = false;
			this.enabled = true;
			Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		}
	}
	
	IEnumerator NetInactiveInvoke()
	{
		Debug.Log ("New Share button NetInactiveInvoked");
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		
		
		Debug.Log ("New Share button disabled");
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Internet down\n please connect and retry",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameConfigurationHolderScript.GetInstance().m_guestMode = true;
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		this.enabled = true;
		Debug.Log ("New Share button enabled");
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
	}
	
	IEnumerator FBInactiveInvoke()
	{
		Debug.Log ("New Share button NetInactiveInvoked");
		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPaused"));
		
		
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
		GameObject dialogueBox = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
		dialogueBox.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("FB servers seems down\n Please try later",0);
		yield return new WaitForSeconds(2);
		Destroy(dialogueBox);
		GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().EnableMenu();
		this.enabled = true;
		Debug.Log ("New Share button enabled");
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPaused"));
	}

	void Init()
	{
		m_guestMode =	GameConfigurationHolderScript.GetInstance().m_guestMode;
		if(m_guestMode)
		{
			this.GetComponent<SpriteRenderer>().sprite = m_loginSprite;
		}
		else
		{
			this.GetComponent<SpriteRenderer>().sprite = m_logoutSprite;
		}
	}

	void Awake () 
	{
		m_loginSprite = Resources.Load<Sprite>("UI/CustomUIButtons/FbLogin");
		m_logoutSprite = Resources.Load<Sprite>("UI/CustomUIButtons/FbLogout");

		Init ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Init ();
		if(IsClicked())
		{

			GameObject.Find("MenuEngine").GetComponent<MenuSuspender>().DisableMenu();
			this.enabled = false;
			
			if(m_guestMode)
			{
				Debug.Log ("New share button is clicked");
				
				NETWORK_CONNECTION_RESULT result = NETWORK_CONNECTION_RESULT.ERROR;
				result = NetworkConnectionDelegate.GetInstance().IsConnectedToInternet();
				
				if(result == NETWORK_CONNECTION_RESULT.NET_ACTIVE)
				{
					Debug.Log ("New share button is connected to internet");
					
					result = NetworkConnectionDelegate.GetInstance().IsLinkActive("https://facebook.com");
					
					if(result == NETWORK_CONNECTION_RESULT.LINK_ACTIVE)
					{
						Debug.Log ("New share button FB active");
						StartCoroutine("AsYourselfInvoke");
						
					}
					else
					{
						StartCoroutine("FBInactiveInvoke");
					}
				}
				else
				{
					StartCoroutine("NetInactiveInvoke");
				}
			}
			else
			{
				
				StartCoroutine(LogoutInvoke());
			}
		}
	
	}
}
