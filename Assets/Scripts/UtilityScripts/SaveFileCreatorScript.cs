﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

public class CustomFile
{
	public FileStream m_file;
	public string m_name;
	public string m_stringToSerialize;

	public CustomFile()
	{
		m_name = null;
		m_stringToSerialize = null;
	}
	public CustomFile(string fileName)
	{
		m_file = File.Create(Application.persistentDataPath+"/"+fileName+".dat");
		m_name = fileName;
		m_file.Close();
	}
	public CustomFile(FileStream file,string fileName)
	{
		m_file = file;
		m_name = fileName.Split('.')[0];
		m_file.Close();
	}
	
	public void OverWriteFile(string fileName)
	{
		File.Delete(Application.persistentDataPath +"/"+fileName+".dat");
		this.m_file = File.Create(Application.persistentDataPath+"/"+fileName+".dat");
	}
	
	public bool Exists(string fileName)
	{
		if(File.Exists(Application.persistentDataPath+"/"+fileName+".dat"))
		{
			return true;
		}
		return false;
	}
	
	public string RetrieveFullDataPath()
	{
		string path = null;
		path = Application.persistentDataPath+"/"+m_name+".dat";
		return path;
	}
	
	public string RetrieveFullDataPath(CustomFile file)
	{
		string path = null;
		path = Application.persistentDataPath+"/"+file.m_name+".dat";
		return path;
	}
};

public class SaveFileCreatorScript : MonoBehaviour 
{
	public bool m_initialized;
	static SaveFileCreatorScript m_instance;
	private List<CustomFile> m_fileList;

	void Awake()
	{
		m_initialized=  false;
		SaveFileCreatorScript.m_instance = this;
		Initialize();
	}
	
	public static SaveFileCreatorScript GetInstance()
	{
		return m_instance;
	}


	public void Initialize () 
	{
		if(!m_initialized)
		{
			m_fileList = new List<CustomFile>();
			DirectoryInfo dirInfo = new DirectoryInfo(Application.persistentDataPath);
			foreach(FileInfo file in dirInfo.GetFiles())
			{
				m_fileList.Add(new CustomFile(new FileStream(file.FullName,FileMode.Open,FileAccess.ReadWrite),file.Name));
			}
			m_initialized = true;
		}
	}

	public bool FileExists(string fileName)
	{
		if(m_initialized)
		{
		foreach(CustomFile file in m_fileList)
			{
				if(file.m_name == fileName)
				{
					return true;
				}
			}
			Debug.Log(fileName +"file doesnt exist");
			return false;
		}
		Debug.Log("saveFileCreator not initialized");
		return false;

	}
	bool CheckAndRecreateFile(string fileName)
	{
		foreach(CustomFile tempFile in m_fileList)
		{
			if(tempFile.Exists(fileName))
			{
				print ("name exists .... overwriting");
				tempFile.OverWriteFile(fileName);
				return true;
			}
		}
		return false;
	}
	
	public bool CreateFile(string fileName)
	{
		if(!CheckAndRecreateFile(fileName))
		{
			m_fileList.Add(new CustomFile(fileName));
			return true;
		}
		return false;
	}

	public CustomFile GetFile(string fileName)
	{
		//HACK :- get it fixed, for some reason the instance is either lost or rest, chck and fix
		if(m_fileList.Count== 0)
		{
			m_initialized = false;
			Initialize();
		}
		foreach(CustomFile tempFile in m_fileList)
		{
			if(tempFile.m_name == fileName)
			{
				return tempFile;
			}
		}

		return null;
	}

	string GetVector3AsString(Vector3 vector3)
	{
		string tempString = null;
		tempString = vector3.x.ToString("r")+","+vector3.y.ToString("r")+","+vector3.z.ToString("r");
		return tempString;
	}

	Vector3 GetStringAsVector3(string vectorString)
	{
		Vector3 tempVector;
		float[] tempArray = new float[3];
		Regex pattern = new Regex("[()]");
		vectorString = pattern.Replace(vectorString,"");
		string[] values = vectorString.Split(new string[]{","},System.StringSplitOptions.None);
		for(int index = 0;index < values.Length;index++)
		{
			tempArray[index] = float.Parse(values[index]);
		}
		tempVector.x = tempArray[0];
		tempVector.y = tempArray[1];
		tempVector.z = tempArray[2];

		return tempVector;
	}
	
	public void SaveVectorsInFile(string fileName,Vector3[] vectorArray,string key)
	{
		string stringToSerialize = null;

		stringToSerialize+= key+"\t";

		for(int index = 0; index< vectorArray.Length;index++)
		{
			stringToSerialize += vectorArray[index].ToString("r");
			stringToSerialize += ";";

		}
		stringToSerialize+= "\tendOfSave";
		stringToSerialize += "\n";

		GetFile (fileName).m_stringToSerialize += stringToSerialize;

	}

	public void SaveFloatsInFile(string fileName,float[] floatArray,string key)
	{
		string stringToSerialize = null;
		
		stringToSerialize+= key+"\t";
		
		for(int index = 0; index< floatArray.Length;index++)
		{
			stringToSerialize += floatArray[index].ToString("r");
			stringToSerialize += ";";
		}
		stringToSerialize+= "\tendOfSave";
		stringToSerialize+= "\n";

		GetFile(fileName).m_stringToSerialize += stringToSerialize;
	}

	public void SaveDateTimeInFile(string fileName,System.DateTime timeToSave,string key)
	{
		string stringToSerialize = null;
		
		stringToSerialize+= key+"\t";
		stringToSerialize += timeToSave.ToString("g");
		stringToSerialize+= "\tendOfSave";
		stringToSerialize+= "\n";
		
		GetFile(fileName).m_stringToSerialize += stringToSerialize;
	}

	public void SaveFloatInFile(string fileName,float floater,string key)
	{
		string stringToSerialize = null;
		
		stringToSerialize+= key+"\t";
		stringToSerialize += floater.ToString("r");
		stringToSerialize+= "\tendOfSave";
		stringToSerialize+= "\n";
		
		GetFile(fileName).m_stringToSerialize += stringToSerialize;
	}

	public void SaveStringInFile(string fileName,string stringToSave,string key)
	{
		string stringToSerialize = null;
		stringToSerialize += key+"\t";
		stringToSerialize += stringToSave;
		stringToSerialize+= "\tendOfSave";
		stringToSerialize+= "\n";
		GetFile(fileName).m_stringToSerialize += stringToSerialize;
	}

	public void CloseFileAndSerializeValues(string fileName)
	{
		FileStream fileToWriteIn = File.OpenWrite(GetFile(fileName).RetrieveFullDataPath());
		
		if(GetFile(fileName).m_stringToSerialize.Length !=0)
		{
			BinaryFormatter tempBinaryFormatter = new BinaryFormatter();
			tempBinaryFormatter.Serialize(fileToWriteIn,GetFile(fileName).m_stringToSerialize);
		}
		FileStream fileToClose = GetFile(fileName).m_file;
		fileToClose.Close();
	}

	public void CloseFile(string fileName)
	{
		FileStream fileToClose = GetFile(fileName).m_file;
		fileToClose.Close();
	}
	

	public string GetSubstringFromString(string fileName,string key)
	{
		FileStream fileToReadFrom = new FileStream(Application.persistentDataPath+"/"+fileName+".dat",FileMode.Open,FileAccess.Read);

		if(fileToReadFrom== null)
		{
			return "";
		}
		string contentsOfFile = null;

		BinaryFormatter bf = new BinaryFormatter();
		try
		{
			contentsOfFile = (string)bf.Deserialize(fileToReadFrom);
		}
		catch(System.SystemException x)
		{
			Debug.Log(x.Message);
			return "";
		}

		fileToReadFrom.Close();

		string[] substringArray = contentsOfFile.Split(new string[]{"\n"},System.StringSplitOptions.None);

		foreach(string substring in substringArray)
		{
			if(substring.StartsWith(key) && substring.EndsWith("endOfSave"))
			{
				string tempKey = key+"\t";
				int keyTextCount = tempKey.Length;

				string stringToReturn = null;
				stringToReturn = substring;
				stringToReturn = stringToReturn.Remove(0,keyTextCount);
				stringToReturn = stringToReturn.Remove(stringToReturn.IndexOf("\t"));
				return stringToReturn;
			}
		}
		return null;
	}

	public Vector3[] GetStoredVectorArray(string fileName,string key)
	{
		Vector3[] tempVector3Array;
		string primarySubstring = GetSubstringFromString(fileName,key);
		string[] vector3Strings = primarySubstring.Split(new string[]{";"},System.StringSplitOptions.None);

		tempVector3Array = new Vector3[vector3Strings.Length-1];
		for(int index = 0; index<vector3Strings.Length-1;index++)
		{
			tempVector3Array[index] = GetStringAsVector3(vector3Strings[index]);
		}
		return tempVector3Array;
	}

	public float[] GetStoredFloatArray(string fileName,string key)
	{
		float[] tempFloatArray;
		string primarySubstring = GetSubstringFromString(fileName,key);
		string[] floatStrings = primarySubstring.Split(new string[]{";"},System.StringSplitOptions.None);
		tempFloatArray = new float[floatStrings.Length-1];
		for(int index = 0; index<floatStrings.Length-1;index++)
		{
			tempFloatArray[index] = float.Parse(floatStrings[index]);
		}
		return tempFloatArray;
	}

	public System.DateTime GetStoredDateTime(string fileName,string key)
	{
		System.DateTime tempDateTime;
		
		string primarySubstring = GetSubstringFromString(fileName,key);
		if(primarySubstring.Length >0)
		{

			tempDateTime = System.DateTime.Parse(primarySubstring);
		}
		else
		{
			tempDateTime = System.DateTime.MinValue;
		}
		return tempDateTime;
	}

	public string GetStoredString(string fileName,string key)
	{

		string primarySubstring = GetSubstringFromString(fileName,key);

		return primarySubstring;
	}

	public float GetStoredFloat(string fileName,string key)
	{
		float tempFloat;
	
		string primarySubstring = GetSubstringFromString(fileName,key);
		if(primarySubstring.Length >0)
		{
		tempFloat = float.Parse(primarySubstring);
		}
		else
		{
			tempFloat = 0;
		}
			return tempFloat;
	}


}
