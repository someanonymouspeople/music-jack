﻿using UnityEngine;
using System.Collections;

public class PopupSpawner {

	//public delegate void popupButtonCallBack();
	public static PopupSpawner m_instance;

	public static PopupSpawner GetInstance()
	{
		if(m_instance!= null)
		{
		return m_instance;
		}

		m_instance = new PopupSpawner();
		return m_instance;

	}
	public void SpawnSimplePopUp(string overLayPrefabName,float bufferPercent,string label,string text,string buttonText)
	{
		float pixelsPerUnit = Camera.main.pixelHeight/(Camera.main.orthographicSize * 2);
		GameObject overLaySprite = GameObject.Instantiate(Resources.Load(overLayPrefabName))as GameObject;
		overLaySprite.transform.position = Vector3.zero;
		PopupTextScript tempScript = overLaySprite.AddComponent<PopupTextScript>();
		Bounds prefabBounds = overLaySprite.renderer.bounds;
		Vector2 posToPlaceTextRect = new Vector2((overLaySprite.transform.position.x -prefabBounds.extents.x) + (prefabBounds.size.x * bufferPercent/200),(overLaySprite.transform.position.y +prefabBounds.extents.y) - (prefabBounds.size.y * bufferPercent/200));
		tempScript.SetText(text);
		posToPlaceTextRect = Camera.main.WorldToScreenPoint(new Vector2(posToPlaceTextRect.x,-posToPlaceTextRect.y));
		tempScript.SetViewPortRect(new Rect(posToPlaceTextRect.x,posToPlaceTextRect.y,(prefabBounds.size.x - (prefabBounds.size.x*bufferPercent/100))*pixelsPerUnit,(prefabBounds.size.y - (prefabBounds.size.y*bufferPercent/100))*pixelsPerUnit));
		tempScript.SetTextRectSize(new Rect(0,0,(prefabBounds.size.x - (prefabBounds.size.x*bufferPercent/100))*pixelsPerUnit -bufferPercent,300));
	}
	void Update () 
	{
	
	}
}
