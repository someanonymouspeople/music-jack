﻿using UnityEngine;
using System.Collections;

public enum SCROLL_BAR_TYPE
{
	HORIZONTAL,
	VERTICAL
}
public class PopupTextScript : MonoBehaviour {

	float m_scrollBarValue;
	string m_guiText;
	public Vector2 m_scrollPos;
	Rect m_scrollBarRect;
	float m_scrollSize;
	float m_minValue,m_maxValue;
	SCROLL_BAR_TYPE m_type;
	bool m_scrollBarInitialized;
	Rect m_viewPortRect;
	Rect m_textAreaRect;
	public void Start()
	{
		m_scrollPos = Vector2.zero;
	}

	public void SetText(string text)
	{
		m_guiText = text;
	}
	public void SetTextRectSize(Rect textRect)
	{
		m_textAreaRect = textRect;
	}

	public void SetViewPortRect(Rect viewPortRect)
	{
		m_viewPortRect = viewPortRect;
	}
	void OnGUI()
	{


		m_scrollPos = GUI.BeginScrollView(m_viewPortRect,m_scrollPos,m_textAreaRect);
		m_guiText = GUI.TextArea(m_textAreaRect,m_guiText);
	
		GUI.EndScrollView();

	}
}
