﻿using UnityEngine;
using System.Collections;

public class GamplayInstructionsScript : MonoBehaviour {

	public Sprite[] m_instructionScreens;
	SkipCallBackButton m_skipButton;
	NeverPlayAgainCallBackButton m_neverPlayAgainButton;
	GameObject[] m_slides;
	GameObject m_slideHolder;
	int m_activeSlide;
	Vector3 m_scaleToLerpTo;
	float m_xToLerpSlideHolderTo;
	GameplayScript.InstructionsSettingCallBack m_callBack;
	void Awake()
	{
		m_xToLerpSlideHolderTo = 0;
		m_skipButton = this.GetComponentInChildren<SkipCallBackButton>();
		m_neverPlayAgainButton = this.GetComponentInChildren<NeverPlayAgainCallBackButton>();

		m_skipButton.AssignCallBack(SkipButtonCallBack);
		m_neverPlayAgainButton.AssignCallBack(NeverPlayAgainButtonCallBack);

		m_scaleToLerpTo = this.transform.localScale;
		m_activeSlide = 0;
		m_slideHolder = new GameObject();
		Vector3 posToSetHolder = this.gameObject.transform.position;


		m_slideHolder.transform.position = posToSetHolder;

		m_slideHolder.transform.parent = this.gameObject.transform;
		m_slideHolder.transform.localScale = Vector3.one;
		m_slides = new GameObject[m_instructionScreens.Length];
		Vector3 localPositionToPlaceNextSlide = Vector3.zero;
		for(int i = 0 ;i <m_instructionScreens.Length;i++)
		{
			m_slides[i] = new GameObject();
			SpriteRenderer renderer = m_slides[i].AddComponent<SpriteRenderer>();
			renderer.sortingLayerName = "HUD";
			renderer.sortingOrder = 1;
			renderer.sprite = m_instructionScreens[i];
			m_slides[i].transform.parent = m_slideHolder.transform;
			m_slides[i].transform.localPosition = localPositionToPlaceNextSlide;
			m_slides[i].transform.localScale = Vector3.one;
			localPositionToPlaceNextSlide.x += m_slides[i].renderer.bounds.size.x * 1.25f;

		}
	}

	public void DisableButtons()
	{
		Destroy(m_skipButton.gameObject);
		Destroy(m_neverPlayAgainButton.gameObject);

	}

	public void InitializeInstructionTool(GameplayScript.InstructionsSettingCallBack callBack)
	{
		m_callBack = callBack;
	}

		
	public void SkipButtonCallBack(GameObject button)
	{
		m_callBack.Invoke(true,this.gameObject);
	}

	public void NeverPlayAgainButtonCallBack(GameObject button)
	{
		m_callBack.Invoke(false,this.gameObject);
	}
	public void DeactivateTool()
	{
		m_scaleToLerpTo = Vector3.zero;
		this.enabled = false;
	}

	public void SetNextSlide()
	{
		m_activeSlide++;
		m_activeSlide = Mathf.Clamp(m_activeSlide,0,m_slides.Length-1);
		m_xToLerpSlideHolderTo = (-m_activeSlide* m_slides[m_activeSlide].renderer.bounds.size.x * 1.25f);

	}
	public void SetPrevSlide()
	{
		m_activeSlide--;
		m_activeSlide = Mathf.Clamp(m_activeSlide,0,m_slides.Length-1);
		m_xToLerpSlideHolderTo = (-m_activeSlide* m_slides[m_activeSlide].renderer.bounds.size.x * 1.25f);
	}

	void Update () 
	{
		Vector3 localScale = this.transform.localScale;
		localScale = Vector3.Lerp(localScale,m_scaleToLerpTo,0.1f);
		this.transform.localScale = localScale;

		Vector3 slideHolderPos = m_slideHolder.transform.localPosition;
		slideHolderPos.x = Mathf.Lerp(slideHolderPos.x, m_xToLerpSlideHolderTo,0.25f);
		m_slideHolder.transform.localPosition = slideHolderPos;
	
	}
}
