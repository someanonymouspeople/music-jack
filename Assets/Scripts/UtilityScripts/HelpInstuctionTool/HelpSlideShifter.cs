﻿using UnityEngine;
using System.Collections;

public class HelpSlideShifter : ClickableButtonScript {

	public bool m_direction;
	
	GamplayInstructionsScript m_intructionScript;
	
	void Start () 
	{
		m_intructionScript = this.gameObject.transform.parent.GetComponent<GamplayInstructionsScript>();
	}
	
	void Update () 
	{
		if(IsClicked())
		{
			if(m_direction)
			{
				m_intructionScript.SetNextSlide();
			}
			else
			{
				m_intructionScript.SetPrevSlide();
			}
			
		}
	}
}
