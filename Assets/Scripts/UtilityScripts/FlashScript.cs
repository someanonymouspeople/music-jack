﻿using UnityEngine;
using System.Collections;

public class FlashScript : MonoBehaviour {

	public GameObject m_objectToFlash;
	public float m_flashHalfTime;
	public float m_hangTime;
	public float m_timer;
	public float m_hangTimer;
	public bool m_frontFlash;
	void Awake () 
	{
		m_timer = 0;
		m_hangTimer = 0;

		if(m_flashHalfTime == 0)
		{
			m_flashHalfTime = 0.25f;
		}
		m_frontFlash = true;

	}

	public void Flash()
	{
		m_frontFlash = false;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(m_timer < m_flashHalfTime && !m_frontFlash )
		{
			Color curColor = m_objectToFlash.renderer.material.color;
			float m_currentAlpha =curColor.a;
			m_currentAlpha += 1/m_flashHalfTime * Time.deltaTime;
			curColor.a = m_currentAlpha;
			m_objectToFlash.renderer.material.color = curColor;

			m_timer += Time.deltaTime;
			if(m_timer > m_flashHalfTime)
			{
				m_frontFlash = true;
			}
		}
		else
		{
			if(m_frontFlash == true)
			{
			if(m_hangTimer < m_hangTime)
			{
				m_hangTimer += Time.deltaTime;
			}
			else
			{
				if(m_timer > 0)
				{
					Color curColor = m_objectToFlash.renderer.material.color;
					float m_currentAlpha =curColor.a;
					m_currentAlpha -= 1/m_flashHalfTime * Time.deltaTime;
					curColor.a = m_currentAlpha;
					m_objectToFlash.renderer.material.color = curColor;

					m_timer -= Time.deltaTime;
				}
			}
			}
		}
	
	}
}
