﻿using UnityEngine;
using System.Collections;

public class MainCamScript : MonoBehaviour {

	static MainCamScript m_instance;
	// Use this for initialization

	static MainCamScript GetInstance()
	{
		return m_instance;
	}

	void Awake () 
	{
		if(MainCamScript.GetInstance())
		{
			Destroy(this.gameObject);
			return;
		}
		m_instance = this;
		DontDestroyOnLoad(this);
	}

}
