﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
//public enum XML_TEMPLATES
//{
//	XML_ACHIEVEMENTS,
//	XML_SONGS
//}
//public class XMLReader : MonoBehaviour {
//
//	public XmlSerializer m_serializer; 
//	public FileStream m_fileStream;
//	public static XMLReader m_instance;
//	public bool m_initialized = false;
//	public Object m_objectToStoreIn;
//	public XML_TEMPLATES m_currentTemplate;
//
//	public void Awake()
//	{
//		if(XMLReader.GetInstance())
//		{
//			Destroy(this);
//			return;
//		}
//		m_instance = this;
//	}
//
//
//	public void SetTemplate(XML_TEMPLATES template,Object objectToStoreIn)
//	{
//		switch (template)
//		{
//		case XML_TEMPLATES.XML_ACHIEVEMENTS:
//		{
//			m_serializer = new XmlSerializer<AchievementListElement>();
//			m_initialized = true;
//			m_objectToStoreIn = (AchievementListElement)objectToStoreIn;
//			m_currentTemplate = template;
//		}
//			break;
//		case XML_TEMPLATES.XML_SONGS:
//		{
//			m_currentTemplate = template;
//		}
//			break;
//		}
//	}
//
//	public void ReadElementsFromFile(string fileName)
//	{
//		if(m_initialized)
//		{
//			m_fileStream = File.Open(fileName);
//
//			m_objectToStoreIn = m_serializer.Deserialize(m_fileStream);
//		}
//	}
//}
//

public static class XmlSupport 
{
	public static T DeserializeXml<T> (this string xml) where T : class
	{
		if( xml != null ) {
			var s = new XmlSerializer (typeof(T));
			xml = Application.dataPath+"/Resources/"+xml;
			using (var m = File.Open(xml,FileMode.Open))
			{
				return (T)s.Deserialize (m);
			} 
		}
		return null;
	}
	
	public static object DeserializeXml(this string xml, System.Type tp) 
	{
		var s = new XmlSerializer (tp);
		xml = Application.persistentDataPath+"/"+xml;
		using (var m= File.Open(xml,FileMode.Open))
		{
			return s.Deserialize (m);
		}
	}
	
	public static string SerializeXml (this object item)
	{
		var s = new XmlSerializer (item.GetType ());
		using (var m = new MemoryStream())
		{
			s.Serialize (m, item);
			m.Flush ();
			return Encoding.UTF8.GetString (m.GetBuffer ());
		}
		
	}
	
}