﻿using UnityEngine;
using System.Collections;

public class CapeClothScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.GetComponent<InteractiveCloth>().enabled = false;
		this.GetComponent<InteractiveCloth>().transform.position = this.transform.parent.transform.position;
		this.GetComponent<InteractiveCloth>().enabled = true;

	
	}


}
