﻿using UnityEngine;
using System.Collections;

public class DirectoryListPageFlip : ClickableButtonScript {
	
	public bool m_direction;
	
	DirectorySetterManagerScript m_dirListScript;
	
	void Start () 
	{
		m_dirListScript = this.gameObject.transform.parent.GetComponent<DirectorySetterManagerScript>();
	}
	
	void Update () 
	{
		if(IsClicked())
		{
			if(m_direction)
			{
				m_dirListScript.SetNextPage();
			}
			else
			{
				m_dirListScript.SetPrevPage();
			}
			
		}
	}
}
