﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
public class DirectorySetterManagerScript : MonoBehaviour {
	

	public string m_rootPath;

	public delegate void DirButtonCallBack(string path);
	public Vector3 m_libraryOffset;
	public TextMesh m_pathTextMesh;
	public TextMesh m_pageNumberTextMesh;

	public string m_callBackPath;
	public Vector2 m_commonScale;
	public List<GameObject> m_songListElementList;
	List<GameObject> m_pageList;
	GameObject m_library;
	int m_distanceBetweenPages = 15;
	int m_activePage;
	public DirectorySetterCancelButton m_cancelButton;
	public DirectorySetterProceedButton m_proceedButton;
	public DirectorySetterSelectButton m_selectButton;
	GameConfigurationScript.DirectorySetterManagerCallBack m_callBack;
	string[] supportedExt;
	string m_path;
	string m_prevPath;

	public void SetCallBack(GameConfigurationScript.DirectorySetterManagerCallBack callBack)
	{
		m_callBack = callBack;
	}

	IEnumerator WaitForIt(GameObject dialogueBox)
	{
		yield return new WaitForSeconds(3);
		Destroy(dialogueBox);
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPausedSquared"));
		this.enabled = true;
	}

	bool OkCallBack(MusicJunkieDialogBoxScript script)
	{
		Destroy(script.gameObject);
		this.enabled = true;
		Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("HideWhenPausedSquared"));
		return true;
	}
	void Awake()
	{

		m_library = new GameObject();
		m_library.name = "Library";
		m_pageList = new List<GameObject>();
		m_callBackPath = "";
#if UNITY_STANDALONE
		m_rootPath = "C:\\AudioFilesToLoad\\";
#elif UNITY_ANDROID
		m_rootPath = "../";
#elif UNITY_IPHONE
		m_rootPath = "..\\";
#endif
		m_path = m_rootPath;
		m_callBackPath = m_path;
		m_cancelButton = this.GetComponentInChildren<DirectorySetterCancelButton>();
		m_proceedButton = this.GetComponentInChildren<DirectorySetterProceedButton>();
		m_selectButton = this.GetComponentInChildren<DirectorySetterSelectButton>();

		m_pathTextMesh = this.transform.FindChild("PathTextMesh").GetComponent<TextMesh>();
		m_pageNumberTextMesh = this.transform.FindChild("PageNumberTextMesh").GetComponent<TextMesh>();
		m_selectButton.Disable();
		m_proceedButton.Disable();
		m_cancelButton.Disable();

		m_activePage = 0;
		m_songListElementList = new List<GameObject>();
		m_pageList = new List<GameObject>();
		GameObject sampleElement = Instantiate(Resources.Load("GameObjects/Utilities/SongListElement")) as GameObject;
		//float scaleFactor = sampleElement.transform.localScale.x/4;
		
		//m_commonScale.x = sampleElement.transform.localScale.x * scaleFactor;
		//m_commonScale.y = sampleElement.transform.localScale.y * scaleFactor;

		Destroy(sampleElement);
		List<DirectoryInfo> dirList = new List<DirectoryInfo>();
		supportedExt = new string[2];
		#if UNITY_STANDALONE
		supportedExt[0] = "ogg";
		supportedExt[1] = "wav";
		#endif
		
		#if UNITY_ANDROID
		supportedExt[0] = "wav";
		supportedExt[1] = "mp3";
		#endif
		
		#if UNITY_IPHONE
		supportedExt[0] = "aac";
		supportedExt[1] = "mp3";
		#endif

//		string[] dirInfo = GetDirectoriesInPath(m_path);
//		List<string> dirToPopulateWithList = new List<string>();
//		foreach(string dir in dirInfo)
//		{
//			if(FindIfDirOrSubDirsContain(dir,supportedExt))
//			{
//				dirToPopulateWithList.Add(dir);
//			}
//		}
		PopulateListWithPath(m_path);
		if(AppManager.GetInstance())
		{
			m_prevPath = GameConfigurationHolderScript.GetInstance().m_songDataPath;
			m_cancelButton.SetShouldExit(true);
		}

		Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("HideWhenPausedSquared"));
		if(!AppManager.GetInstance())
		{
			GameObject go = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;

			MJUIButtonFiller okFiller = new MJUIButtonFiller("OK",OkCallBack);
			go.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Music Jack would like to access your music\n Please select a folder with your music in it\n This folder can be changed anytime\n via the settings menu\n(Supported file types:- mp3,wave)",1,okFiller);
			go.transform.FindChild("TextMesh").GetComponent<TextMesh>().characterSize = 0.03f;
			go.gameObject.transform.localScale = new Vector3(1.4f,1.4f,0);

		}
		else
		{
			GameObject go = GameObject.Instantiate(Resources.Load("UI/MusicJunkieDialogBox")) as GameObject;
			MJUIButtonFiller okFiller = new MJUIButtonFiller("OK",OkCallBack);
			go.GetComponent<MusicJunkieDialogBoxScript>().InitializeDialogBox("Music Jack would like to access your music\n Please select a folder with your music in it\n(Supported file types:- mp3,wave)",1,okFiller);
			go.transform.FindChild("TextMesh").GetComponent<TextMesh>().characterSize = 0.03f;
			go.gameObject.transform.localScale = new Vector3(1.4f,1.4f,0);
		}
		this.enabled = false;
		//PopulateListWithList(dirToPopulateWithList);

	}
	public bool FindIfDirOrSubDirsContain(string dir,string[] ext)
	{
		if(DoesContainFileOfExt(dir,ext))
		{
			return true;
		}
		else
		{
			bool doesContain = false;
			string[] dirs;
			try
			{
				dirs = Directory.GetDirectories(dir);

			}
			catch
			{
				Debug.Log("attempting to access unauthorized dirs in "+dir);
				return false;
			}
			foreach(string dirElement in dirs)
			{
				doesContain = FindIfDirOrSubDirsContain(dirElement,ext);
				if(doesContain)
				{
					return true;
				}
			}
		}
		return false;
	}

	public string[] GetDirectoriesInPath(string path)
	{
		string[] dirs;
		try
		{
			dirs = Directory.GetDirectories(path);

		}
		catch
		{
			return null;
		}
		return dirs;

	}

	bool IsValidFileType(string filename,string[] extensions)
	{
		foreach(string ext in extensions)
		{
			if(filename.IndexOf(ext) > -1) 
			{
				return true;
			}
		}
		return false;
	}

	bool DoesContainFileOfExt(string dir,string[] ext)
	{
		string[] files ;
		try
		{
			files = Directory.GetFiles(dir);

		}
		catch
		{
			Debug.Log("attempting to access unauthorized files in "+dir);
			return false;
		}
		foreach(string file in files)
		{
			if(IsValidFileType(file,ext))
			{
				return true;
			}
		}
		return false;
		
	}
	
	void PopulateListWithList(List<string> dirList)
	{
		Destroy(m_library);
		m_pageList.Clear();
		m_songListElementList.Clear();
		m_library = new GameObject();
		m_library.name = "Library";
		//m_activePage = 0;
		GameObject sampleElement = Instantiate(Resources.Load("GameObjects/Utilities/DirListElement")) as GameObject;
		
		List<GameObject> dirsPerPage = new List<GameObject>();
		int arrayIndex = 0;
		
		float posToPlacePageY = (sampleElement.transform.localScale.y * m_commonScale.y * 4/2) - this.transform.GetChild(0).renderer.bounds.extents.y * m_commonScale.y;
		Vector3 posToPlaceElement = new Vector3(this.gameObject.transform.position.x,posToPlacePageY,this.gameObject.transform.position.z);
		
		
		foreach(string dirElement in dirList)
		{
			string dir = dirElement;
			GameObject listElement = Instantiate(Resources.Load("GameObjects/Utilities/DirListElement")) as GameObject;
			string[] splitStrings;
			splitStrings = dir.Split('\\');
			string nameToSet = splitStrings[splitStrings.Length-1];
			listElement.GetComponent<DirectoryListButton>().InitDirListButton(dir,CustomDirButtonCallBack);
			listElement.transform.localScale = m_commonScale;
			//listElement.gameObject.tag = "Directory";
			listElement.GetComponent<MeshRenderer>().enabled = false;
			if(nameToSet.Length>21)
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = nameToSet.Remove(21);
				listElement.transform.GetComponentInChildren<TextMesh>().text += "...";
			}
			else
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = nameToSet;
			}
			m_songListElementList.Add(listElement);
			
			//if elements per page count not met
			if(arrayIndex<4)
			{
				dirsPerPage.Add(listElement);
				arrayIndex ++;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
			}
			else
			{
				GameObject currentPage = new GameObject();

				currentPage.tag= "Page";
				currentPage.name = "Page " + m_pageList.Count;
				foreach(GameObject element in dirsPerPage)
				{
					element.transform.parent = currentPage.transform;
				}
				
				m_pageList.Add(currentPage);
				dirsPerPage.Clear();
				arrayIndex = 0;
				dirsPerPage.Add(listElement);
				posToPlaceElement.y = posToPlacePageY;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
				arrayIndex ++;
			}
		}
		
		
		//if leftover elements add them to a new page as well
		if(dirsPerPage.Count >0)
		{
			GameObject currentPage = new GameObject();
			currentPage.gameObject.tag = "Page";
			currentPage.name = "Page "+ m_pageList.Count;
			foreach(GameObject element in dirsPerPage)
			{
				element.transform.parent = currentPage.transform;
			}
			m_pageList.Add(currentPage);
		}
		
		//for sanity's sake and for ease of repositioning assign the pages to a library 
		foreach(GameObject page in m_pageList)
		{
			page.transform.parent = m_library.transform;
		}
		
		//destroy sample taken
		Destroy(sampleElement);
		
		//reposition the list appropriately
		RePosLibrary();

		m_library.transform.parent = this.gameObject.transform;
		m_library.transform.localScale = Vector3.one;
	}

	void GetDirectoriesThatCanBeAccessed(string path)
	{

	}

	void PopulateListWithPath(string path)
	{
		Destroy(m_library);
		m_pageList.Clear();
		m_songListElementList.Clear();
		m_library = new GameObject();
		m_library.name = "Library";

		//m_activePage = 0;
		GameObject sampleElement = Instantiate(Resources.Load("GameObjects/Utilities/DirListElement")) as GameObject;

		List<GameObject> dirsPerPage = new List<GameObject>();
		int arrayIndex = 0;

		float posToPlacePageY = (sampleElement.transform.localScale.y * m_commonScale.y * 4/2) - this.transform.GetChild(0).renderer.bounds.extents.y * m_commonScale.y;
		Vector3 posToPlaceElement = new Vector3(this.gameObject.transform.position.x,posToPlacePageY,this.gameObject.transform.position.z);


		foreach(string dir in Directory.GetDirectories(path))
		{
			GameObject listElement = Instantiate(Resources.Load("GameObjects/Utilities/DirListElement")) as GameObject;
			string[] splitStrings;
			splitStrings = dir.Split('\\');
			string nameToSet = splitStrings[splitStrings.Length-1];
			listElement.GetComponent<DirectoryListButton>().InitDirListButton(dir,CustomDirButtonCallBack);
			listElement.transform.localScale = m_commonScale;
			//listElement.gameObject.tag = "Directory";
			listElement.GetComponent<MeshRenderer>().enabled = false;
			if(nameToSet.Length>25)
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = nameToSet.Remove(25);
				listElement.transform.GetComponentInChildren<TextMesh>().text += "...";
			}
			else
			{
				listElement.transform.GetComponentInChildren<TextMesh>().text = nameToSet;
			}
			m_songListElementList.Add(listElement);
			
			//if elements per page count not met
			if(arrayIndex<4)
			{
				dirsPerPage.Add(listElement);
				arrayIndex ++;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
			}
			else
			{
				GameObject currentPage = new GameObject();
				currentPage.tag= "Page";
				currentPage.name = "Page " + m_pageList.Count;
				foreach(GameObject element in dirsPerPage)
				{
					element.transform.parent = currentPage.transform;
				}
				
				m_pageList.Add(currentPage);
				dirsPerPage.Clear();
				arrayIndex = 0;
				dirsPerPage.Add(listElement);
				posToPlaceElement.y = posToPlacePageY;
				listElement.transform.position = posToPlaceElement;
				posToPlaceElement.y -= sampleElement.renderer.bounds.size.y * m_commonScale.y;
				arrayIndex ++;
			}
		}

		
		//if leftover elements add them to a new page as well
		if(dirsPerPage.Count >0)
		{
			GameObject currentPage = new GameObject();
			currentPage.gameObject.tag = "Page";
			currentPage.name = "Page "+ m_pageList.Count;
			foreach(GameObject element in dirsPerPage)
			{
				element.transform.parent = currentPage.transform;
			}
			m_pageList.Add(currentPage);
		}
		
		//for sanity's sake and for ease of repositioning assign the pages to a library 
		foreach(GameObject page in m_pageList)
		{
			page.transform.parent = m_library.transform;
		}
		
		//destroy sample taken
		Destroy(sampleElement);
		
		//reposition the list appropriately

		RePosLibrary();
		m_library.transform.parent = this.gameObject.transform;
		m_library.transform.localScale = Vector3.one;


	}

	private void RePosLibrary()
	{

		Vector3 posToPlacePage = new Vector3(0,0,m_library.transform.position.z);
		for(int i = 0; i<m_library.transform.childCount;i++)
		{
			GameObject pageToConsider = m_library.transform.GetChild(i).gameObject;
			if(pageToConsider.gameObject.tag == "Page")
			{
				pageToConsider.transform.position = posToPlacePage;
				posToPlacePage.x += m_distanceBetweenPages;
			}
		}

	}

	public void SetNextPage()
	{
		if((m_activePage+1) < m_pageList.Count)
		{
			Vector2 posToPlaceLibrary = m_library.transform.localPosition;
			posToPlaceLibrary.x -= m_distanceBetweenPages;
			m_library.gameObject.transform.localPosition = posToPlaceLibrary;
			m_activePage ++;
		}
		
	}
	
	public void SetPrevPage()
	{
		if((m_activePage-1) >=0)
		{
			Vector2 posToPlaceLibrary = m_library.transform.localPosition;
			posToPlaceLibrary.x+= m_distanceBetweenPages;
			m_library.gameObject.transform.localPosition = posToPlaceLibrary;
			m_activePage--;
		}
	}

	void CustomDirButtonCallBack(string path)
	{
		m_callBackPath = path;
	}

	void SelectCallBack()
	{
		Destroy(m_library);
		m_callBack.Invoke(m_callBackPath);
		Debug.Log(m_callBackPath+" selected");
	}
	void ProceedCallBack()
	{
		m_activePage = 0;
		m_path = m_callBackPath;
//		string[] dirInfo = GetDirectoriesInPath(m_path);
//		List<string> dirToPopulateWithList = new List<string>();
//		foreach(string dir in dirInfo)
//		{
//			if(FindIfDirOrSubDirsContain(dir,supportedExt))
//			{
//				dirToPopulateWithList.Add(dir);
//			}
//		}
		m_cancelButton.SetShouldExit(false);
		m_proceedButton.Disable();
		m_selectButton.Disable();
		//PopulateListWithList(dirToPopulateWithList);
		m_cancelButton.Enable();
		PopulateListWithPath(m_path);
	}
	void CancelCallBack()
	{
		if(AppManager.GetInstance())
		{
			if(AppManager.GetInstance().GetCurrentScene()== GAME_SCENES.SCENE_SETTINGS)
			{
				if(m_cancelButton.m_shouldExit)
				{
					if(m_path.Length <= m_rootPath.Length)
					{
						Destroy(m_library);
						m_callBack.Invoke(m_prevPath);
						return;
					}
					
				}
			}
			else
			{
				m_cancelButton.SetShouldExit(false);
			}
		}
		m_activePage = 0;
		int lengthOfPath = m_path.Length;
		int lengthOfLastIteration = 0;
		string[] pathSplits = m_path.Split('\\');
		pathSplits = pathSplits.Where(x => !string.IsNullOrEmpty(x)).ToArray();

		lengthOfLastIteration = pathSplits[pathSplits.Length-1].Length;

		m_path = m_path.Remove(lengthOfPath-lengthOfLastIteration);
		m_path = m_path.TrimEnd('\\');

		if(m_path.Length<= m_rootPath.Length)
		{
			m_path = m_rootPath;
			m_cancelButton.Disable();
			if(AppManager.GetInstance())
			{
				if(AppManager.GetInstance().GetCurrentScene()==GAME_SCENES.SCENE_SETTINGS)
				{
				m_cancelButton.SetShouldExit(true);
				}
			}
		}

//		string[] dirInfo = GetDirectoriesInPath(m_path);
//		List<string> dirToPopulateWithList = new List<string>();
//		foreach(string dir in dirInfo)
//		{
//			if(FindIfDirOrSubDirsContain(dir,supportedExt))
//			{
//				dirToPopulateWithList.Add(dir);
//			}
//		}
		m_callBackPath = m_path;
		PopulateListWithPath(m_path);
		//PopulateListWithList(dirToPopulateWithList);
		m_selectButton.Disable();
		m_proceedButton.Disable();



	}



	void Update()
	{
		Vector3 pos = m_library.transform.localPosition;
		pos = m_libraryOffset;
		pos.x -= m_activePage*m_distanceBetweenPages;
		m_library.transform.localPosition = pos;

		m_pageNumberTextMesh.text = "Page "+(m_activePage+1)+"/"+m_pageList.Count;

		string trimmedCallBack = m_callBackPath;
		if(trimmedCallBack.Length>29)
		{
			trimmedCallBack = trimmedCallBack.Remove(0,trimmedCallBack.Length-29);
			trimmedCallBack = "..."+trimmedCallBack;
		}
		string pathToSet = "Path:-"+trimmedCallBack;
		m_pathTextMesh.text = pathToSet;
		if(m_proceedButton.IsClicked() && m_proceedButton.enabled)
		{
			ProceedCallBack();
		}
		if(m_cancelButton.IsClicked()&& m_cancelButton.enabled)
		{
			CancelCallBack();
		}
		if(m_selectButton.IsClicked()&& m_selectButton.enabled)
		{
			SelectCallBack();
		}
	}
}
