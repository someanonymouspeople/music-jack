﻿using UnityEngine;
using System.Collections;
using System.IO;
public class DirectorySetterProceedButton : ClickableButtonScript {
//
//
//bool IsValidFileType(string filename,string[] extensions)
//{
//	foreach(string ext in extensions)
//	{
//		if(filename.IndexOf(ext) > -1) 
//		{
//			return true;
//		}
//	}
//	return false;
//}
//
//bool DoesContainFileOfExt(string dir,string[] ext)
//{
//	foreach(string file in Directory.GetFiles(dir))
//	{
//		if(IsValidFileType(file,ext))
//		{
//			return true;
//		}
//	}
//	return false;
//}
//
//public bool FindIfDirOrSubDirsContain(string dir,string[] ext)
//{
//	if(DoesContainFileOfExt(dir,ext))
//	{
//		return true;
//	}
//	else
//	{
//		bool doesContain = false;
//		foreach(string dirElement in Directory.GetDirectories(dir))
//		{
//			doesContain = FindIfDirOrSubDirsContain(dirElement,ext);
//			if(doesContain)
//			{
//				return true;
//			}
//		}
//	}
//	return false;
//}
//
	public void CheckAndEnable(string path)
	{
		string[] supportedExt = new string[2];
		#if UNITY_STANDALONE
		supportedExt[0] = "ogg";
		supportedExt[1] = "wav";
		#endif
		
		#if UNITY_ANDROID
		supportedExt[0] = "wav";
		supportedExt[1] = "mp3";
		#endif
		
		#if UNITY_IPHONE
		supportedExt[0] = "aac";
		supportedExt[1] = "mp3";
		#endif
		if(Directory.GetDirectories(path).Length>0)
		{
			this.renderer.material.color = Color.black;
			Vector2 localScale = this.transform.localScale;
			localScale.x= 1.25f;
			localScale.y = 1.25f;
			this.transform.localScale = localScale;
			this.enabled = true;
		}
		else
		{
			this.Disable();
		}
	}
	
	public void Disable()
	{
		this.renderer.material.color = Color.grey;
		Vector2 localScale = this.transform.localScale;
		localScale = Vector2.one;
		this.transform.localScale = localScale;
		this.enabled= false;
	}
}
