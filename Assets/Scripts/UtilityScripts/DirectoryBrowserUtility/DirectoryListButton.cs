﻿using UnityEngine;
using System.Collections;

public class DirectoryListButton : ClickableButtonScript {
	
	int m_index;
	string m_path;
	DirectorySetterManagerScript.DirButtonCallBack m_callBack;
	public void InitDirListButton(string dirPath,DirectorySetterManagerScript.DirButtonCallBack callBack)
	{
		m_path = dirPath;
		m_callBack = callBack;
	}
	// Update is called once per frame
	void Update () 
	{
		if(IsClicked())
		{

			if(!GameObject.Find("SongSelectArrow"))
			{
				GameObject tempArrow = (GameObject)Instantiate(Resources.Load("GameObjects/Utilities/SongSelectArrow"));
				tempArrow.name = "SongSelectArrow";
				tempArrow.transform.parent= this.transform.parent.transform;
				tempArrow.transform.localScale = Vector3.one;
			}

			m_callBack.Invoke(m_path);
			Vector3 posToSetArrow = this.gameObject.transform.position;
			posToSetArrow.x += this.gameObject.renderer.bounds.extents.x;
			GameObject.Find("SongSelectArrow").transform.position = posToSetArrow;
			GameObject.Find("DirectorySetterPrefab(Clone)").GetComponent<DirectorySetterManagerScript>().m_proceedButton.CheckAndEnable(m_path);
			GameObject.Find("DirectorySetterPrefab(Clone)").GetComponent<DirectorySetterManagerScript>().m_selectButton.Enable();
		}
	}
}
