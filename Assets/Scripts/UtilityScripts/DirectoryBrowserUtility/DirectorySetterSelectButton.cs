﻿using UnityEngine;
using System.Collections;

public class DirectorySetterSelectButton : ClickableButtonScript 
{

	public void Enable()
	{
		this.renderer.material.color = Color.black;
		Vector2 localScale = this.transform.localScale;
		localScale.x= 1.25f;
		localScale.y = 1.25f;
		this.transform.localScale = localScale;
		this.enabled = true;
	}

	public void Disable()
	{
		this.renderer.material.color = Color.grey;

		Vector2 localScale = this.transform.localScale;
		localScale = Vector2.one;
		this.transform.localScale = localScale;
		this.enabled = false;
	}
}
