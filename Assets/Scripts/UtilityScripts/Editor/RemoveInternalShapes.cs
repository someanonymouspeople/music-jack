﻿#if(UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System.Collections;

public class RemoveInternalShapes : MonoBehaviour {
	
	[MenuItem ("PolygonColliderModder/Remove Internal Shapes", true)]
	static bool Validate ()
	{
		// Check if there are any selected GameObject with more than one path
		foreach (GameObject lObj in Selection.gameObjects)
		{
			PolygonCollider2D lCollider = lObj.GetComponent<PolygonCollider2D>();
			if (lCollider!=null && lCollider.pathCount > 1)
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	
	[MenuItem ("PolygonColliderModder/Remove Internal Shapes")]
	static void RemoveShapes ()
	{
		foreach (GameObject lObj in Selection.gameObjects)
		{
			PolygonCollider2D lCollider = lObj.GetComponent<PolygonCollider2D>();
			if (lCollider==null)
			{
				continue;
			}
			
			// Allow undo action
			Undo.RecordObject (lCollider, "Remove Interior Shapes");
			
			//Get the largest shape and take its path as the exterios path

			Vector2[] pathArray;
			int exteriorShape =0;
			float m_maxDistBetween =0;
			for (int i=0, length=lCollider.pathCount; i<length ; ++i)
			{
				float distBetween =0;
				float leftMost =0 ;
				float rightMost = 0;

				pathArray = lCollider.GetPath(i);
				
				foreach (Vector2 point in pathArray)
				{
					if (point.x < leftMost)
					{
						leftMost = point.x;
					}
					else if(point.x > rightMost)
					{
						rightMost = point.x;
					}
				}

				distBetween = rightMost - leftMost;
				if(m_maxDistBetween < distBetween)
				{
					exteriorShape = i;
					m_maxDistBetween = distBetween;
				}
			}

			// Initialize collider with exterior path
			pathArray = lCollider.GetPath (exteriorShape);
			
			// Set only the exterior path
			lCollider.pathCount = 1;
			lCollider.SetPath (0, pathArray);
		}
	}
}

#endif
