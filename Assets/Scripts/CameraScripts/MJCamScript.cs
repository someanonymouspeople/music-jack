﻿using UnityEngine;
using System.Collections;

public class MJCamScript : MonoBehaviour {

	public static MJCamScript m_instance;
	public float m_shakeTime;
	public float m_commonTimer;
	public float m_shakeIntensity;
	public float m_shakeDecay;
	Vector3 m_startLocal;
	bool m_isShaking;
	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
		m_isShaking = false;
		if(m_shakeIntensity==0)
		{
			m_shakeIntensity = 0.3f;
		}
		if(m_shakeTime == 0)
		{
			m_shakeTime = 0.25f;
		}
		m_commonTimer = 0;
		m_instance = this;
	}

	public static MJCamScript GetInstance()
	{
		return m_instance;
	}

	public void UpdateShake()
	{
		Vector3 shakeInterm = Random.insideUnitSphere * m_shakeIntensity;
		shakeInterm.z = 0;
		transform.localPosition = transform.localPosition + shakeInterm;
//		Quaternion originRotation = Quaternion.identity;
//		transform.rotation = new Quaternion(
//			originRotation.x + Random.Range(-m_shakeIntensity,m_shakeIntensity)*.2f,
//			originRotation.y + Random.Range(-m_shakeIntensity,m_shakeIntensity)*.2f,
//			originRotation.z + Random.Range(-m_shakeIntensity,m_shakeIntensity)*.2f,
//			originRotation.w + Random.Range(-m_shakeIntensity,m_shakeIntensity)*.2f);

		m_shakeIntensity = m_shakeDecay;
	}

	public void StartShake(float shakeIntensity,float time)
	{
		if(!m_isShaking)
		{
			m_startLocal = this.transform.localPosition;
			m_isShaking = true;
			m_shakeTime = time;
			m_shakeIntensity = shakeIntensity;
			m_shakeDecay = (m_shakeIntensity *Time.deltaTime)/time;
			StartCoroutine(ShakeCam());
		}

	}
	IEnumerator ShakeCam()
	{
		while(m_commonTimer<m_shakeTime)
		{
			UpdateShake();
			m_commonTimer += Time.deltaTime;
			yield return null;
		}
		m_isShaking = false;
		m_commonTimer = 0;
		//transform.localPosition = m_startLocal;
		yield break;

	}

}
